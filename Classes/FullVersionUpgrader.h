//
//  FullVersionUpgrader.h
//  AmusementPark
//
//  Created by Amresh Kumar on 21/10/11.
//  Copyright (c) 2011 Permeative Technologies Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FullVersionUpgrader : NSObject
+(FullVersionUpgrader*)sharedUpgrader;
-(void)showAlert;
@end
