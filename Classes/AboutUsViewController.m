//
//  AboutUsViewController.m
//  AmusementPark
//
//  Created by HARSHA on 04/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AboutUsViewController.h"
#import "AboutUsCell.h"
#import "AmusementParkAppDelegate.h"

@interface AboutUsViewController()
-(BOOL)isLinkAvailableForItemAtIndex:(int)index Section:(int)Section;
-(NSString*)iTunesLinkForItenAtIndex:(int)index Section:(int)Section;
-(NSArray*)GetAppNames;
@end
@implementation AboutUsViewController

-(id)init
{
  if((self=[super init]))
  {
    m_AppName=nil;
    m_FreeApps=nil;
    m_FullApps=nil;
    m_FreeName=nil;
    m_FullName=nil;
    
    //Getting App Names from web services.
    m_AppName=[[NSMutableArray alloc] initWithArray:[self GetAppNames]];
    
    if ([m_AppName count]==0) //if there is no internet
    {
      AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[UIApplication sharedApplication].delegate;
      [appDelegate LoadItuneDictWhenNoInternet];
    }
    //Removing Current App Name 
    NSString *ProductName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    [m_AppName removeObject:ProductName];
    
    
    m_FullApps=[[NSMutableArray alloc]init];
    m_FreeApps=[[NSMutableArray alloc]init];
    m_FreeName=[[NSMutableArray alloc]initWithObjects:@"Disney World",@"Universal Studio",@"Disney World Wait Times",@"Universal Studios Wait Times",@"Disney World Maps",@"Universal Maps",@"Disney Dining",@"Universal Dining", nil];
    m_FullName=[[NSMutableArray alloc]initWithObjects:@"Disney & Universal",@"Disney World",@"Universal Studio",@"Disney World Wait Times",@"Universal Studio Wait Times",@"Disney World Maps",@"Universal Maps",@"Disney Dining",@"Universal Dining", nil];
    
    //Remove Current App name From Free App Names
    if(IsUniversalStudioFreeWaitTimes)
      [m_FreeName removeObject:@"Universal Studios Wait Times"];
    else if(IsDisneyWorldFreeWaitTimes)
      [m_FreeName removeObject:@"Disney World Wait Times"];
    
    //Sort the app according to free and full version
    for (int i=0;i<[m_AppName count];i++)
    {
      BOOL Flag=NO;

      NSString *str = [m_AppName objectAtIndex:i];
      NSArray *arr = [str componentsSeparatedByString:@" "];
      for(int i=0;i<[arr count];i++)
      {
        
        if([[arr objectAtIndex:i] isEqualToString:@"Free"])
        {
          Flag=YES;
          break;
        }
        
      } 
      
      if (Flag)
        [m_FreeApps addObject:[m_AppName objectAtIndex:i]];
      else
        [m_FullApps addObject:[m_AppName objectAtIndex:i]];
      
    }
    
    m_BackButton=nil;
    m_TableView=nil;
   // m_SectionArray=[[NSMutableArray alloc] initWithObjects:@"About Us",@"More Apps",@"Free Apps",nil];
       m_SectionArray=[[NSMutableArray alloc] initWithObjects:@"About Us",nil];
    RowArray1=[[NSMutableArray alloc]initWithObjects:@"Appyea",@"CraterZone",nil];
    
    m_ApptasmicInfoViewController=nil;
    m_ApptasmicViewController=nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (LoadData) name:@"DataLoadedNow" object:nil];

  }
  return self;
}

-(void)LoadData
{
  [m_AppName addObjectsFromArray:[self GetAppNames]];
  
  //Sort the app according to free and full version
  for (int i=0;i<[m_AppName count];i++)
  {
    BOOL Flag=NO;
    
    NSString *str = [m_AppName objectAtIndex:i];
    NSArray *arr = [str componentsSeparatedByString:@" "];
    for(int i=0;i<[arr count];i++)
    {
      
      if([[arr objectAtIndex:i] isEqualToString:@"Free"])
      {
        Flag=YES;
        break;
      }
      
    } 
    
    if (Flag)
      [m_FreeApps addObject:[m_AppName objectAtIndex:i]];
    else
      [m_FullApps addObject:[m_AppName objectAtIndex:i]];
    
  }
  [m_TableView reloadData];
}
-(void)loadView
{
      appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
  
  UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
	imageView.frame=CGRectMake(0, 0, 320, 48.5);
	//[self.navigationController.navigationBar addSubview:imageView];
	[imageView release];
  
    UIImageView *pBGImageView;
    if (screenBounds.size.height == 568) {
        pBGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        pBGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }

  //	pBGImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@ImagesCommon/Background ofType:@"png"]];
  pBGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
     pBGImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[self.view addSubview:pBGImageView];  
  [pBGImageView release];
  pBGImageView=nil;
  
  m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
	[m_BackButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/homebtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
  //[self.navigationController.navigationBar addSubview:m_BackButton];  
  [m_BackButton retain];
  
    if(m_TableView==nil)
    {
        if (screenBounds.size.height == 568) {
            m_TableView=[[UITableView alloc]initWithFrame:CGRectMake(-5,15,330,450) style:UITableViewStyleGrouped];
        }
        else{
            m_TableView=[[UITableView alloc]initWithFrame:CGRectMake(-5,15,330,380) style:UITableViewStyleGrouped];
        }
    }
    m_TableView.backgroundView=nil;
    
 
	m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.backgroundColor=[UIColor clearColor];
	m_TableView.scrollEnabled = YES;
	[self.view addSubview:m_TableView];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubViews];
}


- (void) didRotate:(NSNotification *)notification{
    [self checkOrientation];
    
    
}
-(void)layoutSubViews  //cp
{
    [m_TableView reloadData];
    CGRect screenBound = [[UIScreen mainScreen]bounds];
    if (appDelegate.isPortrait) {
        m_BackButton.frame=CGRectMake(5, 3, 53.5, 30);
        m_TableView.frame = CGRectMake(0,15,320,365);
        if (screenBound.size.height == 568) {
            m_TableView.frame = CGRectMake(0,15,320,365);
            
        }
    }
    else
    {
        m_BackButton.frame=CGRectMake(5, 3, 53.5, 30);
        m_TableView.frame = CGRectMake(0,15,480,320);
        if (screenBound.size.height == 568) {
            m_TableView.frame = CGRectMake(0,15,568,320);
            
        }
    }
}

- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}

-(void)viewDidLoad
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void)viewWillAppear:(BOOL)animated
{
  [self.navigationController setNavigationBarHidden:NO];
  self.navigationItem.hidesBackButton=YES;
  [self.navigationController.navigationBar addSubview:m_BackButton];
}

-(void)viewDidAppear:(BOOL)animated
{
  if(m_ApptasmicInfoViewController){[m_ApptasmicInfoViewController.view removeFromSuperview];[m_ApptasmicInfoViewController release];m_ApptasmicInfoViewController=nil;}
  if(m_ApptasmicViewController){[m_ApptasmicViewController.view removeFromSuperview];[m_ApptasmicViewController release];m_ApptasmicViewController=nil;}
}

#pragma mark tableView delegate methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if(section==0)
    return 2;
//  else if(section==1) 
//    return [m_FullApps count];
//  else
//    return [m_FreeApps count];
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 49;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
  // create the parent view that will hold header Label
  UIView* customView = [[[UIView alloc] initWithFrame:CGRectMake(20, 0.0, 320.0, 20)] autorelease];
  
  // create the button object
  UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
  headerLabel.backgroundColor = [UIColor clearColor];
  headerLabel.opaque = NO;
  headerLabel.textColor = [UIColor whiteColor];
  headerLabel.highlightedTextColor = [UIColor whiteColor];
  headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
  headerLabel.frame = CGRectMake(20, 0.0, 320.0, 20);
  
  // If you want to align the header text as centered
  //headerLabel.frame = CGRectMake(150.0, 0.0, 300.0, 44.0);
  headerLabel.text =[m_SectionArray objectAtIndex:section];
  
  [customView addSubview:headerLabel];
  
  return customView;
}

-(void)backAction
{
  [m_BackButton removeFromSuperview];
  [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  return 30;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	//return 3;
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  AboutUsCell *cell=nil;
  cell = (AboutUsCell *)[tableView dequeueReusableCellWithIdentifier:@"AboutUsCell"];
  if(cell==nil)
  {
    cell=[[[AboutUsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AboutUsCell"]autorelease];
    cell.accessoryType = UITableViewCellAccessoryNone;
  }
  
  if(indexPath.section==0)
  {
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 49)];
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/AboutUS/1" ofType:@"png"]];
    
    
    UIImageView *selbgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 49)];
    selbgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/AboutUS/11" ofType:@"png"]];
     
      if (!appDelegate.isPortrait) {
          bgImgView.frame = CGRectMake(0, 0, 480, 49);
          selbgImgView.frame = CGRectMake(0, 0, 480, 49);
          if ([[UIScreen mainScreen]bounds].size.height == 568) {
              bgImgView.frame = CGRectMake(0, 0, 568, 49);
              selbgImgView.frame = CGRectMake(0, 0, 568, 49);
          }
      }
      cell.backgroundView=bgImgView;
      [bgImgView release];
      cell.selectedBackgroundView=selbgImgView;
      [selbgImgView release];
    
      if (!appDelegate.isPortrait) {  //cp
          CGRect frame = cell.m_TitleLabel.frame;
          frame.origin.x = 100;
          cell.m_TitleLabel.frame = frame;
      }
      else
      {
          CGRect frame = cell.m_TitleLabel.frame;
          frame.origin.x = 75;
          cell.m_TitleLabel.frame = frame;
      }
    cell.m_TitleLabel.text=[RowArray1 objectAtIndex:indexPath.row];
  }
  else if(indexPath.section==1)
  {
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 49)];
    NSString *imageName=[[m_FullApps objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *imagePath=[NSString stringWithFormat:@"ImagesCommon/AboutUS/%@",imageName];
    UIImage *img=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:imagePath ofType:@"png"]];
    if(img)
    {
      bgImgView.image=img;   
      cell.m_TitleLabel.text=@"";
    }
    else
      cell.m_TitleLabel.text=[m_FullApps objectAtIndex:indexPath.row];
    cell.backgroundView=bgImgView;    
    cell.selectedBackgroundView=nil;
    [bgImgView release];
    
  }
  
  else if(indexPath.section==2)
  {
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 49)];
    NSString *imageName=[[m_FreeApps objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *imagePath=[NSString stringWithFormat:@"ImagesCommon/AboutUS/%@",imageName];
    UIImage *img=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:imagePath ofType:@"png"]];
    if(img)
    {
      bgImgView.image=img;   
      cell.m_TitleLabel.text=@"";
    }
    else
      cell.m_TitleLabel.text=[m_FreeApps objectAtIndex:indexPath.row];
    cell.backgroundView=bgImgView;    
    cell.selectedBackgroundView=nil;
    [bgImgView release];
    
  }

  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  if(indexPath.section==0)
  {
    if (indexPath.row==0)
    {
      [m_BackButton removeFromSuperview];
      if(m_ApptasmicViewController)
      {
        [m_ApptasmicViewController.view removeFromSuperview];
        [m_ApptasmicViewController release];
        m_ApptasmicViewController=nil;
      }
      m_ApptasmicViewController=[[ApptasmicViewController alloc]init];
      [self.navigationController pushViewController:m_ApptasmicViewController animated:YES];      
    }
    else if(indexPath.row==1)
    {
      [m_BackButton removeFromSuperview];
      if(m_ApptasmicInfoViewController)
      {
        [m_ApptasmicInfoViewController.view removeFromSuperview];
        [m_ApptasmicInfoViewController release];
        m_ApptasmicInfoViewController=nil;
      }
      m_ApptasmicInfoViewController=[[ApptasmicInfoViewController alloc]init];
      [self.navigationController pushViewController:m_ApptasmicInfoViewController animated:YES];      
    }
  }
  else if(indexPath.section==1)
  {
    if ([self isLinkAvailableForItemAtIndex:indexPath.row Section:indexPath.section])
    {
      NSString *link=[self iTunesLinkForItenAtIndex:indexPath.row Section:indexPath.section];
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
    }
    else
    {
      UIAlertView *alert=[[[UIAlertView alloc]initWithTitle:@"Coming Soon..." message:[m_FullName objectAtIndex:indexPath.row] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] autorelease];
      [alert show];
    }
  }
  
  else if(indexPath.section==2)
  {
    if ([self isLinkAvailableForItemAtIndex:indexPath.row Section:indexPath.section])
    {
      NSString *link=[self iTunesLinkForItenAtIndex:indexPath.row Section:indexPath.section];
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
    }
    else
    {
      UIAlertView *alert=[[[UIAlertView alloc]initWithTitle:@"Coming Soon..." message:[m_FreeName objectAtIndex:indexPath.row] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] autorelease];
      [alert show];
    }
  }

}

- (void)didReceiveMemoryWarning 
{
  [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
}


- (void)dealloc
{
  if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
  if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
  if(m_SectionArray){[m_SectionArray removeAllObjects];[m_SectionArray release];m_SectionArray=nil;}
  if(RowArray1){[RowArray1 removeAllObjects];[RowArray1 release];RowArray1=nil;}
  if(m_ApptasmicInfoViewController){[m_ApptasmicInfoViewController.view removeFromSuperview];[m_ApptasmicInfoViewController release];m_ApptasmicInfoViewController=nil;}
  if(m_ApptasmicViewController){[m_ApptasmicViewController.view removeFromSuperview];[m_ApptasmicViewController release];m_ApptasmicViewController=nil;}
  
  if(m_AppName){[m_AppName removeAllObjects];[m_AppName release];m_AppName=nil;}
  if(m_FreeApps){[m_FreeApps removeAllObjects];[m_FreeApps release];m_FreeApps=nil;}
  if(m_FullApps){[m_FullApps removeAllObjects];[m_FullApps release];m_FullApps=nil;}
  if(m_FreeName){[m_FreeName removeAllObjects];[m_FreeName release];m_FreeName=nil;}
  if(m_FullName){[m_FullName removeAllObjects];[m_FullName release];m_FullName=nil;}


  
  [super dealloc];
}

-(BOOL)isLinkAvailableForItemAtIndex:(int)index Section:(int)Section
{
  AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[UIApplication sharedApplication].delegate;
  NSString *link;
  if (Section==1)
  {
    NSString *appName=[m_FullApps objectAtIndex:index];
    link=[appDelegate.ITunesLinkDictionary objectForKey:appName];

  }
  else if(Section==2)
  {
    NSString *appName=[m_FreeApps objectAtIndex:index];
    link=[appDelegate.ITunesLinkDictionary objectForKey:appName];

  }
  if([link length]>0)
    return YES;
  return NO;
}
-(NSString*)iTunesLinkForItenAtIndex:(int)index Section:(int)Section
{
  AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[UIApplication sharedApplication].delegate;
  NSString *link;
  if (Section==1)
  {
    NSString *appName=[m_FullApps objectAtIndex:index];
    link=[appDelegate.ITunesLinkDictionary objectForKey:appName];

  }
  
  else if(Section==2)
  {
    NSString *appName=[m_FreeApps objectAtIndex:index];
    link=[appDelegate.ITunesLinkDictionary objectForKey:appName];
  }
   
  NSLog(@"\n Link=%@ \n",link);
  return link;
}
-(NSArray *)GetAppNames
{
  AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[UIApplication sharedApplication].delegate;
  return appDelegate.AppNames;
}

@end
