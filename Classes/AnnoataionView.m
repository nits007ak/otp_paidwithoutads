//
//  AnnoataionView.m
//  AmusementPark
//
//  Created by HARSHA on 01/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AnnoataionView.h"

@implementation AnnoataionView

@synthesize m_WaitTimeLabel,m_WaitTimeImageView,m_TitleLabel,m_TitleImageView,m_Button,m_delegate,m_Type;
@synthesize m_Xcoordinate,m_Ycoordinate;
- (id)initWithFrame:(CGRect)frame annotation:(AnnotationInfo *)pAnnotation
{
  m_AnnoataionInfo=pAnnotation;
  
  self = [super initWithFrame:frame];
  self.backgroundColor = [UIColor clearColor];
    
  pLandIndex=m_AnnoataionInfo.m_LandIndex;
  pIndex=m_AnnoataionInfo.m_Index;
  m_Type=m_AnnoataionInfo.m_Type;
  
  m_WaitTimeImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
  
  int waittime=[m_AnnoataionInfo.Waittime intValue];
  if(waittime<=20)
  {
    if([m_Type isEqualToString:@"RIDE"])
      m_WaitTimeImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/AnnotGreen.png"];
    else
      m_WaitTimeImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/SquareAnnotGreen.png"];    
  }
  else if(waittime<60)
  {
    if([m_Type isEqualToString:@"RIDE"])
      m_WaitTimeImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/AnnotYellow.png"];
    else
      m_WaitTimeImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/SquareAnnotYellow.png"];
  }
  else if(waittime>=60)
  {
    if([m_Type isEqualToString:@"RIDE"])
      m_WaitTimeImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/AnnotRed.png"];
    else
      m_WaitTimeImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/SquareAnnotRed.png"];
  }
  
  int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];
  
  if(waittime==-1 || open==0)
  {
    if([m_Type isEqualToString:@"RIDE"])
      m_WaitTimeImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/AnnotClosedRound.png"];
    else
      m_WaitTimeImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/AnnotClosedSquare.png"];    
  }
  
  m_WaitTimeImageView.userInteractionEnabled=YES;
  [self addSubview:m_WaitTimeImageView];
  [m_WaitTimeImageView release];
  
  m_WaitTimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
  m_WaitTimeLabel.textColor=[UIColor blackColor];
  m_WaitTimeLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:15];
  if(waittime != -1 && open==1)
    m_WaitTimeLabel.text=m_AnnoataionInfo.Waittime;
  else 
      m_WaitTimeLabel.text=@"";
  m_WaitTimeLabel.textAlignment=UITextAlignmentCenter;
  m_WaitTimeLabel.backgroundColor=[UIColor clearColor];
  [self addSubview:m_WaitTimeLabel];
  [m_WaitTimeLabel release];
  
  int strleng=[pAnnotation.title length];
  if(strleng==8)
    m_TitleImageView=[[UIImageView alloc]initWithFrame:CGRectMake(29, 5, strleng*10, 20)];
  else if(strleng==9 || strleng==22)
    m_TitleImageView=[[UIImageView alloc]initWithFrame:CGRectMake(29, 5, strleng*9, 20)];
  else if(strleng==5 || strleng==4)
    m_TitleImageView=[[UIImageView alloc]initWithFrame:CGRectMake(29, 5, strleng*10, 20)];
  else 
    m_TitleImageView=[[UIImageView alloc]initWithFrame:CGRectMake(29, 5, strleng*8, 20)];
  m_TitleImageView.image=[UIImage imageNamed:@"ImagesInPaidVersion/Map/TextBubbles.png"];
  if([m_Type isEqualToString:@"RIDE"])
    m_TitleImageView.layer.opacity=.8;
  else 
    m_TitleImageView.layer.opacity=.5;

  m_TitleImageView.userInteractionEnabled=YES;
  [self addSubview:m_TitleImageView];
  [m_TitleImageView release];
    
  if(strleng==8)
  {
    m_TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(29, 5, strleng*10, 20)];
    width=strleng*10;
  }
  else if(strleng==9 || strleng==22)
  {
    m_TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(29, 5, strleng*9, 20)];
    width=strleng*9;
  }
  else if(strleng==5 || strleng==4)
  {
    m_TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(29, 5, strleng*10, 20)];
    width=strleng*10;
  }
  else
  {
    m_TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(29, 5, strleng*8, 20)];
    width=strleng*8;
  }
  
  m_TitleLabel.backgroundColor=[UIColor clearColor];
  m_TitleLabel.textColor=[UIColor whiteColor];
  m_TitleLabel.text=m_AnnoataionInfo.title;
  m_TitleLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:14];
  m_TitleLabel.textAlignment=UITextAlignmentCenter;
  [self addSubview:m_TitleLabel];
  [m_TitleLabel release];
  
  
  m_Button=[UIButton buttonWithType:UIButtonTypeCustom];
  m_Button.frame=CGRectMake(0, 0, 30+width, 30);
  m_Button.backgroundColor=[UIColor clearColor];
  [m_Button addTarget:self action:@selector(ButtonTapped) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:m_Button];
  
  [self calculateCoOrdinates];
  
  return self;
}

-(void)calculateCoOrdinates
{
  CLLocation *attractionLocation=[[CLLocation alloc]initWithLatitude:m_AnnoataionInfo.m_AnnotX longitude:m_AnnoataionInfo.m_AnnotY]; 
  
  CLLocation *parkLoaction=[[CLLocation alloc]initWithLatitude:m_AnnoataionInfo.m_ParkX longitude:m_AnnoataionInfo.m_ParkY];
  
  CLLocation *XLocation=[[CLLocation alloc]initWithLatitude:m_AnnoataionInfo.m_ParkX longitude:m_AnnoataionInfo.m_AnnotY];
  
  CLLocation *YLocation=[[CLLocation alloc]initWithLatitude:m_AnnoataionInfo.m_AnnotX longitude:m_AnnoataionInfo.m_ParkY];
  
  CLLocationDistance distanceX=[attractionLocation distanceFromLocation:XLocation];
  CLLocationDistance distanceY=[attractionLocation distanceFromLocation:YLocation];                   

  float divisionValue=[[NSUserDefaults standardUserDefaults]floatForKey:@"DIVIDEVALUE"];
  
  float heightRatio=[[NSUserDefaults standardUserDefaults]floatForKey:@"HEIGHTRATIO"];

  
  NSNumber *XNumber=[NSNumber numberWithDouble:distanceX];
  self.m_Ycoordinate=(([XNumber intValue])/divisionValue*heightRatio);
  
  NSNumber *YNumber=[NSNumber numberWithDouble:distanceY];
  self.m_Xcoordinate=(([YNumber intValue])/divisionValue*heightRatio);
  
  [attractionLocation release];
  [parkLoaction release];
  [XLocation release];
  [YLocation release];
}

-(void)showTitle
{
  m_Button.frame=CGRectMake(0, 0, 30+width, 30);
  m_TitleLabel.hidden=NO;
  m_TitleImageView.hidden=NO;
}

-(void)hideTitle
{
  m_Button.frame=CGRectMake(0, 0, 30, 30);
  m_TitleLabel.hidden=YES;
  m_TitleImageView.hidden=YES;
}

-(void)ButtonTapped
{
  if(m_delegate)
    [m_delegate gotoDetailPagewithlandIndex:pLandIndex Index:pIndex type:m_Type];
}

- (void)dealloc 
{
  if(m_Button){[m_Button removeFromSuperview];m_Button=nil;}
  if(m_Type){[m_Type release];m_Type=nil;}
  if(m_delegate){m_delegate=nil;}
  [super dealloc];
}


@end
