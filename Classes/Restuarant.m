//
//  Restuarant.m
//  AmusementPark
//
//  Created by HARSHA on 08/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Restuarant.h"


@implementation Restuarant
@synthesize m_CuisineName,m_LandId,m_ParkId,m_PhonoNo,m_RestDesc,m_RestId,m_RestName,m_StudioId,m_Reserve,m_ImageId,m_Latitude,m_Longitude,m_RestCost,m_Distance,m_RestServiceType,m_WaitTime;

-(id)init
{
  if(self=[super init])
  {
    m_CuisineName=nil;
    m_LandId=nil;
    m_ParkId=nil;
    m_PhonoNo=nil;
    m_RestDesc=nil;
    m_RestId=nil;
    m_RestName=nil;
    m_StudioId=nil;
    m_ImageId=nil;
    m_Latitude=nil;
    m_Longitude=nil;
    m_RestCost=nil;
    m_RestServiceType=nil;
    m_WaitTime=nil;
    m_Reserve=-1;
    m_Distance=0;
  }
  return self;
}


-(void)dealloc
{
  if(m_CuisineName){[m_CuisineName release];m_CuisineName=nil;}
  if(m_LandId){[m_LandId release];m_LandId=nil;}
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_PhonoNo){[m_PhonoNo release];m_PhonoNo=nil;}
  if(m_RestDesc){[m_RestDesc release];m_RestDesc=nil;}
  if(m_RestId){[m_RestId release];m_RestId=nil;}
  if(m_RestName){[m_RestName release];m_RestName=nil;}
  if(m_StudioId){[m_StudioId release];m_StudioId=nil;}
  if(m_ImageId){[m_ImageId release];m_ImageId=nil;}
  if(m_Latitude ){[m_Latitude release];m_Latitude=nil;}
  if(m_Longitude){[m_Longitude release];m_Longitude=nil;}
  if(m_RestCost){[m_RestCost release];m_RestCost=nil;}
  if(m_RestServiceType){[m_RestServiceType release];m_RestServiceType=nil;}
  if(m_WaitTime){[m_WaitTime release];m_WaitTime=nil;}
  [super dealloc];
}
@end
