//
//  UIDeviceHardware.h
//  AmusementPark
//
//  Created by HARSHA on 12/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIDeviceHardware : NSObject
{
  
}
  - (NSString *) platform;
  - (NSString *) platformString;

@end
