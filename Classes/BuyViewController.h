//
//  BuyViewController.h
//  AmusementPark
//
//  Created by Kavitha Pujar on 09/01/12.
//  Copyright (c) 2012 Permeative Technologies Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmusementParkAppDelegate.h"


@protocol BuyViewControllerDelegate
-(void)DonePurchasing;
-(void)AddBigBanner;
@end

@interface BuyViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
  
  UITableView *m_TableView;
  NSMutableArray *tImageArray,*tSelectedImgArray;
  id<BuyViewControllerDelegate> m_delegate;
    AmusementParkAppDelegate * appDelegate;
  UIView *dimView;
  int Flag;
  
  
}
@property(nonatomic,assign) id<BuyViewControllerDelegate> m_delegate;
@property(nonatomic,retain)IBOutlet UIImageView *BGImage;
@property(nonatomic,retain)IBOutlet UIButton *KeepFreeButton;
@property(nonatomic,retain)IBOutlet UIImageView *HomeBtnBg;

-(IBAction)Cancel:(id)sender;
+(BuyViewController*)sharedBuyViewController;
-(void)MakeInAppPurchaseWithProductId:(NSString *)AppIdentifier withTag:(int)Tag;

@end
