    //
//  FavoriteViewController.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FavoriteViewController.h"
#import "RestuarantDetailCell.h"
#import "AmusementParkDB.h"
#import "Favourite.h"
#import "LandInfo.h"


@implementation FavoriteViewController
@synthesize m_delegate;

-(id)initWithParkId:(NSString *)pParkId
{
	if(self=[super init])
	{
    m_ParkId=pParkId;
    [m_ParkId retain];
		m_LandsArray=[[NSMutableArray alloc]init];
		m_FavouriteArray=[[NSMutableArray alloc]init];
		m_RidesDetailViewController=nil;
		m_RestuarantDetailViewController=nil;
    m_SubmitWaitTimeViewController=nil;
		m_TableView=nil;
    pTitleLabel=nil;
    m_Homebtn=nil;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRecieved) name:@"NotificationName" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SelectedTwiceNotificationRecieved) name:@"SelectedTwice" object:nil];
	}
	return self;
}


-(void)loadView
{
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	self.view.backgroundColor=[UIColor clearColor];
	
  if(m_Homebtn==nil)
  {
  m_Homebtn=[UIButton buttonWithType:UIButtonTypeCustom];
	m_Homebtn.frame=CGRectMake(5, 8, 53.5, 30);
	[m_Homebtn setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/homebtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_Homebtn addTarget:self action:@selector(backtoHomeAction) forControlEvents:UIControlEventTouchUpInside];
	[m_Homebtn retain];  
  }
  
  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 8, 190, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=@"Favorites";
  
    UIImageView * m_BGImageView;
    
    if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, -40, 320, 545)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, -40, 320, 460)];
        
    }
  m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];	
	[m_BGImageView release];
  
//  UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ImagesCommon/Rides/tabbar/tabbar.png"]];
//	imageView.frame=CGRectMake(0, 0, 320, 48.5);
//	[self.navigationController.navigationBar insertSubview:imageView atIndex:0];
//	[imageView release];
	
    if(m_TableView==nil)
    {
	    
        if (screenBounds.size.height == 568) {
            m_TableView = [[UITableView alloc] initWithFrame: CGRectMake(-5,10,330,426) style:
                           UITableViewStyleGrouped];
            
            // code for 4-inch screen
        } else {
            // code for 3.5-inch screen
            m_TableView = [[UITableView alloc] initWithFrame: CGRectMake(-5,10,330,360) style:
                           UITableViewStyleGrouped];
            
        }
    }
    m_TableView.backgroundView=nil;
  
 [m_TableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];    
	m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.scrollEnabled = YES;
  m_TableView.backgroundColor=[UIColor clearColor];
	[self.view addSubview:m_TableView];
  
}

-(void)NotificationRecieved
{
 [super viewWillAppear:YES];
  if([m_LandsArray count]){[m_LandsArray removeAllObjects];}
  if([m_FavouriteArray count]){[m_FavouriteArray removeAllObjects];}
  [m_LandsArray addObjectsFromArray:[[AmusementParkDB sharedPtDatabase] getLandsFromDBWithParkId:m_ParkId]];
  for(int i=0;i<[m_LandsArray count];i++)
  {
    LandInfo *pLandInfo=[m_LandsArray objectAtIndex:i];
    Favourite *pFavourite=[[Favourite alloc]init];
    pFavourite.m_LandId=pLandInfo.m_LandId;
    pFavourite.m_LandName=pLandInfo.m_LandName;
    [pFavourite.m_RideArray addObjectsFromArray:[[AmusementParkDB sharedPtDatabase]getRidesWithLandId:pFavourite.m_LandId]];
    [pFavourite.m_RestuarantArray addObjectsFromArray:[[AmusementParkDB sharedPtDatabase]getRestsWithLandId:pFavourite.m_LandId]];
    [pFavourite.m_CombinedArray addObjectsFromArray:pFavourite.m_RideArray];
    [pFavourite.m_CombinedArray addObjectsFromArray:pFavourite.m_RestuarantArray];
    if([pFavourite.m_CombinedArray count])
    [m_FavouriteArray addObject:pFavourite];
    [pFavourite release];
    pFavourite=nil;
  }    
  [m_TableView reloadData];
}

-(void)SelectedTwiceNotificationRecieved
{
  int index=[[NSUserDefaults standardUserDefaults]integerForKey:@"INDEX"];
  if(index==3)
  {
    while ([self.navigationController.navigationBar.subviews count] > 2)
    {
      int n=[self.navigationController.navigationBar.subviews count]-1;
      
      [[[self.navigationController.navigationBar subviews] objectAtIndex:n] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:m_Homebtn];
    [self.navigationController.navigationBar addSubview:pTitleLabel];
  }  
}

-(void)backtoHomeAction
{
	if(m_delegate)
		[m_delegate gotoHome];
}

-(void)BackAction
{
  [self NotificationRecieved];
}

#pragma mark tableView delegate methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	Favourite *pFavourite=[m_FavouriteArray objectAtIndex:section];
  return [pFavourite.m_CombinedArray count];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [m_FavouriteArray count];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  Favourite *pFavourite=[m_FavouriteArray objectAtIndex:indexPath.section];
  if([[pFavourite.m_CombinedArray objectAtIndex:indexPath.row] isKindOfClass:[Ride class]])
  {
    if(indexPath.row==0 || indexPath.row==([pFavourite.m_CombinedArray count]-1))
      return 54;
    else 
      return 51;
  }
  else
  {
    if(indexPath.row==0 || indexPath.row==([pFavourite.m_CombinedArray count]-1))
      return 135/2+3;
    else 
      return 135/2;
  }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
  // create the parent view that will hold header Label
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(20, 0.0, 320.0, 20)];
    
    // create the button object
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.opaque = NO;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.highlightedTextColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    headerLabel.frame = CGRectMake(20, 0.0, 320.0, 20);
    
    // If you want to align the header text as centered
  Favourite *pFavourite=[m_FavouriteArray objectAtIndex:section];
  headerLabel.text =pFavourite.m_LandName;
    
    [customView addSubview:headerLabel];
    
    return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  return 30;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
  Favourite *pFavourite=[m_FavouriteArray objectAtIndex:indexPath.section];
	if([[pFavourite.m_CombinedArray objectAtIndex:indexPath.row] isKindOfClass:[Ride class]])
	{
			RidesDetailCell *cell = (RidesDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"RidesDetailCell"];
		if(cell == nil)
		{  
      cell=[[[RidesDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RidesDetailCell"]autorelease];
      cell.m_delegate=self;
			cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle=UITableViewCellSelectionStyleGray;
		}
    cell.m_CustomButton.section=indexPath.section;
    cell.m_CustomButton.row=indexPath.row;
    cell.m_CustomButton.hidden=YES;

    Ride *pRide=[pFavourite.m_CombinedArray objectAtIndex:indexPath.row];
			cell.m_TitleLabel.text=pRide.m_RideName;
    
    NSNumber *number = [NSNumber numberWithInt:pRide.m_Distance];
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
    [frmtr setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *commaString = [frmtr stringFromNumber:number];
    NSString *pYardsStr=[commaString stringByAppendingString:@" yards"];
    cell.m_YardsLabel.text=pYardsStr;
    [frmtr release];
    
    int waittime=[pRide.m_WaitTime intValue];
    if(waittime<=20){
      UIImage *img=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/green1" ofType:@"png"]];
      cell.m_WaitTimeImageView.image=img;
    }
    else if(waittime<60){
      UIImage *img=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/yellow1" ofType:@"png"]];
      cell.m_WaitTimeImageView.image=img;
    }
    else if(waittime>=60){
      UIImage *img=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/red1" ofType:@"png"]];
      cell.m_WaitTimeImageView.image=img;
    }
    
    int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];

    if(waittime==-1 || open==0)
    {
      if(indexPath.row==0)
      {
        if([pFavourite.m_CombinedArray count]!=1)
        {
          cell.m_WaitTimeImageView.frame=CGRectMake(22,17,45,29);
          cell.m_WaitTimeLabel.frame=CGRectMake(22,17,45,29);
          cell.m_TitleLabel.frame=CGRectMake(75,17,190,15);
          cell.m_YardsLabel.frame=CGRectMake(75,31,150,15);
          cell.m_CustomButton.frame=CGRectMake(274, 13, 40, 33);                        
        }
        else
        {
          cell.m_WaitTimeImageView.frame=CGRectMake(22,13,45,29);
          cell.m_WaitTimeLabel.frame=CGRectMake(22,13,45,29);
          cell.m_TitleLabel.frame=CGRectMake(75,11,190,15);
          cell.m_YardsLabel.frame=CGRectMake(75,25,150,15);
          cell.m_CustomButton.frame=CGRectMake(274,7, 40, 33);                                
        }
      }
      else
      {
        cell.m_WaitTimeImageView.frame=CGRectMake(22,12,45,29);
        cell.m_WaitTimeLabel.frame=CGRectMake(22,12,45,29);
        cell.m_TitleLabel.frame=CGRectMake(75,12,190,15);
        cell.m_YardsLabel.frame=CGRectMake(75,26,150,15);
        cell.m_CustomButton.frame=CGRectMake(274, 8, 40, 33);        
      }
      UIImage *img=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]];
      cell.m_WaitTimeImageView.image=img;
      cell.m_WaitTimeLabel.text=@"";
      [cell SetOpacityLow];
      cell.m_WaitTimeLabel.layer.opacity=1.0;
      cell.userInteractionEnabled=YES;
    }
    else if(waittime==75)
    {
      if(indexPath.row==0)
      {
        if([pFavourite.m_CombinedArray count]!=1)
        {
          cell.m_WaitTimeImageView.frame=CGRectMake(22,12,42,32);
          cell.m_WaitTimeLabel.frame=CGRectMake(22,12,42,32);
          cell.m_TitleLabel.frame=CGRectMake(75,17,190,15);
          cell.m_YardsLabel.frame=CGRectMake(75,31,150,15);
          cell.m_CustomButton.frame=CGRectMake(274, 13, 40, 33);  
        }
        else 
        {
          cell.m_WaitTimeImageView.frame=CGRectMake(22,10,42,32);
          cell.m_WaitTimeLabel.frame=CGRectMake(22,10,42,32);
          cell.m_TitleLabel.frame=CGRectMake(75,13,190,15);
          cell.m_YardsLabel.frame=CGRectMake(75,27,150,15);
          cell.m_CustomButton.frame=CGRectMake(274,9, 40, 33);                      
        }
      }
      else 
      {
        cell.m_WaitTimeImageView.frame=CGRectMake(22,7,42,32);
        cell.m_WaitTimeLabel.frame=CGRectMake(22,7,42,32);
        cell.m_TitleLabel.frame=CGRectMake(75,12,190,15);
        cell.m_YardsLabel.frame=CGRectMake(75,26,150,15);
        cell.m_CustomButton.frame=CGRectMake(274, 8, 40, 33);                
      }
      cell.m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",pRide.m_WaitTime];
      [cell SetOpacityHigh];
    }
    else 
    {
      if(indexPath.row==0)
      {
        if([pFavourite.m_CombinedArray count]!=1)
        {
          cell.m_WaitTimeImageView.frame=CGRectMake(22,13,32,32);
          cell.m_WaitTimeLabel.frame=CGRectMake(22,13,32,32);
          cell.m_TitleLabel.frame=CGRectMake(75,17,190,15);
          cell.m_YardsLabel.frame=CGRectMake(75,31,150,15);
          cell.m_CustomButton.frame=CGRectMake(274, 13, 40, 33);                        
        }
        else 
        {
          cell.m_WaitTimeImageView.frame=CGRectMake(22,10,32,32);
          cell.m_WaitTimeLabel.frame=CGRectMake(22,10,32,32);
          cell.m_TitleLabel.frame=CGRectMake(75,13,190,15);
          cell.m_YardsLabel.frame=CGRectMake(75,27,150,15);
          cell.m_CustomButton.frame=CGRectMake(274, 9, 40, 33);                                  
        }
      }
      else
      {
        cell.m_WaitTimeImageView.frame=CGRectMake(22,9,32,32);
        cell.m_WaitTimeLabel.frame=CGRectMake(22,9,32,32);
        cell.m_TitleLabel.frame=CGRectMake(75,12,190,15);
        cell.m_YardsLabel.frame=CGRectMake(75,26,150,15);
        cell.m_CustomButton.frame=CGRectMake(274, 8, 40, 33);                
      }
      cell.m_WaitTimeLabel.text=pRide.m_WaitTime;
      [cell SetOpacityHigh];
    }
    
    
    UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 135/2, 51)];
    if([pFavourite.m_CombinedArray count]==1)
    bgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/singlecell2.png"];
		else if(indexPath.row==0)
    bgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/uppercell2.png"];
		else if(indexPath.row==[pFavourite.m_CombinedArray count]-1)
    bgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/downcell2.png"];
		else
    bgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/middlecell2.png"];
		cell.backgroundView=bgImgView;
		[bgImgView release];    
    
    UIImageView *selbgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,316, 51)];
    if([pFavourite.m_CombinedArray count]==1)
    selbgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/selsinglecell2.png"];
		else if(indexPath.row==0)
    selbgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/seluppercell2.png"];
		else if(indexPath.row==[pFavourite.m_CombinedArray count]-1)
    selbgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/seldowncell2.png"];
		else
    selbgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/selmiddlecell2.png"];
		cell.selectedBackgroundView=selbgImgView;
		[selbgImgView release];    
    
		return cell;
	}
	else
	{
		RestuarantDetailCell *cell = (RestuarantDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"RestuarantDetailCell"];
		if(cell == nil)
		{  
      cell=[[[RestuarantDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RestuarantDetailCell"]autorelease];
			cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle=UITableViewCellSelectionStyleGray;
		}
    Restuarant *pRest=[pFavourite.m_CombinedArray objectAtIndex:indexPath.row];

		cell.m_TitleLabel.text=pRest.m_RestName;
    
    if([pRest.m_RestServiceType isEqualToString:@"Quick"])
      cell.m_ImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/FoodPage/Quick" ofType:@"png"]];
    else 
      cell.m_ImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/FoodPage/Table" ofType:@"png"]];
    
    int waittime=[pRest.m_WaitTime intValue];
    if(waittime<=20)
      cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/Block/green1" ofType:@"png"]];
    else if(waittime<60)
      cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/yellow1" ofType:@"png"]];
    else if(waittime>=60)
      cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/red1" ofType:@"png"]];
    
    int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];

    if(waittime==-1 || open==0)
    {
      cell.m_WaitTimeImageView.frame=CGRectMake(21,22,45,29);
      cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]];
      cell.m_WaitTimeLabel.frame=CGRectMake(21,22,45,29);
      cell.m_WaitTimeLabel.text=@"";
      [cell SetOpacityLow];
      cell.userInteractionEnabled=YES;
    }
    else if(waittime==75)
    {
      cell.m_WaitTimeImageView.frame=CGRectMake(21,17,42,31);
      cell.m_WaitTimeLabel.frame=CGRectMake(21,17,42,31);
      cell.m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",pRest.m_WaitTime];
      [cell SetOpacityHigh];
    }
    else 
    {
      cell.m_WaitTimeImageView.frame=CGRectMake(21,17,31,31);
      cell.m_WaitTimeLabel.frame=CGRectMake(21,17,31,31);
      cell.m_WaitTimeLabel.text=pRest.m_WaitTime;
      [cell SetOpacityHigh];
    }
    
    NSNumber *number = [NSNumber numberWithInt:pRest.m_Distance];
    NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
    [frmtr setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *commaString = [frmtr stringFromNumber:number];
    NSString *pYardsStr=[commaString stringByAppendingString:@" yards"];
    cell.m_YardsLabel.text=pYardsStr;
    [frmtr release];
    
    UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 135/2)];
    if([pFavourite.m_CombinedArray count]==1)
      bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restsingle1.png"];
    else if(indexPath.row==0)
      bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restup1.png"];
    else if(indexPath.row==[pFavourite.m_CombinedArray count]-1)
      bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restbottom1.png"];
    else
      bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restmiddle1.png"];
    cell.backgroundView=bgImgView;
    [bgImgView release];  
    
    UIImageView *selbgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 135/2)];
    if([pFavourite.m_CombinedArray count]==1)
      selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestsingle1.png"];
    else if(indexPath.row==0)
      selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestup1.png"];
    else if(indexPath.row==[pFavourite.m_CombinedArray count]-1)
      selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestbottom1.png"];
    else
      selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestmiddle1.png"];
    cell.selectedBackgroundView=selbgImgView;
    [selbgImgView release];  
    
		return cell;
	}
}


-(void)gotoDetailPage:(int)pSection Row:(int)pRow
{
  [m_Homebtn removeFromSuperview];
  [pTitleLabel removeFromSuperview];
  Favourite *pFavourite=[m_FavouriteArray objectAtIndex:pSection];
  if([[pFavourite.m_CombinedArray objectAtIndex:pRow] isKindOfClass:[Ride class]])
	{
		if(m_RidesDetailViewController)
		{
			[m_RidesDetailViewController.view removeFromSuperview];
			[m_RidesDetailViewController release];
			m_RidesDetailViewController=nil;
		}
    Ride *pRide=[pFavourite.m_CombinedArray objectAtIndex:pRow];
    //printf("\n\n%s\n\n",[pRide.m_RideId UTF8String]);
		m_RidesDetailViewController=[[RidesDetailViewController alloc]initwithStudioId:pRide.m_StudioId ParkId:m_ParkId LandId:pRide.m_LandId RideId:pRide.m_RideId LandName:pFavourite.m_LandName Lat:pRide.m_Latitude Long:pRide.m_Longitude Distance:pRide.m_Distance isFavourite:YES];
    m_RidesDetailViewController.m_delegate=self;
		[self.navigationController pushViewController:m_RidesDetailViewController animated:YES];
		
	}  
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [m_Homebtn removeFromSuperview];
  [pTitleLabel removeFromSuperview];
  Favourite *pFavourite=[m_FavouriteArray objectAtIndex:indexPath.section];
  if([[pFavourite.m_CombinedArray objectAtIndex:indexPath.row] isKindOfClass:[Ride class]])
	{
		if(m_RidesDetailViewController)
		{
			[m_RidesDetailViewController.view removeFromSuperview];
			[m_RidesDetailViewController release];
			m_RidesDetailViewController=nil;
		}
    Ride *pRide=[pFavourite.m_CombinedArray objectAtIndex:indexPath.row];
    //printf("\n\n%s\n\n",[pRide.m_RideId UTF8String]);
		m_RidesDetailViewController=[[RidesDetailViewController alloc]initwithStudioId:pRide.m_StudioId ParkId:m_ParkId LandId:pRide.m_LandId RideId:pRide.m_RideId LandName:pFavourite.m_LandName Lat:pRide.m_Latitude Long:pRide.m_Longitude Distance:pRide.m_Distance isFavourite:YES];
    m_RidesDetailViewController.m_delegate=self;
		[self.navigationController pushViewController:m_RidesDetailViewController animated:YES];
	}  
	else
	{
      if(m_RestuarantDetailViewController)
      {
        [m_RestuarantDetailViewController.view removeFromSuperview];
        [m_RestuarantDetailViewController release];
        m_RestuarantDetailViewController=nil;
      }
      Restuarant *pRestuarant=[pFavourite.m_CombinedArray objectAtIndex:indexPath.row];
    m_RestuarantDetailViewController=[[RestuarantDetailViewController alloc]initwithStudioId:pRestuarant.m_StudioId ParkId:pRestuarant.m_ParkId LandId:pRestuarant.m_LandId RestId:pRestuarant.m_RestId LandName:pFavourite.m_LandName Distance:pRestuarant.m_Distance isFavourite:YES imagePath:pRestuarant.m_ImageId pLat:pRestuarant.m_Latitude pLong:pRestuarant.m_Longitude];
      m_RestuarantDetailViewController.m_delegate=self;
      [self.navigationController pushViewController:m_RestuarantDetailViewController animated:YES];
	}
}


-(void)viewDidLoad
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)viewWillAppear:(BOOL)animated
{
  while ([self.navigationController.navigationBar.subviews count] > 2)
  {
    int n=[self.navigationController.navigationBar.subviews count]-1;
    
    [[[self.navigationController.navigationBar subviews] objectAtIndex:n] removeFromSuperview];
  }
  [self.navigationController.navigationBar addSubview:m_Homebtn];
  [self.navigationController.navigationBar addSubview:pTitleLabel];
  
  [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
	if(m_SubmitWaitTimeViewController)
  {
    [m_SubmitWaitTimeViewController.view removeFromSuperview];
    m_SubmitWaitTimeViewController.m_delegate=nil;
    [m_SubmitWaitTimeViewController release];
    m_SubmitWaitTimeViewController=nil;
  }
  if(m_RidesDetailViewController)
  {
    [m_RidesDetailViewController.view removeFromSuperview];
    m_RidesDetailViewController.m_delegate=nil;
    [m_RidesDetailViewController release];
    m_RidesDetailViewController=nil;
  }
	if(m_RestuarantDetailViewController)
  {
    [m_RestuarantDetailViewController.view removeFromSuperview];
    m_RestuarantDetailViewController.m_delegate=nil;
    [m_RestuarantDetailViewController release];
    m_RestuarantDetailViewController=nil;
  }
  [super viewDidAppear:animated];
}

- (void)dealloc 
{
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_Homebtn){[m_Homebtn removeFromSuperview];[m_Homebtn release];m_Homebtn=nil;}
	if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
	if(m_LandsArray){[m_LandsArray removeAllObjects];[m_LandsArray release];m_LandsArray=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
	if(m_FavouriteArray){[m_FavouriteArray removeAllObjects];[m_FavouriteArray release];m_FavouriteArray=nil;}
	if(m_RidesDetailViewController){[m_RidesDetailViewController.view removeFromSuperview];m_RidesDetailViewController.m_delegate=nil;[m_RidesDetailViewController release];m_RidesDetailViewController=nil;}
	if(m_RestuarantDetailViewController){[m_RestuarantDetailViewController.view removeFromSuperview];m_RestuarantDetailViewController.m_delegate=nil;[m_RestuarantDetailViewController release];m_RestuarantDetailViewController=nil;}
	if(m_SubmitWaitTimeViewController){[m_SubmitWaitTimeViewController.view removeFromSuperview];m_SubmitWaitTimeViewController.m_delegate=nil;[m_SubmitWaitTimeViewController release];m_SubmitWaitTimeViewController=nil;}
    [super dealloc];
}


@end
