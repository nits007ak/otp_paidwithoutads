//
//  AboutUsViewController.h
//  AmusementPark
//
//  Created by HARSHA on 04/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApptasmicInfoViewController.h"
#import "ApptasmicViewController.h"
#import "AmusementParkAppDelegate.h"

@interface AboutUsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UIButton *m_BackButton;
    UITableView *m_TableView;
    NSMutableArray *m_SectionArray;
    NSMutableArray *RowArray1;
    AmusementParkAppDelegate * appDelegate;
    ApptasmicViewController *m_ApptasmicViewController;
    ApptasmicInfoViewController *m_ApptasmicInfoViewController;
    
    NSMutableArray *m_AppName;
    NSMutableArray *m_FreeApps,*m_FullApps;
    NSMutableArray *m_FreeName,*m_FullName;
}
@end
