    //
//  FilterViewController.m
//  AmusementPark
//
//  Created by Smaranika on 7/26/11.
//  Copyright 2011 iPhone_Lancers. All rights reserved.
//

#import "FilterViewController.h"
#import "RidesDetailCell.h"

@implementation FilterViewController
@synthesize m_delegate;
-(id)init
{
	if((self=[super init]))
	{
		m_TableView=nil;
		m_TitleArray=nil;
    pTitleLabel=nil;
    m_SelectedIndex=1;
    
    
		m_TitleArray=[[NSMutableArray alloc]initWithObjects:@"Filter By",@"Ride",@"Restaurant",nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification) name:@"NotificationName" object:nil];
	}
	return self;
}

-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	self.view.backgroundColor=[UIColor whiteColor];
	[self.navigationItem setHidesBackButton:YES animated:NO];
	
    
	if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }
    
  m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];	
	[m_BGImageView release];
	
  if(pTitleLabel==nil)
    pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(90, 8, 140, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:19];
  pTitleLabel.textColor=[UIColor whiteColor];
    
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=@"Filter";
  
  if(m_BackButton==nil)
  {
    m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
    m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
    [m_BackButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/backbtn" ofType:@"png"]] forState:UIControlStateNormal];
    [m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [m_BackButton retain];
  }
	
  if(m_TableView==nil)
    m_TableView = [[UITableView alloc]init];
  m_TableView.frame=CGRectMake(0,10,320,325);
  [m_TableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    m_TableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.scrollEnabled = YES;
	m_TableView.backgroundColor=[UIColor clearColor];
	m_TableView.rowHeight=53;
	[self.view addSubview:m_TableView];
    
    [self layoutSubViews];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

- (void) didRotate:(NSNotification *)notification{
    
    
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubViews];
}
-(void)layoutSubViews
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (appDelegate.isPortrait) {
        m_BGImageView.frame = CGRectMake(0, 0, 320, 480);
        m_BackButton.frame = CGRectMake(5, 8, 53.5, 30);
        pTitleLabel.frame = CGRectMake(90, 8, 140, 30);
        if(screenBounds.size.height == 568) m_BGImageView.frame = CGRectMake(0, 0, 320, 568);
    }
    else{
        m_BackButton.frame = CGRectMake(5, 3, 53.5, 30);
        pTitleLabel.frame = CGRectMake(180, 3, 140, 30);
        m_BGImageView.frame = CGRectMake(0, 0, 480 , 320);
        if (screenBounds.size.height == 568) {
            
            pTitleLabel.frame = CGRectMake(185, 1, 200, 30);
            m_BGImageView.frame = CGRectMake(0, 0, 568 , 320);
        }
    }
    
    [m_TableView reloadData];
}



-(void)backAction
{
	if(m_delegate && [m_delegate respondsToSelector:@selector(BackFromFilterViewControllerDelegate)])
		[m_delegate BackFromFilterViewControllerDelegate];
	[m_BackButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)receivedNotification
{
  [[NSNotificationCenter defaultCenter]removeObserver:self];
  [self backAction];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	RidesDetailCell *cell = (RidesDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"MainPageCell"];
	if(cell == nil)
	{  
    cell=[[[RidesDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainPageCell"]autorelease];
    cell.m_CustomButton.hidden=YES;
    cell.accessoryType = UITableViewCellAccessoryNone;
		cell.selectionStyle=UITableViewCellSelectionStyleGray;
	}
	
  cell.m_WaitTimeLabel.hidden=YES;
  cell.m_TitleLabel.text=[m_TitleArray objectAtIndex:indexPath.row];
  UIImageView *bgImgView = [[UIImageView alloc]init];
  if(indexPath.row==0)
  {
    cell.m_TitleLabel.frame=CGRectMake(120,15,250,30);
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/uppercell3" ofType:@"png"]];
    cell.m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:17];
    cell.m_TitleLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:1.0];
    cell.m_TitleLabel.shadowColor=[UIColor whiteColor];
    cell.m_TitleLabel.shadowOffset=CGSizeMake(0, 1);
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
  }
  else if(indexPath.row==[m_TitleArray count]-1)
  {
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/downcell3" ofType:@"png"]];
    cell.m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    cell.m_TitleLabel.textAlignment=UITextAlignmentCenter;
    cell.m_TitleLabel.frame=CGRectMake(25,10,250,30);
  }
  else
  {
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/normalcell" ofType:@"png"]];
    cell.m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
    cell.m_TitleLabel.textAlignment=UITextAlignmentCenter;
    cell.m_TitleLabel.frame=CGRectMake(25,10,250,30);
  }

    if (!appDelegate.isPortrait) {
        CGRect frame = cell.m_TitleLabel.frame;
        int xVal = frame.origin.x;
        frame.origin.x = xVal +100;
        cell.m_TitleLabel.frame = frame;
        
    }

  m_SelectedIndex=[[NSUserDefaults standardUserDefaults] integerForKey:@"FilterIndex"];

  
  if(indexPath.row==1)
  {
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"RideFilterIndex"] == 1)
    {
      cell.m_Selected.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/marked" ofType:@"png"]];
        bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/selectedcell3" ofType:@"png"]];
    }
    else
      cell.m_Selected.image=nil;
  }
  
  else if(indexPath.row==2)
  {
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"RestFilterIndex"] == 1)
    {
      cell.m_Selected.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/marked" ofType:@"png"]];
       bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/downcell2old" ofType:@"png"]];
    }
    else
      cell.m_Selected.image=nil;
  }
  
  cell.backgroundView=bgImgView;
  [bgImgView release];
	
  UIImageView *selBgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,316, 51)];
  if(indexPath.row==2)
    selBgImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/downcell2old" ofType:@"png"]];
  else
    selBgImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/selectedcell3" ofType:@"png"]];  
  cell.selectedBackgroundView=selBgImageView;
  [selBgImageView release];
  selBgImageView=nil;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if(indexPath.row!=0)
  {
    if(indexPath.row==1)
    {
      if([[NSUserDefaults standardUserDefaults]integerForKey:@"RideFilterIndex"] == 1)
        [[NSUserDefaults standardUserDefaults]setInteger:2 forKey:@"RideFilterIndex"];
      else
        [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"RideFilterIndex"];
    }
    else if(indexPath.row==2)
    {
      if([[NSUserDefaults standardUserDefaults]integerForKey:@"RestFilterIndex"] == 1)
        [[NSUserDefaults standardUserDefaults]setInteger:2 forKey:@"RestFilterIndex"];
      else
        [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"RestFilterIndex"];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    m_SelectedIndex=indexPath.row;
    [tableView reloadData];
    
  }
}


-(void)viewDidLoad
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount) {
        
    }
    else{
        [self.navigationController.navigationBar addSubview:pTitleLabel];
        [self.navigationController.navigationBar addSubview:m_BackButton];
    }
	[super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}

-(void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
  if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
	if(m_TitleArray){[m_TitleArray removeAllObjects];[m_TitleArray release];m_TitleArray=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
	if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
  if(m_delegate){m_delegate=nil;}
    [super dealloc];
}


@end
