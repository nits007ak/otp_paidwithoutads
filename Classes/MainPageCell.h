//
//  MainPageCell.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<QuartzCore/QuartzCore.h>


@interface MainPageCell : UITableViewCell 
{
	UIImageView *m_ImageView;
	UILabel *m_TitleLabel;
	UILabel *m_TImeLabel;
}
@property(nonatomic,assign) UIImageView *m_ImageView;
@property(nonatomic,assign) UILabel *m_TitleLabel;
@property(nonatomic,assign) UILabel *m_TImeLabel;
@end
