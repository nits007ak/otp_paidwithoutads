//
//  MainPageCell.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MainPageCell.h"


@implementation MainPageCell
@synthesize m_TitleLabel,m_TImeLabel,m_ImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{    
  self = [super initWithStyle:style  reuseIdentifier:reuseIdentifier];
  if (self) 
	{
		m_ImageView=[[UIImageView alloc]initWithFrame:CGRectMake(7, 7, 65, 45)];
		m_ImageView.layer.cornerRadius = 5.0;
		m_ImageView.layer.masksToBounds = YES;
		[m_ImageView setBackgroundColor:[UIColor clearColor]];
		[self.contentView addSubview:m_ImageView];
		
		m_TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80,8 ,220,30)];
		m_TitleLabel.backgroundColor = [UIColor clearColor];
		m_TitleLabel.textAlignment = UITextAlignmentLeft;
   	m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
		m_TitleLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:1.0];
		[self.contentView addSubview:m_TitleLabel];
		
		m_TImeLabel = [[UILabel alloc]initWithFrame:CGRectMake(80,30,150,20)];
		m_TImeLabel.backgroundColor = [UIColor clearColor];
		m_TImeLabel.textAlignment = UITextAlignmentLeft;
   	m_TImeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:11];
		m_TImeLabel.textColor = [UIColor colorWithRed:98.0/255 green:85.0/255 blue:99.0/255 alpha:1.0];
    m_TImeLabel.shadowOffset = CGSizeMake(0, 1.7);
		[self.contentView addSubview:m_TImeLabel];
		
    
    if(IsUniversalStudioFreeWaitTimes||IsDisneyWorldFreeWaitTimes)
    {
      m_ImageView.frame=CGRectMake(7, 5.5, 94, 64);
      
      m_TitleLabel.frame =CGRectMake(110,16 ,220,30);
      
      m_TImeLabel.frame = CGRectMake(110,38,150,20);
    }

     
  }
  return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated 
{
  [super setSelected:selected animated:animated];
}


- (void)dealloc 
{
	if(m_ImageView){[m_ImageView removeFromSuperview];[m_ImageView release];m_ImageView=nil;}
	if(m_TitleLabel ){[m_TitleLabel  removeFromSuperview];[m_TitleLabel  release];m_TitleLabel =nil;}
	if(m_TImeLabel ){[m_TImeLabel  removeFromSuperview];[m_TImeLabel  release];m_TImeLabel =nil;}
  [super dealloc];
}


@end
