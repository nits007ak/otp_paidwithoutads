//
//  FullVersionUpgrader.m
//  AmusementPark
//
//  Created by Amresh Kumar on 21/10/11.
//  Copyright (c) 2011 Permeative Technologies Pvt. Ltd. All rights reserved.
//

#import "FullVersionUpgrader.h"
#import "UpgradeViewController.h"
#import "AmusementParkAppDelegate.h"
static FullVersionUpgrader *_instance=nil;

@interface FullVersionUpgrader()<UIAlertViewDelegate>

@end

@implementation FullVersionUpgrader

+(FullVersionUpgrader*)sharedUpgrader
{
  if(!_instance)
  {
    _instance=[[FullVersionUpgrader alloc]init];
  }
  return _instance;
}
-(void)showAlert
{
  UIAlertView *alert=[[[UIAlertView alloc]initWithTitle:@"Upgrade" message:@"Please upgrade to full version to use this feature" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil] autorelease];
  [alert show];  
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if(buttonIndex==1)
  {
    UpgradeViewController *mUpgradeViewController=[[[UpgradeViewController alloc]init] autorelease];
    AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.RootViewController presentModalViewController:mUpgradeViewController animated:YES];
  }
}
@end
