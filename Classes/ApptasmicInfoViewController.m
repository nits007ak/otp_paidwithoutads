    //
//  AboutUsViewController.m
//  AmusementPark
//
//  Created by HARSHA on 31/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ApptasmicInfoViewController.h"


@implementation ApptasmicInfoViewController

-(id)init
{
  if(self=[super init])
  {
    m_BackButton=nil;
    m_BGImageView=nil;
    m_CompanyInfoLabel=nil;
    m_CompanyInfoTextLabel=nil;
  }
  return self;
}

- (void)dealloc 
{ 
  if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
  if(m_BGImageView){[m_BGImageView removeFromSuperview];[m_BGImageView release];m_BGImageView=nil;}
  if(m_CompanyInfoLabel){[m_CompanyInfoLabel removeFromSuperview];[m_CompanyInfoLabel release];m_CompanyInfoLabel=nil;}
  if(m_CompanyInfoTextLabel){[m_CompanyInfoTextLabel removeFromSuperview];[m_CompanyInfoTextLabel release];m_CompanyInfoTextLabel=nil;}
  [super dealloc];
}


-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
	imageView.frame=CGRectMake(0, 0, 320, 48.5);
	//[self.navigationController.navigationBar addSubview:imageView];
	[imageView release];
    
    if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }
    m_BGImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];
    
    m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_BackButton.frame=CGRectMake(5, 8, 58, 30);
	[m_BackButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/AboutUS/abtus" ofType:@"png"]] forState:UIControlStateNormal];
	[m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:m_BackButton];
    [m_BackButton retain];
    
    UIImageView *pImageView=[[UIImageView alloc]initWithFrame:CGRectMake(7, 20, 305, 180)];
    pImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/TransparentInfoBack2" ofType:@"png"]];
    pImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:pImageView];
    [pImageView release];
    
	m_CompanyInfoLabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 22, 270, 27)];
	m_CompanyInfoLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
	m_CompanyInfoLabel.text=@"What We Do";
	m_CompanyInfoLabel.backgroundColor=[UIColor clearColor];
    m_CompanyInfoLabel.textColor=[UIColor colorWithRed:32.2/100 green:8.2/100 blue:34.9/100 alpha:1.0];
	m_CompanyInfoLabel.textAlignment=UITextAlignmentLeft;
    m_CompanyInfoLabel.shadowColor = [UIColor colorWithRed:82.4/100 green:60.8/100 blue:76.1/100 alpha:1.0];
    m_CompanyInfoLabel.shadowOffset = CGSizeMake(0, 1.0);
	[self.view addSubview:m_CompanyInfoLabel];
    
    m_CompanyInfoTextLabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 25, 269, 200)];
    m_CompanyInfoTextLabel.numberOfLines=0;
	m_CompanyInfoTextLabel.font=[UIFont fontWithName:@"HelveticaNeue" size:12];
	m_CompanyInfoTextLabel.text=@"Craterzone is a high spirited mobile design & development company enterprising to offer distinctive Mobile apps, web services & customized solutions to its global clientele. CraterZone has extensive experience in  mobile creative designing , mobile websites & Mobile applications development on Android, IOS & BlackBerry.";
	m_CompanyInfoTextLabel.backgroundColor=[UIColor clearColor];
    m_CompanyInfoTextLabel.textColor=[UIColor whiteColor];
    m_CompanyInfoTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	m_CompanyInfoTextLabel.textAlignment=UITextAlignmentLeft;
    m_CompanyInfoTextLabel.shadowColor = [UIColor colorWithRed:43.1/100 green:13.7/100 blue:32.9/100 alpha:1.0];
    m_CompanyInfoTextLabel.shadowOffset = CGSizeMake(0,1.4);
	[self.view addSubview:m_CompanyInfoTextLabel];
    
    CGSize size = [m_CompanyInfoTextLabel.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(269, 200) lineBreakMode:UILineBreakModeWordWrap];
    textlabel = [[UILabel alloc]initWithFrame:CGRectMake(25, size.height+65, 269, 13)];
    
    [textlabel setBackgroundColor:[UIColor clearColor]];
    textlabel.shadowOffset = CGSizeMake(0,1.4);
    textlabel.shadowColor = [UIColor colorWithRed:43.1/100 green:13.7/100 blue:32.9/100 alpha:1.0];
    [textlabel setText:@"contact@craterzone.com"];
    
    
    [textlabel setTextAlignment:UITextAlignmentLeft];
    [textlabel setTextColor:[UIColor whiteColor]];
    [textlabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(labelClicked:)];
    [textlabel setUserInteractionEnabled:YES];
    [textlabel addGestureRecognizer:gesture];
    
    
    CGSize labelsize = [textlabel.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(269, 30) lineBreakMode:UILineBreakModeWordWrap];
    
    blueView = [[UIView alloc]initWithFrame:CGRectMake(25, textlabel.frame.origin.y+12,labelsize.width , 1)];
    [blueView setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:blueView];
    [self.view addSubview:textlabel];
    
    
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubViews];
}


- (void) didRotate:(NSNotification *)notification{
    [self checkOrientation];
    
    
}
-(void)layoutSubViews  //cp
{
    
    CGSize size = [m_CompanyInfoTextLabel.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(269, 200) lineBreakMode:UILineBreakModeWordWrap];
    CGSize labelsize = [textlabel.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(269, 30) lineBreakMode:UILineBreakModeWordWrap];
    
    if (appDelegate.isPortrait) {
        m_BackButton.frame=CGRectMake(5, 8, 58, 30);
        textlabel.frame= CGRectMake(25, size.height+65, 269, 13);
        blueView.frame = CGRectMake(25, textlabel.frame.origin.y+12,labelsize.width , 1);
        
    }
    else
    {
        m_BackButton.frame=CGRectMake(5, 3, 58, 30);
        textlabel.frame= CGRectMake(25, size.height+44, 269, 13);
        blueView.frame = CGRectMake(25, textlabel.frame.origin.y+12,labelsize.width , 1);
    }
}
- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}
-(void)labelClicked:(UITapGestureRecognizer *)geture
{
    [self sendMail];
    // [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.craterzone.com/"]]];
}

-(void)sendMail
{
    MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
    mailViewController.mailComposeDelegate = self;
    [mailViewController setSubject:@""];
    [mailViewController setMessageBody:nil isHTML:NO];
    [mailViewController setToRecipients:[NSArray arrayWithObject:@"contact@craterzone.com"]];
    [self presentModalViewController:mailViewController animated:YES];
}
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:( MFMailComposeResult)result error: ( NSError*)error {
    
    [self dismissModalViewControllerAnimated:YES];
    
}



-(void)backAction
{
  [m_BackButton removeFromSuperview];
  [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewDidLoad
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void)viewWillAppear:(BOOL)animated
{
  [self.navigationController setNavigationBarHidden:NO];
  self.navigationItem.hidesBackButton=YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
