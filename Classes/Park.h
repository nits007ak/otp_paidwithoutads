//
//  Park.h
//  AmusementPark
//
//  Created by HARSHA on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Park : NSObject 
{
  NSString *m_ParkId;
  NSString *m_ParkName;
  NSString *m_ParkImageUrl;
  NSString *m_ParkStartTime;
  NSString *m_ParkCloseTime;
  NSString *m_ParkLatitude;
  NSString *m_ParkLongitude;
  int m_ParkOpen;
}

@property(nonatomic,retain) NSString *m_ParkId;
@property(nonatomic,retain) NSString *m_ParkName;
@property(nonatomic,retain) NSString *m_ParkImageUrl;
@property(nonatomic,retain) NSString *m_ParkStartTime;
@property(nonatomic,retain) NSString *m_ParkCloseTime;
@property(nonatomic,retain) NSString *m_ParkLatitude;
@property(nonatomic,retain) NSString *m_ParkLongitude;
@property(nonatomic) int m_ParkOpen;
@end
