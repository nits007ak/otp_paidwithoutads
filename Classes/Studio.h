//
//  Studio.h
//  AmusementPark
//
//  Created by HARSHA on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "Park.h"

@interface Studio : NSObject
{
  NSString *m_StudioId;
  NSString *m_StudioName;
  int independent;
  NSMutableArray *m_ParkArray;
}
@property(nonatomic,retain)NSString *m_StudioId;
@property(nonatomic,retain)NSString *m_StudioName;
@property(nonatomic,assign) NSMutableArray *m_ParkArray;
@property(nonatomic) int independent;
@end
