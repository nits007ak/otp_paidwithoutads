//
//  SortRideViewController.h
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmusementParkAppDelegate.h"


@protocol SortRideViewControllerDelegate<NSObject>

-(void)BackFromSortRideViewController:(int)selectedIndex;

@end


@interface SortRideViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UILabel *pTitleLabel;
	UITableView *m_TableView;
	NSMutableArray *m_TitleArray;
	UIButton *m_BackButton;
    AmusementParkAppDelegate * appDelegate;
	id<SortRideViewControllerDelegate> m_delegate;
	int m_SelectedIndex;
    UIImageView * m_BGImageView;
    
    BOOL isRide;
}
@property(nonatomic ,assign)id<SortRideViewControllerDelegate> m_delegate;
-(id)initWithBool:(BOOL)pRide;
@end
