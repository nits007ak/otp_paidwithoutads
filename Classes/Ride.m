//
//  Ride.m
//  AmusementPark
//
//  Created by HARSHA on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Ride.h"


@implementation Ride
@synthesize m_StudioId,m_ParkId,m_LandId,m_RideId,m_RideName,m_WaitTime,m_ImageId,m_Description,m_Latitude,m_Longitude,m_Distance,m_RideLength,m_FastPass,m_Age,m_Height;

-(id)init
{
  if(self=[super init])
  {
    m_StudioId=nil;
    m_ParkId=nil;
    m_LandId=nil;
    m_RideId=nil;
    m_RideName=nil;
    m_WaitTime=nil;
    m_ImageId=nil;
    m_Description=nil;
    m_Distance=0;
    m_Latitude=nil;
    m_Longitude=nil;
    m_RideLength=nil;
    m_FastPass=nil;
    m_Age=0;
    m_Height=0;
  }
  return self;
}

-(void)dealloc
{
    if(m_StudioId){[m_StudioId release];m_StudioId=nil;}
    if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
    if(m_LandId){[m_LandId release];m_LandId=nil;}
    if(m_RideId){[m_RideId release];m_RideId=nil;}
    if(m_RideName){[m_RideName release];m_RideName=nil;}
    if(m_WaitTime){[m_WaitTime release];m_WaitTime=nil;}
    if(m_ImageId){[m_ImageId release];m_ImageId=nil;}
    if(m_Description){[m_Description release];m_Description=nil;}
    if(m_Latitude){[m_Latitude release];m_Latitude=nil;}
    if(m_Longitude){[m_Longitude release];m_Longitude=nil;}
    if(m_RideLength){[m_RideLength release];m_RideLength=nil;}
    if(m_FastPass){[m_FastPass release];m_FastPass=nil;}
    [super dealloc];
}
@end
