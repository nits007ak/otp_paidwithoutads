//
//  BuyViewController.m
//  AmusementPark
//
//  Created by Kavitha Pujar on 09/01/12.
//  Copyright (c) 2012 Permeative Technologies Pvt Ltd. All rights reserved.
//

#import "BuyViewController.h"
#import "AmusementParkAppDelegate.h"
#import "InAppPurchaseObserver.h"
#import "AboutUsCell.h"
//#import "FlurryAnalytics.h"
#import "Flurry.h"


@interface BuyViewController()
//-(void)RefreshTableView;
//-(BOOL)checkForAll;
@end

@implementation BuyViewController
@synthesize KeepFreeButton;
@synthesize BGImage;
@synthesize m_delegate;
@synthesize HomeBtnBg;

static BuyViewController *_instance=nil;

+(BuyViewController*)sharedBuyViewController
{
  if(!_instance)
  {
    _instance=[[BuyViewController alloc]init];
  }
  return _instance;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
      
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(InAppPurchaseSucceeded) name:gInAppTransactionNotification object:nil];
      
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(InAppPurchaseCancelled) name:gInAppTransactionCancelNotification object:nil];  
      
      m_TableView=nil;
      
      tImageArray=[[NSMutableArray alloc]initWithObjects:@"AdsNormal",@"MapNormal",@"RidesNormal",@"TicketsNormal",nil];
      tSelectedImgArray=[[NSMutableArray alloc]initWithObjects:@"AdsPressed",@"MapPressed",@"RidesPressed",@"TicketsPressed",nil];
      
//      if ([[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
//      {
//        [tImageArray removeObject:@"AdsNormal"];
//        [tSelectedImgArray removeObject:@"AdsPressed"];
//      }
//      
//      if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
//      {
//        [tImageArray removeObject:@"MapNormal"];
//        [tSelectedImgArray removeObject:@"MapPressed"];
//      }
//
//      if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdDescription"])
//      {
//        [tImageArray removeObject:@"RidesNormal"];
//        [tSelectedImgArray removeObject:@"RidesPressed"];
//      }
//
//      if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"])
//      {
//        [tImageArray removeObject:@"TicketsNormal"];
//        [tSelectedImgArray removeObject:@"TicketsPressed"];
//      }
      
      Flag=0;

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
  if(m_TableView==nil)
    m_TableView=[[UITableView alloc]initWithFrame:CGRectMake(-5,50,330,300) style:UITableViewStyleGrouped];
    
    m_TableView.backgroundView = nil;
  m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.backgroundColor=[UIColor clearColor];
	m_TableView.scrollEnabled = NO;
  [self.view addSubview:m_TableView];
  
  UIImage* splashImage = [[UIImage alloc] initWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"ImagesCommon/Background.png"]];
    BGImage.autoresizingMask = UIViewAutoresizingFlexibleWidth;
  [BGImage setImage:splashImage];
  [splashImage release];
  
  UIImage* CancelBGImage = [[UIImage alloc] initWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"ImagesCommon/Rides/tabbar/tabbar.png"]];
   
  //[HomeBtnBg setImage:CancelBGImage];
  [CancelBGImage release];
  
  KeepFreeButton.frame=CGRectMake(5, 8, 53.5, 30);
  [KeepFreeButton setImage:[UIImage imageNamed:@"ImagesCommon/Rides/homebtn.png"] forState:UIControlStateNormal];
  [KeepFreeButton setImage:[UIImage imageNamed:@"ImagesCommon/Rides/homebtn.png"] forState:UIControlStateSelected];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self checkOrientation];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    [self layoutSubView];
}


- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
    
}



-(void)layoutSubView
{
    
    if (appDelegate.isPortrait) {
        
        m_TableView.frame = CGRectMake(-5,50,330,300);
        BGImage.frame = CGRectMake(0, 0, 320, 480);
        if ([[UIScreen  mainScreen]bounds].size.height == 568) {
            BGImage.frame = CGRectMake(0, 0, 320, 568);
        }
    }
    else
    {
        BGImage.frame = CGRectMake(0, 0, 480,320);
        m_TableView.frame = CGRectMake(-5,50,490,300);
        if ([[UIScreen mainScreen]bounds].size.height ==568) {
            m_TableView.frame = CGRectMake(-5,50,578,300);
            BGImage.frame = CGRectMake(0, 0, 568,320);
        }
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [tImageArray count];  
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 51;
}

//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//  //return KeepFreeButton;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//  return 100;
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  AboutUsCell *cell=nil;
  cell = (AboutUsCell *)[tableView dequeueReusableCellWithIdentifier:@"AboutUsCell"];
  if(cell==nil)
  {
    cell=[[[AboutUsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AboutUsCell"]autorelease];
    cell.accessoryType = UITableViewCellAccessoryNone;
  }
  cell.selectionStyle=UITableViewCellSelectionStyleGray;
  UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 49)];
  NSString *imagePath=[NSString stringWithFormat:@"ImagesCommon/UpGrade/%@",[tImageArray objectAtIndex:indexPath.row]];
  bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:imagePath ofType:@"png"]];
  
    
  UIImageView *selbgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 49)];
  NSString *imagePath2=[NSString stringWithFormat:@"ImagesCommon/UpGrade/%@",[tSelectedImgArray objectAtIndex:indexPath.row]];

  selbgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:imagePath2 ofType:@"png"]];
  
    if (!appDelegate.isPortrait) {
        bgImgView.frame = CGRectMake(0, 0, 476, 49);
        selbgImgView.frame = CGRectMake(0, 0, 476, 49);
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            bgImgView.frame = CGRectMake(0, 0, 564, 49);
             selbgImgView.frame = CGRectMake(0, 0, 564, 49);
        }
    }
    cell.backgroundView=bgImgView;
    [bgImgView release];
    cell.selectedBackgroundView=selbgImgView;
  [selbgImgView release]; 
  
  return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
  NSString *str=[tSelectedImgArray objectAtIndex:indexPath.row];

  if ([str compare:@"AdsPressed"]==0) 
  {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
    {
      NSString *productIdentifier=nil;
      if(IsDisneyWorldFreeWaitTimes)
        productIdentifier=DisneyUniversalInAppIdentifier;
      else if(IsUniversalStudioFreeWaitTimes)
        productIdentifier=UniversalStudioInAppIdentifier;
      [self MakeInAppPurchaseWithProductId:productIdentifier withTag:1];
        
        NSDictionary *Parameters= [NSDictionary dictionaryWithObjectsAndKeys:@"REMOVE ADS Purchased", @"BuyView", nil]; 
        [Flurry logEvent:@"BUY_EVENT" withParameters:Parameters];
        
    }
    else
    {
      UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have already purchased this item! Please select another item." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
      [Alert show];
      [Alert release];
      Alert=nil;
    }
    
  }
  
  else if([str compare:@"MapPressed"]==0)
  {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
    {
      NSString *productIdentifier=nil;
      if(IsDisneyWorldFreeWaitTimes)
        productIdentifier=DisneyMapsInAppIdentifier;
      else if(IsUniversalStudioFreeWaitTimes)
        productIdentifier=UniversalMapsInAppIdentifier;
        
        NSDictionary *Parameters= [NSDictionary dictionaryWithObjectsAndKeys:@"MAP Purchase", @"BuyView", nil]; 
       [Flurry logEvent:@"BUY_EVENT" withParameters:Parameters];
        

      [self MakeInAppPurchaseWithProductId:productIdentifier withTag:2];

    }
    else
    {
      UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have already purchased this item! Please select another item." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
      [Alert show];
      [Alert release];
      Alert=nil;
    }

    
  }
  else if([str compare:@"RidesPressed"]==0)
  {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"AdDescription"])
    {
      NSString *productIdentifier=nil;
      if(IsDisneyWorldFreeWaitTimes)
        productIdentifier=DisneyDescriptionInAppIdentifier;
      else if(IsUniversalStudioFreeWaitTimes)
        productIdentifier=UniversalDescriptionInAppIdentifier;
        
        NSDictionary *Parameters= [NSDictionary dictionaryWithObjectsAndKeys:@"RIDES Purchase", @"BuyView", nil]; 
        [Flurry logEvent:@"BUY_EVENT" withParameters:Parameters];
        
        [self MakeInAppPurchaseWithProductId:productIdentifier withTag:3];
        
    }
    else
    {
      UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have already purchased this item! Please select another item." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
      [Alert show];
      [Alert release];
      Alert=nil;
    }


  }
  
  else if([str compare:@"TicketsPressed"]==0)
  {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"])
    {
      NSString *productIdentifier=nil;
      if(IsDisneyWorldFreeWaitTimes)
        productIdentifier=DisneyTicketsInAppIdentifier;
      else if(IsUniversalStudioFreeWaitTimes)
        productIdentifier=UniversalTicketsInAppIdentifier;
        
        NSDictionary *Parameters= [NSDictionary dictionaryWithObjectsAndKeys:@"TICKETS Purchase", @"BuyView", nil]; 
        [Flurry logEvent:@"BUY_EVENT" withParameters:Parameters];

      [self MakeInAppPurchaseWithProductId:productIdentifier withTag:4];

    }
    else
    {
      UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have already purchased this item! Please select another item." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
      [Alert show];
      [Alert release];
      Alert=nil;
    }

  }
  
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)MakeInAppPurchaseWithProductId:(NSString *)AppIdentifier withTag:(int)Tag
{
  Flag=Tag;
  NSString *productIdentifier=nil;
  productIdentifier=AppIdentifier;
  
    if(productIdentifier)
     {
       SKPayment *payment = [SKPayment paymentWithProductIdentifier:productIdentifier];
       [[SKPaymentQueue defaultQueue] addPayment:payment];
       
       dimView=[[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
       dimView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
       [[UIApplication sharedApplication].keyWindow addSubview:dimView];
       [dimView release];
        
        UIActivityIndicatorView *busyIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [dimView addSubview:busyIndicator];
        busyIndicator.center=dimView.center;
        [busyIndicator release];
       [busyIndicator startAnimating];
     }
  
}

#pragma mark -- SKProductsRequestDelegate

- (void)InAppPurchaseSucceeded
{
  
    
  if (Flag==1)
  {
    Flag=0;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsInAppPurchaseDone"];
    [[NSNotificationCenter defaultCenter] postNotificationName:AdRemovedNotification object:nil];
    
    UIAlertView *SucceededAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This item has been purchased successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [SucceededAlert show];
    [SucceededAlert release];
    SucceededAlert=nil;

    
  }
  
  if (Flag==2)
  {
    Flag=0;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ADDMAP"];
    [[NSNotificationCenter defaultCenter] postNotificationName:MapAddedNotification object:nil];

    UIAlertView *SucceededAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This item has been purchased successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [SucceededAlert show];
    [SucceededAlert release];
    SucceededAlert=nil;

    
  }
  
  if (Flag==3)
  {
    Flag=0;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdDescription"];
    [[NSNotificationCenter defaultCenter] postNotificationName:AdDescriptionNotification object:nil];

    UIAlertView *SucceededAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This item has been purchased successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [SucceededAlert show];
    [SucceededAlert release];
    SucceededAlert=nil;

  }
  
  if (Flag==4)
  {
    Flag=0;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdTickets"];
    [[NSNotificationCenter defaultCenter] postNotificationName:AdTicketsNotification object:nil];
    
    UIAlertView *SucceededAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This item has been purchased successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [SucceededAlert show];
    [SucceededAlert release];
    SucceededAlert=nil;

    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"UsingPushNotification"])
    {
      AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
      [appDelegate IsInappPurchaseSucceded:YES];
    }
    
  }
  
  [m_delegate DonePurchasing];
  [m_delegate AddBigBanner];

 // [self RefreshTableView];

	if(dimView)
	{
		[dimView removeFromSuperview];
		dimView=nil;
	}
  
}
-(void)InAppPurchaseCancelled
{

  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"UsingPushNotification"])
  {
    AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate IsInappPurchaseSucceded:NO];
  }
  
//  if (Flag==1)
//  {
//    Flag=0;
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsInAppPurchaseDone"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:AdRemovedNotification object:nil];
//    
//    UIAlertView *SucceededAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This item has been purchased successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [SucceededAlert show];
//    [SucceededAlert release];
//    SucceededAlert=nil;
//
//    
//  }
//  
//  
//  if (Flag==2)
//  {
//    Flag=0;
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ADDMAP"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:MapAddedNotification object:nil];
//
//    UIAlertView *SucceededAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This item has been purchased successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [SucceededAlert show];
//    [SucceededAlert release];
//    SucceededAlert=nil;
//
//    
//  }
//  
//  if (Flag==3)
//  {
//    Flag=0;
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdDescription"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:AdDescriptionNotification object:nil];
//
//    UIAlertView *SucceededAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This item has been purchased successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [SucceededAlert show];
//    [SucceededAlert release];
//    SucceededAlert=nil;
//
//    
//  }
//  
//  if (Flag==4)
//  {
//    Flag=0;
//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdTickets"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:AdTicketsNotification object:nil];
//    
//    UIAlertView *SucceededAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"This item has been purchased successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [SucceededAlert show];
//    [SucceededAlert release];
//    SucceededAlert=nil;
//
//  }
//  
//  [m_delegate DonePurchasing];
//  [m_delegate AddBigBanner];
////
//  //[self RefreshTableView];
//  
 
  if(dimView)
	{
		[dimView removeFromSuperview];
		dimView=nil;
	}
}

-(IBAction)Cancel:(id)sender;

{
  AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
  [appDelegate.RootViewController dismissModalViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//-(void)RefreshTableView
//{
//  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
//  {
//    [tImageArray removeObject:@"AdsNormal"];
//    [tSelectedImgArray removeObject:@"AdsPressed"];
//  }
//  
//  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
//  {
//    [tImageArray removeObject:@"MapNormal"];
//    [tSelectedImgArray removeObject:@"MapPressed"];
//  }
//  
//  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdDescription"])
//  {
//    [tImageArray removeObject:@"RidesNormal"];
//    [tSelectedImgArray removeObject:@"RidesPressed"];
//  }
//  
//  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"])
//  {
//    [tImageArray removeObject:@"TicketsNormal"];
//    [tSelectedImgArray removeObject:@"TicketsPressed"];
//  }
//  
//  [m_TableView reloadData];
//
//}

//-(BOOL)checkForAll
//{
//  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"] && [[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"] && [[NSUserDefaults standardUserDefaults] boolForKey:@"AdDescription"] && [[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"])
//  {
//    return YES;
//  }
//  else
//    return NO;
//  
//}
-(void)dealloc
{
  Flag=0;
  if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
  if(tSelectedImgArray){[tSelectedImgArray removeAllObjects];[tSelectedImgArray release];tSelectedImgArray=nil;}
  if(tImageArray){[tImageArray removeAllObjects];[tImageArray release];tImageArray=nil;}

}

@end
