//
//  InAppPurchaseObserver.h
//  Octopus Paul
//
//  Created by Shriram on 27/07/09.
//


#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import <StoreKit/SKPaymentTransaction.h>

extern NSString* const gInAppTransactionNotification;
extern NSString* const gInAppTransactionCancelNotification;

@interface InAppPurchaseObserver : NSObject <SKPaymentTransactionObserver> 
{
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;


@end      
