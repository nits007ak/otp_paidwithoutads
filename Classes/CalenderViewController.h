//
//  CalenderViewControllersample.h
//
//  Created by HARSHA on 01/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmusementParkAppDelegate.h"


@protocol CalenderViewControllerDelegate

-(void)BackAction1:(NSString *)date;

@end

@interface CalenderViewController : UIViewController
{
	int x,y;
	NSMutableArray *m_ButtonArray;
	NSMutableArray *m_LabelsArray;
    NSMutableArray *m_BoxLabelArray;
	UIButton *_uiBtnWeekDay;
	UIButton *m_NextButton;
	UIButton *m_PrevButton;
	UIButton *m_monthButton;
	UIButton * m_BackButton;
	UIButton *m_DayButton;
	UIButton *m_YearButton;
	NSDate *m_Date;
	int iDay,iMonth,iYear;
	UIView *_mainView;
	UIImageView *bgImageView;
	UIImageView *CalBgView;
	UIImageView* _uiCalendarView;
	UIImageView *_uiHeaderView;
	UILabel *_uiLblMonthYear;
	UIInterfaceOrientation mOrientation;
	UILabel *m_TitleLabel;
	UILabel *m_DayLabel;
	UILabel *m_DateLabel;
    UILabel *pTitleLabel;
	UIImage *m_Image;
	UIButton *m_CurrentButton;
	UIView *m_PickerView;
	UIDatePicker *m_DatePicker;
	UINavigationBar *m_NavigBar;
	NSString *m_SelectedDate;
	BOOL isPortrait,IsSet;
    UIView * monthYearView;
	id<CalenderViewControllerDelegate> m_delegate;
    AmusementParkAppDelegate * appDelegate;
}
@property (nonatomic, retain)NSDate *m_Date;


- (void) createHeaderView;
- (void) setMonthAndYearValue:(NSDate*)date;
-(void)fillCalenderValues:(NSDate *)date;
@property(nonatomic,assign)id<CalenderViewControllerDelegate> m_delegate;
@end
