//
//  DetailViewController.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"
#import "DealsViewController.h"
#import "FoodViewController.h"
#import "RidesViewController.h"
#import "FavoriteViewController.h"
#import "FullVersionUpgrader.h"
#import "AdsManager.h"

@interface DetailViewController()
-(void)SetFrame;
-(BOOL)CheckForBothInAppPurchase;

@end

@implementation DetailViewController

-(id)initwithStudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId bundleName:(NSString *)pParkName
{
  if(self=[super init])
  {
    m_StudioId=pStudioId;
    [m_StudioId retain];
    m_ParkId=pParkId;
    [m_ParkId retain];
    m_ParkName=pParkName;
    [m_ParkName retain];
    m_pRidesViewController=nil;
    m_pFoodViewController=nil;
    m_pFavoriteViewController=nil;
    m_pDealsViewController=nil;
    m_MapImageViewController=nil;
    mapNavCntr=nil;
    rideNavCntr=nil;
    favNavCntr=nil;
    dealNavCntr=nil;
    foodNavCntr=nil;
    
    
    m_BGImageView=nil;
    m_Tabc=nil;
    m_NavigImage=nil;
    m_MapButton=nil;
    m_RidesButton=nil;
    m_FavButton=nil;
    m_FoodButton=nil;
    m_DealsButton=nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AddMap) name:MapAddedNotification object:nil];
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AdDescription) name:AdDescriptionNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AdTickets) name:AdTicketsNotification object:nil];

    
  }
	return self;
}


-(void)loadView
{
    
   // [[NSUserDefaults standardUserDefaults]setBool:1 forKey:@"AdTickets"];
    //   [[NSUserDefaults standardUserDefaults]synchronize]; //this is only for temprary cp
    ////
    //  [[NSUserDefaults standardUserDefaults]setBool:1 forKey:@"ADDMAP"];
      //[[NSUserDefaults standardUserDefaults]synchronize]; //this is only for temprary cp
    
    
    
    
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
    
	self.view.backgroundColor=[UIColor whiteColor];
    
    if (appDelegate.isIndependent)
    {
        m_pRidesViewController=[[RidesViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId bundleName:m_ParkName];
        m_pRidesViewController.m_delegate=self;
        rideNavCntr = [[UINavigationController alloc] initWithRootViewController:m_pRidesViewController];
        rideNavCntr.delegate=self;
        if(IsIOS5)
        {
            [rideNavCntr.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
            //UIImage * image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]];
            //[rideNavCntr.navigationBar setBarTintColor:[UIColor colorWithPatternImage:image]];
        }
        else
        {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
            imageView.frame=CGRectMake(0, 0, 320, 49);
            [imageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth )];
            [rideNavCntr.navigationBar insertSubview:imageView atIndex:0];
            [imageView release];
        }
        
        
        
        m_Tabc= [[UITabBarController alloc] init];
        NSArray *pnavigArray=[[NSArray alloc]initWithObjects:rideNavCntr,nil];
        [m_Tabc setViewControllers:pnavigArray];
        m_Tabc.selectedIndex=0;
        m_Tabc.delegate=self;
        [self.view addSubview:m_Tabc.view];
        [pnavigArray release];
        pnavigArray=nil;
    }
    else
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
        {
            if(m_MapImageViewController)
            {
                [m_MapImageViewController.view removeFromSuperview];
                m_MapImageViewController.m_delegate=nil;
                [m_MapImageViewController release];
                m_MapImageViewController=nil;
            }
            m_MapImageViewController=[[MapImageViewController alloc]initWithName:m_ParkName StudioId:m_StudioId ParkId:m_ParkId isRide:YES];//name pf the park to show
            m_MapImageViewController.m_delegate=self;
            mapNavCntr = [[UINavigationController alloc] initWithRootViewController:m_MapImageViewController];
            if(IsIOS5)
            {
                [mapNavCntr.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
            }
            else
            {
                UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
                
                imageView.frame=CGRectMake(0, 0, 320, 48.5);
                imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                [mapNavCntr.navigationBar insertSubview:imageView atIndex:0];
                [imageView release];
            }
            mapNavCntr.delegate=self;
            //[mapNavCntr.tabBarItem setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]]];
            //mapNavCntr.tabBarItem.title=@"Maps";
        }
        
        m_pRidesViewController=[[RidesViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId bundleName:m_ParkName];
        m_pRidesViewController.m_delegate=self;
        rideNavCntr = [[UINavigationController alloc] initWithRootViewController:m_pRidesViewController];
        rideNavCntr.delegate=self;
        if(IsIOS5)
        {
            [rideNavCntr.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
        }
        else
        {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
            imageView.frame=CGRectMake(0, 0, 320, 48.5);
            imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            [rideNavCntr.navigationBar insertSubview:imageView atIndex:0];
            [imageView release];
        }
        //[rideNavCntr.tabBarItem setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride2" ofType:@"png"]]];
        //rideNavCntr.tabBarItem.title=@"Rides";
        
        m_pDealsViewController=[[DealsViewController alloc]init];
        m_pDealsViewController.m_delegate=self;
        dealNavCntr = [[UINavigationController alloc] initWithRootViewController:m_pDealsViewController];
        dealNavCntr.delegate=self;
        if(IsIOS5)
        {
            [dealNavCntr.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
        }
        else
        {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
            imageView.frame=CGRectMake(0, 0, 320, 48.5);
            imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            [dealNavCntr.navigationBar insertSubview:imageView atIndex:0];
            [imageView release];
        }
        //	[dealNavCntr.tabBarItem setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]]];
        //	dealNavCntr.tabBarItem.title=@"Deals";
        
        m_pFoodViewController=[[FoodViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId];
        m_pFoodViewController.m_delegate=self;
        foodNavCntr = [[UINavigationController alloc] initWithRootViewController:m_pFoodViewController];
        foodNavCntr.delegate=self;
        if(IsIOS5)
        {
            [foodNavCntr.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
        }
        else
        {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
            imageView.frame=CGRectMake(0, 0, 320, 48.5);
            imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            [foodNavCntr.navigationBar insertSubview:imageView atIndex:0];
            [imageView release];
        }
        //[foodNavCntr.tabBarItem setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]]];
        //	foodNavCntr.tabBarItem.title=@"Food";
        
        
        m_Tabc= [[UITabBarController alloc] init];
        
        
        
        
        if([[NSUserDefaults standardUserDefaults]boolForKey:@"ADDMAP"])
        {
            NSArray *pnavigArray=[[NSArray alloc]initWithObjects:mapNavCntr,foodNavCntr,rideNavCntr,nil];
            [m_Tabc setViewControllers:pnavigArray];
            [pnavigArray release];
            pnavigArray=nil;
        }
        
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"AdTickets"])
        {
            NSArray *pnavigArray=[[NSArray alloc]initWithObjects:foodNavCntr,rideNavCntr,dealNavCntr,nil];
            [m_Tabc setViewControllers:pnavigArray];
            [pnavigArray release];
            pnavigArray=nil;
        }
        
        if([self CheckForBothInAppPurchase])
        {
            NSArray *pnavigArray=[[NSArray alloc]initWithObjects:mapNavCntr,foodNavCntr,rideNavCntr,dealNavCntr,nil];
            [m_Tabc setViewControllers:pnavigArray];
            [pnavigArray release];
            pnavigArray=nil;
            
        }
        if(![self CheckForBothInAppPurchase] && ![[NSUserDefaults standardUserDefaults]boolForKey:@"AdTickets"] && ![[NSUserDefaults standardUserDefaults]boolForKey:@"ADDMAP"])
        {
            NSArray *pnavigArray=[[NSArray alloc]initWithObjects:foodNavCntr,rideNavCntr,nil];
            [m_Tabc setViewControllers:pnavigArray];
            [pnavigArray release];
            pnavigArray=nil;
            
        }
        
        m_Tabc.selectedIndex=1;
        [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"INDEX"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"ADDMAP"]) {
            m_Tabc.selectedIndex=2;
            [[NSUserDefaults standardUserDefaults]setInteger:2 forKey:@"INDEX"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        if ([self CheckForBothInAppPurchase]) {
            m_Tabc.selectedIndex=2;
            [[NSUserDefaults standardUserDefaults]setInteger:2 forKey:@"INDEX"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        
        m_Tabc.delegate=self;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
        imageView.frame=CGRectMake(0, 0, 320, 49);
        [imageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth )];  //cp
        [m_Tabc.tabBar insertSubview:imageView atIndex:0];
        [imageView release];
        imageView=nil;
        [self.view addSubview:m_Tabc.view];
        
    }
    
	
	
 
    
    [self checkOrientation];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}
  


-(void)layoutSubView
{
    [self removeSubViews];
    [self addButtonSubViews];
    CGRect screenBounds = [[UIScreen mainScreen]bounds];
    if (appDelegate.isPortrait) {
        
        if (screenBounds.size.height == 568) {
            m_NavigImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 514, 320, 55)];//498
            
        } else {
            
            m_NavigImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 430, 320, 50)];
        }
    }
    else{
        if (screenBounds.size.height == 568) {
            m_NavigImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 271, 568, 50)];//498
            
        } else {
            
            m_NavigImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 271, 480, 50)];
        }
    }
    
    [self setFramesOfSubviews];
    
    int index = [[NSUserDefaults standardUserDefaults]integerForKey:@"INDEX"];
    
    m_Tabc.selectedIndex=index;
    
    if (appDelegate.isIndependent) {
        [self hideTabBar];
        [self hideTabBar:m_Tabc];
    }

    
}

-(void)hideTabBar
{
    CGRect frame = m_NavigImage.frame;
    frame.origin.y = 600;
    m_NavigImage.frame = frame;
}

- (void)hideTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 600, view.frame.size.width, view.frame.size.height)];
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
        }
    }
    
    [UIView commitAnimations];
}

-(void)removeSubViews
{
    if (m_NavigImage) {
        [m_NavigImage removeFromSuperview];
    }
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
    {
        
        if (m_MapButton) {
            [m_MapButton removeFromSuperview];
        }
    }
    if (m_FoodButton) {
        [m_FoodButton removeFromSuperview];
    }
    if (m_RidesButton) {
        [m_RidesButton removeFromSuperview];
    }
    
    if (([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"]))
    {
        if (m_DealsButton) {
            [m_DealsButton removeFromSuperview];
        }
    }
    
}

-(void)addButtonSubViews
{
    m_MapButton=[UIButton buttonWithType:UIButtonTypeCustom];
    m_FoodButton=[UIButton buttonWithType:UIButtonTypeCustom];
    m_RidesButton=[UIButton buttonWithType:UIButtonTypeCustom];
    // m_FavButton=[UIButton buttonWithType:UIButtonTypeCustom];
    m_DealsButton=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]] forState:UIControlStateNormal];
    [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
    [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
    // [m_FavButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/fav" ofType:@"png"]] forState:UIControlStateNormal];
    [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]] forState:UIControlStateNormal];
    
    int index=[[NSUserDefaults standardUserDefaults]integerForKey:@"INDEX"];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"] && (![[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"]))
    {
        switch (index) {
                
            case 0:
            {
                
                [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map1" ofType:@"png"]] forState:UIControlStateNormal];
                
                
            }
                
                break;
            case 1:
            {
                [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food1" ofType:@"png"]] forState:UIControlStateNormal];
            }
                break;
            case 2:
            {
                [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride1" ofType:@"png"]] forState:UIControlStateNormal];
            }
                break;
            default:
                break;
        }
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"] && (![[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"]))
    {
        switch (index) {
                
            case 0:
            {
                
                [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food1" ofType:@"png"]] forState:UIControlStateNormal];
                
                
            }
                
                break;
            case 1:
            {
                [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride1" ofType:@"png"]] forState:UIControlStateNormal];
            }
                break;
            case 2:
            {
                [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals1" ofType:@"png"]] forState:UIControlStateNormal];
            }
                break;
            default:
                break;
        }
        
    }
    else if([self CheckForBothInAppPurchase])
    {
        switch (index) {
                
            case 0:
            {
                
                
                [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map1" ofType:@"png"]] forState:UIControlStateNormal];
                
            }
                
                break;
            case 1:
            {
                [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food1" ofType:@"png"]] forState:UIControlStateNormal];
            }
                break;
            case 2:
            {
                [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride1" ofType:@"png"]] forState:UIControlStateNormal];
            }
                break;
            case 3:
            {
                [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals1" ofType:@"png"]] forState:UIControlStateNormal];
            }
            default:
                break;
        }
    }
    else
    {
        switch (index) {
                
            case 0:
            {
                [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food1" ofType:@"png"]] forState:UIControlStateNormal];
                
            }
                break;
            case 1:
            {
                [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride1" ofType:@"png"]] forState:UIControlStateNormal];
            }
                break;
                
            default:
                break;
        }
    }
    
    
    
}

-(void)setFramesOfSubviews
{
    if (appDelegate.isPortrait) {
        
        m_FoodButton.frame=CGRectMake(48, 2, 64,44);
        m_RidesButton.frame=CGRectMake(208, 2, 64,44);
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
        {
            m_MapButton.frame=CGRectMake(0, 2, 64,44);
            m_FoodButton.frame=CGRectMake(128, 2, 64,44);
            m_RidesButton.frame=CGRectMake(256, 2, 64,44);
        }
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"]) {
            m_FoodButton.frame=CGRectMake(0, 2, 64,44);
            m_RidesButton.frame=CGRectMake(128, 2, 64,44);
            m_DealsButton.frame=CGRectMake(256, 2, 64,44);
        }
        
        
        if([self CheckForBothInAppPurchase])
        {
            m_MapButton.frame=CGRectMake(0, 2, 64,44);
            m_FoodButton.frame=CGRectMake(84, 2, 64,44);
            m_RidesButton.frame=CGRectMake(170, 2, 64,44);
            m_DealsButton.frame=CGRectMake(256, 2, 64,44);
        }
        
    }
    else
    {
        m_FoodButton.frame=CGRectMake(88, 2, 64,44);
        m_RidesButton.frame=CGRectMake(328, 2, 64,44);
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
        {
            m_MapButton.frame=CGRectMake(48, 2, 64,44);
            m_FoodButton.frame=CGRectMake(208, 2, 64,44);
            m_RidesButton.frame=CGRectMake(364, 2, 64,44);
        }
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"]) {
            m_FoodButton.frame=CGRectMake(48, 2, 64,44);
            m_RidesButton.frame=CGRectMake(208, 2, 64,44);
            m_DealsButton.frame=CGRectMake(364, 2, 64,44);
        }
        if ([self CheckForBothInAppPurchase]) {
            m_MapButton.frame=CGRectMake(28, 2, 64,44);
            m_FoodButton.frame=CGRectMake(148, 2, 64,44);
            m_RidesButton.frame=CGRectMake(268, 2, 64,44);
            m_DealsButton.frame=CGRectMake(388, 2, 64,44);
        }
        
        CGRect screenBounds = [[UIScreen mainScreen]bounds];
        if (screenBounds.size.height == 568) {
            m_FoodButton.frame=CGRectMake(110, 2, 64,44);
            m_RidesButton.frame=CGRectMake(394, 2, 64,44);
            
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
            {
                m_MapButton.frame=CGRectMake(62, 2, 64,44);
                m_FoodButton.frame=CGRectMake(251, 2, 64,44);
                m_RidesButton.frame=CGRectMake(440, 2, 64,44);
            }
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"]) {
                m_FoodButton.frame=CGRectMake(62, 2, 64,44);
                m_RidesButton.frame=CGRectMake(251, 2, 64,44);
                m_DealsButton.frame=CGRectMake(440, 2, 64,44);
            }
            if ([self CheckForBothInAppPurchase]) {
                m_MapButton.frame=CGRectMake(39, 2, 64,44);
                m_FoodButton.frame=CGRectMake(181, 2, 64,44);
                m_RidesButton.frame=CGRectMake(323, 2, 64,44);
                m_DealsButton.frame=CGRectMake(465, 2, 64,44);
            }
            
        }
    }
    m_NavigImage.image = [UIImage imageNamed:@"ImagesCommon/Rides/tabbar/tabbar.png"];
    [m_NavigImage setAutoresizingMask:(UIViewAutoresizingFlexibleWidth )];  //cp
    m_NavigImage.userInteractionEnabled=NO;
    [self.view addSubview:m_NavigImage];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
    {
        [m_NavigImage addSubview:m_MapButton];
    }
    if (([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"]))
    {
        [m_NavigImage addSubview:m_DealsButton];
    }
    [m_NavigImage addSubview:m_FoodButton];
    [m_NavigImage addSubview:m_RidesButton];
    //[m_NavigImage addSubview:m_FavButton];
    
    
    
    
}
-(void)SetFrame
{
  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
  {
    m_MapButton.frame=CGRectMake(10, 2, 64,44);
    
    m_FoodButton.frame=CGRectMake(120, 2, 64,44);
    
    m_RidesButton.frame=CGRectMake(240, 2, 64,44);
    
    
  }
  if([self CheckForBothInAppPurchase])
  {
    m_MapButton.frame=CGRectMake(0, 2, 64,44);
    
    m_FoodButton.frame=CGRectMake(84, 2, 64,44);
    
    m_RidesButton.frame=CGRectMake(170, 2, 64,44);
    
    m_DealsButton.frame=CGRectMake(256, 2, 64,44);

    
  }


}

-(BOOL)CheckForBothInAppPurchase
{
  if([[NSUserDefaults standardUserDefaults]boolForKey:@"ADDMAP"] && [[NSUserDefaults standardUserDefaults]boolForKey:@"AdTickets"])
  {
    return YES;
  }
  
  else
    return NO;
  
}

-(void)setTabBarVisiblity:(BOOL)isHidden
{
  m_NavigImage.hidden=!isHidden;
  m_NavigImage.frame=CGRectMake(0, self.view.bounds.size.height-48.5, 320, 48.5);
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
   [viewController viewWillAppear:animated];
}
- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
  [viewController viewDidAppear:animated];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationName" object:nil];
    int selIndex=tabBarController.selectedIndex;
    int index=[[NSUserDefaults standardUserDefaults]integerForKey:@"INDEX"];
    [[NSUserDefaults standardUserDefaults]setInteger:selIndex forKey:@"INDEX"];
    if(index==selIndex)
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SelectedTwice" object:nil];
    
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"AdTickets"])
    {
        
        if(tabBarController.selectedIndex==0)
        {
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food1" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
            [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]] forState:UIControlStateNormal];
        }
        else if(tabBarController.selectedIndex==1)
        {
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride1" ofType:@"png"]] forState:UIControlStateNormal];
            [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]] forState:UIControlStateNormal];
        }
        else if(tabBarController.selectedIndex==2)
        {
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
            [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals1" ofType:@"png"]] forState:UIControlStateNormal];
        }
    }
    
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"ADDMAP"])
    {
        
        if(tabBarController.selectedIndex==0)
        {
            [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map1" ofType:@"png"]] forState:UIControlStateNormal];
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
        }
        else if(tabBarController.selectedIndex==1)
        {
            [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]] forState:UIControlStateNormal];
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food1" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
        }
        else if(tabBarController.selectedIndex==2)
        {
            [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]] forState:UIControlStateNormal];
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride1" ofType:@"png"]] forState:UIControlStateNormal];
        }
        
    }
    
    if([self CheckForBothInAppPurchase])
    {
        
        if(tabBarController.selectedIndex==0)
        {
            [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map1" ofType:@"png"]] forState:UIControlStateNormal];
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
            [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]] forState:UIControlStateNormal];
        }
        else if(tabBarController.selectedIndex==1)
        {
            [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]] forState:UIControlStateNormal];
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food1" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
            [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]] forState:UIControlStateNormal];
        }
        else if(tabBarController.selectedIndex==2)
        {
            [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]] forState:UIControlStateNormal];
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride1" ofType:@"png"]] forState:UIControlStateNormal];
            [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]] forState:UIControlStateNormal];
        }
        
        else if(tabBarController.selectedIndex==3)
        {
            [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]] forState:UIControlStateNormal];
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
            [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals1" ofType:@"png"]] forState:UIControlStateNormal];
        }
        
    }
    if(![self CheckForBothInAppPurchase] && ![[NSUserDefaults standardUserDefaults]boolForKey:@"ADDMAP"] && ![[NSUserDefaults standardUserDefaults]boolForKey:@"AdTickets"])
    {
        
        if(tabBarController.selectedIndex==0)
        {
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food1" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride" ofType:@"png"]] forState:UIControlStateNormal];
        }
        else if(tabBarController.selectedIndex==1)
        {
            [m_FoodButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/food" ofType:@"png"]] forState:UIControlStateNormal];
            [m_RidesButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/ride1" ofType:@"png"]] forState:UIControlStateNormal];
        }
        
    }
    
    [viewController viewDidAppear:YES];
    
}


-(void)AddMap
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ADDMAP"];
  
  if(m_MapImageViewController)
  {
    [m_MapImageViewController.view removeFromSuperview];
    m_MapImageViewController.m_delegate=nil;
    [m_MapImageViewController release];
    m_MapImageViewController=nil;
  }
  m_MapImageViewController=[[MapImageViewController alloc]initWithName:m_ParkName StudioId:m_StudioId ParkId:m_ParkId isRide:YES];//name pf the park to show
  m_MapImageViewController.m_delegate=self;
  mapNavCntr = [[UINavigationController alloc] initWithRootViewController:m_MapImageViewController];
  if(IsIOS5)
  {
    [mapNavCntr.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
  }
  else
  {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
    imageView.frame=CGRectMake(0, 0, 320, 48.5);
    [mapNavCntr.navigationBar insertSubview:imageView atIndex:0];
    [imageView release];
  }
  
  
  mapNavCntr.delegate=self;
  [mapNavCntr.tabBarItem setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]]];
  mapNavCntr.tabBarItem.title=@"Maps";  
  
  m_MapButton=[UIButton buttonWithType:UIButtonTypeCustom];
  m_MapButton.frame=CGRectMake(0, 2, 64,44);
  [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/map" ofType:@"png"]] forState:UIControlStateNormal];
  [m_NavigImage addSubview:m_MapButton];
  
  m_MapButton.frame=CGRectMake(10, 2, 64,44);
    
  m_FoodButton.frame=CGRectMake(120, 2, 64,44);
    
  m_RidesButton.frame=CGRectMake(240, 2, 64,44);
  
  NSArray *pnavigArray=[[NSArray alloc]initWithObjects:mapNavCntr,foodNavCntr,rideNavCntr,nil];
  [m_Tabc setViewControllers:pnavigArray];
  [pnavigArray release];
  pnavigArray=nil;
  
  m_Tabc.delegate=self;

  
  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"AdTickets"])
  {
    m_MapButton.frame=CGRectMake(0, 2, 64,44);
    
    m_FoodButton.frame=CGRectMake(84, 2, 64,44);
    
    m_RidesButton.frame=CGRectMake(170, 2, 64,44);
    
    m_DealsButton.frame=CGRectMake(256, 2, 64,44);
    
    NSArray *pnavigArray=[[NSArray alloc]initWithObjects:mapNavCntr,foodNavCntr,rideNavCntr,dealNavCntr,nil];
    [m_Tabc setViewControllers:pnavigArray];
    [pnavigArray release];
    pnavigArray=nil;
    
    m_Tabc.delegate=self;


  }
  
   
}

-(void)AdApns
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdApns"];
  
}

-(void)AdDescription
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdDescription"];
  
}

-(void)AdTickets
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdTickets"];
  
  if(m_pDealsViewController)
  {
    [m_pDealsViewController.view removeFromSuperview];
    m_pDealsViewController.m_delegate=nil;
    [m_pDealsViewController release];
    m_pDealsViewController=nil;
  }
  m_pDealsViewController=[[DealsViewController alloc]init];
	m_pDealsViewController.m_delegate=self;
	dealNavCntr = [[UINavigationController alloc] initWithRootViewController:m_pDealsViewController];
  dealNavCntr.delegate=self;
  m_pDealsViewController=[[DealsViewController alloc]init];
	m_pDealsViewController.m_delegate=self;
	dealNavCntr = [[UINavigationController alloc] initWithRootViewController:m_pDealsViewController];
  dealNavCntr.delegate=self;
  if(IsIOS5)
  {
    [dealNavCntr.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
  }
  else
  {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
    imageView.frame=CGRectMake(0, 0, 320, 48.5);
    [dealNavCntr.navigationBar insertSubview:imageView atIndex:0];
    [imageView release];
  }
	[dealNavCntr.tabBarItem setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]]];
	dealNavCntr.tabBarItem.title=@"Deals";
  
  m_DealsButton=[UIButton buttonWithType:UIButtonTypeCustom];
  m_DealsButton.frame=CGRectMake(256, 2, 64,44);
  [m_DealsButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/deals" ofType:@"png"]] forState:UIControlStateNormal];
    [m_NavigImage addSubview:m_DealsButton];  
  
  
  m_FoodButton.frame=CGRectMake(10, 2, 64,44);
  
  m_RidesButton.frame=CGRectMake(120, 2, 64,44);
  
  m_DealsButton.frame=CGRectMake(240, 2, 64,44);

  NSArray *pnavigArray=[[NSArray alloc]initWithObjects:foodNavCntr,rideNavCntr,dealNavCntr,nil];
  [m_Tabc setViewControllers:pnavigArray];
  [pnavigArray release];
  pnavigArray=nil;
  m_Tabc.delegate=self;
  
  if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ADDMAP"])
  {
    m_MapButton.frame=CGRectMake(0, 2, 64,44);
    
    m_FoodButton.frame=CGRectMake(84, 2, 64,44);
    
    m_RidesButton.frame=CGRectMake(170, 2, 64,44);
    
    m_DealsButton.frame=CGRectMake(256, 2, 64,44);
    
    NSArray *pnavigArray=[[NSArray alloc]initWithObjects:mapNavCntr,foodNavCntr,rideNavCntr,dealNavCntr,nil];
    [m_Tabc setViewControllers:pnavigArray];
    [pnavigArray release];
    pnavigArray=nil;
    
    m_Tabc.delegate=self;
  }
}
-(void)viewDidLoad
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void)gotoHome
{
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning 
{
  [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
  
  [super viewDidUnload];
}

- (void)dealloc
{
  printf("\nDetailVc dealloc\n");
  [mapNavCntr release];mapNavCntr=nil;
	[rideNavCntr release];rideNavCntr=nil;
	[favNavCntr release];favNavCntr=nil;
	[dealNavCntr release];dealNavCntr=nil;
	[foodNavCntr release];foodNavCntr=nil;
  
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_StudioId){[m_StudioId release];m_StudioId=nil;}
  if(m_ParkName){[m_ParkName release];m_ParkName=nil;}
  if(m_NavigImage){[m_NavigImage removeFromSuperview];[m_NavigImage release];m_NavigImage=nil;}
  if(m_Tabc){m_Tabc.delegate=nil;[m_Tabc.view removeFromSuperview];[m_Tabc release];m_Tabc=nil;}
  if(m_MapImageViewController){[m_MapImageViewController.view removeFromSuperview];[m_MapImageViewController release];m_MapImageViewController=nil;}
	if(m_pDealsViewController){[m_pDealsViewController.view removeFromSuperview];m_pDealsViewController.m_delegate=nil;[m_pDealsViewController release];m_pDealsViewController=nil;}
	if(m_pRidesViewController){[m_pRidesViewController.view removeFromSuperview];m_pRidesViewController.m_delegate=nil;[m_pRidesViewController release];m_pRidesViewController=nil;}
	if(m_pFavoriteViewController){[m_pFavoriteViewController.view removeFromSuperview];m_pFavoriteViewController.m_delegate=nil;[m_pFavoriteViewController release];m_pFavoriteViewController=nil;}
	if(m_pFoodViewController){[m_pFoodViewController.view removeFromSuperview];m_pFoodViewController.m_delegate=nil;[m_pFoodViewController release];m_pFoodViewController=nil;}
  [super dealloc];
}


@end
