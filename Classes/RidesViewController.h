//
//  RidesViewController.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortRideViewController.h"
#import "ParadeTimeViewController.h"
#import "RidesDetailViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "RidesDetailCell.h"
#import "SubmitWaitTimeViewController.h"
#import "AmusementParkAppDelegate.h"
#import "MapImageViewController.h"
#import "RestuarantDetailViewController.h"
#import "RestuarantDetailCell.h"
#import "Restuarant.h"


@protocol RidesViewControllerDelegate<NSObject>

-(void)gotoHome;

@end
@interface RidesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SortRideViewControllerDelegate,
													ParadeTimeViewControllerDelegate,RidesDetailViewControllerDelegate,CLLocationManagerDelegate,
                                  RidesDetailCellDelegate,SubmitWaitTimeViewControllerDelegate , MapImageViewControllerDelegate , RestuarantDetailViewControllerDelegate>
{
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    NSString *m_StudioId;
    NSString *m_ParkId;
    NSString *m_LandId;
    NSString *m_RideId;
    int m_RideDistance,m_SelectedSortType;
	UITableView *m_TableView;
	NSMutableArray *m_RidesArray;
    UILabel *m_DateLabel;
    UILabel *pTitleLabel;
	UIButton *m_Homebtn;
     NSString *m_ParkName;
	UIButton *m_Sortbutton;
     UIButton *m_MapButton;
	UIImageView *m_BGImageView;
    RestuarantDetailViewController *m_RestuarantDetailViewController;
    MapImageViewController *m_MapImageViewController;
    AmusementParkAppDelegate * appDelegate;
	SortRideViewController *m_SortRideViewController;
	RidesDetailViewController *m_RidesDetailViewController;
	ParadeTimeViewController *m_ParadeTimeViewController;
    SubmitWaitTimeViewController *m_SubmitWaitTimeViewController;
    UIActivityIndicatorView *m_activityIndicator;
    
    CLLocationManager *m_LocationManager;
    CLLocation *m_Location;
	id<RidesViewControllerDelegate> m_delegate;
    BOOL isFirstTime;
    UIImageView* DateBarImageView ;
    NSTimer *m_Timer;
    int AdOffset;
}
@property(nonatomic,assign) id<RidesViewControllerDelegate> m_delegate;
-(void)connectToServer;
-(void)SelectSortType:(int)index;
-(void)sortOnName;
-(void)sortOnWaitTime;
-(void)sortOnDistance;
-(void)stopUpDatingLocation;
-(void)CalculateDistanceButtonAction;
-(void)refreshData;
-(id)initwithStudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId bundleName:(NSString *)pParkName;
@end
