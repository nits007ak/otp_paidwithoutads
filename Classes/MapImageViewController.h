//
//  MapImageViewController.h
//  AmusementPark
//
//  Created by HARSHA on 01/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "RidesDetailViewController.h"
#import "RestuarantDetailViewController.h"
#import "AnnoataionView.h"
#import "FilterViewController.h"
#import "AmusementParkAppDelegate.h"

@protocol MapImageViewControllerDelegate

-(void)gotoHome;

@end


@interface MapImageViewController : UIViewController<UIScrollViewDelegate,CLLocationManagerDelegate,AnnoataionViewDelegate,RidesDetailViewControllerDelegate,RestuarantDetailViewControllerDelegate,FilterViewControllerDelegate,UIAlertViewDelegate>
{
  UIScrollView *m_ScrollView;
  UIImageView *m_ImageView;
  
  UILabel *pTitleLabel;
  UIButton *m_HomeButton;
  UIButton *m_FilterButton;
  
  NSURLConnection *mURLConnection;
  NSMutableData *mResponseData;
  UIActivityIndicatorView *m_activityIndicator;
  CLLocationManager *m_LocationManager;
  CLLocation *m_Location;

  NSString *m_parkName;
  NSString *m_ParkId;
  NSString *m_StudioId;  
  
  NSMutableArray *m_RestuarantArray;
  NSMutableArray *m_RidesArray;
  NSMutableArray *m_RestAnnotArray;
  NSMutableArray *m_RideAnnotArray;
  
  BOOL isRide,isRest,isFirst,isShowing,m_Increment;
  int isRestAnnot,isRideAnnot;

    AmusementParkAppDelegate * appDelegate;
  RidesDetailViewController *m_RidesDetailViewController;
  RestuarantDetailViewController *m_RestuarantDetailViewController;
  FilterViewController *m_FilterViewController;
  
  id<MapImageViewControllerDelegate> m_delegate;
    
  float m_Width,m_Height,m_ZoomScale;
  
  int m_Xcoordinate;
  int m_Ycoordinate;
  double m_Latitude,m_Longitude;
    UIImageView *m_UserLocationImageView;
  UITapGestureRecognizer *m_TapRecognizer;
  CGPoint oldCenter;
}

@property(nonatomic,assign) id<MapImageViewControllerDelegate> m_delegate;

- (id)initWithName:(NSString*)refName StudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId isRide:(BOOL)ride;
-(void)connectToServer;
-(void)calculateDistance;
-(void)CalculateUserDistance;
-(void)addRideAnnoataions;
-(void)addRestAnnotations;
-(void)getRidesArray;
-(void)getRestArray;
-(void)calculateUserLocation;
-(void)addIndRestAnnotations;
-(void)removeRideAnnotations;
-(void)removeRestAnnotations;
@end
