//
//  FBConnectionViewController.h
//  AmusementPark
//
//  Created by CraterZone on 28/11/13.
//
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>
#import "AmusementParkAppDelegate.h"

@interface FBConnectionViewController : UIViewController<FBLoginViewDelegate , UIAlertViewDelegate>
{
    
   IBOutlet UIButton *buttonPostStatus;
    IBOutlet UIButton *buttonPostPhoto;
    IBOutlet UIButton *buttonPickFriends;
    UIButton *buttonPickPlace;
    IBOutlet UILabel *labelFirstName;
    IBOutlet FBProfilePictureView *profilePic;
}
@property (strong, nonatomic) IBOutlet FBProfilePictureView *profilePic;
@property (nonatomic, retain) NSString *m_RideName;
@property (nonatomic, retain) NSString *m_ParkName;
@property (strong, nonatomic)  UILabel *labelFirstName;
@property (strong, nonatomic) id<FBGraphUser> loggedInUser;

- (IBAction)postStatusUpdateClick:(UIButton *)sender;
- (IBAction)postPhotoClick:(UIButton *)sender;
- (IBAction)pickFriendsClick:(UIButton *)sender;
- (IBAction)pickPlaceClick:(UIButton *)sender;



- (void)showAlert:(NSString *)message
           result:(id)result
            error:(NSError *)error;
@end
