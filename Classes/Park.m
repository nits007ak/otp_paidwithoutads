//
//  Park.m
//  AmusementPark
//
//  Created by HARSHA on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Park.h"

@implementation Park
@synthesize m_ParkId,m_ParkName,m_ParkImageUrl,m_ParkStartTime,m_ParkCloseTime,m_ParkLatitude,m_ParkLongitude,m_ParkOpen;
-(id)init
{
  if(self=[super init])
  {
    m_ParkId=nil;
    m_ParkName=nil;
    m_ParkImageUrl=nil;
    m_ParkStartTime=nil;
    m_ParkCloseTime=nil;
    m_ParkLatitude=nil;
    m_ParkLongitude=nil;
    m_ParkOpen=0;
  }
  return self;
}

-(void)dealloc
{
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_ParkName){[m_ParkName release];m_ParkName=nil;}
  if(m_ParkImageUrl){[m_ParkImageUrl release];m_ParkImageUrl=nil;}
  if(m_ParkStartTime){[m_ParkStartTime release];m_ParkStartTime=nil;}
  if(m_ParkCloseTime){[m_ParkCloseTime release];m_ParkCloseTime=nil;}
  if(m_ParkLatitude){[m_ParkLatitude release];m_ParkLatitude=nil;}
  if(m_ParkLongitude){[m_ParkLongitude release];m_ParkLongitude=nil;}
  [super dealloc];
}
@end
