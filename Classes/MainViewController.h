//
//  MainViewController.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageResourceDownloader.h"
#import "AboutUsViewController.h"
#import "ConnectionClass.h"
#import "AmusementParkAppDelegate.h"
#import <RevMobAds/RevMobAds.h>

@class DetailViewController;
@class IndependentRestViewController;

@interface MainViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,ImageResourceDownloaderDelegate,ConnectionClassDelegate>
{
	UIImageView *m_BGImageView;
    UIImageView *pTitleImageView;
	UITableView *m_TableView;
	NSMutableArray *m_StudioArray;
    AmusementParkAppDelegate * appDelegate;
	DetailViewController *m_DetailViewController;
    IndependentRestViewController *m_IndependentRestViewController;
	UIButton *m_TicketInfoButton;
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    BOOL isImage,isFirst;
    UIImageView *pImage1;
    ImageResourceDownloader *m_ImageResourceDownloader;
    UIActivityIndicatorView *m_activityIndicator;
    AboutUsViewController *m_AboutUsViewController;
    ConnectionClass *mConnectClass;
    BOOL connectionPresent;
    BOOL homePageDataLoaded;
    RevMobBannerView *ad;
}
-(void)connectToServer;
-(void)TicketInfoAction;
-(void)downloadImages;
-(void)removeVonnection;
-(NSString *)getBundleName:(NSString *)ParkName;
@end
