//
//  DealsViewController.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmusementParkAppDelegate.h"


@protocol DealsViewControllerDelegate

-(void)gotoHome;

@end

@interface DealsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>
{
  	id<DealsViewControllerDelegate> m_delegate;
    AmusementParkAppDelegate * appDelegate;
    UIButton *m_Homebtn;
    UILabel *pTitleLabel;
    UIActivityIndicatorView *m_activityIndicator;
    UITableView *m_TableView;
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    NSMutableArray *m_DealsArray;
  int Length;
}
@property(nonatomic,assign) id<DealsViewControllerDelegate> m_delegate;
-(void)connectToServer;
@end
