    //
//  CalenderViewControllersample.m
//  Diary Pro
//
//  Created by HARSHA on 01/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalenderViewController.h"


NSTimeInterval secondsPerDay1 = 24 * 60 * 60;

@implementation CalenderViewController
@synthesize m_delegate , m_Date;
-(id)init
{
if(self==[super init])
{
	x=50;
	y=100;
	m_ButtonArray=[[NSMutableArray alloc]init];
	m_LabelsArray=[[NSMutableArray alloc]init];
    m_BoxLabelArray=[[NSMutableArray alloc]init];
	m_NextButton=nil;
	m_PrevButton=nil;
	m_Date=nil;
	_mainView=nil;
	bgImageView=nil;
	CalBgView=nil;
	_uiCalendarView=nil;
	_uiHeaderView=nil;
	_uiLblMonthYear=nil;
	m_monthButton=nil;
	m_DayButton=nil;
    pTitleLabel=nil;
	m_CurrentButton=nil;
	IsSet=NO;
	m_BackButton=nil;
}
	return self;

}

- (void)dealloc
{
  if(m_SelectedDate){[m_SelectedDate release];m_SelectedDate=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
	if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
	if(m_ButtonArray){[m_ButtonArray removeAllObjects];[m_ButtonArray release];m_ButtonArray=nil;}
	if(m_LabelsArray){[m_LabelsArray removeAllObjects];[m_LabelsArray release];m_LabelsArray=nil;}
  if(m_BoxLabelArray){[m_BoxLabelArray removeAllObjects];[m_BoxLabelArray release];m_BoxLabelArray=nil;}
	if(_mainView){[_mainView removeFromSuperview];[_mainView release];_mainView=nil;}
	if(m_TitleLabel){[m_TitleLabel removeFromSuperview];[m_TitleLabel release];m_TitleLabel=nil;}
	if(m_DayLabel){[m_DayLabel removeFromSuperview];[m_DayLabel release];m_DayLabel=nil;}
	if(m_DateLabel){[m_DateLabel removeFromSuperview];[m_DateLabel release];m_DateLabel=nil;}
  if(m_Date){[m_Date release];m_Date=nil;}
	if(bgImageView){[bgImageView removeFromSuperview];[bgImageView release];bgImageView=nil;}
	if(CalBgView){[CalBgView removeFromSuperview];[CalBgView release];CalBgView=nil;}
	if(_uiCalendarView){[_uiCalendarView removeFromSuperview];[_uiCalendarView release];_uiCalendarView=nil;}
	if(_uiHeaderView){[_uiHeaderView removeFromSuperview];[_uiHeaderView release];_uiHeaderView=nil;}
	if(_uiLblMonthYear){[_uiLblMonthYear removeFromSuperview];[_uiLblMonthYear release];_uiLblMonthYear=nil;}
    [super dealloc];
}


-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	self.view.backgroundColor=[UIColor clearColor];
  [self.navigationItem setHidesBackButton:YES animated:NO];

  NSDate *date = [NSDate date];
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"YYYY-MM-dd"];
  m_SelectedDate = [dateFormat stringFromDate:date];  
  [m_SelectedDate retain];
  [dateFormat release];        
  
  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 8, 220, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=@"Pick a Date";
  
  if(m_BackButton)
  {[m_BackButton release];m_BackButton=nil;}
	m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
	[m_BackButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/backbtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	[m_BackButton retain];
	
    if(bgImageView==nil)
    {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 568) {
            bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 320,568)];
            // code for 4-inch screen
        } else {
            // code for 3.5-inch screen
            bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 320,480)];
        }
        
    }
  bgImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
    bgImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;  //cp
	[self.view addSubview:bgImageView];  
  
  UIImageView *pImageView=[[UIImageView alloc]init];
  pImageView.frame=CGRectMake(0, 5, 320, 59);
     pImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth; //cp
  pImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/CalenderHeader" ofType:@"png"]];
  [self.view addSubview:pImageView];
   
  [pImageView release];
  
	[self createHeaderView];
	[_uiCalendarView addSubview:_uiHeaderView];
	
	[_mainView addSubview:_uiCalendarView];
	
	[self.view addSubview:_mainView];

	m_Date=[NSDate date];
	[m_Date retain];
	[self setMonthAndYearValue:m_Date];
	[self fillCalenderValues:m_Date];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}

-(void)layoutSubView
{
    [self fillCalenderValues:self.m_Date];
    if (appDelegate.isPortrait) {
        pTitleLabel.frame  = CGRectMake(60, 8, 220, 30);
        m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
        monthYearView.frame = CGRectMake(0, 5, 320, 37);
    }
    else
    {
        pTitleLabel.frame  = CGRectMake(150, 3, 220, 30);
        m_BackButton.frame=CGRectMake(5, 3, 53.5, 30);
        monthYearView.frame = CGRectMake(100, 5, 320, 37);
        if ([[UIScreen mainScreen] bounds].size.height == 568) {
            monthYearView.frame = CGRectMake(140, 5, 320, 37);
            pTitleLabel.frame  = CGRectMake(185, 3, 220, 30);
        }
    }
    
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}
-(void)backAction
{
	if(m_delegate)
		[m_delegate BackAction1:m_SelectedDate];
	[m_BackButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self.navigationController.navigationBar.subviews count]>appDelegate.subviewCount) {
        
    }
    else{
        [self.navigationController.navigationBar addSubview:m_BackButton];
        [self.navigationController.navigationBar addSubview:pTitleLabel];
        [self.navigationItem setHidesBackButton:YES animated:NO];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}
- (void) createHeaderView 
{
    if (monthYearView) {
        monthYearView = nil;
    }
    monthYearView = [[UIView alloc]initWithFrame:CGRectMake(0, 5, 320, 37)];
    monthYearView.backgroundColor = [UIColor clearColor];
    
    if(_uiLblMonthYear==nil)
        _uiLblMonthYear = [[UILabel alloc ] initWithFrame:CGRectMake(75 ,0,164,36.5)];
	_uiLblMonthYear.textAlignment = UITextAlignmentCenter;
	_uiLblMonthYear.text = @"";
	_uiLblMonthYear.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    _uiLblMonthYear.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:17];
    _uiLblMonthYear.shadowColor=[UIColor colorWithRed:108.0/255 green:113.0/255 blue:116.0/255 alpha:1.0];
    _uiLblMonthYear.shadowOffset=CGSizeMake(0,-1);
	_uiLblMonthYear.backgroundColor = [UIColor clearColor];
	
    UIImageView *pPreviousImage=[[UIImageView alloc]initWithFrame:CGRectMake(50 , 14, 7, 11)];
    pPreviousImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/DatePage/leftArrow" ofType:@"png"]];
    [monthYearView addSubview:pPreviousImage];
    [pPreviousImage release];
    
	m_PrevButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_PrevButton.frame=CGRectMake(45 , 12, 25,25);
	[m_PrevButton addTarget:self action:@selector(PrevAction) forControlEvents:UIControlEventTouchUpInside];
    m_PrevButton.backgroundColor=[UIColor clearColor];
    
    UIImageView *pNextImage=[[UIImageView alloc]initWithFrame:CGRectMake(255,14, 7,11)];
    pNextImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/DatePage/rightArrow" ofType:@"png"]];
    [monthYearView addSubview:pNextImage];
    [pNextImage release];
	
	m_NextButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_NextButton.frame=CGRectMake(250,12,25,25);
	[m_NextButton addTarget:self action:@selector(NextAction) forControlEvents:UIControlEventTouchUpInside];
    m_NextButton.backgroundColor=[UIColor clearColor];
	
	[monthYearView addSubview:_uiLblMonthYear];
	[monthYearView addSubview:m_NextButton];
	[monthYearView addSubview:m_PrevButton];
    [self.view addSubview:monthYearView];
}

- (void) setMonthAndYearValue:(NSDate*)date 
{
	
	NSCalendar *_gregorian=[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *monthComponents = [_gregorian components:(NSMonthCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit ) fromDate:date];
	NSInteger year = [monthComponents year];
	NSInteger month = [monthComponents month];
	monthComponents = nil;
	
	switch (month) {
		case 1:
			_uiLblMonthYear.text = @"January";
			break;
			
		case 2:
			_uiLblMonthYear.text = @"February";
			break;
			
		case 3:
			_uiLblMonthYear.text = @"March";
			break;
			
		case 4:
			_uiLblMonthYear.text = @"April";
			break;
			
		case 5:
			_uiLblMonthYear.text = @"May";
			break;
			
		case 6:
			_uiLblMonthYear.text = @"June";
			break;
			
		case 7:
			_uiLblMonthYear.text = @"July";
			break;
			
		case 8:
			_uiLblMonthYear.text = @"August";
			break;
			
		case 9:
			_uiLblMonthYear.text = @"September";
			break;
			
		case 10:
			_uiLblMonthYear.text = @"October";
			break;
			
		case 11:
			_uiLblMonthYear.text = @"November";
			break;
			
		case 12:
			_uiLblMonthYear.text = @"December";
			break;
			
		default:
			break;
	}
	
	_uiLblMonthYear.text = [ [_uiLblMonthYear.text stringByAppendingString:@" "] stringByAppendingString:[[NSNumber numberWithInt:year] stringValue] ];
	[_gregorian release];
}


-(void)NextAction
{
	NSCalendar *calender=[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *datecomps=[calender components:(NSMonthCalendarUnit | NSYearCalendarUnit |  NSDayCalendarUnit ) fromDate:m_Date];
	
	NSInteger month=[datecomps month];
	NSInteger year=[datecomps year];
		
	NSDateComponents *firstDateComps = [[NSDateComponents alloc] init];
	if(month==12)
	{
		[firstDateComps setYear:year+1];
		[firstDateComps setMonth:1];
	}
	else
	{
		[firstDateComps setYear:year];
		[firstDateComps setMonth:month+1];
	}
	[firstDateComps setDay:1];
	if(m_Date){[m_Date release];m_Date=nil;}
	m_Date = [calender dateFromComponents:firstDateComps];
	[m_Date retain];
	[firstDateComps release];
	[calender release];
	[self setMonthAndYearValue:m_Date];
	[self fillCalenderValues:m_Date];
}

-(void)PrevAction
{
	NSCalendar *calender=[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *datecomps=[calender components:(NSMonthCalendarUnit | NSYearCalendarUnit |  NSDayCalendarUnit ) fromDate:m_Date];
	
	NSInteger month=[datecomps month];
	NSInteger year=[datecomps year];
		
	NSDateComponents *firstDateComps = [[NSDateComponents alloc] init];
	if(month==1)
	{
	[firstDateComps setYear:year-1];
	[firstDateComps setMonth:12];
	}
	else
	{
		[firstDateComps setYear:year];
		[firstDateComps setMonth:month-1];
	}
	
	[firstDateComps setDay:1];
	if(m_Date){[m_Date release];m_Date=nil;}
	m_Date = [calender dateFromComponents:firstDateComps];
	[m_Date retain];
	[firstDateComps release];
	[calender release];
	[self setMonthAndYearValue:m_Date];
	[self fillCalenderValues:m_Date];
}

-(void)fillCalenderValues:(NSDate *)date
{
	y=63;
    int buttonWith = 46.5;
    CGFloat imageWidth = 320;
    if (!appDelegate.isPortrait) {
        buttonWith = 68.5;
        imageWidth = 480;
        CGRect frame = [[UIScreen mainScreen] bounds];
        if (frame.size.height == 568) {
            buttonWith = 81.14;
            imageWidth = 568;
        }
        
        
    }
    
	if([m_ButtonArray count])
	{
        for(UIButton *button in m_ButtonArray)
            [button removeFromSuperview];
        [m_ButtonArray removeAllObjects];
	}
    
	for(NSDictionary *dict in m_LabelsArray)
	{
		UIImageView *image = [dict objectForKey:@"Image"];
		[image removeFromSuperview];
	}
	[m_LabelsArray removeAllObjects];
    
    if([m_BoxLabelArray count])
    {
        for(UIImageView *pBoxImgView in m_BoxLabelArray)
            [pBoxImgView removeFromSuperview];
    }
    [m_BoxLabelArray removeAllObjects];
    
    //	if(self.m_Date){[self.m_Date release];self.m_Date=nil;}
    //	self.m_Date=date;
    //	[self.m_Date retain];
    if (self.m_Date) {
        // self.m_Date = nil;
        
    }
    self.m_Date=date;
    // NSLog(@"date  -= %@",self.m_Date);
	NSCalendar *calender=[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *datecomps=[calender components:(NSMonthCalendarUnit | NSYearCalendarUnit |  NSDayCalendarUnit ) fromDate:self.m_Date];
    
	NSInteger month=[datecomps month];
	NSInteger year=[datecomps year];
	NSRange daysInMonth1= [calender rangeOfUnit: NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate: self.m_Date];
    
	NSDateComponents *firstDateComps = [[NSDateComponents alloc] init];
	[firstDateComps setYear:year];
	[firstDateComps setMonth:month];
	[firstDateComps setDay:1];
	
	NSDate *firstDate = [calender dateFromComponents:firstDateComps];
	NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
	int CurrentMonthDays=daysInMonth1.length;
	
	NSDateComponents *weekdayComponents =[calender components:NSWeekdayCalendarUnit fromDate:firstDate];
	
	NSInteger weekday = [weekdayComponents weekday];
	
	int PreviousmonthDays;
	if(weekday==1)
		PreviousmonthDays=0;
	else if(weekday==2)
		PreviousmonthDays=1;
	else if(weekday==3)
		PreviousmonthDays=2;
	else if(weekday==4)
		PreviousmonthDays=3;
	else if(weekday==5)
		PreviousmonthDays=4;
	else if(weekday==6)
		PreviousmonthDays=5;
	else if(weekday==7)
		PreviousmonthDays=6;
	
	NSDate *nextdate;
	if(PreviousmonthDays!=7)
	{
		NSDateComponents *nextDateComponents=[[NSDateComponents alloc]init];
		[nextDateComponents setMonth:month-1];
		[nextDateComponents setDay:1];
		[nextDateComponents setYear:year];
		NSDate *temp=[calender dateFromComponents:nextDateComponents];
		NSRange daysnextmonth = [calender rangeOfUnit: NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate: temp];
		[nextDateComponents setDay:daysnextmonth.length-PreviousmonthDays+1];
		nextdate=[calender dateFromComponents:nextDateComponents];
		NSDateFormatter *nextdateFormatter=[[NSDateFormatter alloc]init];
		[nextdateFormatter setDateFormat:@"EEEE"];
		[nextdateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
		[nextdateFormatter release];
		[nextDateComponents release];
        
	}
	
	[firstDateComps setDay:daysInMonth1.length];
	NSDate *lastdate = [calender dateFromComponents:firstDateComps];
	
	NSDateComponents *lastdayComponents =[calender components:NSWeekdayCalendarUnit fromDate:lastdate];
	
	NSInteger lastday = [lastdayComponents weekday];
	
	
	int NextMonthDays;
	if(lastday==1)
		NextMonthDays=6;
	else if(lastday==2)
		NextMonthDays=5;
	else if(lastday==3)
		NextMonthDays=4;
	else if(lastday==4)
		NextMonthDays=3;
	else if(lastday==5)
		NextMonthDays=2;
	else if(lastday==6)
		NextMonthDays=1;
	else if(lastday==7)
		NextMonthDays=0;
	
	[firstDateComps setDay:1];
	[firstDateComps setMonth:month-1];
	NSDate *PreviousMonthDate = [calender dateFromComponents:firstDateComps];
	NSRange daysInMonth2 = [calender rangeOfUnit: NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate: PreviousMonthDate];
	int lastMonthDays=daysInMonth2.length;
	
	float btnHeight, btnWidth;
	float horzSpace, vertSpace;
	
	horzSpace = 0;
	vertSpace = 0;
	btnHeight = 33;
	//btnWidth  = 46.5;
    int nextindex=0,index=0;
	int lastindex=lastMonthDays-PreviousmonthDays+1;
	int numberofrows;
	if(((NextMonthDays+PreviousmonthDays+CurrentMonthDays)%7)==0)
        numberofrows=(NextMonthDays+PreviousmonthDays+CurrentMonthDays)/7;
    else
        numberofrows=(NextMonthDays+PreviousmonthDays+CurrentMonthDays)/7+1;
	int buttonIndex=1;
	for(int week = 1 ; week <=numberofrows ; week++)
	{
		x =0;
		
        UIImageView *pImageView=[[UIImageView alloc]init];
        if(week==numberofrows)
        {
            pImageView.frame=CGRectMake(x,y,imageWidth, 42);
            pImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/DatePage/BottomRow" ofType:@"png"]];
            
        }
        else
        {
            pImageView.frame=CGRectMake(x,y,imageWidth, 33);
            pImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/DatePage/UpperRow" ofType:@"png"]];
            
        }
        
        [self.view addSubview:pImageView];
        [m_BoxLabelArray addObject:pImageView];
        [pImageView release];
        
		for(int weekDay = 1 ; weekDay <= 7 ; weekDay++)
		{
			_uiBtnWeekDay = [UIButton buttonWithType:UIButtonTypeCustom];
			_uiBtnWeekDay.frame = CGRectMake(x, y, buttonWith,33);
			_uiBtnWeekDay.tag = buttonIndex;
			[_uiBtnWeekDay setTitleColor:[UIColor colorWithRed:108.0/255 green:113.0/255 blue:116.0/255 alpha:1.0] forState:UIControlStateNormal];
            [_uiBtnWeekDay.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:13]];
			[_uiBtnWeekDay setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            
			[_uiBtnWeekDay addTarget:self action:@selector(uiBtnWeekDay_clicked:) forControlEvents:UIControlEventTouchUpInside];
			[self.view addSubview:_uiBtnWeekDay];
			x += horzSpace + buttonWith;
			if(buttonIndex<=PreviousmonthDays)
			{
				[_uiBtnWeekDay setTitle:[NSString stringWithFormat:@"%d",lastindex++] forState:UIControlStateNormal];
				_uiBtnWeekDay.enabled=NO;
				[_uiBtnWeekDay setAlpha:.2];
			}
			else if(buttonIndex>(PreviousmonthDays+CurrentMonthDays))
			{
				[_uiBtnWeekDay setTitle:[NSString stringWithFormat:@"%d",++index] forState:UIControlStateNormal];
				_uiBtnWeekDay.enabled=NO;
				[_uiBtnWeekDay setAlpha:.2];
			}
			else if(buttonIndex<=PreviousmonthDays+CurrentMonthDays)
			{
				[_uiBtnWeekDay setTitle:[NSString stringWithFormat:@"%d",++nextindex] forState:UIControlStateNormal];
				[_uiBtnWeekDay setTitleColor:[UIColor colorWithRed:108.0/255 green:113.0/255 blue:116.0/255 alpha:1.0] forState:UIControlStateNormal];
				_uiBtnWeekDay.enabled=YES;
				[_uiBtnWeekDay setAlpha:1.0];
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
                NSString *onlydate = [dateFormatter1 stringFromDate:nextdate];
                NSString *tdDate = [dateFormatter1 stringFromDate:[NSDate date]];
                [dateFormatter1 release];
                
                
                if([tdDate compare:onlydate] == NSOrderedSame)
                {
                    [_uiBtnWeekDay setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/DatePage/block2" ofType:@"png"]]  forState:UIControlStateNormal];
                }
                
                NSArray *pDateArray =[onlydate componentsSeparatedByString:@"-"];
                int month = [[pDateArray objectAtIndex:1] intValue];
                int year = [[pDateArray objectAtIndex:0] intValue];
				iMonth=month;
				iYear=year;
			}
            else{}
			[m_ButtonArray addObject:_uiBtnWeekDay];
			nextdate = [nextdate addTimeInterval:secondsPerDay1];
			buttonIndex++;
		}
		y += vertSpace + btnHeight;
	}
	[dateFormatter release];
	[firstDateComps release];
	[calender release];
}


#pragma mark -
- (void) uiBtnWeekDay_clicked: (id) sender
{	
	if(IsSet)
	{
		[m_CurrentButton setBackgroundImage:m_Image forState:UIControlStateNormal];
    [m_CurrentButton setTitleColor:[UIColor colorWithRed:108.0/255 green:113.0/255 blue:116.0/255 alpha:1.0] forState:UIControlStateNormal];
    m_CurrentButton.titleLabel.shadowColor=[UIColor clearColor];
		if(m_CurrentButton){[m_CurrentButton release];m_CurrentButton=nil;}
		if(m_Image){[m_Image release];m_Image=nil;}
	}
	IsSet=YES;
	UIButton* btnTemp = (UIButton*)sender;
	iDay = [btnTemp.currentTitle intValue];
  if(m_SelectedDate)
  {
    [m_SelectedDate release];
    m_SelectedDate=nil;
  }
	m_SelectedDate=[NSString stringWithFormat:@"%d-%02d-%02d",iYear,iMonth,iDay];
  [m_SelectedDate retain];
    //printf("%s\n",[m_SelectedDate UTF8String]);
	NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
	[dateFormatter1 setDateFormat:@"dd-MM-yyyy"];
	NSString *dateSelected = [dateFormatter1 stringFromDate:[NSDate date]];
	if([m_SelectedDate compare:dateSelected] == NSOrderedSame)
		m_Image=[btnTemp imageForState:UIControlStateSelected];
    else
	    m_Image=[btnTemp imageForState:UIControlStateNormal];

	//printf("%s",[dateSelected UTF8String]);

	m_CurrentButton=btnTemp;
	[m_CurrentButton retain];
	m_Image=[btnTemp imageForState:UIControlStateNormal];
	[m_Image retain];
  [btnTemp setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
	[btnTemp setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/DatePage/block3" ofType:@"png"]] forState:UIControlStateNormal];
  btnTemp.titleLabel.shadowColor=[UIColor blackColor];
  btnTemp.titleLabel.shadowOffset=CGSizeMake(0,-1);
  [dateFormatter1 release];dateFormatter1=nil;
    
    if(m_delegate)
		[m_delegate BackAction1:m_SelectedDate];
	[m_BackButton removeFromSuperview];
    [pTitleLabel removeFromSuperview];
	[self.navigationController popViewControllerAnimated:YES];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}


@end
