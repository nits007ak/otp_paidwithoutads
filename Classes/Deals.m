
//
//  Deals.m
//  AmusementPark
//
//  Created by HARSHA on 26/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Deals.h"


@implementation Deals
@synthesize m_Title,m_Desc;

-(id)init
{
  if(self=[super init])
  {
    m_Desc=nil;
    m_Title=nil;
  }
  return self;
}

-(void)dealloc
{
  if(m_Title){[m_Title release];m_Title=nil;}
  if(m_Desc){[m_Desc release];m_Desc=nil;}
  [super dealloc];
}

@end
