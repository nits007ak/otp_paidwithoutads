//
//  TicketPopupView.h
//  AmusementPark
//
//  Created by Amresh Kumar on 17/01/12.
//  Copyright (c) 2012 Permeative Technologies Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketPopupView : UIView<UIWebViewDelegate>
{
  UIWebView *webView;
}
-(id)initWithLink:(NSString*)webLink;
-(void)show;
@end
