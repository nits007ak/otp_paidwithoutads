//
//  FBConnectController.h
//  FaceBookApp
//
//  Created by Mallikarjun Patil on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect/FBConnect.h"

@protocol FBConnectControllerDelegate<NSObject>

-(void)showLogOutButton;
-(void)hideLogOutButton;

@end



@interface FBConnectController :  NSObject <FBSessionDelegate, FBRequestDelegate> 
{
	FBSession* _session;
	FBLoginDialog *_loginDialog;
	NSString *_facebookName;
	BOOL _posting;
	id <FBConnectControllerDelegate> delegate;
	
  NSString *m_RideName;
  NSString *m_ParkName;
  BOOL ride;
}
+ (FBConnectController*)sharedFBConnectController;

@property (nonatomic, retain) FBSession *session;
@property (nonatomic, retain) FBLoginDialog *loginDialog;
@property (nonatomic, copy) NSString *facebookName;
@property (nonatomic, assign) BOOL posting;
@property (nonatomic,retain)id <FBConnectControllerDelegate> delegate;

- (void)showFaceBookPageWithRide:(NSString *)pRideName Park:(NSString *)pParkName isRide:(BOOL)isRide;
- (void)logoutButtonTapped;
- (void)postToWall;
- (void)getFacebookName;


@end
