//
//  Favourite.h
//  AmusementPark
//
//  Created by HARSHA on 08/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Favourite : NSObject
{
  NSString *m_LandName;
  NSString *m_LandId;
  NSMutableArray *m_RideArray;
  NSMutableArray *m_RestuarantArray;
  NSMutableArray *m_CombinedArray;
}
@property(nonatomic,retain) NSString *m_LandName;
@property(nonatomic,retain) NSString *m_LandId;
@property(nonatomic,assign) NSMutableArray *m_RideArray;
@property(nonatomic,assign) NSMutableArray *m_RestuarantArray;
@property(nonatomic,assign) NSMutableArray *m_CombinedArray;
@end
