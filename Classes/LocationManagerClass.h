//
//  LocationManagerClass.h
//  AmusementPark
//
//  Created by HARSHA on 07/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface LocationManagerClass : NSObject<CLLocationManagerDelegate>
{
  CLLocationManager *m_LocationManager;
  CLLocation *m_Location;
}

@end
