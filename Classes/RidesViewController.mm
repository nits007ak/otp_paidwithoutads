//
//  RidesViewController.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RidesViewController.h"
#import "SortRideViewController.h"
#import "XMLDataParser.h"
#import "Land.h"
#import "Ride.h"
#import "AdsManager.h"
#import "SubmitWaitTimeViewController.h"


@implementation RidesViewController
@synthesize m_delegate;
-(id)initwithStudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId bundleName:(NSString *)pParkName;
{
	if(self=[super init])
	{
         m_ParkName=pParkName;
    m_SelectedSortType=0;
    m_StudioId=pStudioId;
    [m_StudioId retain];
    m_ParkId=pParkId;
    [m_ParkId retain];
    m_LandId=nil;
    m_RideId=nil;
    m_Location=nil;
    m_LocationManager=nil;
    mURLConnection=nil;
    mResponseData=nil;
    m_activityIndicator=nil;
		m_SortRideViewController=nil;
		m_RidesDetailViewController=nil;
		m_ParadeTimeViewController=nil;
    m_SubmitWaitTimeViewController=nil;
		m_TableView=nil;
		m_RidesArray=nil;
		m_DateLabel=nil;
		m_Homebtn=nil;
		m_Sortbutton=nil;
    pTitleLabel=nil;
		m_BGImageView=nil;
    isFirstTime=YES;
    
    m_Timer=nil;
    
		m_RidesArray=[[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRecieved) name:@"SelectedTwice" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdsRemoved) name:AdRemovedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AddMap) name:MapAddedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AdDescription) name:AdDescriptionNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AdTickets) name:AdTicketsNotification object:nil];

    
	}
	return self;
}


-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
//    if (screenBounds.size.height == 568) {
//        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
//        // code for 4-inch screen
//    } else  {
//        // code for 3.5-inch screen
//        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
//    }

    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    }else if (screenBounds.size.height == 480){
            self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }else if (screenBounds.size.height == 1024){
    
                    self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)] autorelease];
    }
    
  
  //  UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ImagesCommon/Rides/tabbar/tabbar.png"]];
  //	imageView.frame=CGRectMake(0, 0, 320, 48.5);
  //	[self.navigationController.navigationBar insertSubview:imageView atIndex:0];
  //	[imageView release];
  
    
    if(m_BGImageView==nil)
    {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
//        if (screenBounds.size.height == 568) {
//            m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 320,568)];
//            // code for 4-inch screen
//        } else {
//            // code for 3.5-inch screen
//            m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 320,480)];
//        }

        if (screenBounds.size.height == 568) {
            m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 320,568)];
            // code for 4-inch screen
        }else if (screenBounds.size.height == 480){
             m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 320,480)];
        }else if (screenBounds.size.height == 1024){
             m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 768,1024)];
            
        }

        
        
    }

  m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];
	

   
        m_MapButton=[UIButton buttonWithType:UIButtonTypeCustom];
        m_MapButton.frame=CGRectMake(255, 8, 54, 30);
        [m_MapButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/FoodPage/mapicon" ofType:@"png"]] forState:UIControlStateNormal];
        [m_MapButton addTarget:self action:@selector(mapButtonAction) forControlEvents:UIControlEventTouchUpInside];
         [self.navigationController.navigationBar addSubview:m_MapButton];
        
    
  if(m_Homebtn==nil)
  {
    m_Homebtn=[UIButton buttonWithType:UIButtonTypeCustom];
    m_Homebtn.frame=CGRectMake(5, 8, 53.5, 30);
    [m_Homebtn setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/homebtn" ofType:@"png"]] forState:UIControlStateNormal];
    [m_Homebtn addTarget:self action:@selector(backtoHomeAction) forControlEvents:UIControlEventTouchUpInside];
    [m_Homebtn retain];
  }
  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 8, 200, 30)];    pTitleLabel.frame=CGRectMake(60, 8, 254, 30);
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
      [pTitleLabel setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight)];
  pTitleLabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
  
  NSDate *date = [NSDate date];
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"EEEE - MMMM d, YYYY"];
  NSString *dateString = [dateFormat stringFromDate:date];  
  [dateFormat release];    
  
  AdOffset=0;
    AdOffset=49;
  
  int FullVersionBannerOffset=0;
  FullVersionBannerOffset=30;
  
  if([[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
  {
    FullVersionBannerOffset=0;
    AdOffset=30;

  }
    DateBarImageView= [[UIImageView alloc] initWithFrame:CGRectMake(0, 5+AdOffset, 320, 27)];
    DateBarImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/datebar" ofType:@"png"]];
    [m_DateLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];  //cp
    [self.view addSubview:DateBarImageView];
    
    if(m_DateLabel==nil)
        m_DateLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 3+AdOffset, 320, 27)];
	m_DateLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
	m_DateLabel.text=dateString;
	m_DateLabel.backgroundColor=[UIColor clearColor];
    m_DateLabel.textColor=[UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
	m_DateLabel.textAlignment=UITextAlignmentCenter;
    m_DateLabel.shadowColor = [UIColor whiteColor];
    m_DateLabel.shadowOffset = CGSizeMake(0, 1.0);
    [m_DateLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];  //cp
	[self.view addSubview:m_DateLabel];
	
    if(m_TableView==nil)
    {
        if (screenBounds.size.height == 568) {
            m_TableView=[[UITableView alloc]initWithFrame:CGRectMake(-5,40+AdOffset+FullVersionBannerOffset,330,410-AdOffset) style:UITableViewStyleGrouped];
            // code for 4-inch screen
        } else {
            // code for 3.5-inch screen
            m_TableView=[[UITableView alloc]initWithFrame:CGRectMake(-5,40+AdOffset+FullVersionBannerOffset,330,325-AdOffset-FullVersionBannerOffset) style:UITableViewStyleGrouped];
        }
        
    }
    m_TableView.backgroundView=nil;
    
  
	m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.backgroundColor=[UIColor clearColor];
	m_TableView.scrollEnabled = YES;
    [m_TableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	[self.view addSubview:m_TableView];
  
  if(m_activityIndicator==nil)
    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
  [m_activityIndicator startAnimating];
  m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    [m_activityIndicator setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin)];
  [self.view addSubview:m_activityIndicator];  
  [self connectToServer];
       
   
[self checkOrientation];
[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubViews];
}


- (void) didRotate:(NSNotification *)notification{
    [self checkOrientation];
    
    
}
-(void)layoutSubViews  //cp
{
    [m_TableView reloadData];
    CGRect screenBounds = [[UIScreen mainScreen]bounds];
    if (appDelegate.isPortrait) {
        pTitleLabel.frame=CGRectMake(60, 8, 254, 30);
        m_Homebtn.frame=CGRectMake(5, 8, 53.5, 30);
        DateBarImageView.frame = CGRectMake(0, 5+AdOffset, 320, 27);
        m_BGImageView.frame = CGRectMake(0, 0, 320, 480);
        m_MapButton.frame = CGRectMake(260, 8, 53.5, 30);
        [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,320,50)];
         [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 80)];
        if(screenBounds.size.height == 568)m_BGImageView.frame = CGRectMake(0, 0, 320, 568);
    }
    else
    {
        m_MapButton.frame = CGRectMake(425,1, 53.5, 30);
        pTitleLabel.frame = CGRectMake(60, 1, 370, 30);
        m_Homebtn.frame=CGRectMake(5, 3, 53.5, 30);
        m_BGImageView.frame = CGRectMake(0, 0, 480, 320);
        DateBarImageView.frame = CGRectMake(0, 5+AdOffset, 480, 27);
        [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,480,50)];
         [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(373, 80)];
        if(screenBounds.size.height == 568)
        {
            m_MapButton.frame = CGRectMake(512,1, 53.5, 30);
            pTitleLabel.frame = CGRectMake(60, 1, 500, 30);
            m_BGImageView.frame = CGRectMake(0, 0, 568, 320);
            DateBarImageView.frame = CGRectMake(0, 5+AdOffset, 568,27);
            [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,568,50)];
             [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(461, 80)];
        }
    }
}
- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}

-(void)mapButtonAction
{
    if(m_Timer)
    {
        [m_Timer invalidate];
        //[self.m_Timer release];
        m_Timer=nil;
    }
    
    [m_Homebtn removeFromSuperview];
    [m_MapButton removeFromSuperview];
    [pTitleLabel removeFromSuperview];
    if(m_MapImageViewController)
    {
        [m_MapImageViewController.view removeFromSuperview];
        //   [m_MapImageViewController release];
        m_MapImageViewController=nil;
    }
    m_MapImageViewController=[[MapImageViewController alloc]initWithName:m_ParkName StudioId:m_StudioId ParkId:m_ParkId isRide:NO];
    m_MapImageViewController.m_delegate=self;
    [self.navigationController pushViewController:m_MapImageViewController animated:YES];
}

-(void)AdsRemoved
{
  int AdOffsetlocal=30;
  m_TableView.frame=CGRectMake(-5,40+AdOffsetlocal,330,325-AdOffsetlocal);
  DateBarImageView.frame=CGRectMake(0, 5+AdOffsetlocal, 320, 27);
  m_DateLabel.frame=CGRectMake(0, 3+AdOffsetlocal, 320, 27);
  [[AdsManager sharedAdsManager]RemoveAdBannerAndFullVersionBanner];
  [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,4,320,50)];
  [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 80)];

}

-(void)AddMap
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ADDMAP"];
}

-(void)AdTickets
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdTickets"];
  
}

-(void)AdDescription
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdDescription"];
  
}



-(void)NotificationRecieved
{
  int index=[[NSUserDefaults standardUserDefaults]integerForKey:@"INDEX"];
  if(index==1)
    {
      while ([self.navigationController.navigationBar.subviews count] > 2)
      {
        int n=[self.navigationController.navigationBar.subviews count]-1;
        
        [[[self.navigationController.navigationBar subviews] objectAtIndex:n] removeFromSuperview];
      }
      [self.navigationController.navigationBar addSubview:m_Homebtn];
      [self.navigationController.navigationBar addSubview:pTitleLabel]; 
    }
}

-(void)sortButtonAction
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  if(m_SortRideViewController)
  {
	  [m_SortRideViewController.view removeFromSuperview];
    m_SortRideViewController.m_delegate=nil;
	  [m_SortRideViewController release];
	  m_SortRideViewController=nil;
  }
	[m_Homebtn removeFromSuperview];
	[m_Sortbutton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	m_SortRideViewController=[[SortRideViewController alloc]initWithBool:YES];
	m_SortRideViewController.m_delegate=self;
	[self.navigationController pushViewController:m_SortRideViewController animated:YES];
  
}

-(void)refreshData
{
  [self BackAction];
}

-(void)BackAction
{
    m_TableView.userInteractionEnabled=NO;
    [self connectToServer];
    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    if (!appDelegate.isPortrait) {
        m_activityIndicator.frame = CGRectMake(225, 200, 30, 30);
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            m_activityIndicator.frame = CGRectMake(269, 200, 30, 30);
        }
    }
    [m_activityIndicator startAnimating];
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    //   [m_activityIndicator setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin)];
    [self.view addSubview:m_activityIndicator];
}

-(void)BackFromSortRideViewController:(int)selectedIndex
{
  [self SelectSortType:selectedIndex];
}

-(void)backtoHomeAction
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  if(m_delegate && [m_delegate respondsToSelector:@selector(gotoHome)])
    [m_delegate gotoHome];
}

#pragma mark URLConnectionManager Methods

-(void)connectToServer
{
  m_TableView.userInteractionEnabled=NO;
  m_Homebtn.userInteractionEnabled=NO;
  m_Sortbutton.userInteractionEnabled=NO;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    NSString * urlStr;
    if (appDelegate.isIndependent) {
        urlStr = [NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getindrest&parkid=%@",m_ParkId];
    }
    else
    {
        urlStr = [NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getride&parkid=%@",m_ParkId];
    }
	[request setURL:[NSURL URLWithString:urlStr]];
	[request setHTTPMethod:@"POST"];
	mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  m_TableView.userInteractionEnabled=YES;
  m_Homebtn.userInteractionEnabled=YES;
  m_Sortbutton.userInteractionEnabled=YES;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }   
  
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
      AmusementParkAppDelegate *appD=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
      [appD showAlertView];
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  m_TableView.userInteractionEnabled=YES;
  m_Homebtn.userInteractionEnabled=YES;
  m_Sortbutton.userInteractionEnabled=YES;
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }   
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
        if (appDelegate.isIndependent) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *filePath = [paths objectAtIndex:0];
            paths=nil;
            filePath = [filePath stringByAppendingPathComponent:@"IndependentRestuarants.xml"];
            [mResponseData writeToFile:filePath atomically:YES];
            filePath=nil;
            //[mResponseData release];
            mResponseData=nil;
            if([m_RidesArray count]){[m_RidesArray removeAllObjects];}
            [m_RidesArray addObjectsFromArray:[XMLDataParser getIndependentRestuarantsFromXML]];
            if([m_RidesArray count])
            {
                [self CalculateDistanceButtonAction];
            }
        }
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *filePath = [paths objectAtIndex:0];
            paths=nil;
            filePath = [filePath stringByAppendingPathComponent:@"ParkRides.xml"];
            [mResponseData writeToFile:filePath atomically:YES];
            filePath=nil;
            [mResponseData release];
            mResponseData=nil;
            if([m_RidesArray count]){[m_RidesArray removeAllObjects];}
            [m_RidesArray addObjectsFromArray:[XMLDataParser getRidesFromXML]];
            m_TableView.userInteractionEnabled=YES;
            if([m_RidesArray count])
            {
                [self CalculateDistanceButtonAction];
            }
        }
		
	}
    
    [m_TableView reloadData];
}	

-(void)SelectSortType:(int)index
{
  if(index==0)
  {
    [self sortOnDistance];
    m_SelectedSortType=0;
  }
  else if(index==1)
  {
    [self sortOnName];
    m_SelectedSortType=1;
  }
  else if(index==2)
  {
    [self sortOnWaitTime];
    m_SelectedSortType=2;
  }
}

-(void)CalculateDistanceButtonAction
{
  if(m_LocationManager){[m_LocationManager stopUpdatingHeading];m_LocationManager.delegate=nil;[m_LocationManager release];m_LocationManager=nil;isFirstTime=YES;}
  m_LocationManager = [[CLLocationManager alloc] init];
  m_LocationManager.delegate = self;
  m_LocationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
  m_LocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
  [m_LocationManager startUpdatingLocation];  
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation 
{
  if(isFirstTime)
  {
    isFirstTime=NO;
    if(m_Location){[m_Location release];m_Location=nil;}
    m_Location=m_LocationManager.location;
    [m_Location retain];
    [self stopUpDatingLocation];
  }
  else 
  {
    CLLocationDistance distance=[newLocation distanceFromLocation:m_Location];
    NSNumber *d=[NSNumber numberWithDouble:distance];
    int dist=[d intValue];
    if(dist>=0.0)
    {
      if(m_Location){[m_Location release];m_Location=nil;}
      m_Location=m_LocationManager.location;
      [m_Location retain];
      [self stopUpDatingLocation];      
    }
  }
}

-(void)stopUpDatingLocation
{
    if (appDelegate.isIndependent) {
        for(int i=0;i<[m_RidesArray count];i++)
        {
            Restuarant *pRestuarant=[m_RidesArray objectAtIndex:i];
            CLLocation *rideLocation=[[CLLocation alloc]initWithLatitude:[pRestuarant.m_Latitude doubleValue] longitude:[pRestuarant.m_Longitude doubleValue]];
            CLLocationDistance distance=[m_Location distanceFromLocation:rideLocation];
            NSNumber *d=[NSNumber numberWithDouble:distance];
            
            pRestuarant.m_Distance=[d intValue];
            
            rideLocation=nil;
        }
        [m_TableView reloadData];
    }
    else
    {
        for(int i=0;i<[m_RidesArray count];i++)
        {
            Land *pLand=[m_RidesArray objectAtIndex:i];
            for(int j=0;j<[pLand.m_RideArray count];j++)
            {
                Ride *pRide=[pLand.m_RideArray objectAtIndex:j];
                CLLocation *rideLocation=[[CLLocation alloc]initWithLatitude:[pRide.m_Latitude doubleValue] longitude:[pRide.m_Longitude doubleValue]];
                CLLocationDistance distance=[m_Location distanceFromLocation:rideLocation];
                NSNumber *d=[NSNumber numberWithDouble:distance];
                //printf("%d\n",[d intValue]);
                [rideLocation release];
                rideLocation=nil;
                pRide.m_Distance=[d intValue];
            }
        }
        switch (m_SelectedSortType)
        {
            case 0:
                [self sortOnDistance];
                break;
            case 1:
                [self sortOnName];
                break;
            case 2:
                [self sortOnWaitTime];
                break;
            default:
                break;
        }
    }
  
}


-(void)sortOnDistance
{  
  for(int i=0;i<[m_RidesArray count];i++)
  {
    Land *pLand=[m_RidesArray objectAtIndex:i];
    NSMutableArray *pArray=[[NSMutableArray alloc]init];
    if([pLand.m_RideArray count]>1)
    {
      int arrayCount=[pLand.m_RideArray count];
      for(int j=0;j<arrayCount-1;j++)
      {
        Ride *pMinRide=[pLand.m_RideArray objectAtIndex:0];
        for(int k=1;k<[pLand.m_RideArray count];k++)
        {
          Ride *curRide=[pLand.m_RideArray objectAtIndex:k];
          int curDist=curRide.m_Distance;
          int minDist=pMinRide.m_Distance;
          if(curDist < minDist)
          {
            pMinRide=curRide;
          }
        }
        [pArray addObject:pMinRide];
        [pLand.m_RideArray removeObject:pMinRide];
      }
      [pArray addObjectsFromArray:pLand.m_RideArray];
      [pLand.m_RideArray removeAllObjects];
      [pLand.m_RideArray addObjectsFromArray:pArray];
    }
    [pArray removeAllObjects];[pArray release];pArray=nil;
  }
  [m_TableView reloadData];
  
}

-(void)sortOnName
{
  for(int i=0;i<[m_RidesArray count];i++)
  {
    Land *pLand=[m_RidesArray objectAtIndex:i];
    NSMutableArray *pArray=[[NSMutableArray alloc]init];
    if([pLand.m_RideArray count]>1)
    {
      int arrayCount=[pLand.m_RideArray count];
      for(int j=0;j<arrayCount-1;j++)
      {
        Ride *pMinRide=[pLand.m_RideArray objectAtIndex:0];
        for(int k=1;k<[pLand.m_RideArray count];k++)
        {
          Ride *curRide=[pLand.m_RideArray objectAtIndex:k];
          NSComparisonResult result = [curRide.m_RideName caseInsensitiveCompare:pMinRide.m_RideName];
          
          if(result == NSOrderedAscending || result==NSOrderedSame)
          {
            pMinRide=curRide;
          }
        }
        [pArray addObject:pMinRide];
        [pLand.m_RideArray removeObject:pMinRide];
      }
      [pArray addObjectsFromArray:pLand.m_RideArray];
      [pLand.m_RideArray removeAllObjects];
      [pLand.m_RideArray addObjectsFromArray:pArray];
    }
    [pArray removeAllObjects];[pArray release];pArray=nil;
  }
  [m_TableView reloadData];
}

-(void)sortOnWaitTime
{
  for(int i=0;i<[m_RidesArray count];i++)
  {
    Land *pLand=[m_RidesArray objectAtIndex:i];
    NSMutableArray *pArray=[[NSMutableArray alloc]init];
    if([pLand.m_RideArray count]>1)
    {
      int arrayCount=[pLand.m_RideArray count];
      for(int j=0;j<arrayCount-1;j++)
      {
        Ride *pMinRide=[pLand.m_RideArray objectAtIndex:0];
        for(int k=1;k<[pLand.m_RideArray count];k++)
        {
          Ride *curRide=[pLand.m_RideArray objectAtIndex:k];
          int curTime=[curRide.m_WaitTime intValue];
          int minTime=[pMinRide.m_WaitTime intValue];
          if(curTime > minTime)
          {
            pMinRide=curRide;
          }
        }
        [pArray addObject:pMinRide];
        [pLand.m_RideArray removeObject:pMinRide];
      }
      [pArray addObjectsFromArray:pLand.m_RideArray];
      [pLand.m_RideArray removeAllObjects];
      [pLand.m_RideArray addObjectsFromArray:pArray];
    }
    [pArray removeAllObjects];[pArray release];pArray=nil;
  }
  [m_TableView reloadData];
}


#pragma mark tableView delegate methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (appDelegate.isIndependent) {
        return [m_RidesArray count];
    }
  Land *pLand=[m_RidesArray objectAtIndex:section];
  return [pLand.m_RideArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (appDelegate.isIndependent) {
        if(indexPath.row==0 || indexPath.row==([m_RidesArray count]-1))
            return 135/2+3;
        else
            return 135/2;
    }
    
    
  if(indexPath.section==0)
    return 55;
  else
  {
    Land *pLand=[m_RidesArray objectAtIndex:indexPath.section-1];
    if(indexPath.row==0 || indexPath.row==([pLand.m_RideArray count]-1))
      return 54;
    else 
      return 51;
  }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
    
    if (appDelegate.isIndependent) {
        return nil;
    }
    
    
  // create the parent view that will hold header Label
  UIView* customView = [[[UIView alloc] initWithFrame:CGRectMake(20, 0.0, 320.0, 20)] autorelease];
    
    // create the button object
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.opaque = NO;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.highlightedTextColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    headerLabel.frame = CGRectMake(20, 0.0, 320.0, 20);
    
    // If you want to align the header text as centered
    Land *pLand=[m_RidesArray objectAtIndex:section];
    headerLabel.text =pLand.m_LandName;
    
    [customView addSubview:headerLabel];
    
    return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  return 30;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (appDelegate.isIndependent) {
        return 1;
    }
    
    return [m_RidesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (appDelegate.isIndependent) {
        RestuarantDetailCell *cell = (RestuarantDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"RestuarantDetailCell"];
        if(cell == nil)
        {
            cell=[[[RestuarantDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RestuarantDetailCell"]autorelease];
            
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle=UITableViewCellSelectionStyleGray;
        }
        Restuarant *pRestuarant=[m_RidesArray objectAtIndex:indexPath.row];
        cell.m_TitleLabel.text=pRestuarant.m_RestName;
        
        if([pRestuarant.m_RestServiceType isEqualToString:@"Quick"])
            cell.m_ImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/FoodPage/Quick" ofType:@"png"]];
        else
            cell.m_ImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/FoodPage/Table" ofType:@"png"]];
        
        int waittime=[pRestuarant.m_WaitTime intValue];
        if(waittime<=20)
            cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/green1" ofType:@"png"]];
        else if(waittime<60)
            cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/yellow1" ofType:@"png"]];
        else if(waittime>=60)
            cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/red1" ofType:@"png"]];
        
        int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];
        
        if(waittime==-1 || open==0)
        {
            cell.m_WaitTimeImageView.frame=CGRectMake(21,22,45,29);
            cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]];
            cell.m_WaitTimeLabel.frame=CGRectMake(21,22,45,29);
            cell.m_WaitTimeLabel.text=@"";
            [cell SetOpacityLow];
            cell.userInteractionEnabled=YES;
        }
        else if(waittime==75)
        {
            cell.m_WaitTimeImageView.frame=CGRectMake(21,17,42,31);
            cell.m_WaitTimeLabel.frame=CGRectMake(21,17,42,31);
            cell.m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",pRestuarant.m_WaitTime];
            [cell SetOpacityHigh];
        }
        else
        {
            cell.m_WaitTimeImageView.frame=CGRectMake(21,17,31,31);
            cell.m_WaitTimeLabel.frame=CGRectMake(21,17,31,31);
            cell.m_WaitTimeLabel.text=pRestuarant.m_WaitTime;
            [cell SetOpacityHigh];
        }
        
        NSNumber *number = [NSNumber numberWithInt:pRestuarant.m_Distance];
        NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
        [frmtr setNumberStyle: NSNumberFormatterDecimalStyle];
        NSString *commaString = [frmtr stringFromNumber:number];
        NSString *pYardsStr=[commaString stringByAppendingString:@" yards"];
        cell.m_YardsLabel.text=pYardsStr;
        [frmtr release];
        
        UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 135/2)];
        if([m_RidesArray count]==1)
            bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restsingle1.png"];
        else if(indexPath.row==0)
            bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restup1.png"];
        else if(indexPath.row==[m_RidesArray count]-1)
            bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restbottom1.png"];
        else
            bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restmiddle1.png"];
        cell.backgroundView=bgImgView;
        [bgImgView release];
        
        UIImageView *selbgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 135/2)];
        if([m_RidesArray count]==1)
            selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestsingle1.png"];
        else if(indexPath.row==0)
            selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestup1.png"];
        else if(indexPath.row==[m_RidesArray count]-1)
            selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestbottom1.png"];
        else
            selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestmiddle1.png"];
        cell.selectedBackgroundView=selbgImgView;
        [selbgImgView release];  
        
        return cell;
    }
    else
    {
        RidesDetailCell *cell;
        
        cell = (RidesDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"MainPageCell"];
		if(cell == nil)
        {
            cell=[[[RidesDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainPageCell"]autorelease];
            cell.m_delegate=self;
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle=UITableViewCellSelectionStyleGray;
        }
        cell.m_CustomButton.section=indexPath.section;
        cell.m_CustomButton.row=indexPath.row;
        
        
        Land *pLand=[m_RidesArray objectAtIndex:indexPath.section];
        Ride *pRide=[pLand.m_RideArray objectAtIndex:indexPath.row];
        
        
        NSNumber *number = [NSNumber numberWithInt:pRide.m_Distance];
        NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
        [frmtr setNumberStyle: NSNumberFormatterDecimalStyle];
        NSString *commaString = [frmtr stringFromNumber:number];
        NSString *pYardsStr=[commaString stringByAppendingString:@" yards"];
        cell.m_YardsLabel.text=pYardsStr;
        [frmtr release];
        
        UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,316, 51)];
        if([pLand.m_RideArray count]==1)
            bgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/singlecell2.png"];
        else if(indexPath.row==0)
            bgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/uppercell2.png"];
        else if(indexPath.row==[pLand.m_RideArray count]-1)
            bgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/downcell2.png"];
        else
            bgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/middlecell2.png"];
        cell.backgroundView=bgImgView;
        [bgImgView release];
        
        UIImageView *selbgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,316, 51)];
        if([pLand.m_RideArray count]==1)
            selbgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/selsinglecell2.png"];
        else if(indexPath.row==0)
            selbgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/seluppercell2.png"];
        else if(indexPath.row==[pLand.m_RideArray count]-1)
            selbgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/seldowncell2.png"];
        else
            selbgImgView.image=[UIImage imageNamed:@"ImagesCommon/Rides/selmiddlecell2.png"];
        cell.selectedBackgroundView=selbgImgView;
        [selbgImgView release];
        
        cell.m_TitleLabel.text=pRide.m_RideName;
        int waittime=[pRide.m_WaitTime intValue];
        if(waittime<=20)
            cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/green1" ofType:@"png"]];
        else if(waittime<60)
            cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/yellow1" ofType:@"png"]];
        else if(waittime>=60)
            cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/red1" ofType:@"png"]];
        
        int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];
        
        if(waittime==-1 || open==0)
        {
            if(indexPath.row==0)
            {
                if([pLand.m_RideArray count]!=1)
                {
                    cell.m_WaitTimeImageView.frame=CGRectMake(22,17,45,29);
                    cell.m_WaitTimeLabel.frame=CGRectMake(22,17,45,29);
                    cell.m_TitleLabel.frame=CGRectMake(75,17,190,15);
                    cell.m_YardsLabel.frame=CGRectMake(75,31,150,15);
                    cell.m_CustomButton.frame=CGRectMake(274, 13, 40, 33);
                }
                else
                {
                    cell.m_WaitTimeImageView.frame=CGRectMake(22,13,45,29);
                    cell.m_WaitTimeLabel.frame=CGRectMake(22,13,45,29);
                    cell.m_TitleLabel.frame=CGRectMake(75,11,190,15);
                    cell.m_YardsLabel.frame=CGRectMake(75,25,150,15);
                    cell.m_CustomButton.frame=CGRectMake(274,7, 40, 33);
                }
            }
            else
            {
                cell.m_WaitTimeImageView.frame=CGRectMake(22,12,45,29);
                cell.m_WaitTimeLabel.frame=CGRectMake(22,12,45,29);
                cell.m_TitleLabel.frame=CGRectMake(75,12,190,15);
                cell.m_YardsLabel.frame=CGRectMake(75,26,150,15);
                cell.m_CustomButton.frame=CGRectMake(274, 8, 40, 33);
            }
            cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]];
            cell.m_WaitTimeLabel.text=@"";
            [cell SetOpacityLow];
            cell.m_WaitTimeLabel.layer.opacity=1.0;
            cell.userInteractionEnabled=YES;
        }
        else if(waittime==75)
        {
            if(indexPath.row==0)
            {
                if([pLand.m_RideArray count]!=1)
                {
                    cell.m_WaitTimeImageView.frame=CGRectMake(22,12,42,32);
                    cell.m_WaitTimeLabel.frame=CGRectMake(22,12,42,32);
                    cell.m_TitleLabel.frame=CGRectMake(75,17,190,15);
                    cell.m_YardsLabel.frame=CGRectMake(75,31,150,15);
                    cell.m_CustomButton.frame=CGRectMake(274, 13, 40, 33);
                }
                else
                {
                    cell.m_WaitTimeImageView.frame=CGRectMake(22,10,42,32);
                    cell.m_WaitTimeLabel.frame=CGRectMake(22,10,42,32);
                    cell.m_TitleLabel.frame=CGRectMake(75,13,190,15);
                    cell.m_YardsLabel.frame=CGRectMake(75,27,150,15);
                    cell.m_CustomButton.frame=CGRectMake(274,9, 40, 33);
                }
            }
            else
            {
                cell.m_WaitTimeImageView.frame=CGRectMake(22,7,42,32);
                cell.m_WaitTimeLabel.frame=CGRectMake(22,7,42,32);
                cell.m_TitleLabel.frame=CGRectMake(75,12,190,15);
                cell.m_YardsLabel.frame=CGRectMake(75,26,150,15);
                cell.m_CustomButton.frame=CGRectMake(274, 8, 40, 33);
            }
            cell.m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",pRide.m_WaitTime];
            [cell SetOpacityHigh];
        }
        else
        {
            if(indexPath.row==0)
            {
                if([pLand.m_RideArray count]!=1)
                {
                    cell.m_WaitTimeImageView.frame=CGRectMake(22,13,32,32);
                    cell.m_WaitTimeLabel.frame=CGRectMake(22,13,32,32);
                    cell.m_TitleLabel.frame=CGRectMake(75,17,190,15);
                    cell.m_YardsLabel.frame=CGRectMake(75,31,150,15);
                    cell.m_CustomButton.frame=CGRectMake(274, 13, 40, 33);
                }
                else
                {
                    cell.m_WaitTimeImageView.frame=CGRectMake(22,10,32,32);
                    cell.m_WaitTimeLabel.frame=CGRectMake(22,10,32,32);
                    cell.m_TitleLabel.frame=CGRectMake(75,13,190,15);
                    cell.m_YardsLabel.frame=CGRectMake(75,27,150,15);
                    cell.m_CustomButton.frame=CGRectMake(274, 9, 40, 33);
                }
            }
            else
            {
                cell.m_WaitTimeImageView.frame=CGRectMake(22,9,32,32);
                cell.m_WaitTimeLabel.frame=CGRectMake(22,9,32,32);
                cell.m_TitleLabel.frame=CGRectMake(75,12,190,15);
                cell.m_YardsLabel.frame=CGRectMake(75,26,150,15);
                cell.m_CustomButton.frame=CGRectMake(274, 8, 40, 33);                
            }
            cell.m_WaitTimeLabel.text=pRide.m_WaitTime;
            [cell SetOpacityHigh];
        }
        
        if (!appDelegate.isPortrait) {
            CGRect frame = cell.m_TitleLabel.frame;
            int width = frame.size.width;
            width = width+100;
            frame.size.width = width;
            cell.m_TitleLabel.frame = frame;
        }
        cell.m_CustomButton.hidden=YES;
        cell.m_YardsLabel.hidden=NO;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"SECTION"]==indexPath.section && [[NSUserDefaults standardUserDefaults]integerForKey:@"ROW"]==indexPath.row)
        {
            [[NSUserDefaults standardUserDefaults]setInteger:-1 forKey:@"SECTION"];
            [[NSUserDefaults standardUserDefaults]setInteger:-1 forKey:@"ROW"];
        }
        return cell;
    }
    return nil;
	
}

-(void)gotoDetailPage:(int)pSection Row:(int)pRow;
{
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
	[m_Homebtn removeFromSuperview];
	[m_Sortbutton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
    if (appDelegate.isIndependent) {
        [m_MapButton setHidden:YES];
    }
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
  
    
    
    if (appDelegate.isIndependent) {
        Restuarant *pRestuarant=[m_RidesArray objectAtIndex:indexPath.row];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [paths objectAtIndex:0];
        paths=nil;
        filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"IndependentFood%@",pRestuarant.m_RestId]];
        if(m_RestuarantDetailViewController)
        {
            m_RestuarantDetailViewController.m_delegate=nil;
            [m_RestuarantDetailViewController.view removeFromSuperview];
            //  [m_RestuarantDetailViewController release];
            m_RestuarantDetailViewController=nil;
        }
        m_RestuarantDetailViewController=[[RestuarantDetailViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId LandId:@"" RestId:pRestuarant.m_RestId LandName:@"" Distance:pRestuarant.m_Distance isFavourite:NO imagePath:filePath pLat:pRestuarant.m_Latitude pLong:pRestuarant.m_Longitude];
        m_RestuarantDetailViewController.isInd=YES;
        m_RestuarantDetailViewController.m_delegate=self;
        filePath=nil;
        [self.navigationController pushViewController:m_RestuarantDetailViewController animated:YES];
    }
    else
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"AdDescription"])
        {
            if(indexPath.section==100)
            {
                if(m_ParadeTimeViewController)
                {
                    [m_ParadeTimeViewController.view removeFromSuperview];
                    m_ParadeTimeViewController.m_delegate=nil;
                    [m_ParadeTimeViewController release];
                    m_ParadeTimeViewController=nil;
                }
                m_ParadeTimeViewController=[[ParadeTimeViewController alloc]initwithParkId:m_ParkId];
                m_ParadeTimeViewController.m_delegate=self;
                [self.navigationController pushViewController:m_ParadeTimeViewController animated:YES];
            }
            else
            {
                
                if(m_RidesDetailViewController)
                {
                    [m_RidesDetailViewController.view removeFromSuperview];
                    m_RidesDetailViewController.m_delegate=nil;
                    [m_RidesDetailViewController release];
                    m_RidesDetailViewController=nil;
                }
                Land *pLand=[m_RidesArray objectAtIndex:indexPath.section];
                Ride *pRide=[pLand.m_RideArray objectAtIndex:indexPath.row];
                if(m_LandId){[m_LandId release];m_LandId=nil;}
                m_LandId=pLand.m_LandId;
                [m_LandId retain];
                if(m_RideId){[m_RideId release];m_RideId=nil;}
                m_RideId=pRide.m_RideId;
                [m_RideId retain];
                m_RideDistance=pRide.m_Distance;
                
                [[NSUserDefaults standardUserDefaults]setInteger:indexPath.section forKey:@"SECTION"];
                [[NSUserDefaults standardUserDefaults]setInteger:indexPath.row forKey:@"ROW"];
                m_RidesDetailViewController=[[RidesDetailViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId LandId:m_LandId RideId:m_RideId LandName:pLand.m_LandName Lat:pRide.m_Latitude Long:pRide.m_Longitude Distance:m_RideDistance isFavourite:NO];
                m_RidesDetailViewController.m_delegate=self;
                [self.navigationController pushViewController:m_RidesDetailViewController animated:YES];
                [m_TableView reloadData];
                
            }
        }
        else
        {
            if(m_RidesDetailViewController)
            {
                [m_RidesDetailViewController.view removeFromSuperview];
                m_RidesDetailViewController.m_delegate=nil;
                [m_RidesDetailViewController release];
                m_RidesDetailViewController=nil;
            }
            Land *pLand=[m_RidesArray objectAtIndex:indexPath.section];
            Ride *pRide=[pLand.m_RideArray objectAtIndex:indexPath.row];
            if(m_LandId){[m_LandId release];m_LandId=nil;}
            m_LandId=pLand.m_LandId;
            [m_LandId retain];
            if(m_RideId){[m_RideId release];m_RideId=nil;}
            m_RideId=pRide.m_RideId;
            [m_RideId retain];
            m_RideDistance=pRide.m_Distance;
            
            [[NSUserDefaults standardUserDefaults]setInteger:indexPath.section forKey:@"SECTION"];
            [[NSUserDefaults standardUserDefaults]setInteger:indexPath.row forKey:@"ROW"];
            //    m_RidesDetailViewController=[[RidesDetailViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId LandId:m_LandId RideId:m_RideId LandName:pLand.m_LandName Lat:pRide.m_Latitude Long:pRide.m_Longitude Distance:m_RideDistance isFavourite:NO];
            //    m_RidesDetailViewController.m_delegate=self;
            //    [self.navigationController pushViewController:m_RidesDetailViewController animated:YES];
            
            SubmitWaitTimeViewController *mSubmitWaitTimeViewController=[[[SubmitWaitTimeViewController alloc]initWithId:m_RideId Latitude:pRide.m_Latitude Longitude:pRide.m_Longitude isRide:YES] autorelease];
            [self.navigationController pushViewController:mSubmitWaitTimeViewController animated:YES];
            
            [m_TableView reloadData];  
        }

    }
    
}

-(void)viewDidLoad
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void)viewWillAppear:(BOOL)animated
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  m_Timer=[NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(refreshData) userInfo:nil repeats:YES];
  [m_Timer retain];
  
    [self layoutSubViews];
   
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount)
    {
        
    }
    else{
        [self.navigationController.navigationBar addSubview:m_Homebtn];
        [self.navigationController.navigationBar addSubview:m_Sortbutton];
        [self.navigationController.navigationBar addSubview:pTitleLabel];
        if (appDelegate.isIndependent) {
            [m_MapButton setHidden:NO];
        }
        else
        {
            m_MapButton.hidden = YES;
        }
        
        
    }
 
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    appDelegate.subviewCount = appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}
- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
  [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated
{
  if(m_RidesDetailViewController){[m_RidesDetailViewController.view removeFromSuperview];m_RidesDetailViewController.m_delegate=nil;[m_RidesDetailViewController release];m_RidesDetailViewController=nil;}
  if(m_SortRideViewController){[m_SortRideViewController.view removeFromSuperview];m_SortRideViewController.m_delegate=nil;[m_SortRideViewController release];m_SortRideViewController=nil;}
	if(m_ParadeTimeViewController){[m_ParadeTimeViewController.view removeFromSuperview];m_ParadeTimeViewController.m_delegate=nil;[m_ParadeTimeViewController release];m_ParadeTimeViewController=nil;}
	if(m_SubmitWaitTimeViewController){[m_SubmitWaitTimeViewController.view removeFromSuperview];m_SubmitWaitTimeViewController=nil;[m_SubmitWaitTimeViewController release];m_SubmitWaitTimeViewController=nil;}  
    if (m_RestuarantDetailViewController) {
        [m_RestuarantDetailViewController.view removeFromSuperview];m_RestuarantDetailViewController.m_delegate = nil;m_RestuarantDetailViewController = nil;//cp today
    }
    if(m_MapImageViewController)
    {
        [m_MapImageViewController.view removeFromSuperview];
        //   [m_MapImageViewController release];
        m_MapImageViewController=nil;
    }
    
   // [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,4,320,50)];
    //[[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 80)];
}

- (void)dealloc 
{
  //printf("\nRidesVcDealloc\n");
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  } 
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  if(m_LandId){[m_LandId release];m_LandId=nil;}
  if(m_RideId){[m_RideId release];m_RideId=nil;}
  if(m_StudioId){[m_StudioId release];m_StudioId=nil;}
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_Location){[m_Location release];m_Location=nil;}
  if(m_LocationManager){[m_LocationManager stopUpdatingHeading];m_LocationManager.delegate=nil;[m_LocationManager release];m_LocationManager=nil;}
	if(m_Sortbutton){[m_Sortbutton removeFromSuperview];[m_Sortbutton release];m_Sortbutton=nil;}
	if(m_Homebtn){[m_Homebtn removeFromSuperview];[m_Homebtn release];m_Homebtn=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
	if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
	if(m_RidesArray){[m_RidesArray removeAllObjects];[m_RidesArray release];m_RidesArray=nil;}
	if(m_BGImageView){[m_BGImageView removeFromSuperview];[m_BGImageView release];m_BGImageView=nil;}
	if(m_RidesDetailViewController){[m_RidesDetailViewController.view removeFromSuperview];m_RidesDetailViewController.m_delegate=nil;[m_RidesDetailViewController release];m_RidesDetailViewController=nil;}
  if(m_SortRideViewController){[m_SortRideViewController.view removeFromSuperview];m_SortRideViewController.m_delegate=nil;[m_SortRideViewController release];m_SortRideViewController=nil;}
	if(m_ParadeTimeViewController){[m_ParadeTimeViewController.view removeFromSuperview];m_ParadeTimeViewController.m_delegate=nil;[m_ParadeTimeViewController release];m_ParadeTimeViewController=nil;}
	if(m_SubmitWaitTimeViewController){[m_SubmitWaitTimeViewController.view removeFromSuperview];m_SubmitWaitTimeViewController.m_delegate=nil;[m_SubmitWaitTimeViewController release];m_SubmitWaitTimeViewController=nil;}
  if(DateBarImageView){[DateBarImageView removeFromSuperview];[DateBarImageView release];DateBarImageView=nil;}

    if (m_RestuarantDetailViewController) {
        [m_RestuarantDetailViewController.view removeFromSuperview];m_RestuarantDetailViewController.m_delegate = nil;m_RestuarantDetailViewController = nil;}
	
    
    [super dealloc];
}


@end
