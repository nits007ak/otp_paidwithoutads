//
//  Favourite.m
//  AmusementPark
//
//  Created by HARSHA on 08/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Favourite.h"


@implementation Favourite
@synthesize m_LandId,m_LandName,m_RideArray,m_RestuarantArray,m_CombinedArray;

-(id)init
{
  if(self=[super init])
  {
    m_LandId=nil;
    m_LandName=nil;
    m_RideArray=[[NSMutableArray alloc]init];
    m_RestuarantArray=[[NSMutableArray alloc]init];
    m_CombinedArray=[[NSMutableArray alloc]init];
  }
  return self;
}

-(void)dealloc
{
  if(m_LandId){[m_LandId release];m_LandId=nil;}
  if(m_LandName){[m_LandName release];m_LandName=nil;}
  if(m_RideArray){[m_RideArray release];m_RideArray=nil;}
  if(m_RestuarantArray){[m_RestuarantArray release];m_RestuarantArray=nil;}
  if(m_CombinedArray){[m_CombinedArray release];m_CombinedArray=nil;}
  [super dealloc];
}
@end
