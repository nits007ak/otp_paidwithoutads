//
//  UpgradeViewController.h
//  AmusementPark
//
//  Created by Amresh Kumar on 22/10/11.
//  Copyright (c) 2011 Permeative Technologies Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmusementParkAppDelegate.h"



@interface UpgradeViewController : UIViewController
{
    AmusementParkAppDelegate * appDelegate;
}
@property(nonatomic,retain)IBOutlet UIImageView *BGImage;

@property(nonatomic,retain)IBOutlet UIButton *KeepFreeButton;
-(IBAction)Upgrade:(id)sender;
-(IBAction)Cancel:(id)sender;

@end
