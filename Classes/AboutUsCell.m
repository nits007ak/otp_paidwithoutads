//
//  AboutUsCell.m
//  AmusementPark
//
//  Created by HARSHA on 04/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AboutUsCell.h"


@implementation AboutUsCell

@synthesize m_TitleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
    {
      m_TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(75,17,190,15)];
      m_TitleLabel.backgroundColor = [UIColor clearColor];
      m_TitleLabel.textAlignment = UITextAlignmentLeft;
      m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
      m_TitleLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:1.0];
      [self addSubview:m_TitleLabel];      
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated 
{
    
    [super setSelected:selected animated:animated];
    
}


- (void)dealloc 
{
  if(m_TitleLabel){[m_TitleLabel removeFromSuperview];[m_TitleLabel release];m_TitleLabel=nil;}
    [super dealloc];
}


@end
