//
//  ApptasmicViewController.h
//  AmusementPark
//
//  Created by HARSHA on 06/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmusementParkAppDelegate.h"

@interface ApptasmicViewController : UIViewController 
{
    AmusementParkAppDelegate * appDelegate;
    UIScrollView *m_ScrollView;
    UIImageView *m_ImageView;
    UIImageView *m_BGImageView;
    UIButton *m_BackButton;
}
-(void)layoutSubViews;  //cp
-(void)checkOrientation;
@end
