//
//  InAppPurchaseObserver.m
//  Octopus Paul
//
//  Created by Shriram on 27/07/09.
//

#import "InAppPurchaseObserver.h"

NSString* const gInAppTransactionNotification = @"InAppTransactionSucceeded";
NSString* const gInAppTransactionCancelNotification = @"InAppTransactionCancelled";

@implementation InAppPurchaseObserver

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
  for (SKPaymentTransaction *transaction in transactions)
  {
    switch (transaction.transactionState)
    {
      case SKPaymentTransactionStatePurchased:
        [self completeTransaction:transaction];
        break;
      case SKPaymentTransactionStateFailed:
        [self failedTransaction:transaction];
        break;
      case SKPaymentTransactionStateRestored:
        [self restoreTransaction:transaction];
      default:
        break;
    }
  }
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction 
{
	//Record the purchase here
}

- (void)completeTransaction: (SKPaymentTransaction *)transaction
{
  //    [self recordTransaction: transaction];
	[[SKPaymentQueue defaultQueue] finishTransaction: transaction];
  //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsInAppPurchaseDone"];
  
  [[NSNotificationCenter defaultCenter] postNotificationName:gInAppTransactionNotification object:nil userInfo:nil];
}

-(void)restoreTransaction: (SKPaymentTransaction *)transaction
{
  //    [self recordTransaction: transaction];
  [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
  //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsInAppPurchaseDone"];
  
  [[NSNotificationCenter defaultCenter] postNotificationName:gInAppTransactionNotification object:nil userInfo:nil];
}

- (void)failedTransaction: (SKPaymentTransaction *)transaction
{
  if (transaction.error.code != SKErrorPaymentCancelled)
  {
    // Optionally, display an error here.
    
    UIAlertView *failedTransaction = [[UIAlertView alloc] initWithTitle:@"In-App Purchase" message:@"Transaction Failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [failedTransaction show];
    [failedTransaction release];    
  }
  [[NSNotificationCenter defaultCenter] postNotificationName:gInAppTransactionCancelNotification object:nil userInfo:nil]; 
    
  [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

@end
