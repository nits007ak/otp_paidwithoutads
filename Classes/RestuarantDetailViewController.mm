//
//  RestuarantDetailViewController.m
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RestuarantDetailViewController.h"
#import "AmusementParkDB.h"
#import "Land.h"
#import "LandInfo.h"
#import "XMLDataParser.h"
//#import "FBConnectController.h"
#import "AmusementParkAppDelegate.h"
#import "FullVersionUpgrader.h"
#import "AdsManager.h"
#import "FBConnectionViewController.h"

@implementation RestuarantDetailViewController
@synthesize m_delegate,isInd;
-(id)initwithStudioId:(NSString *)pStuId ParkId:(NSString *)pParkId LandId:(NSString *)pLandId RestId:(NSString *)pRideId LandName:(NSString *)pLandName Distance:(int)dist isFavourite:(BOOL)fav imagePath:(NSString *)imagePath pLat:(NSString *)lat pLong:(NSString *)longitude
{
	if(self=[super init])
	{
    m_ImagePath=imagePath;
    if(m_ImagePath)
      [m_ImagePath retain];
    isFav=fav;
    m_StudioId=pStuId;
    [m_StudioId retain];
    m_ParkId=pParkId;
    [m_ParkId retain];
    m_LandId=pLandId;
    [m_LandId retain];
    m_RestId=pRideId;
    [m_RestId retain];
    m_LandName=pLandName;
    [m_LandName retain];
    m_Latitude=lat;
    [m_Latitude retain];
    m_Longitude=longitude;
    [m_Longitude retain];
    m_Distance=dist;
    mURLConnection=nil;
    mResponseData=nil;
    m_Restuarant=nil;
    m_activityIndicator=nil;    
    m_RestuarantFromDb=nil;
    pTitleLabel=nil;
		m_ImageView=nil;
    m_FacebookImageView=nil;
    m_FavouriteImageView=nil;
		m_TitleLabel=nil;
		m_CuisineTitleLabel=nil;
		m_CuisineTypeLabel=nil;
		m_ReservationLabel=nil;
		m_ReservationButton=nil;
		m_TableServiceLabel=nil;
    m_DollarLabel=nil;
		m_TableServiceCostLabel=nil;
    m_CallNumberLabel=nil;
    m_WaitTimeLabel=nil;
		m_FavoriteButton=nil;
		m_CallButton=nil;
		m_DescriptionView=nil;
    m_BackButton=nil;
    m_SubmitButton=nil;
    m_FacebookButton=nil;
    m_SubmitWaitTimeViewController=nil;
    isInd=NO;
    isFirst=YES;
    
    m_Timer=nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRecieved) name:@"NotificationName" object:nil];
    m_RestuarantArray=[[NSMutableArray alloc]init];
	}
	return self;
}

-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	self.view.backgroundColor=[UIColor clearColor];
  [self.navigationItem setHidesBackButton:YES animated:NO];
	
    if (!scrollView) {
        scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
        scrollView.backgroundColor = [UIColor clearColor];
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 10);
        [scrollView setScrollEnabled:YES];
    }

  if(pTitleLabel==nil)
    pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(70, 8, 160, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
  
  if(m_BackButton==nil)
  {
    m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
    m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
    [m_BackButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/backbtn"  ofType:@"png"]] forState:UIControlStateNormal];
    [m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [m_BackButton retain];  
    
    m_SubmitButton=[UIButton buttonWithType:UIButtonTypeCustom];
    m_SubmitButton.frame=CGRectMake(235, 8, 80.5, 30);
    [m_SubmitButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/RidesDetailPage/submit" ofType:@"png"]] forState:UIControlStateNormal];
    [m_SubmitButton addTarget:self action:@selector(submitWaitTimeAction) forControlEvents:UIControlEventTouchUpInside];
    [m_SubmitButton retain];		  
  }
  
  int AdOffset=0;
  AdOffset=49;
  
     pBGImageView=[[UIImageView alloc]init];
    
    if (screenBounds.size.height == 568) {
        pBGImageView.frame=CGRectMake(0, 0, 320, 568);
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        pBGImageView.frame=CGRectMake(0, 0, 320, 480);
    }
    if(isInd)
    {
        
        pBGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
    }
    else
    {
        
        pBGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
    }
	[self.view addSubview:pBGImageView];	
	[pBGImageView release];
  
  UIImageView *pRestDetailBG=[[UIImageView alloc]init];
  if(isInd)
  {
    pRestDetailBG.frame=CGRectMake(9, 24+AdOffset, 316, 354-AdOffset);
    pRestDetailBG.image=[UIImage imageNamed:@"ImagesInPaidVersion/RestDetailPage/RestBG2.png"];
  }
  else 
  {
    pRestDetailBG.frame=CGRectMake(8, 18+AdOffset, 305, 341-AdOffset);
    pRestDetailBG.image=[UIImage imageNamed:@"ImagesInPaidVersion/RestDetailPage/RestDetailBG2.png"]; 
  }
    pRestDetailBG.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [scrollView addSubview:pRestDetailBG];
	[pRestDetailBG release];
  
  if(m_ImageView==nil)
    m_ImageView=[[UIImageView alloc]initWithFrame:CGRectMake(25, 34+AdOffset, 92,104)];
	m_ImageView.backgroundColor=[UIColor clearColor];
	[scrollView addSubview:m_ImageView];
	
  if(m_TitleLabel==nil)
    m_TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(135, 30+AdOffset, 165, 20)];
  m_TitleLabel.backgroundColor=[UIColor clearColor];
	m_TitleLabel.textAlignment=UITextAlignmentLeft;
  m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
  m_TitleLabel.textColor = [UIColor colorWithRed:128.0/255 green:85.0/255 blue:35.0/255 alpha:1.0];
	[scrollView addSubview:m_TitleLabel];
	
  if(m_FacebookImageView==nil)
    m_FacebookImageView=[[UIImageView alloc]initWithFrame:CGRectMake(95, 162+AdOffset, 17, 17.5)];
  m_FacebookImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/RidesDetailPage/facebook" ofType:@"png"]];
  m_FacebookImageView.hidden=NO;
  [scrollView addSubview:m_FacebookImageView];  
  
  if(m_FacebookButton)
  {[m_FacebookButton release];m_FacebookButton=nil;}
  m_FacebookButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
	m_FacebookButton.frame=CGRectMake(26, 157+AdOffset, 92, 24.5);
	m_FacebookButton.backgroundColor=[UIColor clearColor];
	[m_FacebookButton addTarget:self action:@selector(launchFacebook) forControlEvents:UIControlEventTouchUpInside];
	[scrollView addSubview:m_FacebookButton];  
  [m_FacebookButton retain];
	
  if(m_CuisineTypeLabel==nil)
    m_CuisineTypeLabel=[[UILabel alloc]initWithFrame:CGRectMake(193, 90+AdOffset, 80, 20)];
  m_CuisineTypeLabel.backgroundColor=[UIColor clearColor];
	m_CuisineTypeLabel.textAlignment=UITextAlignmentLeft;
  m_CuisineTypeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
  m_CuisineTypeLabel.textColor = [UIColor colorWithRed:66.7/100 green:56.5/100 blue:44.3/100 alpha:1.0];
	[scrollView addSubview:m_CuisineTypeLabel];
  
  if(m_ReservationButton==nil)
  {
    m_ReservationButton=[UIButton buttonWithType:UIButtonTypeCustom];
    m_ReservationButton.frame=CGRectMake(215, 110+AdOffset, 18,16);
    m_ReservationButton.hidden=NO;
    [m_ReservationButton retain];
  }
  [scrollView addSubview:m_ReservationButton];	
	
  if(m_DollarLabel==nil)
    m_DollarLabel=[[UILabel alloc]initWithFrame:CGRectMake(155, 138+AdOffset, 120, 12)];
  m_DollarLabel.backgroundColor=[UIColor clearColor];
	[m_DollarLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12]];
  m_DollarLabel.textColor=[UIColor colorWithRed:50.2/100 green:33.3/100 blue:13.7/100 alpha:1.0];
	m_DollarLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_DollarLabel];  
  
  
  if(m_TableServiceLabel==nil)
    m_TableServiceLabel=[[UILabel alloc]initWithFrame:CGRectMake(155, 138+AdOffset, 120, 12)];
  m_TableServiceLabel.backgroundColor=[UIColor clearColor];
	[m_TableServiceLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12]];
  m_TableServiceLabel.textColor=[UIColor colorWithRed:66.7/100 green:56.5/100 blue:44.3/100 alpha:1.0];
	m_TableServiceLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_TableServiceLabel];  
  
  if(m_TableServiceCostLabel==nil)
    m_TableServiceCostLabel=[[UILabel alloc]initWithFrame:CGRectMake(137, 147+AdOffset, 150, 20)];
  m_TableServiceCostLabel.backgroundColor=[UIColor clearColor];
	[m_TableServiceCostLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
	m_TableServiceCostLabel.textAlignment=UITextAlignmentLeft;
  
  m_TableServiceCostLabel.textColor=[UIColor colorWithRed:66.7/100 green:56.5/100 blue:44.3/100 alpha:1.0];
	[scrollView addSubview:m_TableServiceCostLabel];
  
  if(m_FavoriteButton)
  {[m_FavoriteButton release];m_FavoriteButton=nil;}
	m_FavoriteButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_FavoriteButton.frame=CGRectMake(26, 129+AdOffset, 92, 24.5);
  [m_FavoriteButton setBackgroundColor:[UIColor clearColor]];
  if(isFirst)
    m_FavoriteButton.userInteractionEnabled=NO;
	[m_FavoriteButton addTarget:self action:@selector(AddtoFavouriteAction) forControlEvents:UIControlEventTouchUpInside];
	[scrollView addSubview:m_FavoriteButton];	
  [m_FavoriteButton retain];
	
  if(m_FavouriteImageView==nil)
    m_FavouriteImageView=[[UIImageView alloc]initWithFrame:CGRectMake(93, 149+AdOffset, 20.5, 21.5)];
  m_FavouriteImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/RidesDetailPage/favorite" ofType:@"png"]];
  m_FavouriteImageView.hidden=YES;
  [scrollView addSubview:m_FavouriteImageView];  
	
  if(!isFav)
  {
    if([[AmusementParkDB sharedPtDatabase]getDuplicateRest:m_Restuarant])
    {
      m_FavouriteImageView.hidden=NO;
    }    
    else 
    {
      m_FavouriteImageView.hidden=YES;
    }
  }
  else 
  {
    if([[AmusementParkDB sharedPtDatabase]getDuplicateRest:m_RestuarantFromDb])
    {
      m_FavouriteImageView.hidden=NO;
    }    
    else 
    {
      m_FavouriteImageView.hidden=YES;
    }    
  }
  
  if(m_CallNumberLabel==nil)
    m_CallNumberLabel=[[UILabel alloc]initWithFrame:CGRectMake(202, 175+AdOffset, 120, 20)];
  m_CallNumberLabel.backgroundColor=[UIColor clearColor];
  m_CallNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
  m_CallNumberLabel.textColor = [UIColor whiteColor];
	m_CallNumberLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_CallNumberLabel];
  
  if(m_CallButton){[m_CallButton release];m_CallButton=nil;}
  m_CallButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_CallButton.frame=CGRectMake(138, 170+AdOffset, 165, 28);
  m_CallButton.backgroundColor=[UIColor clearColor];
  m_CallButton.titleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
  [m_CallButton setTitleColor:[UIColor colorWithRed:128.0/255 green:85.0/255 blue:35.0/255 alpha:1.0] forState:UIControlStateNormal];
	[m_CallButton addTarget:self action:@selector(CallAction) forControlEvents:UIControlEventTouchUpInside];
  m_CallButton.userInteractionEnabled=NO;
	[scrollView addSubview:m_CallButton];
  [m_CallButton retain];
  
  if(m_WaitTimeLabel==nil)
    m_WaitTimeLabel=[[UILabel alloc]init];
  m_WaitTimeLabel.layer.cornerRadius = 4.0;
  m_WaitTimeLabel.layer.masksToBounds = YES;
	m_WaitTimeLabel.textAlignment=UITextAlignmentCenter;
  m_WaitTimeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24];
  if(isFirst)
    m_WaitTimeLabel.backgroundColor=[UIColor clearColor];
  m_WaitTimeLabel.textColor = [UIColor blackColor];
	[scrollView addSubview:m_WaitTimeLabel];  
  
  if(m_DescriptionView==nil)
    m_DescriptionView = [[UITextView alloc] init];
	m_DescriptionView.frame=CGRectMake(20, 210+AdOffset, 280, 130-AdOffset);
	m_DescriptionView.textAlignment=UITextAlignmentLeft;
  m_DescriptionView.editable=NO;
  m_DescriptionView.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
  m_DescriptionView.textColor = [UIColor blackColor];
	m_DescriptionView.backgroundColor = [UIColor  clearColor];
	m_DescriptionView.autocapitalizationType=UITextAutocapitalizationTypeSentences;
	[scrollView  addSubview:m_DescriptionView];
  
  if(isInd)      //For independent
  {
    m_ImageView.frame=CGRectMake(28, 37+AdOffset, 92,95);
    m_DescriptionView.frame=CGRectMake(20, 220+AdOffset, 280, 130-AdOffset);
    m_CallNumberLabel.frame=CGRectMake(159, 172+AdOffset, 120, 20);
    m_CallButton.frame=CGRectMake(25, 168+AdOffset, 270, 28);
    m_DollarLabel.frame=CGRectMake(127, 132+AdOffset, 30, 12);
    m_TableServiceLabel.frame=CGRectMake(160, 132+AdOffset, 120, 12);
    m_TableServiceCostLabel.frame=CGRectMake(127, 145+AdOffset, 150, 20);
    m_ReservationButton.frame=CGRectMake(215, 110+AdOffset, 18,16);
    m_CuisineTypeLabel.frame=CGRectMake(189, 89+AdOffset, 60, 20);
    m_FacebookButton.frame=CGRectMake(26, 140+AdOffset, 92, 24.5);
    m_FacebookImageView.frame=CGRectMake(100, 144+AdOffset, 17, 17.5);
    m_TitleLabel.frame=CGRectMake(128,28+AdOffset, 165, 20);    
    m_FavoriteButton.hidden=YES;
    m_FavouriteImageView.hidden=YES;
  }
  else //for food
  {
    m_ImageView.frame=CGRectMake(25, 31+AdOffset, 92,90);
    m_DescriptionView.frame=CGRectMake(20, 210+AdOffset, 280, 130-AdOffset);
    m_CallNumberLabel.frame=CGRectMake(195, 159+AdOffset, 120, 20);
    m_CallButton.frame=CGRectMake(130, 157+AdOffset, 165, 28);
    m_DollarLabel.frame=CGRectMake(127, 122+AdOffset, 30, 12);
    m_TableServiceLabel.frame=CGRectMake(160, 142+AdOffset, 120, 12);
    m_TableServiceCostLabel.frame=CGRectMake(128, 134+AdOffset, 150, 20);
    m_ReservationButton.frame=CGRectMake(205, 102+AdOffset, 18,16);
    m_CuisineTypeLabel.frame=CGRectMake(186, 80+AdOffset, 60, 20);
    m_FacebookButton.frame=CGRectMake(26, 157+AdOffset, 92, 24.5);
    m_FacebookImageView.frame=CGRectMake(96, 162+AdOffset, 17, 17.5);
    m_TitleLabel.frame=CGRectMake(128, 28+AdOffset, 165, 20);
    m_FavoriteButton.hidden=NO;
  }
  
  if(isFirst)
  {
    if(!isFav)
    {
      [self connectToServer];
      m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200+AdOffset, 30, 30)];
      [m_activityIndicator startAnimating];
      m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
      [scrollView addSubview:m_activityIndicator];
      m_FavoriteButton.userInteractionEnabled=NO;
    }
    else
    {
      [self getFromDataBase];  
      m_FavoriteButton.userInteractionEnabled=YES;
      m_FavouriteImageView.hidden=NO;
    }
  }
  isFirst=NO;
    
    [self.view addSubview:scrollView];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

-(void)setPortraitframe
{
    int shiftFactor = 30;
    if(isInd)
    {
        
        m_WaitTimeLabel.frame=CGRectMake(252, 107, 45, 29);
        m_ImageView.frame=CGRectMake(26, 40, 92,104);
        m_DescriptionView.frame=CGRectMake(18, 245+25, 265, 120);
        m_CallNumberLabel.frame=CGRectMake(160, 191+30, 120, 20);
        m_CallButton.frame=CGRectMake(25, 193+25, 270, 28);
        m_DollarLabel.frame=CGRectMake(127, 150+30, 30, 12);
        m_TableServiceLabel.frame=CGRectMake(160, 150+30, 120, 12);
        m_TableServiceCostLabel.frame=CGRectMake(127, 162+30, 150, 20);
        m_ReservationButton.frame=CGRectMake(210, 121+40, 18,16);
        m_CuisineTypeLabel.frame=CGRectMake(186, 140,100, 20);
        m_FacebookButton.frame=CGRectMake(26, 156+30, 92, 24.5);
        m_FacebookImageView.frame=CGRectMake(100, 159+34, 17, 17.5);
        m_TitleLabel.frame=CGRectMake(128,80, 165, 20);
        m_FavoriteButton.hidden=YES;
        m_FavouriteImageView.hidden=YES;
    }
    else
    {
        m_WaitTimeLabel.frame=CGRectMake(247,97, 45, 29);
        m_ImageView.frame=CGRectMake(26, 82, 90,88);
        m_DescriptionView.frame=CGRectMake(18, 240, 280, 100);
        m_CallNumberLabel.frame=CGRectMake(195, 185+23, 120, 20);
        m_CallButton.frame=CGRectMake(130, 180, 165, 28);
        m_DollarLabel.frame=CGRectMake(127, 142+shiftFactor, 30, 12);
        m_TableServiceLabel.frame=CGRectMake(160, 142+shiftFactor, 120, 12);
        m_TableServiceCostLabel.frame=CGRectMake(128, 150+shiftFactor, 150, 20);
        m_ReservationButton.frame=CGRectMake(205, 113+40, 18,16);
        m_CuisineTypeLabel.frame=CGRectMake(186, 93+37, 100, 20);
        m_FacebookButton.frame=CGRectMake(26, 185, 92, 24.5);
        m_FacebookImageView.frame=CGRectMake(96, 188+25, 17, 17.5);
        m_TitleLabel.frame=CGRectMake(128, 32+40, 165, 20);
        m_FavoriteButton.hidden=NO;
        m_FavouriteImageView.frame = CGRectMake(93, 149+shiftFactor, 20.5, 21.5);
    }
}

-(void)setLandscapeFrame
{
    if(isInd)
    {
        [self manageWaitTimeLavel];
      //  m_WaitTimeLabel.frame = CGRectMake(244, 62, 45, 29);
        m_ImageView.frame=CGRectMake(34, 40, 140+1,104);
        m_DescriptionView.frame=CGRectMake(18, 255, 460, 120);
        m_CallNumberLabel.frame=CGRectMake(240, 191+30, 120, 20);
        m_CallButton.frame=CGRectMake(25, 193+25, 430, 28);
        m_DollarLabel.frame=CGRectMake(193, 150+30, 30, 12);
        m_TableServiceLabel.frame=CGRectMake(255, 150+30, 120, 12);
        m_TableServiceCostLabel.frame=CGRectMake(200, 162+30, 150, 20);
        m_ReservationButton.frame=CGRectMake(370, 120+33, 18,16);
        m_CuisineTypeLabel.frame=CGRectMake(320, 99+35, 100, 20);
        m_FacebookButton.frame=CGRectMake(30, 156+33, 140, 24.5);
        m_FacebookImageView.frame=CGRectMake(160, 159+34, 17, 17.5);
        m_TitleLabel.frame=CGRectMake(200,80, 250, 20);
        m_FavoriteButton.hidden=YES;
        m_FavouriteImageView.hidden=YES;
        
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            m_ImageView.frame=CGRectMake(39, 40, 168,104);
            m_DescriptionView.frame=CGRectMake(18, 245, 265, 120);
            m_CallNumberLabel.frame=CGRectMake(285, 191, 120, 20);
            m_CallButton.frame=CGRectMake(25, 193, 480, 28);
            m_DollarLabel.frame=CGRectMake(228, 150, 30, 12);
            m_TableServiceLabel.frame=CGRectMake(255, 150, 120, 12);
            m_TableServiceCostLabel.frame=CGRectMake(230, 162, 150, 20);
            m_ReservationButton.frame=CGRectMake(370, 120, 18,16);
            m_CuisineTypeLabel.frame=CGRectMake(320, 99, 100, 20);
            m_FacebookButton.frame=CGRectMake(26, 156, 92, 24.5);
            m_FacebookImageView.frame=CGRectMake(175, 159, 17, 17.5);
            m_TitleLabel.frame=CGRectMake(225,34, 250, 20);
            m_FavoriteButton.hidden=YES;
            m_FavouriteImageView.hidden=YES;
        }
    }
    else
    {
        [self manageWaitTimeLavel];
        m_ImageView.frame=CGRectMake(34, 82, 140,88);
        m_DescriptionView.frame=CGRectMake(18, 255, 440, 100);
        m_CallNumberLabel.frame=CGRectMake(300, 208, 120, 20);
        m_CallButton.frame=CGRectMake(130, 180, 165, 28);
        m_DollarLabel.frame=CGRectMake(193, 142+28, 30, 12);
        // m_TableServiceLabel.frame=CGRectMake(250, 142+40, 120, 12);
        m_TableServiceCostLabel.frame=CGRectMake(193, 150+35, 150, 20);
        m_ReservationButton.frame=CGRectMake(370, 113+38, 18,16);
        m_CuisineTypeLabel.frame=CGRectMake(300, 91+40, 150, 20);
        m_FacebookButton.frame=CGRectMake(26, 185, 92, 24.5);
        m_FacebookImageView.frame=CGRectMake(150, 188+25, 17, 17.5);
        m_TitleLabel.frame=CGRectMake(200, 32+35, 250, 20);
        m_FavoriteButton.hidden=NO;
        m_FavouriteImageView.frame = CGRectMake(136, 149+35, 35, 21.5);
        
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            m_ImageView.frame=CGRectMake(37, 82, 170,88);
            m_DescriptionView.frame=CGRectMake(18, 255, 530, 100);
            m_CallNumberLabel.frame=CGRectMake(380, 208, 120, 20);
            m_CallButton.frame=CGRectMake(130, 180, 165, 28);
            m_DollarLabel.frame=CGRectMake(228, 142+28, 30, 12);
            // m_TableServiceLabel.frame=CGRectMake(250, 142, 120, 12);
            m_TableServiceCostLabel.frame=CGRectMake(228, 150+35, 150, 20);
            m_ReservationButton.frame=CGRectMake(400, 113+38, 18,16);
            m_CuisineTypeLabel.frame=CGRectMake(320, 91+40, 200, 20);
            m_FacebookButton.frame=CGRectMake(26, 185, 92, 24.5);
            m_FacebookImageView.frame=CGRectMake(175, 188+25, 17, 17.5);
            m_TitleLabel.frame=CGRectMake(225, 32+35, 250, 20);
            m_FavoriteButton.hidden=NO;
            m_FavouriteImageView.frame = CGRectMake(162, 149+35, 35, 21.5);
        }
    }
}
- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}

-(void)layoutSubView
{
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (appDelegate.isPortrait) {
        [self setPortraitframe];
        scrollView.scrollEnabled = NO;
        scrollView.contentSize = CGSizeMake(320, 480);
        scrollView.frame = CGRectMake(0, 0, 320, 480);
        pTitleLabel.frame = CGRectMake(70, 8, 160, 30);
        m_SubmitButton.frame=CGRectMake(235, 8, 80.5, 30);
        m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
       
        if (appDelegate.isIndependent) {
             m_DescriptionView.frame=CGRectMake(18, 255, 265, 115);
        }
        pBGImageView.frame=CGRectMake(0, 0, 320, 480);
        [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,320,50)];
         [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 50)];
        if(screenBounds.size.height == 568)pBGImageView.frame=CGRectMake(0, 0, 320, 568);
    }
    else{
        [self setLandscapeFrame];
        scrollView.scrollEnabled = YES;
        scrollView.contentSize = CGSizeMake(480, 500);
        scrollView.frame = CGRectMake(0, 0, 480, 320);
        pTitleLabel.frame = CGRectMake(60, 1,336, 30);
        m_SubmitButton.frame=CGRectMake(395, 1, 80.5, 30);
        m_BackButton.frame=CGRectMake(5, 1, 53.5, 30);
        pBGImageView.frame=CGRectMake(0, 0, 480, 320);
         [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,480,50)];
         [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(373, 50)];
        if (screenBounds.size.height == 568) {
            scrollView.contentSize = CGSizeMake(568, 500);
            scrollView.frame = CGRectMake(0, 0, 568, 320);
            pTitleLabel.frame = CGRectMake(60, 1, 420, 30);
            m_SubmitButton.frame=CGRectMake(480, 1, 80.5, 30);
            pBGImageView.frame=CGRectMake(0, 0, 568, 320);
             [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,568,50)];
             [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(461, 50)];
            
            
        }
        
    }
}


- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}

-(void)submitWaitTimeAction
{
    if(m_Timer)
    {
        [m_Timer invalidate];
        [m_Timer release];
        m_Timer=nil;
    }
    
	[m_SubmitButton removeFromSuperview];
    [m_BackButton removeFromSuperview];
    [pTitleLabel removeFromSuperview];
	if(m_SubmitWaitTimeViewController)
	{
		[m_SubmitWaitTimeViewController.view removeFromSuperview];
		[m_SubmitWaitTimeViewController release];
		m_SubmitWaitTimeViewController=nil;
	}
	m_SubmitWaitTimeViewController=[[SubmitWaitTimeViewController alloc]initWithId:m_RestId Latitude:m_Latitude Longitude:m_Longitude isRide:NO];
	m_SubmitWaitTimeViewController.m_delegate=self;
	[self.navigationController pushViewController:m_SubmitWaitTimeViewController animated:YES];
}

-(void)getFromDataBase
{
  m_FavoriteButton.userInteractionEnabled=YES;
  m_RestuarantFromDb=[[AmusementParkDB sharedPtDatabase]getRestWithRestId:m_RestId];
  m_TitleLabel.text=m_RestuarantFromDb.m_RestName;
  m_CuisineTypeLabel.text=m_RestuarantFromDb.m_CuisineName;
  if(m_RestuarantFromDb.m_Reserve==0)
    [m_ReservationButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/RestDetailPage/cross" ofType:@"png"]] forState:UIControlStateNormal];
  else
    [m_ReservationButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/RestDetailPage/mark" ofType:@"png"]] forState:UIControlStateNormal];
  
  int AdOffset=49;
  
  NSString *str1;
  if([m_RestuarantFromDb.m_RestServiceType isEqualToString:@"Table"])
  {
    m_TableServiceLabel.text=@"Table Service";
    str1=[NSString stringWithFormat:@"(%@ per person)",m_RestuarantFromDb.m_RestCost];
    NSString *str2=[m_RestuarantFromDb.m_RestCost substringToIndex:3];
    NSString *str3=[str2 substringFromIndex:1];
    int cost=[str3 intValue];
    if(cost==5)
    {
      if(isInd)
        m_TableServiceLabel.frame=CGRectMake(145, 150+AdOffset, 120, 12);
      else 
        m_TableServiceLabel.frame=CGRectMake(145, 142+AdOffset, 120, 12);                    
      m_DollarLabel.text=@"$";
    }
    else if(cost==15)
    {
      if(isInd)
        m_TableServiceLabel.frame=CGRectMake(150, 150+AdOffset, 120, 12);
      else 
        m_TableServiceLabel.frame=CGRectMake(150, 142+AdOffset, 120, 12);          
      m_DollarLabel.text=@"$$";
    }
    else if(cost==30)
    {
      if(isInd)
        m_TableServiceLabel.frame=CGRectMake(155, 150+AdOffset, 120, 12);
      else 
        m_TableServiceLabel.frame=CGRectMake(155, 142+AdOffset, 120, 12);          
      m_DollarLabel.text=@"$$$";
    }
    else if(cost==50)
    {
      m_DollarLabel.text=@"$$$$";
      if(isInd)
        m_TableServiceLabel.frame=CGRectMake(160, 150+AdOffset, 120, 12);
      else 
        m_TableServiceLabel.frame=CGRectMake(160, 142+AdOffset, 120, 12);          
      m_DollarLabel.text=@"$$$$";
    }
  }
  else 
  {
    m_DollarLabel.text=@"$";
    if(isInd)
      m_TableServiceLabel.frame=CGRectMake(142, 150+AdOffset, 120, 12);
    else 
      m_TableServiceLabel.frame=CGRectMake(142, 142+AdOffset, 120, 12);
    m_TableServiceLabel.text=@"Quick Service";
    str1=@"($15 and Under per person)";
  }
  m_TableServiceCostLabel.text=str1;
  str1=nil;
  
  int waittime=[m_RestuarantFromDb.m_WaitTime intValue];
  if(waittime<=20)
    m_WaitTimeLabel.backgroundColor=[UIColor colorWithRed:127.0/255 green:190.0/255 blue:0.0/255 alpha:1.0];
  else if(waittime<60)
    m_WaitTimeLabel.backgroundColor=[UIColor colorWithRed:255.0/255 green:223.0/255 blue:0.0/255 alpha:1.0];
  else if(waittime>=60)
    m_WaitTimeLabel.backgroundColor=[UIColor redColor];      
  
  
  int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];
  
  if(waittime==-1 || open==0)
  {
    //(275, 60.5, 26, 26)
    if(isInd)
      m_WaitTimeLabel.frame=CGRectMake(244, 62+AdOffset, 45, 29);
    else 
      m_WaitTimeLabel.frame=CGRectMake(247, 56+AdOffset, 45, 29);
    
    m_WaitTimeLabel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]]];
    m_WaitTimeLabel.text=@"";
  }
  else if(waittime==75)
  {
    if(isInd)
      m_WaitTimeLabel.frame=CGRectMake(249, 60+AdOffset, 42, 30);
    else 
      m_WaitTimeLabel.frame=CGRectMake(250, 55+AdOffset, 42, 30);
    m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",m_RestuarantFromDb.m_WaitTime];
  }
  else 
  {
    if(isInd)
      m_WaitTimeLabel.frame=CGRectMake(263, 63+AdOffset, 30, 30);
    else 
      m_WaitTimeLabel.frame=CGRectMake(264, 55+AdOffset, 30, 30);
    m_WaitTimeLabel.text=m_RestuarantFromDb.m_WaitTime;
  }
  [self manageWaitTimeLavel];
  NSString *str2=[NSString stringWithFormat:@" %@",m_RestuarantFromDb.m_PhonoNo];
  if(m_RestuarantFromDb.m_PhonoNo!=NULL)
  {
    m_CallNumberLabel.text=str2;
    m_CallButton.userInteractionEnabled=YES;
  }
  else
  {
    m_CallNumberLabel.text=@"";
    m_CallButton.userInteractionEnabled=NO;
  }
  m_DescriptionView.text=m_RestuarantFromDb.m_RestDesc;
  
  UIImage *pImage=[[UIImage alloc]initWithContentsOfFile:m_RestuarantFromDb.m_ImageId];
  m_ImageView.image=pImage;
  [pImage release];
  
  [m_RestuarantFromDb retain];
}

-(void)NotificationRecieved
{
  if(!isFav)
  {
    if([[AmusementParkDB sharedPtDatabase]getDuplicateRest:m_Restuarant])
    {
      m_FavouriteImageView.hidden=NO;
    }    
    else 
    {
      m_FavouriteImageView.hidden=YES;
    }
  }
  else 
  {
    if([[AmusementParkDB sharedPtDatabase]getDuplicateRest:m_RestuarantFromDb])
    {
      m_FavouriteImageView.hidden=NO;
    }    
    else 
    {
      m_FavouriteImageView.hidden=YES;
    }    
  }
}

-(void)launchFacebook
{
    NSString *pPark=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"ride"];
    
    
    if(isFav){
        [[NSUserDefaults standardUserDefaults]setObject:m_RestuarantFromDb.m_RestName forKey:@"rideName"];
        [[NSUserDefaults standardUserDefaults]setObject:pPark forKey:@"parkName"];
    }
    // [[FBConnectController sharedFBConnectController]showFaceBookPageWithRide:m_RestuarantFromDb.m_RestName Park:pPark isRide:NO];
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:m_Restuarant.m_RestName forKey:@"rideName"];
        [[NSUserDefaults standardUserDefaults]setObject:pPark forKey:@"parkName"];
    }
    // [[FBConnectController sharedFBConnectController]showFaceBookPageWithRide:m_Restuarant.m_RestName Park:pPark isRide:NO];
    
    
    FBConnectionViewController * connection = [[FBConnectionViewController alloc]initWithNibName:@"FBConnectionViewController" bundle:nil];
    [self presentViewController:connection animated:YES completion:nil];
}


-(void)AddtoFavouriteAction
{
  [[FullVersionUpgrader sharedUpgrader] showAlert];
  
  //  Restuarant *pRest1=[[Restuarant alloc]init];
  //  if([m_RestuarantArray count])
  //  {
  //    pRest1.m_RestId=m_RestId;
  //    pRest1.m_CuisineName=m_Restuarant.m_CuisineName;
  //    pRest1.m_LandId=m_LandId;
  //    pRest1.m_ParkId=m_ParkId;
  //    pRest1.m_StudioId=m_StudioId;
  //    pRest1.m_Reserve=m_Restuarant.m_Reserve;
  //    pRest1.m_RestName=m_Restuarant.m_RestName;
  //    pRest1.m_PhonoNo=m_Restuarant.m_PhonoNo;
  //    pRest1.m_RestDesc=m_Restuarant.m_RestDesc;
  //    pRest1.m_RestCost=m_Restuarant.m_RestCost;
  //    pRest1.m_ImageId=m_ImagePath;
  //    pRest1.m_Distance=m_Distance;
  //    pRest1.m_WaitTime=m_Restuarant.m_WaitTime;
  //    pRest1.m_RestServiceType=m_Restuarant.m_RestServiceType;
  //  }
  //  else 
  //  {
  //    pRest1.m_RestId=m_RestId;
  //    pRest1.m_CuisineName=m_RestuarantFromDb.m_CuisineName;
  //    pRest1.m_LandId=m_LandId;
  //    pRest1.m_ParkId=m_ParkId;
  //    pRest1.m_StudioId=m_StudioId;
  //    pRest1.m_Reserve=m_RestuarantFromDb.m_Reserve;
  //    pRest1.m_RestName=m_RestuarantFromDb.m_RestName;
  //    pRest1.m_PhonoNo=m_RestuarantFromDb.m_PhonoNo;
  //    pRest1.m_RestDesc=m_RestuarantFromDb.m_RestDesc;
  //    pRest1.m_RestCost=m_RestuarantFromDb.m_RestCost;
  //    pRest1.m_WaitTime=m_RestuarantFromDb.m_WaitTime;
  //    pRest1.m_RestServiceType=m_RestuarantFromDb.m_RestServiceType;
  //    pRest1.m_ImageId=m_ImagePath;    
  //    pRest1.m_Distance=m_Distance;
  //  }
  //    if([[AmusementParkDB sharedPtDatabase]getDuplicateRest:pRest1])
  //    {
  //      [[AmusementParkDB sharedPtDatabase]deleteRestFromFavorite:m_RestId];
  //      m_FavouriteImageView.hidden=YES;
  //    }
  //    else
  //    {
  //      [[AmusementParkDB sharedPtDatabase]insertRestuarant:pRest1];
  //      m_FavouriteImageView.hidden=NO;
  //    }  
  //    LandInfo *pLandInfo=[[LandInfo alloc]init];
  //    pLandInfo.m_LandId=pRest1.m_LandId;
  //    pLandInfo.m_LandName=m_LandName;
  //    pLandInfo.m_ParkId=pRest1.m_ParkId;
  //    pLandInfo.m_StudioId=pRest1.m_StudioId;
  //    if([[AmusementParkDB sharedPtDatabase]getDuplicateLand:pLandInfo])
  //    {
  //      
  //    }
  //    else
  //    {
  //      [[AmusementParkDB sharedPtDatabase]insertLand:pLandInfo];
  //    }
  //    [pLandInfo release];
  //    pLandInfo=nil;
  //    [pRest1 release];  
  //    pRest1=nil;    
}


-(void)backAction
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }  
  [m_BackButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
  [m_SubmitButton removeFromSuperview];
  if(m_delegate)
    [m_delegate BackAction];
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)refreshData
{
  [self BackAction];
}

#pragma mark SubmitWaitTimeViewController delegate Methods
-(void)BackAction
{
  int AdOffset=49;
  if(!isFav)
  {
    m_WaitTimeLabel.backgroundColor=[UIColor clearColor];
    m_WaitTimeLabel.text=@"";
    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200+AdOffset, 30, 30)];
    [m_activityIndicator startAnimating];
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    [self.view addSubview:m_activityIndicator];     
    [self connectToServer];
  }
}


-(void)CallAction
{
  //  printf("Call clicked");
  NSString *phoneNumber;
  if(isFav)
  {
    Restuarant *pRest=[[AmusementParkDB sharedPtDatabase]getRestWithRestId:m_RestId];
    phoneNumber = [@"tel://" stringByAppendingString:pRest.m_PhonoNo];
  }
  else 
  {
    Restuarant *pRest=[m_RestuarantArray objectAtIndex:0];
    phoneNumber = [@"tel://" stringByAppendingString:pRest.m_PhonoNo];
  }
  
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}


#pragma mark URLConnectionManager Methods

-(void)connectToServer
{
  m_BackButton.userInteractionEnabled=NO;
  m_CallButton.userInteractionEnabled=NO;
  m_FavoriteButton.userInteractionEnabled=NO;
  m_SubmitButton.userInteractionEnabled=NO;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    
    NSString * urlStr = [NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getrestdetail&restid=%@&lang=%@",m_RestId,[[NSLocale preferredLanguages] objectAtIndex:0]];  //cp
    
	//[request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getrestdetail&restid=%@",m_RestId]]];
    
    [request setURL:[NSURL URLWithString:urlStr]];
	[request setHTTPMethod:@"POST"];
	mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  m_BackButton.userInteractionEnabled=YES;
  m_CallButton.userInteractionEnabled=YES;
  m_FavoriteButton.userInteractionEnabled=YES;
  m_SubmitButton.userInteractionEnabled=YES;
  
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
	//printf("\n %s",[[error localizedDescription] UTF8String]);
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDelegatelocal=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegatelocal showAlertView];
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *filePath = [paths objectAtIndex:0];
		paths=nil;
		filePath = [filePath stringByAppendingPathComponent:@"RestuarantDetail.xml"];
		[mResponseData writeToFile:filePath atomically:YES];
		filePath=nil;
		[mResponseData release];
		mResponseData=nil;
    if([m_RestuarantArray count]){[m_RestuarantArray removeAllObjects];}
    [m_RestuarantArray addObjectsFromArray:[XMLDataParser getRestDetailFromXML]];
    if([m_RestuarantArray count])
    {
      [self downloadImages];
      if(m_Restuarant)
      {
        [m_Restuarant release];
        m_Restuarant=nil;
      }      
      m_Restuarant=[m_RestuarantArray objectAtIndex:0];
      m_Restuarant.m_RestId=m_RestId;
      m_Restuarant.m_LandId=m_LandId;
      m_Restuarant.m_ParkId=m_ParkId;
      m_Restuarant.m_StudioId=m_StudioId;
      [m_Restuarant retain];
      if([[AmusementParkDB sharedPtDatabase]getDuplicateRest:m_Restuarant])
      {
        m_FavouriteImageView.hidden=NO;
      }
      else 
      {
        m_FavouriteImageView.hidden=YES;
      }
      
      m_TitleLabel.text=m_Restuarant.m_RestName;
      m_CuisineTypeLabel.text=m_Restuarant.m_CuisineName;
      
      if(m_Restuarant.m_Reserve==0)
        [m_ReservationButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/RestDetailPage/cross" ofType:@"png"]] forState:UIControlStateNormal];
      else
        [m_ReservationButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/RestDetailPage/mark" ofType:@"png"]] forState:UIControlStateNormal];
      
      int AdOffset=49;
      
      int waittime=[m_Restuarant.m_WaitTime intValue];
      if(waittime<=20)
        m_WaitTimeLabel.backgroundColor=[UIColor colorWithRed:127.0/255 green:190.0/255 blue:0.0/255 alpha:1.0];
      else if(waittime<60)
        m_WaitTimeLabel.backgroundColor=[UIColor colorWithRed:255.0/255 green:223.0/255 blue:0.0/255 alpha:1.0];
      else if(waittime>=60)
        m_WaitTimeLabel.backgroundColor=[UIColor redColor];      
      
      int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];
      
      if(waittime==-1 || open==0)
      {
        //(275, 60.5, 26, 26)
        if(isInd)
          m_WaitTimeLabel.frame=CGRectMake(252, 58+AdOffset, 45, 29);
        else 
          m_WaitTimeLabel.frame=CGRectMake(247, 48+AdOffset, 45, 29);
        
        m_WaitTimeLabel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]]];
        m_WaitTimeLabel.text=@"";
      }
      else if(waittime==75)
      {
        if(isInd)
          m_WaitTimeLabel.frame=CGRectMake(252, 58+AdOffset, 42, 30);
        else 
          m_WaitTimeLabel.frame=CGRectMake(250, 48+AdOffset, 42, 30);
        m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",m_Restuarant.m_WaitTime];
      }
      else 
      {
        if(isInd)
          m_WaitTimeLabel.frame=CGRectMake(252, 58+AdOffset, 30, 30);
        else 
          m_WaitTimeLabel.frame=CGRectMake(264, 48+AdOffset, 30, 30);
        m_WaitTimeLabel.text=m_Restuarant.m_WaitTime;
      }
       //[self manageWaitTimeLavel];
      
      //  printf("\n\nRestuarant serviuce:%s\n\n",[m_Restuarant.m_RestServiceType UTF8String]);
      NSString *str1;
      if([m_Restuarant.m_RestServiceType isEqualToString:@"Table"])
      {
        m_TableServiceLabel.text=@"Table Service";
        str1=[NSString stringWithFormat:@"(%@ per person)",m_Restuarant.m_RestCost];
        NSString *str2=[m_Restuarant.m_RestCost substringToIndex:3];
        NSString *str3=[str2 substringFromIndex:1];
        int cost=[str3 intValue];
        if(cost==5)
        {
          if(isInd)
            m_TableServiceLabel.frame=CGRectMake(145, 132+AdOffset, 120, 12);
          else 
            m_TableServiceLabel.frame=CGRectMake(145, 122+AdOffset, 120, 12);                    
          m_DollarLabel.text=@"$";
        }
        else if(cost==15)
        {
          if(isInd)
            m_TableServiceLabel.frame=CGRectMake(150, 132+AdOffset, 120, 12);
          else 
            m_TableServiceLabel.frame=CGRectMake(150, 122+AdOffset, 120, 12);          
          m_DollarLabel.text=@"$$";
        }
        else if(cost==30)
        {
          if(isInd)
            m_TableServiceLabel.frame=CGRectMake(155, 132+AdOffset, 120, 12);
          else 
            m_TableServiceLabel.frame=CGRectMake(155, 122+AdOffset, 120, 12);          
          m_DollarLabel.text=@"$$$";
        }
        else if(cost==50)
        {
          m_DollarLabel.text=@"$$$$";
          if(isInd)
            m_TableServiceLabel.frame=CGRectMake(160, 132+AdOffset, 120, 12);
          else 
            m_TableServiceLabel.frame=CGRectMake(160, 122+AdOffset, 120, 12);          
          m_DollarLabel.text=@"$$$$";
        }
      }
      else 
      {
        m_DollarLabel.text=@"$";
        if(isInd)
          m_TableServiceLabel.frame=CGRectMake(142, 132+AdOffset, 120, 12);
        else 
          m_TableServiceLabel.frame=CGRectMake(142, 122+AdOffset, 120, 12);
        m_TableServiceLabel.text=@"Quick Service";
        str1=@"($15 and Under per person)";
      }
      m_TableServiceCostLabel.text=str1;
      str1=nil;
      
      NSString *str2=[NSString stringWithFormat:@" %@",m_Restuarant.m_PhonoNo];
      if(m_Restuarant.m_PhonoNo!=NULL)
      {
        m_CallNumberLabel.text=str2;
        m_CallButton.userInteractionEnabled=YES;
      }
      else
      {
        m_CallNumberLabel.text=@"";
        m_CallButton.userInteractionEnabled=NO;
      }
      m_DescriptionView.text=m_Restuarant.m_RestDesc;
    }
	}
    
    [self manageWaitTimeLavel];
}	
-(void)manageWaitTimeLavel
{
    if (!appDelegate.isPortrait) {
        CGRect frame  = m_WaitTimeLabel.frame;
        CGRect frame1 = m_TableServiceLabel.frame ;
        
        
        int xval  = frame.origin.x;
        int xVal = frame1.origin.x;
        
        
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            xval = xval+230;
            xVal = xVal+100;
        }
        else
        {
            xval = xval+150;
            xVal = xVal+70;
        }
        frame.origin.x = xval;
        m_WaitTimeLabel.frame = frame;
        frame1.origin.x = xVal;
        m_TableServiceLabel.frame = frame1;
    }
}
-(void)downloadImages
{
  NSMutableArray *pArray=[[NSMutableArray alloc]init];
  Restuarant *pRestuarant=[m_RestuarantArray objectAtIndex:0];
  RemoteFile *pRemoteFile=[[RemoteFile alloc]init];
  pRemoteFile.m_remoteURL=pRestuarant.m_ImageId;
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *filePath = [paths objectAtIndex:0];
  paths=nil;
  NSString *identifier=[pRestuarant.m_ImageId lastPathComponent];
  NSString *identifier1=[identifier stringByDeletingPathExtension];
  //printf("\n%s\n",[identifier1 UTF8String]);
  filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"Ride%@%@%@",m_LandId,pRestuarant.m_RestId,identifier1]];  
  if(![[NSFileManager defaultManager]fileExistsAtPath:filePath])
  {
    pRemoteFile.m_localFilePath=filePath;
    filePath=nil;
    [pArray addObject:pRemoteFile];
    if(pRestuarant.m_ImageId != NULL)
    {
      m_ImageResourceDownloader=[[ImageResourceDownloader alloc]init];
      m_ImageResourceDownloader.m_delegate=self;
      [m_ImageResourceDownloader downloadImages:pArray];
    }
  } 
  else 
  {
    if(m_ImagePath){[m_ImagePath release];m_ImagePath=nil;}
    m_ImagePath=filePath;
    [m_ImagePath retain];    
    m_FavoriteButton.userInteractionEnabled=YES;
    m_SubmitButton.userInteractionEnabled=YES;
    m_BackButton.userInteractionEnabled=YES;
    if (m_activityIndicator)
    {
      [m_activityIndicator stopAnimating];
      [m_activityIndicator removeFromSuperview];
      [m_activityIndicator release];
      m_activityIndicator=nil;
    }  
    
    UIImage *image=[[UIImage alloc]initWithContentsOfFile:filePath];
    m_ImageView.image=image; 
    [image release];
  }
  [pRemoteFile release];
  [pArray removeAllObjects];
  [pArray release];
}

#pragma mark ImageResourceDownloader delegate methods
- (void)fileDowloadedCompleted:(ImageResourceDownloader*)downloader fileName:(NSString*)name localfile:(NSString *)localfilename
{
  
  UIImage *image=[[UIImage alloc]initWithContentsOfFile:localfilename];
  m_ImageView.image=image;
  [image release];
  
  if(m_ImagePath){[m_ImagePath release];m_ImagePath=nil;}
  m_ImagePath=localfilename;
  [m_ImagePath retain];
}

- (void)batchDowloadCompleted:(ImageResourceDownloader*)downloader
{
  m_FavoriteButton.userInteractionEnabled=YES;
  m_SubmitButton.userInteractionEnabled=YES;
  m_BackButton.userInteractionEnabled=YES;
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }  
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
}

- (void)errorDownloading:(ImageResourceDownloader*)downloader
{  
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDel=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDel showAlertView];
  }
  
  m_SubmitButton.userInteractionEnabled=YES;
  m_FavoriteButton.userInteractionEnabled=YES;
  m_BackButton.userInteractionEnabled=YES;
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
}


- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated
{
  if(m_SubmitWaitTimeViewController)
  {
    [m_SubmitWaitTimeViewController.view removeFromSuperview];
    m_SubmitWaitTimeViewController.m_delegate=nil;
    [m_SubmitWaitTimeViewController release];
    m_SubmitWaitTimeViewController=nil;
  }
 // [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,5,320,50)];

  [super viewDidAppear:animated];
}

-(void)viewDidLoad
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void)viewWillAppear:(BOOL)animated
{
    if(!isFav)
    {
        if(m_Timer)
        {
            [m_Timer invalidate];
            [m_Timer release];
            m_Timer=nil;
        }
        m_Timer=[NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(refreshData) userInfo:nil repeats:YES];
        [m_Timer retain];
    }
    [self layoutSubView];
   
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount)
    {
        
    }
    else
    {
        [self.navigationController.navigationBar addSubview:m_BackButton];
        [self.navigationController.navigationBar addSubview:m_SubmitButton];
        [self.navigationController.navigationBar addSubview:pTitleLabel];
    }
    
    [super viewWillAppear:animated];
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}
- (void)dealloc 
{
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  } 
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }  
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
  if(m_RestuarantArray){[m_RestuarantArray removeAllObjects];[m_RestuarantArray release];m_RestuarantArray=nil;}
  if(m_RestuarantFromDb){[m_RestuarantFromDb release];m_RestuarantFromDb=nil;}
  if(m_ImagePath){[m_ImagePath release];m_ImagePath=nil;}
  if(m_StudioId){[m_StudioId release];m_StudioId=nil;}
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_LandId){[m_LandId release];m_LandId=nil;}
  if(m_RestId){[m_RestId release];m_RestId=nil;}  
  if(m_Latitude){[m_Latitude release];m_Latitude=nil;}
  if(m_Longitude){[m_Longitude release];m_Longitude=nil;}
  if(m_Restuarant){[m_Restuarant release];m_Restuarant=nil;}
  if(m_LandName){[m_LandName release];m_LandName=nil;}
  if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
	if(m_SubmitButton){[m_SubmitButton removeFromSuperview];[m_SubmitButton release];m_SubmitButton=nil;}
  if(m_FavoriteButton){[m_FavoriteButton release];m_FavoriteButton=nil;}
  if(m_CallButton){[m_CallButton release];m_CallButton=nil;}
  if(m_ReservationButton){[m_ReservationButton release];m_ReservationButton=nil;}
  if(m_FacebookButton){[m_FacebookButton release];m_FacebookButton=nil;}
	if(m_ImageView){[m_ImageView removeFromSuperview];[m_ImageView release];m_ImageView=nil;}
  if(m_FavouriteImageView){[m_FavouriteImageView removeFromSuperview];[m_FavouriteImageView release];m_FavouriteImageView=nil;}
  if(m_FacebookImageView){[m_FacebookImageView removeFromSuperview];[m_FacebookImageView release];m_FacebookImageView=nil;}
	if(m_TitleLabel){[m_TitleLabel removeFromSuperview];[m_TitleLabel release];m_TitleLabel=nil;}
  if(m_WaitTimeLabel){[m_WaitTimeLabel removeFromSuperview];[m_WaitTimeLabel release];m_WaitTimeLabel=nil;}
  if(m_CallNumberLabel){[m_CallNumberLabel removeFromSuperview];[m_CallNumberLabel release];m_CallNumberLabel=nil;}
	if(m_CuisineTitleLabel){[m_CuisineTitleLabel removeFromSuperview];[m_CuisineTitleLabel release];m_CuisineTitleLabel=nil;}
	if(m_CuisineTypeLabel){[m_CuisineTypeLabel removeFromSuperview];[m_CuisineTypeLabel release];m_CuisineTypeLabel=nil;}
	if(m_ReservationLabel){[m_ReservationLabel removeFromSuperview];[m_ReservationLabel release];m_ReservationLabel=nil;}
  if(m_DollarLabel){[m_DollarLabel removeFromSuperview];[m_DollarLabel release];m_DollarLabel=nil;}
	if(m_TableServiceLabel){[m_TableServiceLabel removeFromSuperview];[m_TableServiceLabel release];m_TableServiceLabel=nil;}
	if(m_TableServiceCostLabel){[m_TableServiceCostLabel removeFromSuperview];[m_TableServiceCostLabel release];m_TableServiceCostLabel=nil;}
	if(m_DescriptionView){[m_DescriptionView removeFromSuperview];[m_DescriptionView release];m_DescriptionView=nil;}
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
  if(m_SubmitWaitTimeViewController){[m_SubmitWaitTimeViewController.view removeFromSuperview];m_SubmitWaitTimeViewController.m_delegate=nil;[m_SubmitWaitTimeViewController release];m_SubmitWaitTimeViewController=nil;}
  [super dealloc];
}


@end
