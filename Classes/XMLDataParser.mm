//
//  XMLDataParser.m
//  AmusementPark
//
//  Created by HARSHA on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "XMLDataParser.h"
#import "tinyxml.h"
#import "Studio.h"
#import "Park.h"
#import "Land.h"
#import "Ride.h"
#import "Parade.h"
#import "Restuarant.h"
#import "Deals.h"
@implementation XMLDataParser

+(NSArray *)getStudiosFromXml
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"Studio.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
	NSMutableArray	*pStudioArray=nil;	
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
			if(strcmp(rootElement->Value(), "studios")==0)
      {
        pStudioArray=[[[NSMutableArray alloc]init] autorelease];
      TiXmlNode *studioNode=rootElement->FirstChild();
      while (studioNode)
      {
        Studio *pStudio=[[Studio alloc]init];
        if(strcmp(studioNode->Value(),"studio")==0)
        {
          TiXmlNode *pstudioIdNode=studioNode->FirstChild();
          if(pstudioIdNode)
          {
          if(strcmp(pstudioIdNode->Value(), "studioid")==0)
          {
            TiXmlNode *idNode=pstudioIdNode->FirstChild();
            std::string str1 = idNode->Value();
            pStudio.m_StudioId=[NSString stringWithUTF8String:str1.c_str()];
          }
          }
          TiXmlNode *pstudioNameNode=pstudioIdNode->NextSibling();
          if(pstudioNameNode)
          {
           if(strcmp(pstudioNameNode->Value(), "studioname")==0)
          {
            TiXmlNode *nameNode=pstudioNameNode->FirstChild();
            std::string str1 = nameNode->Value();
            pStudio.m_StudioName=[NSString stringWithUTF8String:str1.c_str()];
          }
          }
          TiXmlNode *pIndependentNode=pstudioNameNode->NextSibling();
          if(pIndependentNode)
          {
            if(strcmp(pIndependentNode->Value(), "indendependent")==0)
            {
              TiXmlNode *independentNode=pIndependentNode->FirstChild();
              std::string str1 = independentNode->Value();
              NSString *tempstr=[NSString stringWithUTF8String:str1.c_str()];
              pStudio.independent=[tempstr intValue];
              tempstr=nil;
            }
          }          
          TiXmlNode *pParkNode=pIndependentNode->NextSibling();
            while (pParkNode)
            {
              Park *pPark=[[Park alloc]init];

              if(strcmp(pParkNode->Value(), "park")==0)
              {
                TiXmlNode *pParkInfoNode=pParkNode->FirstChild();
                while (pParkInfoNode)
                {
                  if(strcmp(pParkInfoNode->Value(), "parkid")==0)
                  {
                    TiXmlNode *idNode=pParkInfoNode->FirstChild();
                    if(idNode)
                    {
                    std::string str1=idNode->Value();
                    pPark.m_ParkId=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(pParkInfoNode->Value(), "parkname")==0)
                  {
                    TiXmlNode *nameNode=pParkInfoNode->FirstChild();
                    if(nameNode)
                    {
                    std::string str1=nameNode->Value();
                    pPark.m_ParkName=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(pParkInfoNode->Value(), "parkicon")==0)
                  {
                    TiXmlNode *iconNode=pParkInfoNode->FirstChild();
                    if(iconNode)
                    {
                    std::string str1=iconNode->Value();
                    pPark.m_ParkImageUrl=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(pParkInfoNode->Value(), "parkstart")==0)
                  {
                    TiXmlNode *startNode=pParkInfoNode->FirstChild();
                    if(startNode)
                    {
                    std::string str1=startNode->Value();
                    pPark.m_ParkStartTime=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(pParkInfoNode->Value(), "parkclose")==0)
                  {
                    TiXmlNode *endNode=pParkInfoNode->FirstChild();
                    if(endNode)
                    {
                    std::string str1=endNode->Value();
                    pPark.m_ParkCloseTime=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(pParkInfoNode->Value(), "parkopen")==0)
                  {
                    TiXmlNode *endNode=pParkInfoNode->FirstChild();
                    if(endNode)
                    {
                      std::string str1=endNode->Value();
                      NSString *parkopen=[NSString stringWithUTF8String:str1.c_str()];
                      pPark.m_ParkOpen=[parkopen intValue];
                    }
                  }                  
                  else if(strcmp(pParkInfoNode->Value(), "latitude")==0)
                  {
                    TiXmlNode *latitudeNode=pParkInfoNode->FirstChild();
                    if(latitudeNode)
                    {
                    std::string str1=latitudeNode->Value();
                    pPark.m_ParkLatitude=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(pParkInfoNode->Value(), "longitude")==0)
                  {
                    TiXmlNode *longitudeNode=pParkInfoNode->FirstChild();
                    if(longitudeNode)
                    {
                    std::string str1=longitudeNode->Value();
                    pPark.m_ParkLongitude=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  pParkInfoNode=pParkInfoNode->NextSibling();
                }
              }
              [pStudio.m_ParkArray addObject:pPark];
              [pPark release];
              pParkNode=pParkNode->NextSibling();
            }
          }
         [pStudioArray addObject:pStudio];
         [pStudio release];
         studioNode=studioNode->NextSibling();
        }
      }
    }
  }
  delete xmlDocument;
  NSMutableArray *filteredArray=[[NSMutableArray alloc]init];
  if(IsUniversalStudioFreeWaitTimes)
  {
    for (Studio *studio in pStudioArray)
    {
      if(([studio.m_StudioId compare:@"8"]==0)||([studio.m_StudioId compare:@"17"]==0))
      {
        [filteredArray addObject:studio];
      }
    }
  }
  else if(IsDisneyWorldFreeWaitTimes)
  {
    for (Studio *studio in pStudioArray)
    {
      if(([studio.m_StudioId compare:@"3"]==0)||([studio.m_StudioId compare:@"12"]==0)||([studio.m_StudioId compare:@"20"]==0))
      {
        [filteredArray addObject:studio];
      }
    }
  }
  else
  {
    [filteredArray addObjectsFromArray:pStudioArray];
  }
  return filteredArray;
}

+(NSArray *)getRidesFromXML
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"ParkRides.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
	NSMutableArray	*pParkArray=nil;	
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
			if(strcmp(rootElement->Value(), "lands")==0)
      {
        pParkArray=[[[NSMutableArray alloc]init] autorelease];
        TiXmlNode *landNode=rootElement->FirstChild();
        while (landNode)
        {
          Land *pLand=[[Land alloc]init];
          if(strcmp(landNode->Value(), "land")==0)
          {
            TiXmlNode *LandinfoNode=landNode->FirstChild();
            while (LandinfoNode)
            {
              if(strcmp(LandinfoNode->Value(), "landid")==0)
              {
                TiXmlNode *landIdNode=LandinfoNode->FirstChild();
                if(landIdNode)
                {
                std::string str1=landIdNode->Value();
                pLand.m_LandId=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(LandinfoNode->Value(), "landname")==0)
              {
                TiXmlNode *landnameNode=LandinfoNode->FirstChild();
                if(landnameNode)
                {
                std::string str1=landnameNode->Value();
                pLand.m_LandName=[NSString stringWithUTF8String:str1.c_str()];                
                }
              }
              else if(strcmp(LandinfoNode->Value(), "ride")==0)
              {
                Ride *pRide=[[Ride alloc]init];
                TiXmlNode *rideNode=LandinfoNode->FirstChild();
                while (rideNode)
                {
                  if(strcmp(rideNode->Value(), "rideid")==0)
                  {
                    TiXmlNode *rideIdNode=rideNode->FirstChild();
                    if(rideIdNode)
                    {
                    std::string str1=rideIdNode->Value();
                    pRide.m_RideId=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(rideNode->Value(), "ridename")==0)
                  {
                    TiXmlNode *ridenameNode=rideNode->FirstChild();
                    if(ridenameNode)
                    {
                    std::string str1=ridenameNode->Value();
                    pRide.m_RideName=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(rideNode->Value(), "ridewaittime")==0)
                  {
                    TiXmlNode *rideWaitNode=rideNode->FirstChild();
                    if(rideWaitNode)
                    {
                    std::string str1=rideWaitNode->Value();
                    pRide.m_WaitTime=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(rideNode->Value(), "longitude")==0)
                  {
                    TiXmlNode *rideLongNode=rideNode->FirstChild();
                    if(rideLongNode)
                    {
                    std::string str1=rideLongNode->Value();
                    pRide.m_Longitude=[NSString stringWithUTF8String:str1.c_str()];
                      //printf("\nLongitude:%s\t",[pRide.m_Longitude UTF8String]);
                    }
                  }
                  else if(strcmp(rideNode->Value(), "latitude")==0)
                  {
                    TiXmlNode *rideLatNode=rideNode->FirstChild();
                    if(rideLatNode)
                    {
                    std::string str1=rideLatNode->Value();
                    pRide.m_Latitude=[NSString stringWithUTF8String:str1.c_str()];
                     // printf("Latitude:%s\n",[pRide.m_Latitude UTF8String]);
                    }
                  }                  
                  rideNode=rideNode->NextSibling();
                }
                [pLand.m_RideArray addObject:pRide];
                [pRide release];
              }
              LandinfoNode=LandinfoNode->NextSibling();
            }
          
          }
          [pParkArray addObject:pLand];
          [pLand release];
          landNode=landNode->NextSibling();
        }
      }
    }
  }
  delete xmlDocument;
  return pParkArray;
}

+(NSArray *)getRideDetailFromXML
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"RidesDetail.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
	NSMutableArray	*pRideArray=nil;
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
			if(strcmp(rootElement->Value(), "rides")==0)
      {
        TiXmlNode *rideNode=rootElement->FirstChild();
         while (rideNode)
         {
           pRideArray=[[[NSMutableArray alloc]init] autorelease];
           Ride *pRide=[[Ride alloc]init];
           if(strcmp(rideNode->Value(), "ride")==0)
           {
             TiXmlNode *rideInfoNode=rideNode->FirstChild();
             while (rideInfoNode)
             {
                if(strcmp(rideInfoNode->Value(), "rideid")==0)
                {
                  TiXmlNode *rideidNode=rideInfoNode->FirstChild();
                  if(rideInfoNode)
                  {
                  std::string str1=rideidNode->Value();
                  pRide.m_RideId=[NSString stringWithUTF8String:str1.c_str()];
                  }
                }
                else if(strcmp(rideInfoNode->Value(), "ridename")==0)
                {
                  TiXmlNode *ridenameNode=rideInfoNode->FirstChild();
                  if(ridenameNode)
                  {
                  std::string str1=ridenameNode->Value();
                  pRide.m_RideName=[NSString stringWithUTF8String:str1.c_str()];
                  }
                }
                else if(strcmp(rideInfoNode->Value(), "rideicon")==0)
                {
                  TiXmlNode *rideiconNode=rideInfoNode->FirstChild();
                  if(rideiconNode)
                  {
                  std::string str1=rideiconNode->Value();
                  pRide.m_ImageId=[NSString stringWithUTF8String:str1.c_str()];
                  }
                }
                else if(strcmp(rideInfoNode->Value(), "ridewaittime")==0)
                {
                  TiXmlNode *ridewaitNode=rideInfoNode->FirstChild();
                  if(ridewaitNode)
                  {
                  std::string str1=ridewaitNode->Value();
                  pRide.m_WaitTime=[NSString stringWithUTF8String:str1.c_str()];
                  }
                }
                else if(strcmp(rideInfoNode->Value(), "ridedesc")==0)
                {
                  TiXmlNode *ridedescNode=rideInfoNode->FirstChild();
                  if(ridedescNode)
                  {
                  std::string str1=ridedescNode->Value();
                  pRide.m_Description=[NSString stringWithUTF8String:str1.c_str()];
                  }
                }
             else if(strcmp(rideInfoNode->Value(), "longitude")==0)
             {
               TiXmlNode *ridelongNode=rideInfoNode->FirstChild();
               if(ridelongNode)
               {
                 std::string str1=ridelongNode->Value();
                 pRide.m_Longitude=[NSString stringWithUTF8String:str1.c_str()];
               }
             }
           else if(strcmp(rideInfoNode->Value(), "latitude")==0)
           {
             TiXmlNode *ridelatNode=rideInfoNode->FirstChild();
             if(ridelatNode)
             {
               std::string str1=ridelatNode->Value();
               pRide.m_Latitude=[NSString stringWithUTF8String:str1.c_str()];
             }
           }
           else if(strcmp(rideInfoNode->Value(), "ridelength")==0)
           {
             TiXmlNode *ridelengNode=rideInfoNode->FirstChild();
             if(ridelengNode)
             {
               std::string str1=ridelengNode->Value();
               pRide.m_RideLength=[NSString stringWithUTF8String:str1.c_str()];
             }
           }
           else if(strcmp(rideInfoNode->Value(), "fastpass")==0)
           {
             TiXmlNode *ridefastNode=rideInfoNode->FirstChild();
             if(ridefastNode)
             {
               std::string str1=ridefastNode->Value();
               pRide.m_FastPass=[NSString stringWithUTF8String:str1.c_str()];
             }
           }
           else if(strcmp(rideInfoNode->Value(), "height")==0)
           {
             TiXmlNode *rideheightNode=rideInfoNode->FirstChild();
             if(rideheightNode)
             {
               std::string str1=rideheightNode->Value();
               NSString *str=[NSString stringWithUTF8String:str1.c_str()];
               if([str isEqualToString:@"None"])
                 pRide.m_Height=100000;
               else 
                 pRide.m_Height=[str intValue];
             }
           }
           else if(strcmp(rideInfoNode->Value(), "age")==0)
           {
             TiXmlNode *rideageNode=rideInfoNode->FirstChild();
             if(rideageNode)
             {
               std::string str1=rideageNode->Value();
               NSString *ageStr=[NSString stringWithUTF8String:str1.c_str()];
               if([ageStr isEqualToString:@"All Ages"])
                 pRide.m_Age=100000;
               else 
                 pRide.m_Age=[ageStr intValue];
             }
           }
               rideInfoNode=rideInfoNode->NextSibling();
             }
           }
           [pRideArray addObject:pRide];
           [pRide release];
           rideNode=rideNode->NextSibling();
         }
      }
    }
  }
  delete xmlDocument;
  return pRideArray;
}

+(NSArray *)getParadeFromXML
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"ParadeDetail.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
	NSMutableArray	*pParadeArray=nil;	
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
      if(strcmp(rootElement->Value(), "parades")==0)
      {
        pParadeArray=[[[NSMutableArray alloc]init] autorelease];
        TiXmlNode *pParadeNode=rootElement->FirstChild();
        while (pParadeNode)
        {
          if(strcmp(pParadeNode->Value(), "parade")==0)
          {
            Parade *pParade=[[Parade alloc]init];
            TiXmlNode *pParadeInfoNode=pParadeNode->FirstChild();
            while (pParadeInfoNode)
            {
              if(strcmp(pParadeInfoNode->Value(), "paradeid")==0)
              {
                TiXmlNode *paradeIdNode=pParadeInfoNode->FirstChild();
                if(paradeIdNode)
                {
                std::string str1=paradeIdNode->Value();
                pParade.m_ParadeId=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(pParadeInfoNode->Value(), "paradename")==0)
              {
                TiXmlNode *paradenameNode=pParadeInfoNode->FirstChild();
                if(paradenameNode)
                {
                std::string str1=paradenameNode->Value();
                pParade.m_ParadeName=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(pParadeInfoNode->Value(), "paradelocation")==0)
              {
                TiXmlNode *paradelocationNode=pParadeInfoNode->FirstChild();
                if(paradelocationNode)
                {
                std::string str1=paradelocationNode->Value();
                pParade.m_ParadeLocation=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(pParadeInfoNode->Value(), "paradetype")==0)
              {
                TiXmlNode *paradetypeNode=pParadeInfoNode->FirstChild();
                if(paradetypeNode)
                {
                std::string str1=paradetypeNode->Value();
                pParade.m_Paradetype=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(pParadeInfoNode->Value(), "longitude")==0)
              {
                TiXmlNode *paradelongNode=pParadeInfoNode->FirstChild();
                if(paradelongNode)
                {
                std::string str1=paradelongNode->Value();
                pParade.m_Longitude=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(pParadeInfoNode->Value(), "latitude")==0)
              {
                TiXmlNode *paradelatiNode=pParadeInfoNode->FirstChild();
                if(paradelatiNode)
                {
                std::string str1=paradelatiNode->Value();
                pParade.m_Latitude=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(pParadeInfoNode->Value(), "start")==0)
              {
                TiXmlNode *paradestartNode=pParadeInfoNode->FirstChild();
                if(paradestartNode)
                {
                std::string str1=paradestartNode->Value();
                pParade.m_StartTime=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(pParadeInfoNode->Value(), "end")==0)
              {
                TiXmlNode *paradeEndNode=pParadeInfoNode->FirstChild();
                if(paradeEndNode)
                {
                std::string str1=paradeEndNode->Value();
                pParade.m_EndTime=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(pParadeInfoNode->Value(), "parkstart")==0)
              {
                TiXmlNode *parkstartNode=pParadeInfoNode->FirstChild();
                if(parkstartNode)
                {
                std::string str1=parkstartNode->Value();
                pParade.m_ParkStartTime=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              pParadeInfoNode=pParadeInfoNode->NextSibling();
            }
            [pParadeArray addObject:pParade];
            [pParade release];
            pParade=nil;
          }
          pParadeNode=pParadeNode->NextSibling();
        }
      }
    }
  }
  delete xmlDocument;
  return pParadeArray;
}


+(NSArray *)getRestuarantsFromXML
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"ParkRestuarants.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
	NSMutableArray	*pParkArray=nil;	
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
			if(strcmp(rootElement->Value(), "lands")==0)
      {
        pParkArray=[[[NSMutableArray alloc]init] autorelease];
        TiXmlNode *landNode=rootElement->FirstChild();
        while (landNode)
        {
          Land *pLand=[[Land alloc]init];
          if(strcmp(landNode->Value(), "land")==0)
          {
            TiXmlNode *LandinfoNode=landNode->FirstChild();
            while (LandinfoNode)
            {
              if(strcmp(LandinfoNode->Value(), "landid")==0)
              {
                TiXmlNode *landIdNode=LandinfoNode->FirstChild();
                if(landIdNode)
                {
                std::string str1=landIdNode->Value();
                pLand.m_LandId=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(LandinfoNode->Value(), "landname")==0)
              {
                TiXmlNode *landnameNode=LandinfoNode->FirstChild();
                if(landnameNode)
                {
                std::string str1=landnameNode->Value();
                pLand.m_LandName=[NSString stringWithUTF8String:str1.c_str()];                
                }
              }
              else if(strcmp(LandinfoNode->Value(), "restaurant")==0)
              {
                Restuarant *pRestuarant=[[Restuarant alloc]init];
                TiXmlNode *restNode=LandinfoNode->FirstChild();
                while (restNode)
                {
                  if(strcmp(restNode->Value(), "restid")==0)
                  {
                    TiXmlNode *restIdNode=restNode->FirstChild();
                    if(restIdNode)
                    {
                    std::string str1=restIdNode->Value();
                    pRestuarant.m_RestId=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(restNode->Value(), "restname")==0)
                  {
                    TiXmlNode *restnameNode=restNode->FirstChild();
                    if(restnameNode)
                    {
                    std::string str1=restnameNode->Value();
                    pRestuarant.m_RestName=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(restNode->Value(), "resticon")==0)
                  {
                    TiXmlNode *resticonNode=restNode->FirstChild();
                    if(resticonNode)
                    {
                    std::string str1=resticonNode->Value();
                    pRestuarant.m_ImageId=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(restNode->Value(), "restdesc")==0)
                  {
                    TiXmlNode *restdescNode=restNode->FirstChild();
                    if(restdescNode)
                    {
                    std::string str1=restdescNode->Value();
                    pRestuarant.m_RestDesc=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(restNode->Value(), "service")==0)
                  {
                    TiXmlNode *restServiceNode=restNode->FirstChild();
                    if(restServiceNode)
                    {
                      std::string str1=restServiceNode->Value();
                      pRestuarant.m_RestServiceType=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(restNode->Value(), "waittime")==0)
                  {
                    TiXmlNode *waittimeNode=restNode->FirstChild();
                    if(waittimeNode)
                    {
                      std::string str1=waittimeNode->Value();
                      pRestuarant.m_WaitTime=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(restNode->Value(), "longitude")==0)
                  {
                    TiXmlNode *restlongNode=restNode->FirstChild();
                    if(restlongNode)
                    {
                    std::string str1=restlongNode->Value();
                    pRestuarant.m_Longitude=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  else if(strcmp(restNode->Value(), "latitude")==0)
                  {
                    TiXmlNode *restlatNode=restNode->FirstChild();
                    if(restlatNode)
                    {
                    std::string str1=restlatNode->Value();
                    pRestuarant.m_Latitude=[NSString stringWithUTF8String:str1.c_str()];
                    }
                  }
                  
                  restNode=restNode->NextSibling();
                }
                [pLand.m_RestArray addObject:pRestuarant];
                [pRestuarant release];
              }
              LandinfoNode=LandinfoNode->NextSibling();
            }
            
          }
          [pParkArray addObject:pLand];
          [pLand release];
          landNode=landNode->NextSibling();
        }
      }
    }
  }
  delete xmlDocument;
  return pParkArray;
}

+(NSArray *)getRestDetailFromXML
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"RestuarantDetail.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
	NSMutableArray	*pRestArray=nil;	
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
			if(strcmp(rootElement->Value(), "restaurants")==0)
      {
        TiXmlNode *restNode=rootElement->FirstChild();
        while (restNode)
        {
          Restuarant *pRestuarant=[[Restuarant alloc]init];
          pRestArray=[[[NSMutableArray alloc]init] autorelease];
          if(strcmp(restNode->Value(), "restaurant")==0)
          {
            TiXmlNode *restInfoNode=restNode->FirstChild();
            while (restInfoNode)
            {
              if(strcmp(restInfoNode->Value(), "restid")==0)
              {
                TiXmlNode *restidNode=restInfoNode->FirstChild();
                if(restidNode)
                {
                std::string str1=restidNode->Value();
                pRestuarant.m_RestId=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "restname")==0)
              {
                TiXmlNode *restnameNode=restInfoNode->FirstChild();
                if(restnameNode)
                {
                std::string str1=restnameNode->Value();
                pRestuarant.m_RestName=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "resticon")==0)
              {
                TiXmlNode *resticonNode=restInfoNode->FirstChild();
                if(resticonNode)
                {
                std::string str1=resticonNode->Value();
                pRestuarant.m_ImageId=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "restcuisine")==0)
              {
                TiXmlNode *restCuisineNode=restInfoNode->FirstChild();
                if(restCuisineNode)
                {
                std::string str1=restCuisineNode->Value();
                pRestuarant.m_CuisineName=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "restreserve")==0)
              {
                TiXmlNode *restreserveNode=restInfoNode->FirstChild();
                if(restreserveNode)
                {
                std::string str1=restreserveNode->Value();
                NSString *str=[NSString stringWithUTF8String:str1.c_str()];
                pRestuarant.m_Reserve=[str intValue];
                }
              }
              else if(strcmp(restInfoNode->Value(), "restcost")==0)
              {
                TiXmlNode *restcostNode=restInfoNode->FirstChild();
                if(restcostNode)
                {
                std::string str1=restcostNode->Value();
                pRestuarant.m_RestCost=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "service")==0)
              {
                TiXmlNode *restServiceNode=restInfoNode->FirstChild();
                if(restServiceNode)
                {
                  std::string str1=restServiceNode->Value();
                  pRestuarant.m_RestServiceType=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "waittime")==0)
              {
                TiXmlNode *waittimeNode=restInfoNode->FirstChild();
                if(waittimeNode)
                {
                  std::string str1=waittimeNode->Value();
                  pRestuarant.m_WaitTime=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "restcontact")==0)
              {
                TiXmlNode *restcontactNode=restInfoNode->FirstChild();
                if(restcontactNode)
                {
                std::string str1=restcontactNode->Value();
                pRestuarant.m_PhonoNo=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "restdesc")==0)
              {
                TiXmlNode *restdescNode=restInfoNode->FirstChild();
                if(restdescNode)
                {
                std::string str1=restdescNode->Value();
                pRestuarant.m_RestDesc=[NSString stringWithUTF8String:str1.c_str()];
                }
              }              
              restInfoNode=restInfoNode->NextSibling();
            }
          }
          [pRestArray addObject:pRestuarant];
          [pRestuarant release];pRestuarant=nil;
          restNode=restNode->NextSibling();
        }
      }
    }
  }
  delete xmlDocument;
  return pRestArray;
}

+(int)getWaitTimeResponse
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"Response.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
  int response;
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
			if(strcmp(rootElement->Value(), "updated")==0)
      {
        TiXmlNode *responseNode=rootElement->FirstChild();
        if(responseNode)
        {
        std::string str1=responseNode->Value();
        NSString *responseString=[NSString stringWithUTF8String:str1.c_str()];
        response=[responseString intValue];
        }
      }
    }
  }
  delete xmlDocument;
  return response;
}

+(NSArray *)getDealsFromXml
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"Deals.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
	NSMutableArray	*pDealsArray=nil;	
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
			if(strcmp(rootElement->Value(), "deals")==0)
      {
        pDealsArray=[[[NSMutableArray alloc]init] autorelease];
        TiXmlNode *dealNode=rootElement->FirstChild();
        while (dealNode)
        {
          Deals *pDeal=[[Deals alloc]init];
          if(strcmp(dealNode->Value(), "deal")==0)
          {
            TiXmlNode *dealInfoNode=dealNode->FirstChild();
            while (dealInfoNode)
            {
              if(strcmp(dealInfoNode->Value(), "dealname")==0)
              {
                TiXmlNode *dealnameNode=dealInfoNode->FirstChild();
                if(dealnameNode)
                {
                  std::string str1=dealnameNode->Value();
                  pDeal.m_Title=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(dealInfoNode->Value(), "dealdetails")==0)
              {
                TiXmlNode *dealdescName=dealInfoNode->FirstChild();
                if(dealdescName)
                {
                  std::string str1=dealdescName->Value();
                  pDeal.m_Desc=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              dealInfoNode=dealInfoNode->NextSibling();
            }
          }
          [pDealsArray addObject:pDeal];
          [pDeal release];
          pDeal=nil;
          dealNode=dealNode->NextSibling();
        }
      }
    }
  }
  delete xmlDocument;
  return pDealsArray;
}

+(NSArray *)getIndependentRestuarantsFromXML
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *filePath = [paths objectAtIndex:0];
	paths=nil;
	filePath = [filePath stringByAppendingPathComponent:@"IndependentRestuarants.xml"];	
	TiXmlDocument *xmlDocument=new TiXmlDocument([filePath UTF8String]);
	filePath=nil;
	NSMutableArray	*pRestArray=nil;	
  if(xmlDocument->LoadFile()==false	)
	{
		printf("error in loading xml file\n");
	}
	else
	{
		TiXmlElement *rootElement=xmlDocument->RootElement();
		if(rootElement==NULL)
		{
			printf("Root Node not found\n");
		}
    else
		{
			if(strcmp(rootElement->Value(), "restaurants")==0)
      {
        pRestArray=[[[NSMutableArray alloc]init] autorelease];
        TiXmlNode *restNode=rootElement->FirstChild();
        while (restNode)
        {
          if(strcmp(restNode->Value(), "restaurant")==0)
          {
            Restuarant *pRestuarant=[[Restuarant alloc]init];
            TiXmlNode *restInfoNode=restNode->FirstChild();
            while (restInfoNode)
            {
              if(strcmp(restInfoNode->Value(), "restid")==0)
              {
                TiXmlNode *restidNode=restInfoNode->FirstChild();
                if(restidNode)
                {
                  std::string str1=restidNode->Value();
                  pRestuarant.m_RestId=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "restname")==0)
              {
                TiXmlNode *restnameNode=restInfoNode->FirstChild();
                if(restnameNode)
                {
                  std::string str1=restnameNode->Value();
                  pRestuarant.m_RestName=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "resticon")==0)
              {
                TiXmlNode *resticonNode=restInfoNode->FirstChild();
                if(resticonNode)
                {
                  std::string str1=resticonNode->Value();
                  pRestuarant.m_ImageId=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "service")==0)
              {
                TiXmlNode *restServiceNode=restInfoNode->FirstChild();
                if(restServiceNode)
                {
                  std::string str1=restServiceNode->Value();
                  pRestuarant.m_RestServiceType=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "waittime")==0)
              {
                TiXmlNode *waittimeNode=restInfoNode->FirstChild();
                if(waittimeNode)
                {
                  std::string str1=waittimeNode->Value();
                  pRestuarant.m_WaitTime=[NSString stringWithUTF8String:str1.c_str()];
                }
              }
              else if(strcmp(restInfoNode->Value(), "longitude")==0)
              {
                TiXmlNode *restlongNode=restInfoNode->FirstChild();
                if(restlongNode)
                {
                  std::string str1=restlongNode->Value();
                  pRestuarant.m_Longitude=[NSString stringWithUTF8String:str1.c_str()];
                 // printf("\nLongitude:%s\n",[pRestuarant.m_Longitude UTF8String]);
                }
              }
              else if(strcmp(restInfoNode->Value(), "latitude")==0)
              {
                TiXmlNode *restlatNode=restInfoNode->FirstChild();
                if(restlatNode)
                {
                  std::string str1=restlatNode->Value();
                  pRestuarant.m_Latitude=[NSString stringWithUTF8String:str1.c_str()];
                  //printf("\nLatitude:%s\n",[pRestuarant.m_Latitude UTF8String]);
                }
              }
              else if(strcmp(restInfoNode->Value(), "restdesc")==0)
              {
                TiXmlNode *restdescNode=restInfoNode->FirstChild();
                if(restdescNode)
                {
                  std::string str1=restdescNode->Value();
                  pRestuarant.m_RestDesc=[NSString stringWithUTF8String:str1.c_str()];
                }
              }              
              restInfoNode=restInfoNode->NextSibling();
            }
            [pRestArray addObject:pRestuarant];
            [pRestuarant release];
          }
          restNode=restNode->NextSibling();
        }
      }
    }
  }
  delete xmlDocument;
  return pRestArray;  
}

@end
