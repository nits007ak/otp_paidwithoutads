    //
//  MainViewController.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "MainViewController.h"
#import "DetailViewController.h"
#import "IndependentRestViewController.h"
#import "MainPageCell.h"
#import "XMLDataParser.h"
#import "Studio.h"
#import "Park.h"
#import "AmusementParkAppDelegate.h"
#import "AdsManager.h"
#import "FullVersionUpgrader.h"
#import <iAd/AdBannerView.h>
#import "Chartboost.h"


@implementation MainViewController

-(id)init
{
    if((self=[super init]))
   {
	   m_StudioArray=[[NSMutableArray alloc]init];
     mURLConnection=nil;
     mResponseData=nil;
	   m_DetailViewController=nil;
     m_IndependentRestViewController=nil;
     m_AboutUsViewController=nil;
	   m_TicketInfoButton=nil;
	   m_TableView=nil;
	   m_BGImageView=nil;
     isImage=NO;
     m_ImageResourceDownloader=nil;
     m_activityIndicator=nil;
     mConnectClass=nil;
     isFirst=YES;
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdsRemoved) name:AdRemovedNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AddMap) name:MapAddedNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AdDescription) name:AdDescriptionNotification object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AdTickets) name:AdTicketsNotification object:nil];
   }
	return self;
}

- (void)dealloc 
{
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }  
  if (mConnectClass) {[mConnectClass release];mConnectClass=nil;}	
	if(m_StudioArray){[m_StudioArray removeAllObjects];[m_StudioArray release];m_StudioArray=nil;}
	if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
	if(m_DetailViewController){[m_DetailViewController.view removeFromSuperview];[m_DetailViewController release];m_DetailViewController=nil;}
  if(m_IndependentRestViewController){[m_IndependentRestViewController.view removeFromSuperview];[m_IndependentRestViewController release];m_IndependentRestViewController=nil;}
  if(m_AboutUsViewController){[m_AboutUsViewController.view removeFromSuperview];[m_AboutUsViewController release];m_AboutUsViewController=nil;}
    [super dealloc];
}


-(void)removeVonnection
{
  //printf("removeVonnection 1\n");
  if(mConnectClass)
  {
    mConnectClass.mDelegate=nil;
    [mConnectClass release];
    mConnectClass=nil;
  }
  
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
}

-(void)loadView
{
    
//    [[RevMobAds session] showPopup];
   // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IsInAppPurchaseDone"];
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    Chartboost *cb = [Chartboost sharedChartboost];
    
#ifdef UNIVERSAL_STUDIOS_FREE_RELEASE
    cb.appId = @"51a39ddd17ba47ce37000003";
    cb.appSignature = @"f2ee756f768474b0722dac458ab7fb42728c30c6";
#endif
#ifdef UNIVERSAL_STUDIOS_FREE_DEBUG
    cb.appId = @"51a39ddd17ba47ce37000003";
    cb.appSignature = @"f2ee756f768474b0722dac458ab7fb42728c30c6";
#endif
    
#ifdef DISNEY_WORLD_FREE_DEBUG
    cb.appId = @"51a39da816ba476d19000008";
    cb.appSignature = @"bddfa7ca34662be369dfc1eb8f55f16e8115a5dd";
#endif
#ifdef DISNEY_WORLD_FREE_RELEASE
    cb.appId = @"51a39da816ba476d19000008";
    cb.appSignature = @"bddfa7ca34662be369dfc1eb8f55f16e8115a5dd";
#endif
    
    
    
    // Begin a user session
    [cb startSession];
    
    // Show an interstitial
    [cb showInterstitial];
    
//	[self.navigationController setNavigationBarHidden:YES];
  
  int AdOffset=0;
//  if(!IsFullVersion)
//  {
//    AdOffset=51+12; 
//  }
  
  AdOffset=51+12;
 CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }
	m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
  m_BGImageView.userInteractionEnabled=YES;
	[self.view addSubview:m_BGImageView];
  [m_BGImageView release];
	
  pTitleImageView=[[UIImageView alloc]initWithFrame:CGRectMake(40, 20+AdOffset, 260, 45)];
  pTitleImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"Title" ofType:@"png"]];
  [self.view addSubview:pTitleImageView];
  
	m_TicketInfoButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_TicketInfoButton.frame=CGRectMake(285, 64+AdOffset, 25, 26);
	m_TicketInfoButton.backgroundColor=[UIColor clearColor];
    [m_TicketInfoButton setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin)];   //cp
  [m_TicketInfoButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/info" ofType:@"png"]] forState:UIControlStateNormal];
    
    pImage1=[[UIImageView alloc]initWithFrame:CGRectMake(10, 40+AdOffset, 25, 26)];
    pImage1.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/logo" ofType:@"png"]];
    [pImage1 setUserInteractionEnabled:YES];
    UITapGestureRecognizer * gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked:)];
    [pImage1 addGestureRecognizer:gesture];
    [self.view addSubview:pImage1];

	[m_TicketInfoButton addTarget:self action:@selector(TicketInfoAction) forControlEvents:UIControlEventTouchUpInside];	
  [self.view addSubview:m_TicketInfoButton];
    
	if (screenBounds.size.height == 568) {
        m_TableView = [[UITableView alloc] initWithFrame: CGRectMake(0,90+AdOffset,320,440-AdOffset) style:UITableViewStyleGrouped];
    }
    else{
        m_TableView = [[UITableView alloc] initWithFrame: CGRectMake(0,90+AdOffset,320,350-AdOffset) style:UITableViewStyleGrouped];
    }
    m_TableView.backgroundView=nil;
	m_TableView.backgroundColor=[UIColor clearColor];
	m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.scrollEnabled = YES;
	m_TableView.rowHeight=62;
    m_TableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth);
	[self.view addSubview:m_TableView];
  
  m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 240, 30, 30)];
  [m_activityIndicator startAnimating];
  m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
     m_activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
  [self.view addSubview:m_activityIndicator];
  
  if([[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
  {
    int AdOffset=20;
      pImage1.frame = CGRectMake(10, 64+AdOffset, 25, 26);
    m_TicketInfoButton.frame=CGRectMake(285, 64+AdOffset, 25, 26);
    m_TableView.frame=CGRectMake(0,90+AdOffset,320,380-AdOffset-15);
    pTitleImageView.frame=CGRectMake(30, 20+AdOffset, 260, 45);
      if ([[UIScreen mainScreen]bounds].size.height == 568) {
          m_TableView.frame=CGRectMake(0,90+AdOffset,320,468-AdOffset-15);
      }
  }
    
    
    [self checkOrientation];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}

-(void)layoutSubView
{
    
    CGRect screenBound = [[UIScreen mainScreen]bounds];
    if (appDelegate.isPortrait) {
        m_BGImageView.frame = CGRectMake(0, 0, 320, 480);
        ad.frame = CGRectMake(0, 410, 320, 50);
        [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,320,50)];
         [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213,50)];
        if (screenBound.size.height == 568)
        {
            m_BGImageView.frame = CGRectMake(0, 0, 320, 568);
             ad.frame = CGRectMake(0, 500, 320, 50);
        }
        
        
    }
    else
    {
        
        m_BGImageView.frame = CGRectMake(0, 0, 480, 320);
        ad.frame = CGRectMake(0, 270, 480, 50);
        [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,480,50)];
         [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(373,50)];
        if (screenBound.size.height == 568){
            m_BGImageView.frame = CGRectMake(0, 0, 568,320);
            ad.frame = CGRectMake(0, 270, 568, 50);
            [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,568,50)];
             [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(461,50)];
        }
        
    }
    
}
-(void)tapGestureClicked:(UITapGestureRecognizer *)gesture //cp
{
    NSString * urlStr = @"http://www.appyea.com";
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlStr]];
}


-(void)AdsRemoved
{
  int AdOffset=20;
  m_TicketInfoButton.frame=CGRectMake(285, 64+AdOffset, 25, 26);
  m_TableView.frame=CGRectMake(0,90+AdOffset,320,380-AdOffset-15);
  pTitleImageView.frame=CGRectMake(30, 20+AdOffset, 260, 45);
  
  [[AdsManager sharedAdsManager]RemoveAdBannerAndFullVersionBanner];
  [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,4,320,50)];
  [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 80)];

}

-(void)AddMap
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ADDMAP"];
}

-(void)AdTickets
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdTickets"];
  
}

-(void)AdDescription
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdDescription"];
  
}


-(void)TicketInfoAction
{
  if(m_AboutUsViewController){[m_AboutUsViewController.view removeFromSuperview];[m_AboutUsViewController release];m_AboutUsViewController=nil;}
  m_AboutUsViewController=[[AboutUsViewController alloc]init];
  [self.navigationController pushViewController:m_AboutUsViewController animated:YES];

}

-(void)viewDidAppear:(BOOL)animated
{
    ad = [[RevMobAds session] bannerView];
    //    ad.delegate = self;
    [ad loadAd];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
         ad.frame = CGRectMake(0, 500, 320, 50);
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        ad.frame = CGRectMake(0, 410, 320, 50);
    }
	
    
    
    [self.view addSubview:ad];
    [self.view bringSubviewToFront:ad];
  
    
   
  
  if(m_AboutUsViewController)
  {
    [m_AboutUsViewController.view removeFromSuperview];
    [m_AboutUsViewController release];
    m_AboutUsViewController=nil;
  }
  if(m_DetailViewController)
  {
    [m_DetailViewController.view removeFromSuperview];
    [m_DetailViewController release];
    m_DetailViewController=nil;
  }
  if(m_IndependentRestViewController)
  {
    [m_IndependentRestViewController.view removeFromSuperview];
    [m_IndependentRestViewController release];
    m_IndependentRestViewController=nil;
  }
}



-(void)viewDidLoad
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void)viewWillAppear:(BOOL)animated
{
  [self.navigationController setNavigationBarHidden:YES];
  [self connectToServer];
    [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,320,50)];
    [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213,50)];
     [self layoutSubView];

//  if(!IsFullVersion)
//  {
//    [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self];
//  }
}

#pragma mark URLConnectionManager Methods

-(void)connectToServer
{
  if(isFirst)
  {  
    m_TableView.userInteractionEnabled=NO;
    isFirst=NO;
  }
  
  mConnectClass=[[ConnectionClass alloc]init];
  mConnectClass.mDelegate=self;
  NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Studio.xml"];
  NSString *reqsURL=[NSString stringWithString:@"http://admin.apptasmic.com/xml.php?method=getpark"];
  [mConnectClass connectToServerWithURL:reqsURL saveTo:filePath];
  reqsURL=nil;
  filePath=nil;
}

#pragma mark - NoConnection view methods
-(void)showNoConnectionView
{
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(50, 270, 220, 60)];
    [bgView setBackgroundColor:[UIColor blackColor]];
    [bgView setAlpha:0.5];
    [bgView setTag:123];
    [self.view addSubview:bgView];
    [bgView release];
    
    UILabel *noConnection = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 220, 60)];
    [noConnection setText:@"Cannot connect to internet."];
    [noConnection setTextColor:[UIColor whiteColor]];
    [noConnection setTextAlignment:UITextAlignmentCenter];
    [noConnection setBackgroundColor:[UIColor clearColor]];
    [bgView addSubview:noConnection];
    [noConnection release];
    noConnection = nil;
    bgView = nil;
}

-(void)removeNoConnectionView
{
    [[self.view viewWithTag:123] removeFromSuperview];
}


#pragma mark ConnectionClassDelegate Methods

-(void)responseObtained:(NSString *)docFilePath
{ 
    [self removeNoConnectionView];
    connectionPresent = TRUE;
    if([m_StudioArray count]){[m_StudioArray removeAllObjects];}
    [m_StudioArray addObjectsFromArray:[XMLDataParser getStudiosFromXml]];
		[m_TableView reloadData];
    [self downloadImages];
    homePageDataLoaded = TRUE;
  
  if (mConnectClass) {[mConnectClass release];mConnectClass=nil;}
}  

-(void)connectionFailedWithMessage:(NSString *)msgStr
{
    if(!connectionPresent)
    {
        if(!homePageDataLoaded) //show this overlay only if no home-page data (1 case when app started with no net connection)
        {
            [self removeNoConnectionView];
            [self showNoConnectionView];
        }
        //ping and check connection repeatedly till success
        [self performSelector:@selector(connectToServer) withObject:nil afterDelay:1.0];
    }
    else
    {
        connectionPresent = FALSE;
        m_TableView.userInteractionEnabled=YES;
        
        if (m_activityIndicator)
        {
            [m_activityIndicator stopAnimating];
            [m_activityIndicator removeFromSuperview];
            [m_activityIndicator release];
            m_activityIndicator=nil;
        }      
        
        if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
        {
            AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
            [appDelegate showAlertView];
        }
        if (mConnectClass) {[mConnectClass release];mConnectClass=nil;}
    }
}

-(void)downloadImages
{
  NSMutableArray *pArray=[[NSMutableArray alloc]init];
  for(int i=0;i<[m_StudioArray count];i++)
  {
    Studio *pStudio=[m_StudioArray objectAtIndex:i];
    for(int j=0;j<[pStudio.m_ParkArray count];j++)
    {
      Park *pPark=[pStudio.m_ParkArray objectAtIndex:j];
      RemoteFile *pRemoteFile=[[RemoteFile alloc]init];
      pRemoteFile.m_remoteURL=pPark.m_ParkImageUrl;
      NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
      NSString *filePath = [paths objectAtIndex:0];
      paths=nil;
      filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",pStudio.m_StudioId,pPark.m_ParkId]];
      if(![[NSFileManager defaultManager]fileExistsAtPath:filePath])
      {
        pRemoteFile.m_localFilePath=filePath;
        [pArray addObject:pRemoteFile];
      }
      filePath=nil;
      [pRemoteFile release];
      pRemoteFile=nil;
    }
  }
  m_ImageResourceDownloader=[[ImageResourceDownloader alloc]init];
  m_ImageResourceDownloader.m_delegate=self;
  if([pArray count])
  [m_ImageResourceDownloader downloadImages:pArray];
  else
  {
    m_TableView.userInteractionEnabled=YES;
    if(m_activityIndicator)
    {
      [m_activityIndicator stopAnimating];
      [m_activityIndicator removeFromSuperview];
      [m_activityIndicator release];
      m_activityIndicator=nil;
    }      
  }
  [pArray release];
  pArray=nil;
}

#pragma mark ImageResourceDownloader delegate methods
- (void)fileDowloadedCompleted:(ImageResourceDownloader*)downloader fileName:(NSString*)name localfile:(NSString *)localfilename
{
  [m_TableView reloadData];
}
- (void)batchDowloadCompleted:(ImageResourceDownloader*)downloader
{
  m_TableView.userInteractionEnabled=YES;
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }  
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
}
- (void)errorDownloading:(ImageResourceDownloader*)downloader
{
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate showAlertView];
  }
  
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
}
#pragma mark tableView delegate methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  Studio *pStudio=[m_StudioArray objectAtIndex:section];
  return [pStudio.m_ParkArray count];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [m_StudioArray count];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
  UIView* customView = [[[UIView alloc] initWithFrame:CGRectMake(20, 0.0, 320.0, 20)] autorelease];
  
  UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
  headerLabel.backgroundColor = [UIColor clearColor];
  headerLabel.opaque = NO;
  headerLabel.textColor = [UIColor whiteColor];
  headerLabel.highlightedTextColor = [UIColor whiteColor];
  headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
  headerLabel.frame = CGRectMake(20, 0.0, 320.0, 20);
  
   Studio *pStudio=[m_StudioArray objectAtIndex:section];

  headerLabel.text = pStudio.m_StudioName;
  
  [customView addSubview:headerLabel];
  [headerLabel release];
  headerLabel=nil;
  return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	MainPageCell *cell = (MainPageCell *)[tableView dequeueReusableCellWithIdentifier:@"MainPageCell"];
	if(cell == nil)
	{  
    cell=[[[MainPageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainPageCell"]autorelease];
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.selectionStyle=UITableViewCellSelectionStyleGray;
	}
  
  Studio *pStudio=[m_StudioArray objectAtIndex:indexPath.section];
  
  NSMutableArray *pParkArray=pStudio.m_ParkArray;
  Park *pPark=[pParkArray objectAtIndex:indexPath.row];
  
  cell.m_TitleLabel.text=pPark.m_ParkName; 
  
  if(pPark.m_ParkStartTime!=NULL && pPark.m_ParkCloseTime != NULL)
    cell.m_TImeLabel.text=[NSString stringWithFormat:@"%@ - %@",pPark.m_ParkStartTime,pPark.m_ParkCloseTime];
  else 
    cell.m_TImeLabel.text = @"";
  
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *filePath = [paths objectAtIndex:0];
  paths=nil;
  filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",pStudio.m_StudioId,pPark.m_ParkId]]; 
  UIImage *pImage=[[UIImage alloc]initWithContentsOfFile:filePath];
  cell.m_ImageView.image=pImage;
  [pImage release];
  filePath=nil;
  
	UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, tableView.rowHeight)];
  if([pParkArray count]==1)
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/cellone" ofType:@"png"]];
  else if(indexPath.row==0)
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/uppercell1" ofType:@"png"]];
  else if(indexPath.row==[pParkArray count]-1)
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/downcell1" ofType:@"png"]];
  else
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/middlecell1" ofType:@"png"]];
  
  cell.backgroundView = bgImgView;
  [bgImgView release];
  
  UIImageView *selBgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,320, tableView.rowHeight)];
  if(indexPath.row==0)
    selBgImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/seluppercell1" ofType:@"png"]];
  if(indexPath.row==0&&[pParkArray count]==1)
    selBgImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/selcellone" ofType:@"png"]];
  else if(indexPath.row==[pParkArray count]-1)
    selBgImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/seldowncell1" ofType:@"png"]];

  else if(indexPath.row!=0&&indexPath.row<([pParkArray count]-1))
    selBgImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/MailPage/selmiddlecell1" ofType:@"png"]];

  cell.selectedBackgroundView=selBgImageView;
  [selBgImageView release];
  selBgImageView=nil;
  

	return cell;
}

-(void)viewDidDisappear:(BOOL)animated
{
}

-(NSString *)getBundleName:(NSString *)ParkName
{
  NSString *bundleName=@"";
  float width=0,height=0,divisionFactor=0,maximumZoom=0,heightRatio=0;
  double latitude=0,longitude=0;
  if([ParkName isEqualToString:@"Magic Kingdom®"])
  {
    bundleName=@"MagicKingdom/Map";
    width=1205.0;
    height=794.0;
    latitude=28.42208;
    longitude=-81.58728;    
    divisionFactor=1.0;
    maximumZoom=5.2;
    heightRatio=1.0;
  }
  else if([ParkName isEqualToString:@"Epcot®"])
  {
    bundleName=@"Epcot/Map";
    width=551.0;
    height=468.0;
    latitude=28.37762;
    longitude=-81.55711;
    divisionFactor=3.0;
    maximumZoom=5.0; 
    heightRatio=1.0;
  }  
  else if([ParkName isEqualToString:@"Hollywood Studios™"])
  {
    bundleName=@"Holly/Map";
    width=598.0;
    height=641;
    latitude=28.36365;
    longitude=-81.56578;
    divisionFactor=2.0;
    maximumZoom=5.0;
    heightRatio=1.0;
  }
  else if([ParkName isEqualToString:@"Animal Kingdom®"])
  {
    bundleName=@"AnimalKingdom/Map";
    width=450.0;
    height=542.0;
    latitude=28.368454;
    longitude=-81.598069;
    heightRatio=1.0;
    divisionFactor=3.0;
    maximumZoom=5.0;        
  }
  else if([ParkName isEqualToString:@"Universal Studios Orlando®"])
  {
    bundleName=@"UniversalStudio/Map";
    width=667.0;
    height=766.0;
    latitude=28.48099;
    longitude=-81.47148;
    divisionFactor=1.0;    
    maximumZoom=5.0;
    heightRatio=1.0;
  }
  else if([ParkName isEqualToString:@"Islands of Adventure™"])
  {
    bundleName=@"IOA/Map";
    width=934.0;
    height=740.0;
    latitude=28.47483;
    longitude=-81.47553;
    divisionFactor=1.0;    
    maximumZoom=5.0;
    heightRatio=1.0;    
  }
  else if([ParkName isEqualToString:@"CityWalk® Quick Serve"])
  {
    bundleName=@"UniversalCityWalk/Map";
    width=416.0;
    height=391.0;
    latitude=28.47517;
    longitude=-81.46792;
    divisionFactor=1.0;
    maximumZoom=4.0;
    heightRatio=1.0;
  }
  else if([ParkName isEqualToString:@"CityWalk® Dining"])
  {
      bundleName=@"UniversalCityWalk/Map";
      width=416.0;
      height=391.0;
      latitude=28.47517;
      longitude=-81.46792;
      divisionFactor=1.0;
      maximumZoom=4.0;
      heightRatio=1.0;
  }
  else if([ParkName isEqualToString:@"CityWalk® Bars & Clubs"])
  {
      bundleName=@"UniversalCityWalk/Map";
      width=416.0;
      height=391.0;
      latitude=28.47517;
      longitude=-81.46792;
      divisionFactor=1.0;
      maximumZoom=4.0;
      heightRatio=1.0;
  }
  else if([ParkName isEqualToString:@"Downtown Disney"])
  {
    bundleName=@"DowntownDisney/Map";
    width=1123.0;
    height=662.0;
    latitude=28.37446;
    longitude=-81.52450;
    divisionFactor=1.0;
    maximumZoom=4.0;
    heightRatio=1.0;
  }
  
  [[NSUserDefaults standardUserDefaults]setFloat:width forKey:@"WIDTH"];
  [[NSUserDefaults standardUserDefaults]setFloat:height forKey:@"HEIGHT"];
  [[NSUserDefaults standardUserDefaults]setDouble:latitude forKey:@"LATITUDE"];
  [[NSUserDefaults standardUserDefaults]setDouble:longitude forKey:@"LONGITUDE"];
  [[NSUserDefaults standardUserDefaults]setFloat:divisionFactor forKey:@"DIVIDEVALUE"];
  [[NSUserDefaults standardUserDefaults]setFloat:maximumZoom forKey:@"MAXIMUMZOOM"];
  [[NSUserDefaults standardUserDefaults]setFloat:heightRatio forKey:@"HEIGHTRATIO"];
  return bundleName;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  //check for internet-connection each time a row is selected
  [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"RESETALERTFLAG"];
  [self connectToServer];
    
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
  Studio *pStudio=[m_StudioArray objectAtIndex:indexPath.section];
  
  Park *pPark=[pStudio.m_ParkArray objectAtIndex:indexPath.row];
  
  NSString *selMap=[NSString stringWithFormat:@"%d%d",indexPath.section,indexPath.row];
  [[NSUserDefaults standardUserDefaults]setObject:selMap forKey:@"mapname"];
  


  
    NSString *pParkName=[[self getBundleName:pPark.m_ParkName] retain];
  
    if (pStudio.independent==0) {
        appDelegate.isIndependent = NO;
    }
    else
    {
        appDelegate.isIndependent = YES;
    }
    
    
    [[NSUserDefaults standardUserDefaults]setInteger:2 forKey:@"INDEX"];
    
    [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"SelectdeRideIndex"];
    [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"SelectdeRestIndex"];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(m_DetailViewController)
    {
        [m_DetailViewController.view removeFromSuperview];
        [m_DetailViewController release];
        m_DetailViewController=nil;
    }
    [[NSUserDefaults standardUserDefaults]setObject:pPark.m_ParkName forKey:@"PARKNAME"];
    [[NSUserDefaults standardUserDefaults]setInteger:pPark.m_ParkOpen forKey:@"OPENCLOSE"];
    
    m_DetailViewController=[[DetailViewController alloc]initwithStudioId:pStudio.m_StudioId ParkId:pPark.m_ParkId bundleName:pParkName];
    [self.navigationController pushViewController:m_DetailViewController animated:YES];
    
    
    /*if(pStudio.independent==0)
     {
     [[NSUserDefaults standardUserDefaults]setInteger:2 forKey:@"INDEX"];
     
     [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"SelectdeRideIndex"];
     [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"SelectdeRestIndex"];
     
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
     if(m_DetailViewController)
     {
     [m_DetailViewController.view removeFromSuperview];
     [m_DetailViewController release];
     m_DetailViewController=nil;
     }
     [[NSUserDefaults standardUserDefaults]setObject:pPark.m_ParkName forKey:@"PARKNAME"];
     [[NSUserDefaults standardUserDefaults]setInteger:pPark.m_ParkOpen forKey:@"OPENCLOSE"];
     
     m_DetailViewController=[[DetailViewController alloc]initwithStudioId:pStudio.m_StudioId ParkId:pPark.m_ParkId bundleName:pParkName];
     [self.navigationController pushViewController:m_DetailViewController animated:YES];
     }
     else
     {
     if(m_IndependentRestViewController)
     {
     [m_IndependentRestViewController.view removeFromSuperview];
     [m_IndependentRestViewController release];
     m_IndependentRestViewController=nil;
     }
     [[NSUserDefaults standardUserDefaults]setObject:pPark.m_ParkName forKey:@"PARKNAME"];
     [[NSUserDefaults standardUserDefaults]setInteger:pPark.m_ParkOpen forKey:@"OPENCLOSE"];
     
     m_IndependentRestViewController=[[IndependentRestViewController alloc]initwithStudioId:pStudio.m_StudioId ParkId:pPark.m_ParkId bundleName:pParkName];
     [self.navigationController pushViewController:m_IndependentRestViewController animated:YES];
     
     // IndependentTestoViewController * resto=[[IndependentTestoViewController alloc]initwithStudioId:pStudio.m_StudioId ParkId:pPark.m_ParkId bundleName:pParkName];
     // IndependentTestoViewController * resto = [[IndependentTestoViewController alloc]init];
     //[self.navigationController pushViewController:resto animated:YES];
     
     
     }
     
     */
    [pParkName release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}


#pragma mark -- ADBannerViewDelegate
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
  NSLog(@"%@",banner);
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
  NSLog(@"%@",error);
}
@end
