    //
//  FBConnectController.m
//  FaceBookApp
//
//  Created by Mallikarjun Patil on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FBConnectController.h"


static NSString* kApiKey = @"205813612801096";//  app key
static NSString* kApiSecret =  @"7e20b0557a5dacf1556a4644725edebf";//app secrect key

@implementation FBConnectController

@synthesize session = _session;
@synthesize loginDialog = _loginDialog;
@synthesize facebookName = _facebookName;
@synthesize posting = _posting;
@synthesize delegate;

static FBConnectController*	gFBConnectController = nil;

+ (FBConnectController*)sharedFBConnectController
{
	if(gFBConnectController == nil)
	{
		gFBConnectController = [[FBConnectController alloc] init];
	}
	return gFBConnectController;
} 

- (id)init
{
    if ((self = [super init]))
    {
		_session= nil;
		
    }
    return self;
}



- (void)showFaceBookPageWithRide:(NSString *)pRideName Park:(NSString *)pParkName isRide:(BOOL)isRide
{
	m_RideName=pRideName;
  m_ParkName=pParkName;
  ride=isRide;
		  
	if(!_session)
		_session = [[FBSession sessionForApplication:kApiKey secret:kApiSecret delegate:self] retain];
		
	
	_posting = YES;
	// If we're not logged in, log in first...
	if (![_session isConnected]) 
	{
		self.loginDialog = nil;
		_loginDialog = [[FBLoginDialog alloc] init];	
		[_loginDialog show];	
	}
	// If we have a session and a name, post to the wall!
	else if (_facebookName != nil)
	{
		[self postToWall];
	}
	// Otherwise, we don't have a name yet, just wait for that to come through.
	
	
}
- (void)logoutButtonTapped
{
	[_session logout];
}


#pragma mark FBRequestDelegate methods

- (void)request:(FBRequest*)request didLoad:(id)result
{
	if ([request.method isEqualToString:@"facebook.fql.query"]) 
	{
		NSArray* users = result;
		NSDictionary* user = [users objectAtIndex:0];
		printf("description = %s\n",[[user description] UTF8String]);
		
		NSString* name = [user objectForKey:@"name"];
		self.facebookName = name;
		
		if(delegate)
		[delegate showLogOutButton];
		
		if (_posting) 
		{
			[self postToWall];
			_posting = NO;
		}
	}
}

#pragma mark Post to Wall Helper

- (void)postToWall
{
	FBStreamDialog* dialog = [[[FBStreamDialog alloc] init] autorelease];
	dialog.userMessagePrompt = @"What's on your mind?";
	
  if(ride)
    dialog.message=[NSString stringWithFormat:@"%@ Likes ride %@ in park %@",_facebookName,m_RideName,m_ParkName];
  else 
    dialog.message=[NSString stringWithFormat:@"%@ Likes restaurant %@ in park %@",_facebookName,m_RideName,m_ParkName];
	
  dialog.actionLinks=[NSString stringWithFormat:@"[{\"text\":\"Get Orlando Map Apps!\",\"href\":\"%@\"}]",kAppStoreLink];
	[dialog show];
}


#pragma mark FBSessionDelegate methods

- (void)session:(FBSession*)session didLogin:(FBUID)uid 
{
	[self getFacebookName];
}

- (void)session:(FBSession*)session willLogout:(FBUID)uid 
{
	if(delegate)
	[delegate hideLogOutButton];
	
	_facebookName = nil;
}



#pragma mark Get Facebook Name Helper

- (void)getFacebookName
{
	NSString* fql = [NSString stringWithFormat:
					 @"select uid,name from user where uid == %lld", _session.uid];
	
	
	NSDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[FBRequest requestWithDelegate:self] call:@"facebook.fql.query" params:params];
}

#pragma mark Memory Cleanup
- (void)dealloc 
{
	[_session release];
	_session = nil;
	
    [_loginDialog release];
	_loginDialog = nil;
	
    [_facebookName release];
	_facebookName = nil;
	
    [super dealloc];}



@end
