//
//  XMLDataParser.h
//  AmusementPark
//
//  Created by HARSHA on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface XMLDataParser : NSObject
{
}
+(NSArray *)getStudiosFromXml;
+(NSArray *)getRidesFromXML;
+(NSArray *)getRideDetailFromXML;
+(NSArray *)getParadeFromXML;
+(NSArray *)getRestuarantsFromXML;
+(NSArray *)getRestDetailFromXML;
+(int)getWaitTimeResponse;
+(NSArray *)getIndependentRestuarantsFromXML;
+(NSArray *)getDealsFromXml;
@end
