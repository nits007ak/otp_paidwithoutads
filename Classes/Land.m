//
//  Land.m
//  AmusementPark
//
//  Created by HARSHA on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Land.h"


@implementation Land
@synthesize m_LandId,m_LandName,m_RideArray,m_RestArray;
-(id)init
{
  if(self=[super init])
  {
    m_LandId=nil;
    m_LandName=nil;
    m_RideArray=[[NSMutableArray alloc]init];
    m_RestArray=[[NSMutableArray alloc]init];
  }
  return self;
}

-(void)dealloc
{
  if(m_LandId){[m_LandId release];m_LandId=nil;}
  if(m_LandName){[m_LandName release];m_LandName=nil;}
  if(m_RideArray){[m_RideArray removeAllObjects];[m_RideArray release];m_RideArray=nil;}
  if(m_RestArray){[m_RestArray removeAllObjects];[m_RestArray release];m_RestArray=nil;}
  [super dealloc];
}

@end
