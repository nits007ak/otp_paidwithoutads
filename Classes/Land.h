//
//  Land.h
//  AmusementPark
//
//  Created by HARSHA on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Land : NSObject 
{
  NSString *m_LandId;
  NSString *m_LandName;
  NSMutableArray *m_RideArray; 
  NSMutableArray *m_RestArray;
}
@property(nonatomic,retain) NSString *m_LandId;
@property(nonatomic,retain) NSString *m_LandName;
@property(nonatomic,assign) NSMutableArray *m_RideArray;
@property(nonatomic,assign) NSMutableArray *m_RestArray;
@end
