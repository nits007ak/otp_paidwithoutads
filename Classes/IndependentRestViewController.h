//
//  IndependentRestViewController.h
//  AmusementPark
//
//  Created by HARSHA on 26/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestuarantDetailViewController.h"
#import "MapImageViewController.h"
#import <CoreLocation/CoreLocation.h>

@protocol IndependentRestViewControllerDelegate

-(void)gotoHome;

@end

@interface IndependentRestViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,RestuarantDetailViewControllerDelegate,CLLocationManagerDelegate,MapImageViewControllerDelegate>
{
    id<IndependentRestViewControllerDelegate> m_delegate;
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    UIActivityIndicatorView *m_activityIndicator;
	NSMutableArray *m_RestuarantArray;
	UITableView *m_TableView;
	UILabel *m_DateLabel;
    UILabel *pTitleLabel;
    UIButton *m_Homebtn;
    UIButton *m_MapButton;
    NSString *m_ParkId;
    NSString *m_StudioId;
    NSString *m_ParkName;
	RestuarantDetailViewController *m_RestuarantDetailViewController;
    MapImageViewController *m_MapImageViewController;
    
    NSTimer *m_Timer;
    
    CLLocationManager *m_LocationManager;
    CLLocation *m_Location;
    BOOL isFirstTime;
    
    UIImageView* DateBarImageView;
}
@property(nonatomic,assign) id<IndependentRestViewControllerDelegate> m_delegate;
-(id)initwithStudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId bundleName:(NSString *)pParkName;
-(void)connectToServer;
-(void)stopUpDatingLocation;
-(void)refreshData;

@end
