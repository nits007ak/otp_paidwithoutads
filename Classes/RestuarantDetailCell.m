//
//  RestuarantDetailCell.m
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RestuarantDetailCell.h"


@implementation RestuarantDetailCell

@synthesize m_TitleLabel,m_ImageView,m_WaitTimeLabel,m_YardsLabel,m_WaitTimeImageView;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style  reuseIdentifier:reuseIdentifier];
    if (self)
	{
		m_ImageView=[[UIImageView alloc]initWithFrame:CGRectMake(75, 11, 45.5, 45)];
		m_ImageView.layer.cornerRadius = 5.0;
    m_ImageView.layer.masksToBounds = YES;
		[self addSubview:m_ImageView];
		
    m_WaitTimeImageView=[[UIImageView alloc]initWithFrame:CGRectMake(25,10,29.5,29)];
    m_WaitTimeImageView.backgroundColor=[UIColor clearColor];
    [self addSubview:m_WaitTimeImageView];    
    
    m_WaitTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(25,10,29.5,29)];
		m_WaitTimeLabel.backgroundColor =[UIColor clearColor];
		m_WaitTimeLabel.textAlignment = UITextAlignmentCenter;
    m_WaitTimeLabel.layer.cornerRadius = 4.0;
    m_WaitTimeLabel.layer.masksToBounds = YES;
		m_WaitTimeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24];
		m_WaitTimeLabel.textColor = [UIColor blackColor];
		[self addSubview:m_WaitTimeLabel];    
    
		m_TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(128,20,160,15)];
		m_TitleLabel.backgroundColor = [UIColor clearColor];
		m_TitleLabel.textAlignment = UITextAlignmentLeft;
		m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
		m_TitleLabel.textColor = [UIColor colorWithRed:128.0/255 green:85.0/255 blue:35.0/255 alpha:1.0];
		[self addSubview:m_TitleLabel];
    
    m_YardsLabel = [[UILabel alloc]initWithFrame:CGRectMake(128,35,160,12)];
		m_YardsLabel.backgroundColor = [UIColor clearColor];
		m_YardsLabel.textAlignment = UITextAlignmentLeft;
		m_YardsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    m_YardsLabel.textColor = [UIColor colorWithRed:128.0/255 green:85.0/255 blue:35.0/255 alpha:1.0];
		[self addSubview:m_YardsLabel];    
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
}

-(void)SetOpacityHigh
{
  m_WaitTimeImageView.layer.opacity=1.0;
  m_WaitTimeLabel.layer.opacity=1.0;
  m_TitleLabel.layer.opacity=1.0;
  m_YardsLabel.layer.opacity=1.0;
  m_ImageView.layer.opacity=1.0;
}

-(void)SetOpacityLow
{
  m_WaitTimeImageView.layer.opacity=.3;
  m_WaitTimeLabel.layer.opacity=.2;
  m_TitleLabel.layer.opacity=.2;
  m_YardsLabel.layer.opacity=0.2;
  m_ImageView.layer.opacity=0.2;
}

- (void)dealloc 
{
  if(m_WaitTimeImageView){[m_WaitTimeImageView removeFromSuperview];[m_WaitTimeImageView release];m_WaitTimeImageView=nil;}
	if(m_TitleLabel ){[m_TitleLabel  removeFromSuperview];[m_TitleLabel  release];m_TitleLabel =nil;}
  if(m_ImageView ){[m_ImageView  removeFromSuperview];[m_ImageView  release];m_ImageView =nil;}
  if(m_WaitTimeLabel ){[m_WaitTimeLabel removeFromSuperview];[m_WaitTimeLabel  release];m_WaitTimeLabel =nil;}
  if(m_YardsLabel){[m_YardsLabel  removeFromSuperview];[m_YardsLabel  release];m_YardsLabel =nil;}
	[super dealloc];
}


@end
