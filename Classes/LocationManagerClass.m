//
//  LocationManagerClass.m
//  AmusementPark
//
//  Created by HARSHA on 07/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LocationManagerClass.h"

static LocationManagerClass*	gLocationManagerClass = nil;

@implementation LocationManagerClass

+ (LocationManagerClass*)sharedLocationManagerClass
{
	if(gLocationManagerClass == nil)
	{
		gLocationManagerClass = [[LocationManagerClass alloc] init];
	}
	return gLocationManagerClass;
} 

- (id)init
{
  if ((self = [super init]))
  {
		m_LocationManager= [[CLLocationManager alloc]init];
    m_LocationManager.delegate = self;
    m_LocationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    m_LocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [m_LocationManager startUpdatingLocation];    
    m_Location=nil;
  }
  return self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation 
{
  if(m_Location){[m_Location release];m_Location=nil;}
  m_Location=newLocation;
  [m_Location retain];
}

-(void)dealloc
{
  if(m_Location){[m_Location release];m_Location=nil;}
  if(m_LocationManager){[m_LocationManager stopUpdatingHeading];m_LocationManager.delegate=nil;[m_LocationManager release];m_LocationManager=nil;}
  [super dealloc];
}

@end
