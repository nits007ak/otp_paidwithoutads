//
//  LandInfo.h
//  AmusementPark
//
//  Created by HARSHA on 08/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LandInfo : NSObject 
{
  NSString *m_StudioId;
  NSString *m_ParkId;
  NSString *m_LandId;
  NSString *m_LandName;
}
@property(nonatomic,retain) NSString *m_StudioId;
@property(nonatomic,retain) NSString *m_ParkId;
@property(nonatomic,retain) NSString *m_LandId;
@property(nonatomic,retain) NSString *m_LandName;
@end
