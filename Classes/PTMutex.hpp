///
/// @file     PTMutex.hpp
///
/// @date     2008-09-22
///

#ifndef PTMUTEX_HPP
#define PTMUTEX_HPP

#include <pthread.h>
#include <iostream>

class PTMutex
{
public:
  PTMutex()
  {
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);      
    if(pthread_mutex_init(&m_mutex, &attr) != 0)
    {
      assert(false);
      throw "SMMutex::SMutex: Memory allocation error";
    }
  }
  ~PTMutex()
  {
    pthread_mutex_destroy(&m_mutex);
  }

public:  
  operator pthread_mutex_t*()
  {
    return &m_mutex;
  }
private:
  pthread_mutex_t m_mutex;
	friend class PTScopedLock;
};

class PTScopedLock
{
public:
	PTScopedLock(PTMutex& mutex)
	: m_cs(mutex)
  {
    pthread_mutex_lock(&m_cs.m_mutex);
  }
  
	~PTScopedLock() throw()
  {
    pthread_mutex_unlock(&m_cs.m_mutex);
  }

// Implementation
private:
  PTMutex& m_cs;

  // Private to avoid accidental use
	PTScopedLock( const PTMutex& ) throw();
	PTScopedLock& operator=( const PTMutex& ) throw();
};

#endif
