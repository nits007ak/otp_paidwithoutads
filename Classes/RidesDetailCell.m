//
//  RidesDetailCell.m
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RidesDetailCell.h"
#import "AmusementParkAppDelegate.h"

@implementation RidesDetailCell
@synthesize m_TitleLabel,m_Selected,m_CustomButton,m_YardsLabel,m_delegate,m_WaitTimeLabel,m_WaitTimeImageView;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style  reuseIdentifier:reuseIdentifier];
    if (self)
	{
    m_WaitTimeImageView=[[UIImageView alloc]initWithFrame:CGRectMake(25,3,29.5,29)];
    m_WaitTimeImageView.backgroundColor=[UIColor clearColor];
    [self addSubview:m_WaitTimeImageView];
    
		m_WaitTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(25,3,29.5,29)];
		m_WaitTimeLabel.backgroundColor =[UIColor clearColor];
		m_WaitTimeLabel.textAlignment = UITextAlignmentCenter;
    m_WaitTimeLabel.layer.cornerRadius = 4.0;
    m_WaitTimeLabel.layer.masksToBounds = YES;
		m_WaitTimeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
		m_WaitTimeLabel.textColor = [UIColor blackColor];
		[self addSubview:m_WaitTimeLabel];
    
		m_TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(75,12,190,15)];
		m_TitleLabel.backgroundColor = [UIColor clearColor];
		m_TitleLabel.textAlignment = UITextAlignmentLeft;
		m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
		m_TitleLabel.textColor = [UIColor colorWithRed:52.0/255 green:53.0/255 blue:57.0/255 alpha:1.0];
		[self addSubview:m_TitleLabel];

    m_YardsLabel = [[UILabel alloc]initWithFrame:CGRectMake(75,24,150,15)];
		m_YardsLabel.backgroundColor = [UIColor clearColor];
		m_YardsLabel.textAlignment = UITextAlignmentLeft;
		m_YardsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
		m_YardsLabel.textColor = [UIColor colorWithRed:52.0/255 green:53.0/255 blue:57.0/255 alpha:1.0];
		[self addSubview:m_YardsLabel];
    
    m_CustomButton=[[CustomButton alloc]initWithFrame:CGRectMake(274, 8, 40, 33)];
    m_CustomButton.m_delegate=self;
    [self addSubview:m_CustomButton];
    
		AmusementParkAppDelegate * app = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
        
        
		m_Selected=[[UIImageView alloc]initWithFrame:CGRectMake(265, 15, 20, 19.5)];
        if (!app.isPortrait) {
            m_Selected.frame = CGRectMake(350, 15, 20, 19.5);
        }
		[self addSubview:m_Selected];
           
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didRotate:)
                                                     name:@"UIDeviceOrientationDidChangeNotification" object:nil];
		
    }
    return self;
}
- (void) didRotate:(NSNotification *)notification{
    AmusementParkAppDelegate * app = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        app.isPortrait = NO;
        m_Selected.frame = CGRectMake(350, 15, 20, 19.5);
    }
    else{
        app.isPortrait = YES;
        m_Selected.frame = CGRectMake(265, 15, 20, 19.5);
    }
    
}
-(void)ButtonPressed:(int)pSection Row:(int)pRow;
{
  if(m_delegate)
    [m_delegate gotoDetailPage:pSection Row:pRow];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
}

-(void)SetOpacityHigh
{
  m_WaitTimeImageView.layer.opacity=1.0;
  m_WaitTimeLabel.layer.opacity=1.0;
  m_TitleLabel.layer.opacity=1.0;
  m_Selected.layer.opacity=1.0;
  m_CustomButton.layer.opacity=1.0;  
  m_YardsLabel.layer.opacity=1.0;
}

-(void)SetOpacityLow
{
  m_WaitTimeImageView.layer.opacity=.3;
  m_WaitTimeLabel.layer.opacity=.3;
  m_TitleLabel.layer.opacity=.3;
  m_Selected.layer.opacity=.2;
  m_CustomButton.layer.opacity=.2;  
  m_YardsLabel.layer.opacity=0.3;
}

- (void)dealloc 
{
  if(m_WaitTimeImageView){[m_WaitTimeImageView removeFromSuperview];[m_WaitTimeImageView release];m_WaitTimeImageView=nil;}
	if(m_TitleLabel ){[m_TitleLabel  removeFromSuperview];[m_TitleLabel  release];m_TitleLabel =nil;}
	if(m_WaitTimeLabel ){[m_WaitTimeLabel  removeFromSuperview];[m_WaitTimeLabel  release];m_WaitTimeLabel =nil;}
	if(m_Selected ){[m_Selected  removeFromSuperview];[m_Selected  release];m_Selected =nil;}
  if(m_CustomButton){[m_CustomButton removeFromSuperview];[m_CustomButton release];m_CustomButton=nil;}
  if(m_YardsLabel){[m_YardsLabel removeFromSuperview];[m_YardsLabel release];m_YardsLabel=nil;}
    [super dealloc];
}


@end
