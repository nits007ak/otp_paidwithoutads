//
//  ParadeTimeViewController.h
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalenderViewController.h"
#import "AmusementParkAppDelegate.h"


@protocol ParadeTimeViewControllerDelegate

-(void)BackAction;

@end


@class CalenderViewController;

@interface ParadeTimeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CalenderViewControllerDelegate>
{
    NSString *m_ParkId;
    NSString *m_Datestring;
    NSString *m_DisplayDateString;
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    UIActivityIndicatorView *m_activityIndicator;
	UITableView *m_TableView;
	UILabel *m_DateLabel;
    UILabel *pTitleLabel;
    AmusementParkAppDelegate * appDelegate;
	CalenderViewController *m_CalenderViewController;
	UIButton *m_BackButton;
	UIButton *m_DateButton;
    UIButton *m_CharacterTimeButton;
    UIButton *m_PradeTimeButton;
    UIButton *m_FireworkTimeButton;
    NSMutableArray *m_ParadeArray;
    UIView *m_PickerView;
    int m_SelectedIndex;
    UIImageView * m_BGImageView;
	id<ParadeTimeViewControllerDelegate> m_delegate;
}
@property(nonatomic ,assign)id<ParadeTimeViewControllerDelegate> m_delegate;
-(id)initwithParkId:(NSString *)pParkId;
-(void)connectToServer;
@end
