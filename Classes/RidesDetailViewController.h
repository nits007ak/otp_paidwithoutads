//
//  RidesDetailViewController.h
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<QuartzCore/QuartzCore.h>
#import "SubmitWaitTimeViewController.h"
#import "ImageResourceDownloader.h"
#import "AmusementParkAppDelegate.h"
#import "Ride.h"


@protocol RidesDetailViewControllerDelegate

-(void)BackAction;

@end

@interface RidesDetailViewController : UIViewController<SubmitWaitTimeViewControllerDelegate,ImageResourceDownloaderDelegate,UIAlertViewDelegate>
{
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    UIActivityIndicatorView *m_activityIndicator;
    ImageResourceDownloader *m_ImageResourceDownloader;
    Ride *m_RideFromDb;
	UIView *m_BackgroundView;
	UIImageView *m_ImageView;
    UIImageView *m_FavouriteImageView;
    UIImageView *m_FaceBookImageView;
	UILabel *m_TitleLabel;
	UILabel *m_WaitTimeTitleLabel;
	UILabel *m_WaitTimeLabel;
	UIButton *m_FavoriteButton;
	UIButton *m_FacebookButton;
	UILabel *m_FavoriteLabel;
	UILabel *m_FacebookLabel;
	UILabel *m_DescriptionLabel;
    UILabel *pTitleLabel;
    UILabel *m_RideLengLabel;
    UILabel *m_RideLengSecLabel;
    UILabel *m_HeightReqLabel;
    UILabel *m_AgeLabel;
    UIButton *m_FastPassButton;
	UITextView *m_DescriptonView;
	UIButton *m_BackButton;
	UIButton *m_SubmitButton;
	NSString *m_StudioId;
    NSString *m_ParkId;
    NSString *m_LandId;
    NSString *m_RideId;
    NSString *m_LandName;
    NSString *m_Latitude;
    NSString *m_Longitude;
    NSString *m_ImagePath;
    int m_Distance;
    NSMutableArray *m_RidesArray;
    UIScrollView * scrollView;
    UIImageView * BGImageView;
    Ride *m_Ride;
    BOOL isFav,isFirst;
    int AdOffset;
    AmusementParkAppDelegate * appDelegate;
	id<RidesDetailViewControllerDelegate> m_delegate;
	SubmitWaitTimeViewController *m_SubmitWaitTimeViewController;
    
    NSTimer *m_Timer;
}
-(void)connectToServer;
@property(nonatomic,assign)	id<RidesDetailViewControllerDelegate> m_delegate;
-(void)downloadImages;
-(id)initwithStudioId:(NSString *)pStuId ParkId:(NSString *)pParkId LandId:(NSString *)pLandId RideId:(NSString *)pRideId LandName:(NSString *)pLandName Lat:(NSString *)pLat Long:(NSString *)pLong Distance:(int)dist isFavourite:(BOOL)fav;
-(void)getFromDataBase;
-(void)NotificationRecieved;
-(void)refreshData;
@end
