//
//  Parade.h
//  AmusementPark
//
//  Created by HARSHA on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Parade : NSObject
{
  NSString *m_ParadeId;
  NSString *m_ParadeName;
  NSString *m_ParadeLocation;
  NSString *m_Paradetype;
  NSString *m_StartTime;
  NSString *m_EndTime;
  NSString *m_ParkStartTime;
  NSString *m_ParkEndTime;
  NSString *m_Longitude;
  NSString *m_Latitude;
}
@property(nonatomic,retain) NSString *m_ParadeId;
@property(nonatomic,retain) NSString *m_ParadeName;
@property(nonatomic,retain) NSString *m_ParadeLocation;
@property(nonatomic,retain) NSString *m_Paradetype;
@property(nonatomic,retain) NSString *m_StartTime;
@property(nonatomic,retain) NSString *m_EndTime;
@property(nonatomic,retain) NSString *m_Longitude;
@property(nonatomic,retain) NSString *m_Latitude;
@property(nonatomic,retain) NSString *m_ParkStartTime;
@property(nonatomic,retain)NSString *m_ParkEndTime;
@end
