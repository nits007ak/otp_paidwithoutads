//
//  Studio.m
//  AmusementPark
//
//  Created by HARSHA on 05/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Studio.h"
@implementation Studio
@synthesize m_StudioId,m_StudioName,m_ParkArray,independent;
-(id)init
{
  self=[super init];
  if(self==nil)
    return nil;
  m_StudioId=nil;
  m_StudioName=nil;
  m_ParkArray=[[NSMutableArray alloc]init];
  return self;
}

-(void)dealloc
{
  if(m_StudioId){[m_StudioId release];m_StudioId=nil;}
  if(m_StudioName){[m_StudioName release];m_StudioName=nil;}
  if(m_ParkArray){[m_ParkArray removeAllObjects];[m_ParkArray release];m_ParkArray=nil;}
  [super dealloc];
}
@end
