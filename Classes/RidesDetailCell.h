//
//  RidesDetailCell.h
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"
#import<QuartzCore/QuartzCore.h>

@protocol RidesDetailCellDelegate

-(void)gotoDetailPage:(int)pSection Row:(int)pRow;

@end


@interface RidesDetailCell : UITableViewCell<CustomButtonDelegate>
{
	UILabel *m_WaitTimeLabel;
	UILabel *m_TitleLabel;
  UILabel *m_YardsLabel;
  CustomButton *m_CustomButton;
	UIImageView *m_Selected;
  UIImageView *m_WaitTimeImageView;
  id<RidesDetailCellDelegate>m_delegate;
}
@property(nonatomic,assign) UIImageView *m_WaitTimeImageView;
@property(nonatomic,assign) UILabel *m_WaitTimeLabel;
@property(nonatomic,assign) UILabel *m_TitleLabel;
@property(nonatomic,assign) UIImageView *m_Selected;
@property(nonatomic,assign) CustomButton *m_CustomButton;
@property(nonatomic,assign) id<RidesDetailCellDelegate>m_delegate;
@property(nonatomic,assign) UILabel *m_YardsLabel;
-(void)SetOpacityHigh;
-(void)SetOpacityLow;

@end
