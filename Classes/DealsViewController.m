    //
//  DealsViewController.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DealsViewController.h"
#import "XMLDataParser.h"
#import "DealsViewCell.h"
#import "Deals.h"
#import "AmusementParkAppDelegate.h"

@implementation DealsViewController
@synthesize m_delegate;

-(id)init
{
	if(self=[super init])
	{
		m_Homebtn=nil;
    m_activityIndicator=nil;
    pTitleLabel=nil;
    m_TableView=nil;
    mURLConnection=nil;
    mResponseData=nil;
    m_DealsArray=[[NSMutableArray alloc]init];
	}
	return self;
}


-(void)loadView
{
     appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }

	self.view.backgroundColor=[UIColor whiteColor];
	
  if(m_Homebtn==nil)
  {
	m_Homebtn=[UIButton buttonWithType:UIButtonTypeCustom];
	m_Homebtn.frame=CGRectMake(5, 8, 53.5, 30);
	[m_Homebtn setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/homebtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_Homebtn addTarget:self action:@selector(backtoHomeAction) forControlEvents:UIControlEventTouchUpInside];
	[self.navigationController.navigationBar addSubview:m_Homebtn];
  [m_Homebtn retain];
  }
  
  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(70, 8, 200, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=@"Official Ticket Center";
  [self.navigationController.navigationBar addSubview:pTitleLabel];              
  
  UIImageView * m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
     [m_BGImageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
  m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];	
	[m_BGImageView release];  
  
//  UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ImagesCommon/Rides/tabbar/tabbar.png"]];
//	imageView.frame=CGRectMake(0, 0, 320, 48.5);
//	[self.navigationController.navigationBar insertSubview:imageView atIndex:0];
//	[imageView release];  
  
//  if(m_TableView==nil)
//  m_TableView = [[UITableView alloc]init];
//  m_TableView.frame=CGRectMake(0,10,320,350);
//  [m_TableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//	m_TableView.backgroundColor=[UIColor clearColor];
//	m_TableView.delegate = self;
//	m_TableView.dataSource = self;
//	m_TableView.scrollEnabled = YES;
//	[self.view addSubview:m_TableView];  
//  [self connectToServer];

    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height-20)];
    [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.officialticketcenter.com"]]];
    [webview setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    webview.delegate=self;
    webview.scalesPageToFit =YES;
    [self.view addSubview:webview];
    [webview release];
    
  if(m_activityIndicator==nil)
  m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
  [m_activityIndicator startAnimating];
  m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    m_activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
  [self.view addSubview:m_activityIndicator];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}

-(void)layoutSubView
{
    
    if(!appDelegate.isPortrait){
        m_Homebtn.frame=CGRectMake(5, 1, 53.5, 30);
        pTitleLabel.frame = CGRectMake(60, 1, 370, 30);
        if([[UIScreen mainScreen]bounds].size.height == 568)pTitleLabel.frame = CGRectMake(60, 1, 500, 30);
        
        
    }
    else{
        m_Homebtn.frame=CGRectMake(5, 8, 53.5, 30);
        pTitleLabel.frame = CGRectMake(60, 8, 190, 30);
    }
}
- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}

-(void)backtoHomeAction
{
  [m_Homebtn removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	if(m_delegate)
		[m_delegate gotoHome];
}





#pragma mark URLConnectionManager Methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [m_activityIndicator startAnimating];

}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
 [m_activityIndicator stopAnimating];

}

-(void)connectToServer
{
  m_Homebtn.userInteractionEnabled=NO;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:[NSString stringWithString:@"http://admin.apptasmic.com/xml.php?method=getdeals"]]];
	[request setHTTPMethod:@"POST"];
	mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
  m_Homebtn.userInteractionEnabled=YES;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }      
  
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate showAlertView];
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  m_Homebtn.userInteractionEnabled=YES;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }        
	if(mResponseData)
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *filePath = [paths objectAtIndex:0];
		paths=nil;
		filePath = [filePath stringByAppendingPathComponent:@"Deals.xml"];
		[mResponseData writeToFile:filePath atomically:YES];
		filePath=nil;
		[mResponseData release];
		mResponseData=nil;
    if([m_DealsArray count]){[m_DealsArray removeAllObjects];}
    [m_DealsArray addObjectsFromArray:[XMLDataParser getDealsFromXml]];
		[m_TableView reloadData];
	}
}	



#pragma mark UITableView delegate methods
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return 2;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [m_DealsArray count];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  if(indexPath.row==0)
    return 35;
  else
  {
      return 98;
  }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	DealsViewCell*cell = (DealsViewCell *)[tableView dequeueReusableCellWithIdentifier:@"DealsPageCell"];
  Deals *pDeal=[m_DealsArray objectAtIndex:indexPath.section];
	if(cell == nil)
	{  
    cell=[[[DealsViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DealsPageCell"]autorelease];
		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
	}
  
  if(indexPath.row==0)
  {
    UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 310, 35)];
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Deals/DealsUpperCell" ofType:@"png"]];
    cell.backgroundView = bgImgView;
    [bgImgView release];
    cell.m_TitleLabel.text=pDeal.m_Title;
    cell.m_DescriptonView.hidden=YES;
    cell.m_TitleLabel.hidden=NO;    
  }
  else
  {
    UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 310, 98)];
    bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Deals/DealsLowerCell" ofType:@"png"]];
    cell.backgroundView = bgImgView;
    [bgImgView release];
    cell.m_DescriptonView.text=pDeal.m_Desc;
    cell.m_DescriptonView.hidden=NO;
    cell.m_TitleLabel.hidden=YES;    
  }
	return cell;
}

-(void)viewDidLoad
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}


-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}


- (void)dealloc 
{
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }    
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if(m_DealsArray){[m_DealsArray removeAllObjects];[m_DealsArray release];m_DealsArray=nil;}
  if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
  if(m_Homebtn){[m_Homebtn removeFromSuperview];[m_Homebtn release];m_Homebtn=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
    [super dealloc];
}


@end
