
#import <UIKit/UIKit.h>

@interface RemoteFile : NSObject
{
  NSString* m_remoteURL;
  NSString* m_localFilePath;
}

@property (nonatomic, retain) NSString* m_remoteURL;
@property (nonatomic, retain) NSString* m_localFilePath;

@end

@class ImageResourceDownloader;

@protocol ImageResourceDownloaderDelegate <NSObject>
- (void)fileDowloadedCompleted:(ImageResourceDownloader*)downloader fileName:(NSString*)name localfile:(NSString *)localfilename;
- (void)batchDowloadCompleted:(ImageResourceDownloader*)downloader;
- (void)errorDownloading:(ImageResourceDownloader*)downloader;
@end

@interface ImageResourceDownloader : NSObject 
{
  NSMutableArray* m_urls;
  int m_currentDownloadingURLIndex;
  
  id<ImageResourceDownloaderDelegate> m_delegate;
  
  NSURLConnection* m_connection;     
  
  char* m_pBuffer;
  int m_currentBufferIndex;
  
  BOOL m_bStop;
	
	NSMutableData* mResponseData;
}

@property (assign,nonatomic) id<ImageResourceDownloaderDelegate> m_delegate;

- (void)downloadImages:(NSArray*)remoteFiles;   
- (void)cancel; 



@end
