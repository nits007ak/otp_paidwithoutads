//
//  AdsManager.h
//  AmusementPark
//
//  Created by Amresh Kumar on 21/10/11.
//  Copyright (c) 2011 Permeative Technologies Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GADBannerView.h"
#import <iAd/iAd.h>
#import <StoreKit/StoreKit.h>
#import "BuyViewController.h"
#import "AmusementParkAppDelegate.h"

@class FullVersionBanner;
@class FullVersionBannerBig;
@class RemoveAdsButton;
@interface AdsManager : NSObject<GADBannerViewDelegate,ADBannerViewDelegate,BuyViewControllerDelegate>
{
    UIView *_adBanner;
    ADBannerView *_iAdBanner;
    GADBannerView *_gAdBanner;
   
    FullVersionBanner *_fullVersionBanner;
    FullVersionBannerBig *_fullVersionBannerBig;
    RemoveAdsButton *_removeAdsButton;
    UIView *dimView;
  
}
+(AdsManager*)sharedAdsManager;
-(void)ShowAdBannerFromVC:(UIViewController*)viewController withFrame:(CGRect)frame;
-(void)ShowFullVersionBannerFromVC:(UIViewController*)viewController withOrigin:(CGPoint)origin;
-(void)RemoveAdBannerAndFullVersionBanner;
@end
