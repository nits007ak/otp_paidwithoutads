//
//  PhotoDB.m
//  PhotoLock
//
//  Created by ashwini on 03/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AmusementParkDB.h"
#include "PTMutex.hpp"
@implementation AmusementParkDB
-(id)init
{
	assert(false);
	
	return nil;
}

- (id)customInit
{
	self = [super init];
	if(self == nil)   return nil;
	
	m_pMutex = new PTMutex;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"AmusementPark.sqlite"];
	if([[NSFileManager defaultManager] fileExistsAtPath:path] == NO)
	{
		NSError* error = nil;
		[[NSFileManager defaultManager] copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"AmusementPark.sqlite"] toPath:path error:&error];
	}
	
	if (sqlite3_open([path UTF8String], &m_database) != SQLITE_OK)
	{
		assert(false);
		return nil;
	}
	
	return self;
}



static  AmusementParkDB *g_AmusementParkDB = nil;

+(AmusementParkDB*)sharedPtDatabase
{
	if(g_AmusementParkDB == nil) 
		g_AmusementParkDB = [[AmusementParkDB alloc] customInit];
	
	return g_AmusementParkDB;
}

- (void)close
{
	PTScopedLock scopedLock(*m_pMutex);
	if(m_database)
	{
		sqlite3_close(m_database);
		m_database = NULL;
	}
}


- (BOOL)executeQuery:(NSString *)query
{
	if(m_database == NULL)
		return NO;
	
	BOOL status = NO;
	
	sqlite3_stmt *statement = nil;
	const char *sql = [query cStringUsingEncoding:NSUTF8StringEncoding];
	int resp = sqlite3_prepare_v2(m_database, sql, -1, &statement, NULL);
	if ( resp == SQLITE_OK) 
	{
		int success = sqlite3_step(statement);
		if (success != SQLITE_DONE) 
		{
#if TARGET_IPHONE_SIMULATOR			
			NSLog(@"Error: failed to excecute query domain with message '%s'.", sqlite3_errmsg(m_database));
#endif
		}
		else
			status = YES;
	}
	sqlite3_reset(statement);
	sqlite3_finalize(statement);
	
	return status;
}

-(int)getDuplicate:(Ride *)pRide
{
  NSMutableArray* Rides = [[[NSMutableArray alloc] init] autorelease];
  NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM Rides WHERE StudioId='%@' AND ParkId='%@' AND LandId='%@' AND RideId='%@'", pRide.m_StudioId,pRide.m_ParkId,pRide.m_LandId,pRide.m_RideId];
	sqlite3_stmt *select_stmt = nil;
	if (sqlite3_prepare_v2(m_database, [query UTF8String], -1, &select_stmt, NULL) == SQLITE_OK) 
	{
	  while (sqlite3_step(select_stmt) == SQLITE_ROW) 
    {
      Ride* ride = [[Ride alloc] init];
      ride.m_Distance=sqlite3_column_int(select_stmt, 0);
      ride.m_FastPass=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 1)];
      ride.m_Age=sqlite3_column_int(select_stmt, 2);
      ride.m_Height=sqlite3_column_int(select_stmt, 3);
      ride.m_RideLength=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 4)];
      ride.m_Longitude=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 5)];
      ride.m_Latitude=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 6)];
      ride.m_Description=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 7)];
      ride.m_ImageId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 8)];
      ride.m_LandId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 9)];
      ride.m_ParkId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 10)];
      ride.m_RideId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 11)];
      ride.m_RideName = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 12)];
      ride.m_StudioId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 13)];
      ride.m_WaitTime = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 14)];
      [Rides addObject:ride];
      [ride release];
    }
  }
	sqlite3_reset(select_stmt);
	sqlite3_finalize(select_stmt);  
  [query release];
  return [Rides count];  
}

-(int)getDuplicateRest:(Restuarant *)pRest
{
  NSMutableArray* Restuarent = [[[NSMutableArray alloc] init] autorelease];
  NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM Restuarants WHERE StudioId='%@' AND ParkId='%@' AND LandId='%@' AND RestId='%@'", pRest.m_StudioId,pRest.m_ParkId,pRest.m_LandId,pRest.m_RestId];
	sqlite3_stmt *select_stmt = nil;
	if (sqlite3_prepare_v2(m_database, [query UTF8String], -1, &select_stmt, NULL) == SQLITE_OK) 
	{
	  while (sqlite3_step(select_stmt) == SQLITE_ROW) 
    {
      Restuarant* restuarant = [[Restuarant alloc] init];
      restuarant.m_WaitTime=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 0)];
      restuarant.m_RestServiceType=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 1)];
      restuarant.m_Distance = sqlite3_column_int(select_stmt, 2);
      restuarant.m_ImageId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 3)];
      restuarant.m_RestCost=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 4)];
      restuarant.m_CuisineName=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 5)];
      restuarant.m_LandId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 6)];
      restuarant.m_ParkId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 7)];
      restuarant.m_PhonoNo = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 8)];
      restuarant.m_Reserve = sqlite3_column_int(select_stmt, 9);
      restuarant.m_RestDesc = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 10)];
      restuarant.m_RestId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 11)];
      restuarant.m_RestName = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 12)];
      restuarant.m_StudioId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 13)];
      [Restuarent addObject:restuarant];
      [restuarant release];
    }
  }
	sqlite3_reset(select_stmt);
	sqlite3_finalize(select_stmt);  
  [query release];
  return [Restuarent count];  
}

-(int)getDuplicateLand:(LandInfo *)pLand
{
  NSMutableArray* Lands = [[[NSMutableArray alloc] init] autorelease];
  NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM Lands WHERE LandId='%@' AND ParkId='%@' AND StudioId='%@'", pLand.m_LandId,pLand.m_ParkId,pLand.m_StudioId];
	sqlite3_stmt *select_stmt = nil;
	if (sqlite3_prepare_v2(m_database, [query UTF8String], -1, &select_stmt, NULL) == SQLITE_OK) 
	{
	  while (sqlite3_step(select_stmt) == SQLITE_ROW) 
    {
      LandInfo * land = [[LandInfo alloc] init];
      land.m_StudioId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 0)];
      land.m_ParkId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 1)];
      land.m_LandId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 2)];
      land.m_LandName=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 3 )];
      [Lands addObject:land];
      [land release];
    }
  }
	sqlite3_reset(select_stmt);
	sqlite3_finalize(select_stmt);  
  [query release];
  return [Lands count];  
}

-(void)insertRide:(Ride *)pRide
{
    PTScopedLock scopedLock(*m_pMutex);
	
    NSString *query = @"INSERT INTO Rides(RideDistance,RideFastPass,RideAge,RideHeight,RideLength,Longitude,Latitude,Description,ImagePath,LandId,ParkId,RideId,RideName,StudioId,WaitTime) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    BOOL status;
    sqlite3_stmt *statement = nil;
    const char *sql = [query cStringUsingEncoding:NSUTF8StringEncoding];
    int resp = sqlite3_prepare_v2(m_database, sql, -1, &statement, NULL);
    if ( resp == SQLITE_OK)
    {
      sqlite3_bind_int(statement, 1, (pRide.m_Distance ? pRide.m_Distance : 0));
      sqlite3_bind_text(statement, 2, [(pRide.m_FastPass ? pRide.m_FastPass : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_int(statement, 3, (pRide.m_Age ? pRide.m_Age : 0));
      sqlite3_bind_int(statement, 4, (pRide.m_Height ? pRide.m_Height : 0));
      sqlite3_bind_text(statement, 5, [(pRide.m_RideLength ? pRide.m_RideLength : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 6, [(pRide.m_Longitude ? pRide.m_Longitude : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 7, [(pRide.m_Latitude ? pRide.m_Latitude : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 8, [(pRide.m_Description ? pRide.m_Description : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 9, [(pRide.m_ImageId ? pRide.m_ImageId : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 10, [(pRide.m_LandId ? pRide.m_LandId : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 11, [(pRide.m_ParkId ? pRide.m_ParkId : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 12, [(pRide.m_RideId ? pRide.m_RideId : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement,13, [(pRide.m_RideName ? pRide.m_RideName : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 14, [(pRide.m_StudioId ? pRide.m_StudioId : @"") UTF8String], -1, SQLITE_TRANSIENT);
      sqlite3_bind_text(statement, 15, [(pRide.m_WaitTime ? pRide.m_WaitTime : @"") UTF8String], -1, SQLITE_TRANSIENT);

		
		int success = sqlite3_step(statement);
        if (success != SQLITE_DONE)
        {
#if TARGET_IPHONE_SIMULATOR           
            NSLog(@"Error: failed to excecute query domain with message '%s'.", sqlite3_errmsg(m_database));
#endif
        }
        else
            status = YES;
    }
    sqlite3_reset(statement);
    sqlite3_finalize(statement);
	
    assert(status);
}


-(void)insertRestuarant:(Restuarant *)pRestuarant
{
  PTScopedLock scopedLock(*m_pMutex);
	
  NSString *query = @"INSERT INTO Restuarants(WaitTime,RestService,RestDistance,ImagePath,RestCost,CuisineName,LandId,ParkId,PhoneNo,Reservation,RestDesc,RestId,RestName,StudioId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  BOOL status;
  sqlite3_stmt *statement = nil;
  const char *sql = [query cStringUsingEncoding:NSUTF8StringEncoding];
  int resp = sqlite3_prepare_v2(m_database, sql, -1, &statement, NULL);
  if ( resp == SQLITE_OK)
  {
    sqlite3_bind_text(statement, 1, [(pRestuarant.m_WaitTime ? pRestuarant.m_WaitTime : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 2, [(pRestuarant.m_RestServiceType ? pRestuarant.m_RestServiceType : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_int(statement, 3, (pRestuarant.m_Distance ? pRestuarant.m_Distance : 0));
    sqlite3_bind_text(statement, 4, [(pRestuarant.m_ImageId ? pRestuarant.m_ImageId : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 5, [(pRestuarant.m_RestCost ? pRestuarant.m_RestCost : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 6, [(pRestuarant.m_CuisineName ? pRestuarant.m_CuisineName : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 7, [(pRestuarant.m_LandId ? pRestuarant.m_LandId : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 8, [(pRestuarant.m_ParkId ? pRestuarant.m_ParkId : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 9, [(pRestuarant.m_PhonoNo ? pRestuarant.m_PhonoNo : @"") UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_int(statement, 10, (pRestuarant.m_Reserve ? 1 : 0));
    sqlite3_bind_text(statement, 11, [(pRestuarant.m_RestDesc ? pRestuarant.m_RestDesc : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 12, [(pRestuarant.m_RestId ? pRestuarant.m_RestId : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 13, [(pRestuarant.m_RestName ? pRestuarant.m_RestName : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 14, [(pRestuarant.m_StudioId ? pRestuarant.m_StudioId : @"") UTF8String], -1, SQLITE_TRANSIENT);
		int success = sqlite3_step(statement);
    if (success != SQLITE_DONE)
    {
#if TARGET_IPHONE_SIMULATOR           
      NSLog(@"Error: failed to excecute query domain with message '%s'.", sqlite3_errmsg(m_database));
#endif
    }
    else
      status = YES;
  }
  sqlite3_reset(statement);
  sqlite3_finalize(statement);
	
  assert(status);
}


-(void)insertLand:(LandInfo *)pLand
{
  PTScopedLock scopedLock(*m_pMutex);
	
  NSString *query = @"INSERT INTO Lands(LandId,LandName,StudioId,ParkId) VALUES (?,?,?,?)";
  BOOL status;
  sqlite3_stmt *statement = nil;
  const char *sql = [query cStringUsingEncoding:NSUTF8StringEncoding];
  int resp = sqlite3_prepare_v2(m_database, sql, -1, &statement, NULL);
  if ( resp == SQLITE_OK)
  {
    sqlite3_bind_text(statement, 1, [(pLand.m_LandId ? pLand.m_LandId : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 2, [(pLand.m_LandName? pLand.m_LandName : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 3, [(pLand.m_StudioId ? pLand.m_StudioId : @"") UTF8String], -1, SQLITE_TRANSIENT);
    sqlite3_bind_text(statement, 4, [(pLand.m_ParkId ? pLand.m_ParkId : @"") UTF8String], -1, SQLITE_TRANSIENT);

		int success = sqlite3_step(statement);
    if (success != SQLITE_DONE)
    {
#if TARGET_IPHONE_SIMULATOR           
      NSLog(@"Error: failed to excecute query domain with message '%s'.", sqlite3_errmsg(m_database));
#endif
    }
    else
      status = YES;
  }
  sqlite3_reset(statement);
  sqlite3_finalize(statement);
	
  assert(status);
}

-(NSMutableArray *)getLandsFromDBWithParkId:(NSString *)pParkId
{
  NSMutableArray* Lands = [[[NSMutableArray alloc] init] autorelease];
  NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM Lands WHERE ParkId='%@'",pParkId];
	sqlite3_stmt *select_stmt = nil;
	if (sqlite3_prepare_v2(m_database, [query UTF8String], -1, &select_stmt, NULL) == SQLITE_OK) 
	{
	  while (sqlite3_step(select_stmt) == SQLITE_ROW) 
    {
      LandInfo * land = [[LandInfo alloc] init];
      land.m_StudioId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 0)];
      land.m_ParkId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 1)];
      land.m_LandId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 2)];
      land.m_LandName=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 3 )];
      [Lands addObject:land];
      [land release];
    }
  }
	sqlite3_reset(select_stmt);
	sqlite3_finalize(select_stmt);  
  [query release];
  return Lands;  
}

-(Ride *)getRideWithRideId:(NSString *)pRideId
{
  Ride* ride = [[[Ride alloc] init]autorelease];
  NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM Rides WHERE RideId='%@'",pRideId];
	sqlite3_stmt *select_stmt = nil;
	if (sqlite3_prepare_v2(m_database, [query UTF8String], -1, &select_stmt, NULL) == SQLITE_OK) 
	{
	  while (sqlite3_step(select_stmt) == SQLITE_ROW) 
    {
      ride.m_Distance=sqlite3_column_int(select_stmt, 0);
      ride.m_FastPass=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 1)];
      ride.m_Age=sqlite3_column_int(select_stmt, 2);
      ride.m_Height=sqlite3_column_int(select_stmt, 3);
      ride.m_RideLength=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 4)];
      ride.m_Longitude=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 5)];
      ride.m_Latitude=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 6)];
      ride.m_Description=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 7)];
      ride.m_ImageId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 8)];
      ride.m_LandId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 9)];
      ride.m_ParkId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 10)];
      ride.m_RideId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 11)];
      ride.m_RideName = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 12)];
      ride.m_StudioId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 13)];
      ride.m_WaitTime = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 14)];
    }
  }
	sqlite3_reset(select_stmt);
	sqlite3_finalize(select_stmt);  
  [query release];
  return ride;    
}


-(NSMutableArray *)getRidesWithLandId:(NSString *)pLandId
{
  NSMutableArray* Rides = [[[NSMutableArray alloc] init] autorelease];
  NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM Rides WHERE LandId='%@'",pLandId];
	sqlite3_stmt *select_stmt = nil;
	if (sqlite3_prepare_v2(m_database, [query UTF8String], -1, &select_stmt, NULL) == SQLITE_OK) 
	{
	  while (sqlite3_step(select_stmt) == SQLITE_ROW) 
    {
      Ride* ride = [[Ride alloc] init];
      ride.m_Distance=sqlite3_column_int(select_stmt, 0);
      ride.m_FastPass=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 1)];
      ride.m_Age=sqlite3_column_int(select_stmt, 2);
      ride.m_Height=sqlite3_column_int(select_stmt, 3);
      ride.m_RideLength=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 4)];
      ride.m_Longitude=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 5)];
      ride.m_Latitude=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 6)];
      ride.m_Description=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 7)];
      ride.m_ImageId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 8)];
      ride.m_LandId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 9)];
      ride.m_ParkId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 10)];
      ride.m_RideId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 11)];
      ride.m_RideName = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 12)];
      ride.m_StudioId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt,13)];
      ride.m_WaitTime = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 14)];
      [Rides addObject:ride];
      [ride release];
    }
  }
	sqlite3_reset(select_stmt);
	sqlite3_finalize(select_stmt);  
  [query release];
  return Rides;  
}

-(NSMutableArray *)getRestsWithLandId:(NSString *)pLandId
{
  NSMutableArray* Restuarent = [[[NSMutableArray alloc] init] autorelease];
  NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM Restuarants WHERE LandId='%@'",pLandId];
	sqlite3_stmt *select_stmt = nil;
	if (sqlite3_prepare_v2(m_database, [query UTF8String], -1, &select_stmt, NULL) == SQLITE_OK) 
	{
	  while (sqlite3_step(select_stmt) == SQLITE_ROW) 
    {
      Restuarant* restuarant = [[Restuarant alloc] init];
      restuarant.m_WaitTime=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 0)];
      restuarant.m_RestServiceType=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 1)];
      restuarant.m_Distance = sqlite3_column_int(select_stmt, 2);
      restuarant.m_ImageId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 3)];
      restuarant.m_RestCost=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 4)];
      restuarant.m_CuisineName=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 5)];
      restuarant.m_LandId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 6)];
      restuarant.m_ParkId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 7)];
      restuarant.m_PhonoNo = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 8)];
      restuarant.m_Reserve = sqlite3_column_int(select_stmt, 9);
      restuarant.m_RestDesc = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 10)];
      restuarant.m_RestId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 11)];
      restuarant.m_RestName = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 12)];
      restuarant.m_StudioId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 13)];
      [Restuarent addObject:restuarant];
      [restuarant release];
    }
  }
	sqlite3_reset(select_stmt);
	sqlite3_finalize(select_stmt);  
  [query release];
  return Restuarent;  
}

-(Restuarant *)getRestWithRestId:(NSString *)pRestId
{
  Restuarant* restuarant;
  NSString *query = [[NSString alloc] initWithFormat:@"SELECT * FROM Restuarants WHERE RestId='%@'",pRestId];
	sqlite3_stmt *select_stmt = nil;
	if (sqlite3_prepare_v2(m_database, [query UTF8String], -1, &select_stmt, NULL) == SQLITE_OK) 
	{
	  while (sqlite3_step(select_stmt) == SQLITE_ROW) 
    {
      restuarant = [[[Restuarant alloc] init]autorelease];
      restuarant.m_WaitTime=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 0)];
      restuarant.m_RestServiceType=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 1)];
      restuarant.m_Distance = sqlite3_column_int(select_stmt, 2);
      restuarant.m_ImageId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 3)];
      restuarant.m_RestCost=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 4)];
      restuarant.m_CuisineName=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 5)];
      restuarant.m_LandId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 6)];
      restuarant.m_ParkId=[NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 7)];
      restuarant.m_PhonoNo = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 8)];
      restuarant.m_Reserve = sqlite3_column_int(select_stmt, 9);
      restuarant.m_RestDesc = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 10)];
      restuarant.m_RestId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 11)];
      restuarant.m_RestName = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 12)];
      restuarant.m_StudioId = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(select_stmt, 13)];
    }
  }
	sqlite3_reset(select_stmt);
	sqlite3_finalize(select_stmt);  
  [query release];
  return restuarant;  
}

-(BOOL)deleteRestFromFavorite:(NSString *)pRestId
{
	PTScopedLock scopedLock(*m_pMutex);
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE  FROM Restuarants WHERE RestId='%@'",pRestId];
	BOOL status = [self executeQuery:query];
	[self executeQuery:query];
	[query release];	
	assert(status);	
	return status;
}

-(BOOL)deleteRideFromFavorite:(NSString *)pRideId
{
	PTScopedLock scopedLock(*m_pMutex);
	NSString *query = [[NSString alloc] initWithFormat:@"DELETE  FROM Rides WHERE RideId='%@'",pRideId];
	BOOL status = [self executeQuery:query];
	[self executeQuery:query];
	[query release];	
	assert(status);	
	return status;
}


@end
