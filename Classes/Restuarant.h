//
//  Restuarant.h
//  AmusementPark
//
//  Created by HARSHA on 08/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Restuarant : NSObject 
{
  NSString *m_CuisineName;
  NSString *m_LandId;
  NSString *m_ParkId;
  NSString *m_PhonoNo;
  int m_Reserve;
  NSString *m_RestDesc;
  NSString *m_RestServiceType;
  NSString *m_RestId;
  NSString *m_RestName;
  NSString *m_StudioId;
  NSString *m_ImageId;
  NSString *m_Latitude;
  NSString *m_Longitude;
  NSString *m_RestCost;
  NSString *m_WaitTime;
  int m_Distance;
}
@property(nonatomic,retain) NSString *m_CuisineName;
@property(nonatomic,retain) NSString *m_LandId;
@property(nonatomic,retain) NSString *m_ParkId;
@property(nonatomic,retain) NSString *m_PhonoNo;
@property(nonatomic,retain) NSString *m_RestDesc;
@property(nonatomic,retain) NSString *m_RestId;
@property(nonatomic,retain) NSString *m_RestName;
@property(nonatomic,retain) NSString *m_StudioId;
@property(nonatomic,retain) NSString *m_ImageId;
@property(nonatomic,retain) NSString *m_Latitude;
@property(nonatomic,retain) NSString *m_Longitude;
@property(nonatomic,retain) NSString *m_RestCost;
@property(nonatomic,retain) NSString *m_RestServiceType;
@property(nonatomic,retain) NSString *m_WaitTime;
@property(nonatomic) int m_Reserve;
@property(nonatomic) int m_Distance;

@end
