    //
//  ParadeTimeViewController.m
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ParadeTimeViewController.h"
#import "ParadeDetailCell.h"
#import "Parade.h"
#import "XMLDataParser.h"
#import "AmusementParkAppDelegate.h"

@implementation ParadeTimeViewController
@synthesize m_delegate;
-(id)initwithParkId:(NSString *)pParkId
{
	if(self=[super init])
	{
    m_ParkId=pParkId;
        [m_ParkId retain];
		m_TableView=nil;
		m_DateLabel=nil;
        m_PickerView=nil;
        m_ParadeArray=[[NSMutableArray alloc]init];
		m_CalenderViewController=nil;
		m_BackButton=nil;
		m_DateButton=nil;
        pTitleLabel=nil;
        mURLConnection=nil;
        mResponseData=nil;
        m_activityIndicator=nil;
        m_DisplayDateString=nil;
        m_CharacterTimeButton=nil;
        m_PradeTimeButton=nil;
        m_FireworkTimeButton=nil;
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY-MM-dd"];
        m_Datestring = [dateFormat stringFromDate:date];
        [m_Datestring retain];
        m_SelectedIndex=0;
        [dateFormat release];      
	}
	return self;
}


-(void)loadView
{		
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	self.view.backgroundColor=[UIColor whiteColor];
	[self.navigationItem setHidesBackButton:YES animated:NO];

	
    if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }
  m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];	
	[m_BGImageView release];
	
  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 8, 215, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=@"Parade&FireworksTime";
  pTitleLabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
  
  if(m_BackButton==nil)
  {
	m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
	[m_BackButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/backbtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	[m_BackButton retain];
  }
	
  if(m_DateButton==nil)
  {
	m_DateButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_DateButton.frame=CGRectMake(275, 8, 38.5, 30);
	[m_DateButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/date" ofType:@"png"]] forState:UIControlStateNormal];
	[m_DateButton addTarget:self action:@selector(dateButtonAction) forControlEvents:UIControlEventTouchUpInside];
	[m_DateButton retain];	
  }
  
  NSDate *date = [NSDate date];
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"EEEE - MMMM d, YYYY"];
  m_DisplayDateString= [dateFormat stringFromDate:date];  
  [m_DisplayDateString retain];
  [dateFormat release];        
   
  UIImageView* topBarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, 320, 27)];
  topBarImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/datebar" ofType:@"png"]];
   topBarImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:topBarImageView];
  [topBarImageView release];

  if(m_DateLabel==nil)
	m_DateLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, 320, 27)];
	m_DateLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
	m_DateLabel.text=m_DisplayDateString;
	m_DateLabel.backgroundColor=[UIColor clearColor];
  m_DateLabel.textColor=[UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
	m_DateLabel.textAlignment=UITextAlignmentCenter;
     m_DateLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
  m_DateLabel.shadowColor = [UIColor whiteColor];
  m_DateLabel.shadowOffset = CGSizeMake(0, 1.0);
	[self.view addSubview:m_DateLabel];
  
  if(m_PickerView==nil)
  m_PickerView=[[UIView alloc]initWithFrame:CGRectMake(0,31.5,325,40)];
	m_PickerView.backgroundColor=[UIColor clearColor];
     m_PickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[self.view addSubview:m_PickerView];  
  
  UIImageView *m_NavigImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 325, 43)];
	m_NavigImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/ParadeBar" ofType:@"png"]];
     m_NavigImage.autoresizingMask = UIViewAutoresizingFlexibleWidth;
  [m_PickerView addSubview:m_NavigImage];
	[m_NavigImage release];
  m_NavigImage=nil;
	
  if(m_CharacterTimeButton)
  {[m_CharacterTimeButton release];m_CharacterTimeButton=nil;}
	m_CharacterTimeButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_CharacterTimeButton.frame=CGRectMake(10, 3, 100,34);
	[m_CharacterTimeButton setBackgroundColor:[UIColor clearColor]];
	[m_CharacterTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/CharacterTime2" ofType:@"png"]] forState:UIControlStateNormal];
  [m_CharacterTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/CharacterTime2" ofType:@"png"]] forState:UIControlStateHighlighted];
	[m_CharacterTimeButton addTarget:self action:@selector(CharacterTimeButtonAction) forControlEvents:UIControlEventTouchUpInside];
  [m_CharacterTimeButton retain];
	[m_PickerView addSubview:m_CharacterTimeButton];

  if(m_PradeTimeButton)
  {[m_PradeTimeButton release];m_PradeTimeButton=nil;}
  m_PradeTimeButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_PradeTimeButton.frame=CGRectMake(115, 3, 90, 35);
	[m_PradeTimeButton setBackgroundColor:[UIColor clearColor]];
	[m_PradeTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/ParadeTime1" ofType:@"png"]] forState:UIControlStateNormal];
  [m_PradeTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/ParadeTime2" ofType:@"png"]] forState:UIControlStateHighlighted];
	[m_PradeTimeButton addTarget:self action:@selector(PradeTimeButtonAction) forControlEvents:UIControlEventTouchUpInside];
  [m_PradeTimeButton retain];
	[m_PickerView addSubview:m_PradeTimeButton];

  if(m_FireworkTimeButton)
  {[m_FireworkTimeButton release];m_FireworkTimeButton=nil;}
  m_FireworkTimeButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_FireworkTimeButton.frame=CGRectMake(210, 3, 100, 35);
	[m_FireworkTimeButton setBackgroundColor:[UIColor clearColor]];
	[m_FireworkTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/FireworkTime1" ofType:@"png"]] forState:UIControlStateNormal];
  [m_FireworkTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/FireworkTime2" ofType:@"png"]] forState:UIControlStateHighlighted];
	[m_FireworkTimeButton addTarget:self action:@selector(FireworkTimeButtonAction) forControlEvents:UIControlEventTouchUpInside];
  [m_FireworkTimeButton retain];
	[m_PickerView addSubview:m_FireworkTimeButton];  
  
    if(m_TableView==nil)
        m_TableView = [[UITableView alloc]init];
    
    if (screenBounds.size.height == 568) {
        m_TableView.frame=CGRectMake(0,82,320,370);
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_TableView.frame=CGRectMake(0,82,320,300);
    }
    
     m_TableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [m_TableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    m_TableView.backgroundView=nil;

    
     
	m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.scrollEnabled = YES;
	m_TableView.backgroundColor=[UIColor clearColor];
	m_TableView.rowHeight=30;
	[self.view addSubview:m_TableView];
  
  if(m_activityIndicator==nil)
  m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
  [m_activityIndicator startAnimating];
    m_activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin);
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;

  [self.view addSubview:m_activityIndicator];        
  [self connectToServer];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}

-(void)layoutSubView
{
    if ([m_ParadeArray count]>0) {
        [m_TableView reloadData];
    }
    CGRect frame = [[UIScreen mainScreen]bounds];
    if (appDelegate.isPortrait) {
        pTitleLabel.frame = CGRectMake(60, 8, 215, 30);
        m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
        m_DateButton.frame=CGRectMake(275, 8, 38.5, 30);
        m_PradeTimeButton.frame=CGRectMake(115, 3, 90, 35);
        m_FireworkTimeButton.frame=CGRectMake(210, 3, 100, 35);
        m_BGImageView.frame = CGRectMake(0, 0, 320, 480);
        if (frame.size.height == 568) m_BGImageView.frame = CGRectMake(0, 0, 320, 568);
        
        
        
        
    }
    else
    {
        m_PradeTimeButton.frame=CGRectMake(205, 3, 90, 35);
        pTitleLabel.frame = CGRectMake(160, 3, 215, 30);
        m_BackButton.frame=CGRectMake(5, 3, 53.5, 30);
        m_DateButton.frame=CGRectMake(438, 3, 38.5, 30);
        m_FireworkTimeButton.frame=CGRectMake(370, 3, 100, 35);
        m_BGImageView.frame = CGRectMake(0, 0, 480, 320);
        
        if (frame.size.height == 568) {
            pTitleLabel.frame = CGRectMake(185, 3, 215, 30);
            m_DateButton.frame=CGRectMake(527, 3, 38.5, 30);
            m_PradeTimeButton.frame=CGRectMake(240, 3, 90, 35);
            m_FireworkTimeButton.frame=CGRectMake(458, 3, 100, 35);
            m_BGImageView.frame = CGRectMake(0, 0, 568, 320);
        }
    }
    
}

- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}


-(void)CharacterTimeButtonAction
{
    [m_ParadeArray removeAllObjects];
    [m_TableView reloadData];
    [m_CharacterTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/CharacterTime2" ofType:@"png"]] forState:UIControlStateNormal];
    [m_PradeTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/ParadeTime1" ofType:@"png"]] forState:UIControlStateNormal];
    [m_FireworkTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/FireworkTime1" ofType:@"png"]] forState:UIControlStateNormal];

    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [m_activityIndicator startAnimating];
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    [self.view addSubview:m_activityIndicator];            
    m_SelectedIndex=0;
    [self connectToServer];
}

-(void)PradeTimeButtonAction
{
    [m_ParadeArray removeAllObjects];
    [m_TableView reloadData];
  [m_CharacterTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/CharacterTime1" ofType:@"png"]] forState:UIControlStateNormal];
	[m_PradeTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/ParadeTime2" ofType:@"png"]] forState:UIControlStateNormal];
  [m_FireworkTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/FireworkTime1" ofType:@"png"]] forState:UIControlStateNormal];

    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [m_activityIndicator startAnimating];
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    [self.view addSubview:m_activityIndicator];            
    m_SelectedIndex=1;
    [self connectToServer];
}

-(void)FireworkTimeButtonAction
{
    [self.view addSubview:m_activityIndicator];            
    [m_ParadeArray removeAllObjects];
    [m_TableView reloadData];
    [m_PradeTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/ParadeTime1" ofType:@"png"]] forState:UIControlStateNormal];
    [m_CharacterTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/CharacterTime1" ofType:@"png"]] forState:UIControlStateNormal];
    [m_FireworkTimeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/FireworkTime2" ofType:@"png"]] forState:UIControlStateNormal];

    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [m_activityIndicator startAnimating];
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    [self.view addSubview:m_activityIndicator];            
    m_SelectedIndex=2;
    [self connectToServer];
}

-(void)backAction
{
	if(m_delegate)
		[m_delegate BackAction];
	[m_BackButton removeFromSuperview];
	[m_DateButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	[self.navigationController popViewControllerAnimated:YES];
}
-(void)BackAction1:(NSString *)date
{
  if(m_Datestring)
  {
    [m_Datestring release];
    m_Datestring=nil;
  }
  m_Datestring=date;
  [m_Datestring retain];
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"YYYY-MM-dd"];
  NSDate *date1=[dateFormat dateFromString:m_Datestring];
  [dateFormat release];        
  
  NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
  [dateFormat1 setDateFormat:@"EEEE - MMMM d, YYYY"];
  if(m_DisplayDateString)
  {
    [m_DisplayDateString release];
    m_DisplayDateString=nil;
  }
  m_DisplayDateString= [dateFormat1 stringFromDate:date1];  
  [m_DisplayDateString retain];
  [dateFormat1 release];        
  m_DateLabel.text=[NSString stringWithFormat:@"%@",m_DisplayDateString]; 
  m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
  [m_activityIndicator startAnimating];
    m_activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin);
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
  [self.view addSubview:m_activityIndicator];          
  [self connectToServer];
}

-(void)dateButtonAction
{
	[m_BackButton removeFromSuperview];
	[m_DateButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	if(m_CalenderViewController)
	{
		[m_CalenderViewController.view removeFromSuperview];
		[m_CalenderViewController release];
		m_CalenderViewController=nil;
	}
	m_CalenderViewController=[[CalenderViewController alloc]init];
	m_CalenderViewController.m_delegate=self;
	[self.navigationController pushViewController:m_CalenderViewController animated:YES];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    
   
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount) {
        
    }else{
        [self.navigationController.navigationBar addSubview:pTitleLabel];
        [self.navigationController.navigationBar addSubview:m_BackButton];
        [self.navigationController.navigationBar addSubview:m_DateButton];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    appDelegate.subviewCount =  appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}


#pragma mark URLConnectionManager Methods

-(void)connectToServer
{
  m_BackButton.userInteractionEnabled=NO;
  m_DateButton.userInteractionEnabled=NO;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	m_PradeTimeButton.userInteractionEnabled=NO;
  m_CharacterTimeButton.userInteractionEnabled=NO;
  m_FireworkTimeButton.userInteractionEnabled=NO;
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
  
  if(m_SelectedIndex==0)
      [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getcharacters&parkid=%@&ndate=%@",m_ParkId,m_Datestring]]];
  else if(m_SelectedIndex==1)
      [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getparade&parkid=%@&ndate=%@",m_ParkId,m_Datestring]]];
  else if(m_SelectedIndex==2)
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getfireworks&parkid=%@&ndate=%@",m_ParkId,m_Datestring]]];
	[request setHTTPMethod:@"POST"];
	mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
  m_BackButton.userInteractionEnabled=YES;
  m_DateButton.userInteractionEnabled=YES;
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }  
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  m_PradeTimeButton.userInteractionEnabled=YES;
  m_CharacterTimeButton.userInteractionEnabled=YES;
  m_FireworkTimeButton.userInteractionEnabled=YES;
  
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate showAlertView];
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  m_BackButton.userInteractionEnabled=YES;
  m_DateButton.userInteractionEnabled=YES;
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }    
  
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  m_PradeTimeButton.userInteractionEnabled=YES;
  m_CharacterTimeButton.userInteractionEnabled=YES;
  m_FireworkTimeButton.userInteractionEnabled=YES;  
	if(mResponseData)
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *filePath = [paths objectAtIndex:0];
		paths=nil;
		filePath = [filePath stringByAppendingPathComponent:@"ParadeDetail.xml"];
		[mResponseData writeToFile:filePath atomically:YES];
		filePath=nil;
		[mResponseData release];
		mResponseData=nil;
    if([m_ParadeArray count]){[m_ParadeArray removeAllObjects];}
    [m_ParadeArray addObjectsFromArray:[XMLDataParser getParadeFromXML]];
   if([m_ParadeArray count])
   {
   }
      [m_TableView reloadData];
	}
}	


#pragma mark tableView delegate methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 4;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [m_ParadeArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  if(indexPath.row==0)
    return 71/2;
  else if(indexPath.row==1)
    return 45;
    else if(indexPath.row==2)
        return 45;
      else
        return 52;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
    ParadeDetailCell *cell = (ParadeDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"MainPageCell"];

	if(cell == nil)
	{  
    cell=[[[ParadeDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainPageCell"]autorelease];

		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
	}
  
	UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 307.5, 35)];
	Parade *pParade=[m_ParadeArray objectAtIndex:indexPath.section];
	if(indexPath.row==0)
	{
		bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/uppercell4" ofType:@"png"]];
		cell.m_LocationLabel.hidden=YES;
		cell.m_TypeLabel.hidden=YES;
		cell.m_WhenLabel.hidden=YES;
    cell.pWhenLabel.hidden=YES;
    cell.pTypeLabel.hidden=YES;
    cell.pLocationLabel.hidden=YES;
    cell.m_TitleLabel.hidden=NO;
		cell.m_TitleLabel.frame=CGRectMake(23,3,290,30);
    cell.m_TitleLabel.text=pParade.m_ParadeName;
    cell.m_TitleLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:100.0];

		cell.accessoryType=UITableViewCellAccessoryNone;
	}
	else if(indexPath.row==1)
	{
		bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/normalcell4" ofType:@"png"]];
		cell.m_TypeLabel.hidden=YES;
		cell.m_WhenLabel.hidden=YES;
		cell.m_TitleLabel.hidden=YES;
    cell.pTypeLabel.hidden=YES;
    cell.pWhenLabel.hidden=YES;
    cell.pLocationLabel.hidden=NO;
		cell.m_LocationLabel.hidden=NO;
		cell.m_LocationLabel.frame=CGRectMake(85,7,220,30);
		cell.m_LocationLabel.text=pParade.m_ParadeLocation;
		cell.accessoryType=UITableViewCellAccessoryNone;
	}
	else if(indexPath.row==2)
	{
		bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/normalcell4" ofType:@"png"]];
		cell.m_LocationLabel.hidden=YES;
		cell.m_WhenLabel.hidden=YES;
		cell.m_TitleLabel.hidden=YES;
    cell.pLocationLabel.hidden=YES;
    cell.pWhenLabel.hidden=YES;
		cell.m_TypeLabel.hidden=NO;
    cell.pTypeLabel.hidden=NO;
		cell.m_TypeLabel.frame=CGRectMake(64,7,240,30);
		cell.m_TypeLabel.text=pParade.m_Paradetype;
		cell.accessoryType=UITableViewCellAccessoryNone;
	}
	else if(indexPath.row==3)
	{
		bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Parade/downcell4" ofType:@"png"]];
		cell.m_LocationLabel.hidden=YES;
		cell.m_TypeLabel.hidden=YES;
		cell.m_TitleLabel.hidden=YES;
    cell.pTypeLabel.hidden=YES;
    cell.pLocationLabel.hidden=YES;
		
		cell.m_WhenLabel.frame=CGRectMake(67,7,240,30);
    NSString *timeString;
    if(pParade.m_StartTime != NULL)
    {
      if([pParade.m_StartTime length]>35)
      {
        cell.pWhenLabel.frame=CGRectMake(23,-2,70,30);
      }
      else
      {
        cell.pWhenLabel.frame=CGRectMake(23,6,70,30);
      }

      cell.m_WhenLabel.hidden=NO;
      cell.pWhenLabel.hidden=NO;
      timeString=[NSString stringWithFormat:@"%@",pParade.m_StartTime];
      cell.m_WhenLabel.text=timeString;
    }
    else 
    {
      cell.pWhenLabel.frame=CGRectMake(23,6,70,30);
      cell.m_WhenLabel.hidden=YES;
      cell.pWhenLabel.hidden=NO;
    }
		cell.accessoryType=UITableViewCellAccessoryNone;
	}
	cell.backgroundView=bgImgView;
	[bgImgView release];

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated
{
	if(m_CalenderViewController)
	{
		[m_CalenderViewController.view removeFromSuperview];
    m_CalenderViewController.m_delegate=nil;
		[m_CalenderViewController release];
		m_CalenderViewController=nil;
	}  
}

- (void)dealloc 
{
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }   
  if(m_CalenderViewController)
	{
		[m_CalenderViewController.view removeFromSuperview];
    m_CalenderViewController.m_delegate=nil;
		[m_CalenderViewController release];
		m_CalenderViewController=nil;
	}  
  if(m_PickerView)
  {
    while([[m_PickerView subviews] count]>0)
      [[[m_PickerView subviews] objectAtIndex:0] removeFromSuperview];
    [m_PickerView removeFromSuperview];
    [m_PickerView release];
    m_PickerView=nil;
  }
  if(m_DisplayDateString){[m_DisplayDateString release];m_DisplayDateString=nil;}
  if(m_Datestring){[m_Datestring release];m_Datestring=nil;}
  if(m_ParadeArray){[m_ParadeArray release];m_ParadeArray=nil;}
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_PradeTimeButton){[m_PradeTimeButton removeFromSuperview];[m_PradeTimeButton release];m_PradeTimeButton=nil;}
  if(m_FireworkTimeButton){[m_FireworkTimeButton removeFromSuperview];[m_FireworkTimeButton release];m_FireworkTimeButton=nil;}
  if(m_CharacterTimeButton){[m_CharacterTimeButton removeFromSuperview];[m_CharacterTimeButton release];m_CharacterTimeButton=nil;}
  if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
  if(m_DateButton){[m_DateButton removeFromSuperview];[m_DateButton release];m_DateButton=nil;}
  if(m_DateLabel){[m_DateLabel removeFromSuperview];[m_DateLabel release];m_DateLabel=nil;}
  if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
    [super dealloc];
}


@end
