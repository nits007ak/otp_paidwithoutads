//
//  ParadeDetailCell.h
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ParadeDetailCell : UITableViewCell 
{
	UILabel *m_TitleLabel;
	UILabel *m_LocationLabel;
	UILabel *m_TypeLabel;
	UILabel *m_WhenLabel;
  UILabel *pLocationLabel;
  UILabel *pTypeLabel;
  UILabel *pWhenLabel;
}
@property(nonatomic,assign) UILabel *m_TitleLabel;
@property(nonatomic,assign) UILabel *m_LocationLabel;
@property(nonatomic,assign) UILabel *m_TypeLabel;
@property(nonatomic,assign) UILabel *m_WhenLabel;
@property(nonatomic,assign) UILabel *pLocationLabel;
@property(nonatomic,assign) UILabel *pTypeLabel;
@property(nonatomic,assign) UILabel *pWhenLabel;

@end
