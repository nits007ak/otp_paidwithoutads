//
//  AnnotationInfo.m
//  AmusementPark
//
//  Created by HARSHA on 01/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AnnotationInfo.h"

@implementation AnnotationInfo

@synthesize m_Type,m_LandIndex,m_Index,m_AnnotX,m_AnnotY,m_ParkX,m_ParkY,Waittime,title;

-(id)init
{
  if((self=[super init]))
  {
  m_Type=nil;
  title=nil;
  m_LandIndex=0;
    Waittime=nil;
  m_Index=0;
  m_AnnotX=0.0;
  m_AnnotY=0.0;
  m_ParkX=0.0;
  m_ParkY=0.0;
  }
  return self;
}

-(void)dealloc
{
  if(m_Type){[m_Type release];m_Type=nil;}
  if(title){[title release];title=nil;}
  if(Waittime){[Waittime release];Waittime=nil;}
  [super dealloc];
}

@end
