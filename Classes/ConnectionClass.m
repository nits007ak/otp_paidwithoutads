//
//  ConnectionClass.m
//  BoxAPIDemo
//
//  Created by Tilak on 23/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ConnectionClass.h"
@interface ConnectionClass (PRIVATE)
@end


@implementation ConnectionClass
@synthesize mDelegate;

-(id)init
{
	if (self==[super init])
	{
		mURLConnection=nil;
		mResponseData=nil;
		mSavingFilePath=nil;
		mDelegate=nil;
	}
	return self;
}

-(void)dealloc
{
	if(mURLConnection){[mURLConnection cancel];[mURLConnection release];mURLConnection=nil;}
	if(mResponseData){[mResponseData release];mResponseData=nil;}
	if (mSavingFilePath){[mSavingFilePath release];mSavingFilePath=nil;}
	if (mDelegate) {mDelegate=nil;}
	[super dealloc];
}

-(void)connectToServerWithURL:(NSString *)UrlStr saveTo:(NSString *)filePath
{
	if(mURLConnection){[mURLConnection cancel];[mURLConnection release];mURLConnection=nil;}
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:UrlStr]];
	
    if (mSavingFilePath) {[mSavingFilePath release];mSavingFilePath=nil;}
	mSavingFilePath=[filePath retain];
	
	mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
	if(mURLConnection){[mURLConnection cancel];[mURLConnection release];mURLConnection=nil;}
	if(mResponseData){[mResponseData release];mResponseData=nil;}
	if (mDelegate)
	{
		[mDelegate connectionFailedWithMessage:[error localizedDescription]];
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	if(mURLConnection){[mURLConnection cancel];[mURLConnection release];mURLConnection=nil;}

	if(mResponseData)
	{
		[mResponseData writeToFile:mSavingFilePath atomically:YES];
		[mResponseData release];
		mResponseData=nil;
		if (mDelegate)
		{
			[mDelegate responseObtained:mSavingFilePath];
		}
	}
}	


@end
