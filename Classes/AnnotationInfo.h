//
//  AnnotationInfo.h
//  AmusementPark
//
//  Created by HARSHA on 01/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AnnotationInfo : NSObject 
{
  NSString *m_Type;
  NSString *title;
  NSString *Waittime;
  int m_LandIndex,m_Index;  
  double m_AnnotX;
  double m_AnnotY;
  double m_ParkX;
  double m_ParkY;
}

@property(nonatomic,retain) NSString *m_Type;
@property(nonatomic) int m_LandIndex,m_Index;  
@property(nonatomic,retain) NSString *Waittime;
@property(nonatomic) double m_AnnotX;
@property(nonatomic) double m_AnnotY;
@property(nonatomic) double m_ParkX;
@property(nonatomic) double m_ParkY;
@property(nonatomic,retain) NSString *title;
@end
