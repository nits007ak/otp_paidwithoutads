//
//  DealsViewCell.m
//  AmusementPark
//
//  Created by HARSHA on 26/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DealsViewCell.h"


@implementation DealsViewCell
@synthesize m_TitleLabel,m_DescriptonView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  
  self = [super initWithStyle:style  reuseIdentifier:reuseIdentifier];
  if (self)
	{
    m_TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(22,3,270,35)];
		m_TitleLabel.backgroundColor = [UIColor clearColor];
		m_TitleLabel.textAlignment = UITextAlignmentLeft;
		m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
		m_TitleLabel.textColor = [UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
    m_TitleLabel.shadowColor=[UIColor whiteColor];
    m_TitleLabel.shadowOffset=CGSizeMake(0, 1);
		[self addSubview:m_TitleLabel];    
    
    m_DescriptonView = [[UITextView alloc] init];
    m_DescriptonView.frame=CGRectMake(13,2,280,82);
    m_DescriptonView.textAlignment=UITextAlignmentLeft;
    m_DescriptonView.scrollEnabled=YES;
    m_DescriptonView.editable=NO;
    m_DescriptonView.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    m_DescriptonView.textColor = [UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
    m_DescriptonView.backgroundColor = [UIColor  clearColor];
    [self addSubview:m_DescriptonView];
  }
  return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (void)dealloc 
{
  if(m_TitleLabel){[m_TitleLabel removeFromSuperview];[m_TitleLabel release];m_TitleLabel=nil;}
  if(m_DescriptonView){[m_DescriptonView removeFromSuperview];[m_DescriptonView release];m_DescriptonView=nil;}
    [super dealloc];
}

@end
