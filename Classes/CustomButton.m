//
//  CustomButton.m
//  AmusementPark
//
//  Created by HARSHA on 14/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomButton.h"


@implementation CustomButton
@synthesize m_delegate,m_Button,row,section,m_ImageView;

- (id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self)
    {
      frame.origin.x = 8;
      frame.origin.y = 6;
      frame.size.width=21.5;
      frame.size.height=21.5;
      m_ImageView=[[UIImageView alloc]initWithFrame:frame];
      m_ImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/arrowmark" ofType:@"png"]];
      m_ImageView.userInteractionEnabled=YES;
      [self addSubview:m_ImageView];
      
      frame.origin.x = 0;
      frame.origin.y = 0;
      frame.size.width=40;
      frame.size.height=33;
      m_Button=[UIButton buttonWithType:UIButtonTypeCustom];
      m_Button.frame=frame;
      [m_Button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
      m_Button.backgroundColor=[UIColor clearColor];
      [m_Button retain];
      [self addSubview:m_Button]; 
    }
    return self;
}

-(void)buttonAction
{
 if(m_delegate)
   [m_delegate ButtonPressed:section Row:row];
}


- (void)dealloc
{
  if(m_ImageView){[m_ImageView removeFromSuperview];[m_ImageView release];m_ImageView=nil;}
  if(m_Button){[m_Button removeFromSuperview];[m_Button release];m_Button=nil;}
    [super dealloc];
}


@end
