//
//  RidesTableViewCell.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RidesTableViewCell.h"


@implementation RidesTableViewCell
@synthesize m_TitleLabel,m_TImeLabel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
	{
		m_TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(20,10,100,30)];
		m_TitleLabel.backgroundColor = [UIColor clearColor];
		m_TitleLabel.textAlignment = UITextAlignmentLeft;
		m_TitleLabel.font = [UIFont boldSystemFontOfSize:15];
		m_TitleLabel.enabled=NO;
		m_TitleLabel.textColor = [UIColor blackColor];
		[self addSubview:m_TitleLabel];
		
		m_TImeLabel = [[UILabel alloc]initWithFrame:CGRectMake(180,10,120,30)];
		m_TImeLabel.backgroundColor = [UIColor clearColor];
		m_TImeLabel.textAlignment = UITextAlignmentLeft;
		m_TImeLabel.font = [UIFont boldSystemFontOfSize:15];
		m_TImeLabel.enabled=NO;
		m_TImeLabel.textColor = [UIColor blackColor];
		[self addSubview:m_TImeLabel];
		
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
}


- (void)dealloc 
{
	if(m_TitleLabel ){[m_TitleLabel  removeFromSuperview];[m_TitleLabel  release];m_TitleLabel =nil;}
	if(m_TImeLabel ){[m_TImeLabel  removeFromSuperview];[m_TImeLabel  release];m_TImeLabel =nil;}
    [super dealloc];
}


@end
