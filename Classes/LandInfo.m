//
//  LandInfo.m
//  AmusementPark
//
//  Created by HARSHA on 08/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LandInfo.h"
@implementation LandInfo
@synthesize m_StudioId,m_ParkId,m_LandId,m_LandName;
-(id)init
{
  if(self=[super init])
  {
    m_StudioId=nil;
    m_ParkId=nil;
    m_LandId=nil;
    m_LandName=nil;
  }
  return self;
}

-(void)dealloc
{
  if(m_StudioId){[m_StudioId release];m_StudioId=nil;}
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_LandId){[m_LandId release];m_LandId=nil;}
  if(m_LandName){[m_LandName release];m_LandName=nil;}
  [super dealloc];
}
@end
