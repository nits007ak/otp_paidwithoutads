//
//  FavoriteViewController.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RidesDetailViewController.h"
#import "RestuarantDetailViewController.h"
#import "SubmitWaitTimeViewController.h"
#import "RidesDetailCell.h"

@protocol FavoriteViewControllerDelegate

-(void)gotoHome;

@end

@interface FavoriteViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,RidesDetailViewControllerDelegate,RestuarantDetailViewControllerDelegate,
                                                      SubmitWaitTimeViewControllerDelegate,RidesDetailCellDelegate>
{
  NSString *m_ParkId;
	NSMutableArray *m_LandsArray;
	NSMutableArray *m_FavouriteArray;
	UITableView *m_TableView;
  UIButton *m_Homebtn;
  UILabel *pTitleLabel;
	id<FavoriteViewControllerDelegate> m_delegate;

	RidesDetailViewController *m_RidesDetailViewController;
	RestuarantDetailViewController *m_RestuarantDetailViewController;
  SubmitWaitTimeViewController *m_SubmitWaitTimeViewController;
}
@property(nonatomic,assign) id<FavoriteViewControllerDelegate> m_delegate;
-(id)initWithParkId:(NSString *)pParkId;
-(void)NotificationRecieved;
@end
