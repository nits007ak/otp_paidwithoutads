//
//  FoodViewController.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestuarantDetailViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "SortRideViewController.h"
#import "AmusementParkAppDelegate.h"

@protocol FoodViewControllerDelegate

-(void)gotoHome;

@end

@interface FoodViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,RestuarantDetailViewControllerDelegate,CLLocationManagerDelegate,SortRideViewControllerDelegate>
{
	id<FoodViewControllerDelegate> m_delegate;
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    UIActivityIndicatorView *m_activityIndicator;
	NSMutableArray *m_RestuarantArray;
	UITableView *m_TableView;
	UILabel *m_DateLabel;
    UILabel *pTitleLabel;
    UIButton *m_Homebtn;
    UIButton *m_DistanceButton;
    NSString *m_ParkId;
    UIImageView * m_BGImageView;
    NSString *m_StudioId;
    AmusementParkAppDelegate * appDelegate;
	RestuarantDetailViewController *m_RestuarantDetailViewController;
    SortRideViewController *m_SortRideViewController;
    
    NSTimer *m_Timer;
    int AdOffset;
    
    int m_SelectedSortType;
    CLLocationManager *m_LocationManager;
    CLLocation *m_Location;
    BOOL isFirst;
    UIImageView* DateBarImageView;
}
@property(nonatomic,assign) id<FoodViewControllerDelegate> m_delegate;
-(id)initwithStudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId;
-(void)connectToServer;
-(void)stopUpDatingLocation;
-(void)sortRides;
-(void)SelectSortType:(int)index;
-(void)sortOnName;
-(void)sortOnWaitTime;
-(void)sortOnDistance;
-(void)refreshData;
@end
