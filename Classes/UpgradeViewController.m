//
//  UpgradeViewController.m
//  AmusementPark
//
//  Created by Amresh Kumar on 22/10/11.
//  Copyright (c) 2011 Permeative Technologies Pvt. Ltd. All rights reserved.
//

#import "UpgradeViewController.h"
#import "AmusementParkAppDelegate.h"

@implementation UpgradeViewController

@synthesize KeepFreeButton;
@synthesize BGImage;
UIImageView *m_BGImageView;
  UIButton *UpgradeButton;
  UIButton *KeepFreeButton;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//  if (self) 
//  {
//    // Custom initialization
//  }
//  return self;
//}

- (void)didReceiveMemoryWarning
{
  // Releases the view if it doesn't have a superview.
  [super didReceiveMemoryWarning];
  
  // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void) loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }
   
    m_BGImageView.image=[UIImage imageNamed:@"FullVersion.png"];
    m_BGImageView.userInteractionEnabled=YES;
	[self.view addSubview:m_BGImageView];
    [m_BGImageView release];
   
    
    UpgradeButton=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
    UpgradeButton.frame=CGRectMake(20, 400, 120, 80);
    
    [UpgradeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/BuyApp.png"] forState:UIControlStateNormal];
    [UpgradeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/BuyAppPressed.png"] forState:UIControlStateSelected];
    [UpgradeButton addTarget:self action:@selector(Upgrade) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:UpgradeButton];
    
    KeepFreeButton=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
    KeepFreeButton.frame=CGRectMake(180, 400, 120, 80);
    
    [KeepFreeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/KeepFree.png"] forState:UIControlStateNormal];
    [KeepFreeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/KeepFreePressed.png"] forState:UIControlStateSelected];
    [KeepFreeButton addTarget:self action:@selector(Cancel) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:KeepFreeButton];
    
    
    [self checkOrientation];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}


- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}



-(void)layoutSubView
{

    if (appDelegate.isPortrait) {
        m_BGImageView.frame = CGRectMake(0, 0, 320, 480);
         KeepFreeButton.frame=CGRectMake(180, 350, 120, 80);
         UpgradeButton.frame=CGRectMake(20, 350, 120, 80);
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            m_BGImageView.frame = CGRectMake(0, 0, 320, 568);
            KeepFreeButton.frame=CGRectMake(180, 408, 120, 80);
            UpgradeButton.frame=CGRectMake(20, 408, 120, 80);
        }
    }
    else
    {
        KeepFreeButton.frame=CGRectMake(340, 220, 120, 80);
        UpgradeButton.frame=CGRectMake(20, 220, 120, 80);
        m_BGImageView.frame = CGRectMake(0, 0, 480, 320);
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            KeepFreeButton.frame=CGRectMake(428, 220, 120, 80);
            UpgradeButton.frame=CGRectMake(20, 220, 120, 80);
            m_BGImageView.frame = CGRectMake(0, 0, 568, 320);
        }
    }
        
        
    
}
- (void)viewDidLoad
{
  [super viewDidLoad];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    
    
    //    CGRect bgImageBounds;
//        CGRect upgradeButtonsBound;
//        CGRect keepFreeButtonsBound;
//    CGRect screenBounds = [[UIScreen mainScreen] bounds];
//    if (screenBounds.size.height == 568) {
//        bgImageBounds = CGRectMake(0, 0, 320, 640);
//        upgradeButtonsBound = CGRectMake(50, 480, 140, 80);
//        keepFreeButtonsBound = CGRectMake(180, 480, 140, 80);
//
//        // code for 4-inch screen
//    } else {
//        // code for 3.5-inch screen
//        bgImageBounds = CGRectMake(0, 0, 320, 490);
//        upgradeButtonsBound = CGRectMake(50, 100, 70, 40);
//                keepFreeButtonsBound = CGRectMake(130, 100, 70, 40);
//    }
//    
//  UIImage* splashImage = [[UIImage alloc] initWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"FullVersion.png"]];
//    
//   
//    [BGImage setBounds:bgImageBounds];
//  [BGImage setImage:splashImage];
//  [UpgradeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/BuyApp.png"] forState:UIControlStateNormal];
//    
//  [UpgradeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/BuyAppPressed.png"] forState:UIControlStateSelected];
//
//    [UpgradeButton setBounds:upgradeButtonsBound];
//    [self.view bringSubviewToFront:UpgradeButton];
//  [KeepFreeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/KeepFree.png"] forState:UIControlStateNormal];
//  [KeepFreeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/KeepFreePressed.png"] forState:UIControlStateSelected];
//        [KeepFreeButton setBounds:keepFreeButtonsBound];
//        [self.view bringSubviewToFront:KeepFreeButton];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
  
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  // Return YES for supported orientations
  return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)Upgrade

{
  
  [appDelegate.RootViewController dismissModalViewControllerAnimated:YES];
  if (IsUniversalStudioFreeWaitTimes)
  {
    NSString *link=[appDelegate.ITunesLinkDictionary objectForKey:@"Universal Studios"];
    if([link length]>0)
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
  }
  else if (IsDisneyWorldFreeWaitTimes)
  {
    NSString *link=[appDelegate.ITunesLinkDictionary objectForKey:@"Disney World"];
    if([link length]>0)
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link]];
  }
}
-(void)Cancel
{
  AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
  [appDelegate.RootViewController dismissModalViewControllerAnimated:YES];
}
@end
