//
//  Ride.h
//  AmusementPark
//
//  Created by HARSHA on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Ride : NSObject 
{
  NSString *m_StudioId;
  NSString *m_ParkId;
  NSString *m_LandId;
  NSString *m_LandName;
  NSString *m_RideId;
  NSString *m_RideName;
  NSString *m_WaitTime;
  NSString *m_ImageId;
  NSString *m_Description;
  NSString *m_Latitude;
  NSString *m_Longitude;
  NSString *m_RideLength;
  NSString *m_FastPass;
  int m_Age;
  int m_Height;
  int m_Distance;
}
@property(nonatomic,retain) NSString *m_StudioId;
@property(nonatomic,retain) NSString *m_ParkId;
@property(nonatomic,retain) NSString *m_LandId;
@property(nonatomic,retain) NSString *m_RideId;
@property(nonatomic,retain) NSString *m_RideName;
@property(nonatomic,retain) NSString *m_WaitTime;
@property(nonatomic,retain) NSString *m_ImageId;
@property(nonatomic,retain) NSString *m_Description;
@property(nonatomic,retain) NSString *m_Latitude;
@property(nonatomic,retain) NSString *m_Longitude;
@property(nonatomic,retain) NSString *m_RideLength;
@property(nonatomic,retain) NSString *m_FastPass;
@property(nonatomic) int m_Age;
@property(nonatomic) int m_Height;
//@property(nonatomic,retain) NSString *m_LandName;
@property(nonatomic) int m_Distance;
@end
