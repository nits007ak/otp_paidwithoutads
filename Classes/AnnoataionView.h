//
//  AnnoataionView.h
//  AmusementPark
//
//  Created by HARSHA on 01/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<QuartzCore/QuartzCore.h>
#import "AnnotationInfo.h"
#import <CoreLocation/CoreLocation.h>

@protocol AnnoataionViewDelegate

-(void)gotoDetailPagewithlandIndex:(int)pLandIndex Index:(int)pIndex type:(NSString *)pType;

@end


@interface AnnoataionView : UIView
{
  UILabel *m_WaitTimeLabel;
  UIImageView *m_WaitTimeImageView;
  UILabel *m_TitleLabel;
  UIImageView *m_TitleImageView;
  UIButton *m_Button;
  id<AnnoataionViewDelegate> m_delegate;
  NSString *m_Type;
  int pLandIndex,pIndex,width;  
  int m_Xcoordinate,m_Ycoordinate;
  AnnotationInfo *m_AnnoataionInfo;
}

@property(nonatomic,assign) UILabel *m_WaitTimeLabel;
@property(nonatomic,assign) UIImageView *m_WaitTimeImageView;
@property(nonatomic,retain) UIButton *m_Button;
@property(nonatomic,retain) NSString *m_Type;
@property(nonatomic,assign) UILabel *m_TitleLabel;
@property(nonatomic,assign) UIImageView *m_TitleImageView;
@property(nonatomic,assign) id<AnnoataionViewDelegate> m_delegate;
@property(nonatomic) int m_Xcoordinate,m_Ycoordinate;
- (id)initWithFrame:(CGRect)frame annotation:(AnnotationInfo *)pAnnotation;
-(void)hideTitle;
-(void)showTitle;
-(void)calculateCoOrdinates;

@end
