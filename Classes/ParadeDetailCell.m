//
//  ParadeDetailCell.m
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ParadeDetailCell.h"


@implementation ParadeDetailCell
@synthesize m_TitleLabel,m_LocationLabel,m_TypeLabel,m_WhenLabel,pLocationLabel,pTypeLabel,pWhenLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style  reuseIdentifier:reuseIdentifier];
    if (self)
	{
		m_TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(23,6,20,20)];
		m_TitleLabel.backgroundColor = [UIColor clearColor];
		m_TitleLabel.textAlignment = UITextAlignmentLeft;
		m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
		m_TitleLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:1.0];
    m_TitleLabel.shadowColor=[UIColor whiteColor];
    m_TitleLabel.shadowOffset=CGSizeMake(0, 1);
		[self addSubview:m_TitleLabel];
		
    pLocationLabel = [[UILabel alloc]initWithFrame:CGRectMake(23,6,70,30)];
		pLocationLabel.backgroundColor = [UIColor clearColor];
		pLocationLabel.textAlignment = UITextAlignmentLeft;
    pLocationLabel.text=@"Location : ";
		pLocationLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
		pLocationLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:1.0];
		[self addSubview:pLocationLabel]; 
        
		m_LocationLabel = [[UILabel alloc]initWithFrame:CGRectMake(75,0,20,20)];
		m_LocationLabel.backgroundColor = [UIColor clearColor];
		m_LocationLabel.textAlignment = UITextAlignmentLeft;
		m_LocationLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
		m_LocationLabel.textColor = [UIColor colorWithRed:146.0/255 green:77.0/255 blue:114.0/255 alpha:1.0];
		[self addSubview:m_LocationLabel];
		
    pTypeLabel = [[UILabel alloc]initWithFrame:CGRectMake(23,6,70,30)];
		pTypeLabel.backgroundColor = [UIColor clearColor];
		pTypeLabel.textAlignment = UITextAlignmentLeft;
    pTypeLabel.text=@"Type : ";
		pTypeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
    pTypeLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:1.0];
		[self addSubview:pTypeLabel]; 
    
		m_TypeLabel = [[UILabel alloc]initWithFrame:CGRectMake(75,0,20,20)];
		m_TypeLabel.backgroundColor = [UIColor clearColor];
		m_TypeLabel.textAlignment = UITextAlignmentLeft;
		m_TypeLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
		m_TypeLabel.textColor = [UIColor colorWithRed:146.0/255 green:77.0/255 blue:114.0/255 alpha:1.0];
		[self addSubview:m_TypeLabel];

    pWhenLabel = [[UILabel alloc]initWithFrame:CGRectMake(23,6,70,30)];
		pWhenLabel.backgroundColor = [UIColor clearColor];
		pWhenLabel.textAlignment = UITextAlignmentLeft;
    pWhenLabel.text=@"When : ";
		pWhenLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
		pWhenLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:1.0];
		[self addSubview:pWhenLabel]; 
    
		m_WhenLabel = [[UILabel alloc]initWithFrame:CGRectMake(75,0,20,20)];
		m_WhenLabel.backgroundColor = [UIColor clearColor];
		m_WhenLabel.textAlignment = UITextAlignmentLeft;
    m_WhenLabel.numberOfLines=0;
		m_WhenLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
		m_WhenLabel.textColor = [UIColor colorWithRed:146.0/255 green:77.0/255 blue:114.0/255 alpha:1.0];
		[self addSubview:m_WhenLabel];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
}


- (void)dealloc
{
	if(m_TitleLabel ){[m_TitleLabel  removeFromSuperview];[m_TitleLabel  release];m_TitleLabel =nil;}
	if(m_LocationLabel ){[m_LocationLabel  removeFromSuperview];[m_LocationLabel  release];m_LocationLabel =nil;}
	if(m_TypeLabel ){[m_TypeLabel  removeFromSuperview];[m_TypeLabel  release];m_TypeLabel =nil;}
	if(pLocationLabel ){[pLocationLabel  removeFromSuperview];[pLocationLabel  release];pLocationLabel =nil;}
  if(m_WhenLabel ){[m_WhenLabel  removeFromSuperview];[m_WhenLabel  release];m_WhenLabel =nil;}
	if(pTypeLabel ){[pTypeLabel  removeFromSuperview];[pTypeLabel  release];pTypeLabel =nil;}
	if(pWhenLabel){[pWhenLabel  removeFromSuperview];[pWhenLabel  release];pWhenLabel =nil;}
  
    [super dealloc];
}


@end
