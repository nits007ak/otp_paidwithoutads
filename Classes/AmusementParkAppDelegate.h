//
//  AmusementParkAppDelegate.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@class MainViewController;
@class SplashScreen;
@class AppYeaLogoScreen;
@interface AmusementParkAppDelegate : NSObject <UIApplicationDelegate,UIAlertViewDelegate> 
{
    UIWindow *window;
	MainViewController *m_pMainViewController;
	SplashScreen *m_splashScreen;
    AppYeaLogoScreen *m_appyeaLogoScreen;
	UINavigationController *m_NavigationController;
    NSTimer *m_Timer;
    NSMutableData *buffer;
    NSMutableDictionary *itunesLinkDict;
    NSArray *_appNames;
    NSTimer *Timer;
    int aTimerTaped;
    float aTimerInterval;
    BOOL NoInternetAtFirstLaunch;
    NSString *ticketPageLink;
    BOOL isIndependent;
    int subviewCount;  //cp
    BOOL isPortrait;
    NSMutableDictionary *xmlHelper;
    NSString* currentString;
}

@property (nonatomic)BOOL isIndependent;
@property(nonatomic,readonly)UIViewController *RootViewController;
@property(nonatomic,readonly)NSDictionary *ITunesLinkDictionary;
@property(nonatomic,readonly,getter = getAppNames)NSArray *AppNames;
@property (nonatomic)BOOL isPortrait;
@property(nonatomic, assign)int subviewCount;//cp




-(void)showAlertView;
-(void)resetAlertFlag;
-(void)LoadItuneDictWhenNoInternet;
-(void)checkForInAppPurchase;
-(void)buyTicket;
-(void)IsInappPurchaseSucceded:(BOOL)Success;
@end

