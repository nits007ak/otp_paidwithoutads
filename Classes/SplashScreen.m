//
//  SplashScreen.m
//  XXX
//
//  Created by XXX on 30/12/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "SplashScreen.h"

@implementation SplashScreen


- (id)initWithFrame:(CGRect)frame 
{
  if (self = [super initWithFrame:frame]) 
  {
    self.backgroundColor = [UIColor whiteColor];
    
    UIImage* splashImage = [[UIImage alloc] initWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Default.png"]];
      CGRect screenBounds = [[UIScreen mainScreen] bounds];
      UIImageView* splashImageView;
//      if (screenBounds.size.height == 568) {
//          splashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,548)];
//          // code for 4-inch screen
//      } else {
//          // code for 3.5-inch screen
//          splashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,460)];
//      }

      if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
          splashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,460)];
          if ([UIScreen mainScreen].bounds.size.height == 568) {
          splashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,548)];
          }
      } else if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
           splashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,768,1024)];
         
      }
      
      
    splashImageView.image = splashImage;
    [self addSubview:splashImageView];
    [splashImageView release]; 
    [splashImage release];
  }
  
  return self;
}

- (void)dealloc 
{
  [super dealloc];
}

@end
