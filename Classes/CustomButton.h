//
//  CustomButton.h
//  AmusementPark
//
//  Created by HARSHA on 14/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomButton;
@protocol CustomButtonDelegate

-(void)ButtonPressed:(int)pSection Row:(int)pRow;

@end


@interface CustomButton : UIView 
{
  UIButton *m_Button;
  UIImageView *m_ImageView;
  int row;
  int section;
  id<CustomButtonDelegate>m_delegate;
}
@property(nonatomic,assign) id<CustomButtonDelegate>m_delegate;;
@property(nonatomic,retain) UIButton *m_Button;
@property(nonatomic) int row;
@property(nonatomic) int section;
@property(nonatomic,assign) UIImageView *m_ImageView;
@end
