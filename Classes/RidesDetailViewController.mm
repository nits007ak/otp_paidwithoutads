    //
//  RidesDetailViewController.m
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RidesDetailViewController.h"
#import "AmusementParkDB.h"
#import "XMLDataParser.h"
#import "Land.h"
#import "LandInfo.h"
//#import "FBConnectController.h"
#import "AmusementParkAppDelegate.h"
#import "FullVersionUpgrader.h"
#import "AdsManager.h"
#import "FBConnectionViewController.h"


@implementation RidesDetailViewController
@synthesize m_delegate;
-(id)initwithStudioId:(NSString *)pStuId ParkId:(NSString *)pParkId LandId:(NSString *)pLandId RideId:(NSString *)pRideId LandName:(NSString *)pLandName Lat:(NSString *)pLat Long:(NSString *)pLong Distance:(int)dist isFavourite:(BOOL)fav
{
	if(self=[super init])
	{
        isFav=fav;
        m_StudioId=pStuId;
        [m_StudioId retain];
        m_ParkId=pParkId;
        [m_ParkId retain];
        m_LandId=pLandId;
        [m_LandId retain];
        m_RideId=pRideId;
        [m_RideId retain];
        m_LandName=pLandName;
        [m_LandName retain];
        m_Latitude=pLat;
        [m_Latitude retain];
        m_Longitude=pLong;
        [m_Longitude retain];
        m_Distance=dist;
        m_RideFromDb=nil;
        m_ImagePath=nil;
        m_Ride=nil;
        mURLConnection=nil;
        mResponseData=nil;
        m_activityIndicator=nil;
		m_BackgroundView=nil;
		m_ImageView=nil;
        m_FavouriteImageView=nil;
        m_FaceBookImageView=nil;
		m_TitleLabel=nil;
		m_WaitTimeTitleLabel=nil;
		m_WaitTimeLabel=nil;
		m_FavoriteButton=nil;
		m_FacebookButton=nil;
		m_FavoriteLabel=nil;
		m_FacebookLabel=nil;
		m_DescriptionLabel=nil;
		m_DescriptonView=nil;
		m_SubmitButton=nil;
		pTitleLabel=nil;
        m_RideLengLabel=nil;
        m_RideLengSecLabel=nil;
        m_HeightReqLabel=nil;
        m_AgeLabel=nil;
        m_FastPassButton=nil;
        isFirst=YES;
        
        m_Timer=nil;
        
		m_SubmitWaitTimeViewController=nil;
        m_RidesArray=[[NSMutableArray alloc]init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRecieved) name:@"NotificationName" object:nil];
	}
	return self;
	
}

-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	[self.navigationItem setHidesBackButton:YES animated:NO];
  
    if (!scrollView) {
        scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
        scrollView.backgroundColor = [UIColor clearColor];
        scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 10);
        [scrollView setScrollEnabled:YES];
    }

    
  if(m_BackButton==nil)
  {
	m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
	[m_BackButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/backbtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
  [m_BackButton retain];
  }

  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(70, 8, 160, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  
  if(m_SubmitButton==nil)
  {
	m_SubmitButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_SubmitButton.frame=CGRectMake(235, 8, 80.5, 30);
	[m_SubmitButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/RidesDetailPage/submit" ofType:@"png"]] forState:UIControlStateNormal];
	[m_SubmitButton addTarget:self action:@selector(submitWaitTimeAction) forControlEvents:UIControlEventTouchUpInside];
	[m_SubmitButton retain];		
	}
    
	
    if (screenBounds.size.height == 568) {
        BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }
  BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:BGImageView];	
	[BGImageView release];
  
  
   AdOffset=0;
  AdOffset=51;
  
  UIImageView * RideBGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(2, 5.5+AdOffset, 316, 354-AdOffset)];
  RideBGImageView.image=[UIImage imageNamed:@"ImagesCommon/RidesDetailPage/RidesDetailBG3.png"];
    RideBGImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
 	//[self.view addSubview:RideBGImageView];
    [scrollView addSubview:RideBGImageView];
	[RideBGImageView release];
	
  if(m_ImageView==nil)
	m_ImageView=[[UIImageView alloc]initWithFrame:CGRectMake(24, 31+AdOffset, 92,103)];
	m_ImageView.backgroundColor=[UIColor clearColor];
  m_ImageView.layer.cornerRadius = 4.0;
  m_ImageView.layer.masksToBounds = YES;
	//[self.view addSubview:m_ImageView];
    [scrollView addSubview:m_ImageView];
	
  if(m_TitleLabel==nil)
	m_TitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(130, 22+AdOffset, 163, 30)];
  m_TitleLabel.backgroundColor=[UIColor clearColor];
  m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
  m_TitleLabel.textColor = [UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
	m_TitleLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_TitleLabel];
	
  if(m_WaitTimeTitleLabel==nil)
	m_WaitTimeTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(140, 52+AdOffset, 100, 30)];
	m_WaitTimeTitleLabel.text=@"Wait Time";
  m_WaitTimeTitleLabel.backgroundColor=[UIColor clearColor];
  m_WaitTimeTitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
  m_WaitTimeTitleLabel.textColor = [UIColor blackColor];
	m_WaitTimeTitleLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_WaitTimeTitleLabel];

  if(m_WaitTimeLabel==nil)
	m_WaitTimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(268, 50+AdOffset, 22, 22)];
  m_WaitTimeLabel.layer.cornerRadius = 4.0;
  m_WaitTimeLabel.layer.masksToBounds = YES;
	m_WaitTimeLabel.textAlignment=UITextAlignmentCenter;
  m_WaitTimeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24];
  if(isFirst)
  m_WaitTimeLabel.backgroundColor=[UIColor clearColor];
  m_WaitTimeLabel.textColor = [UIColor blackColor];
	[scrollView addSubview:m_WaitTimeLabel];
	
  if(m_FavoriteButton)
  {[m_FavoriteButton release];m_FavoriteButton=nil;}
	m_FavoriteButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_FavoriteButton.frame=CGRectMake(24, 144+AdOffset, 92, 25.5);
	m_FavoriteButton.backgroundColor=[UIColor clearColor];
	[m_FavoriteButton addTarget:self action:@selector(AddtoFavouriteAction) forControlEvents:UIControlEventTouchUpInside];
  m_FavoriteButton.userInteractionEnabled=NO;
  [m_FavoriteButton retain];
	[scrollView addSubview:m_FavoriteButton];
  
  if(m_FavouriteImageView==nil)
  m_FavouriteImageView=[[UIImageView alloc]initWithFrame:CGRectMake(93, 144+AdOffset, 20.5, 21.5)];
  m_FavouriteImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/RidesDetailPage/favorite" ofType:@"png"]];
  m_FavouriteImageView.hidden=YES;
  [scrollView addSubview:m_FavouriteImageView];
  
  if(!isFirst)
  {
    if(!isFav)
    {
      if([[AmusementParkDB sharedPtDatabase]getDuplicate:m_Ride])
      {
        m_FavouriteImageView.hidden=NO;
      }    
      else 
      {
        m_FavouriteImageView.hidden=YES;
      }
    }
    else 
    {
      if([[AmusementParkDB sharedPtDatabase]getDuplicate:m_RideFromDb])
      {
        m_FavouriteImageView.hidden=NO;
      }    
      else 
      {
        m_FavouriteImageView.hidden=YES;
      }    
    }
  }
  
  if(m_FacebookButton)
  {[m_FacebookButton release];m_FacebookButton=nil;}
	m_FacebookButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_FacebookButton.frame=CGRectMake(24, 180+AdOffset, 92, 25.5);
	m_FacebookButton.backgroundColor=[UIColor clearColor];
	[m_FacebookButton addTarget:self action:@selector(launchFacebook) forControlEvents:UIControlEventTouchUpInside];
	[scrollView addSubview:m_FacebookButton];	
  [m_FacebookButton retain];
	
  if(m_FaceBookImageView==nil)
  m_FaceBookImageView=[[UIImageView alloc]initWithFrame:CGRectMake(96, 183+AdOffset, 17, 17.5)];
  m_FaceBookImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/RidesDetailPage/facebook" ofType:@"png"]];
  [scrollView addSubview:m_FaceBookImageView];  
  
  if(m_RideLengLabel==nil)
	m_RideLengLabel=[[UILabel alloc]initWithFrame:CGRectMake(228, 180+AdOffset, 70, 16)];
  m_RideLengLabel.backgroundColor=[UIColor clearColor];
  m_RideLengLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
  m_RideLengLabel.textColor = [UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
	m_RideLengLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_RideLengLabel];
  
  if(m_RideLengSecLabel==nil)
  m_RideLengSecLabel=[[UILabel alloc]initWithFrame:CGRectMake(223, 196+AdOffset, 70, 16)];
  m_RideLengSecLabel.backgroundColor=[UIColor clearColor];
  m_RideLengSecLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
  m_RideLengSecLabel.textColor = [UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
	m_RideLengSecLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_RideLengSecLabel];  
  
  if(m_AgeLabel==nil)
  m_AgeLabel=[[UILabel alloc]initWithFrame:CGRectMake(220, 150+AdOffset, 70, 20)];
  m_AgeLabel.backgroundColor=[UIColor clearColor];
  m_AgeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
  m_AgeLabel.textColor = [UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
	m_AgeLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_AgeLabel];
  
  if(m_HeightReqLabel==nil)
  m_HeightReqLabel=[[UILabel alloc]initWithFrame:CGRectMake(232, 122+AdOffset, 70, 20)];
  m_HeightReqLabel.backgroundColor=[UIColor clearColor];
  m_HeightReqLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
  m_HeightReqLabel.textColor = [UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
	m_HeightReqLabel.textAlignment=UITextAlignmentLeft;
	[scrollView addSubview:m_HeightReqLabel];  
  
  if(m_FastPassButton==nil)
  {
  m_FastPassButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_FastPassButton.frame=CGRectMake(225, 94+AdOffset, 18, 16);
	m_FastPassButton.backgroundColor=[UIColor clearColor];
  [m_FastPassButton retain];
  }
  [scrollView addSubview:m_FastPassButton];
  
  if(m_DescriptonView==nil)
	m_DescriptonView = [[UITextView alloc] init];
	m_DescriptonView.frame=CGRectMake(20, 225+AdOffset, 280, 120-AdOffset);
	m_DescriptonView.textAlignment=UITextAlignmentLeft;
  m_DescriptonView.editable=NO;
  m_DescriptonView.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
  m_DescriptonView.textColor = [UIColor blackColor];
	m_DescriptonView.backgroundColor = [UIColor  clearColor];
     m_DescriptonView.autoresizingMask = UIViewAutoresizingFlexibleWidth; //cp
	m_DescriptonView.autocapitalizationType=UITextAutocapitalizationTypeSentences;
	[scrollView  addSubview:m_DescriptonView];
  
  if(isFirst)
  {
  if(!isFav)
  {
    [self connectToServer];
    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [m_activityIndicator startAnimating];
    m_activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    [scrollView addSubview:m_activityIndicator];
  }
  else
  {
    [self getFromDataBase];
    m_FavoriteButton.userInteractionEnabled=YES;
    m_FavouriteImageView.hidden=NO;
  }
  }
  isFirst=NO;
    
    [self.view addSubview:scrollView];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
    
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}
-(void)layoutSubView{
    
    
    [self manageWaitTimeLabel];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (appDelegate.isPortrait) {
        m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
        m_SubmitButton.frame=CGRectMake(235, 8, 80.5, 30);
        m_RideLengSecLabel.frame = CGRectMake(223, 196+AdOffset, 70, 16);
        m_TitleLabel.frame  = CGRectMake(130, 22+AdOffset, 163, 30);
        m_ImageView.frame = CGRectMake(24, 31+AdOffset, 92,103);
        [scrollView setScrollEnabled:NO];
        scrollView.frame = CGRectMake(0, 0, 320, 480);
        m_RideLengLabel.frame  = CGRectMake(228, 180+AdOffset, 70, 16);
        m_AgeLabel.frame = CGRectMake(220, 150+AdOffset, 70, 20);
        m_HeightReqLabel.frame = CGRectMake(232, 122+AdOffset, 70, 20);
        m_SubmitButton.frame=CGRectMake(235, 8, 80.5, 30);
        pTitleLabel.frame = CGRectMake(70, 8, 160, 30);
        m_FastPassButton.frame=CGRectMake(225, 94+AdOffset, 18, 16);
        m_FavouriteImageView.frame = CGRectMake(93, 144+AdOffset, 20.5, 21.5);
        m_FaceBookImageView.frame=CGRectMake(96, 183+AdOffset, 17, 17.5);
        m_WaitTimeTitleLabel.frame = CGRectMake(140, 52+AdOffset, 100, 30);
        m_DescriptonView.frame=CGRectMake(20, 225+AdOffset, 280, 120-AdOffset);
        BGImageView.frame = CGRectMake(0, 0, 320, 480);
         [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,320,50)];
        [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 50)];
        if(screenBounds.size.height == 568)BGImageView.frame = CGRectMake(0, 0, 320,568);
        
    }
    else
    {
        
        m_BackButton.frame=CGRectMake(2, 1, 53.5, 30);
        m_SubmitButton.frame=CGRectMake(235, 1, 80.5, 30);
        m_RideLengSecLabel.frame = CGRectMake(380, 196+AdOffset, 70, 16);
        m_HeightReqLabel.frame = CGRectMake(380, 122+AdOffset, 70, 20);
        [scrollView setScrollEnabled:YES];
        m_AgeLabel.frame = CGRectMake(380, 150+AdOffset, 70, 20);
        scrollView.frame = CGRectMake(0, 0, 480, 320);
        scrollView.contentSize = CGSizeMake(480, 500);
        m_SubmitButton.frame=CGRectMake(390, 3, 80.5, 30);
        pTitleLabel.frame = CGRectMake(60, 1, 336, 30);
        m_RideLengLabel.frame  = CGRectMake(380, 180+AdOffset, 70, 16);
        m_FastPassButton.frame=CGRectMake(390, 94+AdOffset, 18, 16);
        m_FaceBookImageView.frame=CGRectMake(150, 183+AdOffset, 17, 17.5);
        m_FavouriteImageView.frame = CGRectMake(138, 145, 35, 24);
        m_ImageView.frame = CGRectMake(34, 31+AdOffset, 140,102);
        m_WaitTimeTitleLabel.frame = CGRectMake(200, 52+AdOffset, 100, 30);
        m_TitleLabel.frame  = CGRectMake(200, 22+AdOffset, 200, 30);
        BGImageView.frame = CGRectMake(0, 0, 480, 320);
         [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,480,50)];
        [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(373, 50)];
        if (screenBounds.size.height == 568) {
            m_TitleLabel.frame  = CGRectMake(250, 22+AdOffset, 250, 30);
            m_RideLengSecLabel.frame = CGRectMake(410, 196+AdOffset, 100, 16);
            m_WaitTimeTitleLabel.frame = CGRectMake(260, 52+AdOffset, 100, 30);
            m_FastPassButton.frame=CGRectMake(410, 94+AdOffset, 18, 16);
            m_RideLengLabel.frame  = CGRectMake(410, 180+AdOffset, 100, 16);
            m_AgeLabel.frame = CGRectMake(410, 150+AdOffset, 70, 20);
            m_HeightReqLabel.frame = CGRectMake(410, 122+AdOffset, 70, 20);
            scrollView.frame = CGRectMake(0, 0, 568, 320);
            m_SubmitButton.frame=CGRectMake(480, 1, 80.5, 30);
            pTitleLabel.frame = CGRectMake(60, 1, 420, 30);
            m_ImageView.frame = CGRectMake(41, 31+AdOffset, 164,103);
            m_FaceBookImageView.frame=CGRectMake(175, 183+AdOffset, 17, 17.5);
            m_FavouriteImageView.frame = CGRectMake(162, 145, 35, 24);
            BGImageView.frame = CGRectMake(0, 0, 568, 320);
            [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,568,50)];
            [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(461, 50)];
            
        }
        
        
    }

}
- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}
-(void)NotificationRecieved
{
  if(!isFav)
  {
    if([[AmusementParkDB sharedPtDatabase]getDuplicate:m_Ride])
    {
      m_FavouriteImageView.hidden=NO;
    }    
    else 
    {
      m_FavouriteImageView.hidden=YES;
    }
  }
  else 
  {
    if([[AmusementParkDB sharedPtDatabase]getDuplicate:m_RideFromDb])
    {
      m_FavouriteImageView.hidden=NO;
    }    
    else 
    {
      m_FavouriteImageView.hidden=YES;
    }    
  }
}

- (void)launchFacebook
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"ride"];
    NSString *pParkName=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
    if(isFav)
    {
        
        [[NSUserDefaults standardUserDefaults]setObject:m_RideFromDb.m_RideName forKey:@"rideName"];
        [[NSUserDefaults standardUserDefaults]setObject:pParkName forKey:@"parkName"];
    }
    
    else{
        [[NSUserDefaults standardUserDefaults]setObject:m_Ride.m_RideName forKey:@"rideName"];
        [[NSUserDefaults standardUserDefaults]setObject:pParkName forKey:@"parkName"];
    }
    
    
      
    FBConnectionViewController * connection = [[FBConnectionViewController alloc]initWithNibName:@"FBConnectionViewController" bundle:nil];
    [self presentViewController:connection animated:YES completion:nil];
}

-(void)viewDidLoad
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void)viewWillAppear:(BOOL)animated
{
    if(!isFav)
    {
        if(m_Timer)
        {
            [m_Timer invalidate];
            [m_Timer release];
            m_Timer=nil;
        }
        m_Timer=[NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(refreshData) userInfo:nil repeats:YES];
        [m_Timer retain];
    }
     [self layoutSubView];
    
   // [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,5,320,50)];
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount)
    {
        
    }
    else
    {
        [self.navigationController.navigationBar addSubview:m_BackButton];
        [self.navigationController.navigationBar addSubview:m_SubmitButton];
        [self.navigationController.navigationBar addSubview:pTitleLabel];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}
-(void)manageWaitTimeLabel
{
    if(!appDelegate.isPortrait)
    {
        CGRect frame = m_WaitTimeLabel.frame;
        int xval = frame.origin.x;
        xval = xval +130;
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            xval = xval +200;
        }
        
        
        if (xval>470) {
            xval = 470;
        }
        frame.origin.x = xval;
        m_WaitTimeLabel.frame = frame;
    }
    else
    {
        CGRect frame = m_WaitTimeLabel.frame;
        
        frame.origin.x = 247;
        m_WaitTimeLabel.frame = frame;
    }
}


#pragma mark URLConnectionManager Methods

-(void)connectToServer
{
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  m_FavoriteButton.userInteractionEnabled=NO;
  m_SubmitButton.userInteractionEnabled=NO;
  m_BackButton.userInteractionEnabled=NO;
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	[request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getridedetail&rideid=%@",m_RideId]]];
	[request setHTTPMethod:@"POST"];
	mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }  
  m_FavoriteButton.userInteractionEnabled=YES;
  m_SubmitButton.userInteractionEnabled=YES;
  m_BackButton.userInteractionEnabled=YES;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
	//printf("\n %s",[[error localizedDescription] UTF8String]);
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDelegatelocal=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegatelocal showAlertView];
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{  
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *filePath = [paths objectAtIndex:0];
		paths=nil;
		filePath = [filePath stringByAppendingPathComponent:@"RidesDetail.xml"];
		[mResponseData writeToFile:filePath atomically:YES];
		filePath=nil;
		[mResponseData release];
		mResponseData=nil;
    if([m_RidesArray count]){[m_RidesArray removeAllObjects];}
     [m_RidesArray addObjectsFromArray:[XMLDataParser getRideDetailFromXML]];
     if([m_RidesArray count])
     {
       if(m_Ride){[m_Ride release];m_Ride=nil;}
      m_Ride=[m_RidesArray objectAtIndex:0];
       m_Ride.m_StudioId=m_StudioId;
       m_Ride.m_ParkId=m_ParkId;
       m_Ride.m_LandId=m_LandId;
       m_Ride.m_RideId=m_RideId;       
      [m_Ride retain];
       
       if([[AmusementParkDB sharedPtDatabase]getDuplicate:m_Ride])
       {
         m_FavouriteImageView.hidden=NO;
       }
       int AdOffsetlocal=0;
        AdOffsetlocal=49;
       
      m_TitleLabel.text=m_Ride.m_RideName;
       
      int waittime=[m_Ride.m_WaitTime intValue];
       if(waittime<=20)
        m_WaitTimeLabel.backgroundColor=[UIColor colorWithRed:127.0/255 green:190.0/255 blue:0.0/255 alpha:1.0];
       else if(waittime<60)
        m_WaitTimeLabel.backgroundColor=[UIColor colorWithRed:255.0/255 green:223.0/255 blue:0.0/255 alpha:1.0];
       else if(waittime>=60)
        m_WaitTimeLabel.backgroundColor=[UIColor redColor];      

       int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];

       if(waittime==-1 || open==0)
       {
         m_WaitTimeLabel.frame=CGRectMake(247, 52.5+AdOffsetlocal, 45, 29);
         m_WaitTimeLabel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]]];
         m_WaitTimeLabel.text=@"";
       }
       else if(waittime==75)
       {
         m_WaitTimeLabel.frame=CGRectMake(251, 52+AdOffsetlocal, 42, 30);
         m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",m_Ride.m_WaitTime];
       }
       else 
       {
         m_WaitTimeLabel.frame=CGRectMake(263, 52+AdOffsetlocal, 30, 30);
         m_WaitTimeLabel.text=m_Ride.m_WaitTime;
       }
       
         [self manageWaitTimeLabel];
      m_DescriptonView.text=m_Ride.m_Description;
      m_DescriptionLabel.text=m_Ride.m_RideName;
      if([m_Ride.m_FastPass intValue])
      {
        [m_FastPassButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/RestDetailPage/mark" ofType:@"png"]] forState:UIControlStateNormal];
      }
      else 
      {
        [m_FastPassButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/cross1" ofType:@"png"]] forState:UIControlStateNormal];
      }
      if(m_Ride.m_Height)
      {
        if(m_Ride.m_Height==100000)
        {
          m_HeightReqLabel.text=@"None";
        }
        else 
        {
          if(m_Ride.m_Height>=12)
          {
            int feet=m_Ride.m_Height/12;
            int inch=m_Ride.m_Height%12;
          if(inch>0)
            m_HeightReqLabel.text=[NSString stringWithFormat:@"%d\" %d'",feet,inch];
          else 
            m_HeightReqLabel.text=[NSString stringWithFormat:@"%d\"",feet];
          }
        }
      }
      else 
        m_HeightReqLabel.text=[NSString stringWithFormat:@"%d'",m_Ride.m_Height];
      
       if(m_Ride.m_RideLength!=NULL)
       {
         NSArray *subStringArray=[m_Ride.m_RideLength componentsSeparatedByString:@","];
         if([subStringArray count]>1)
         {
           m_RideLengLabel.text=[subStringArray objectAtIndex:0];
           m_RideLengSecLabel.text=[subStringArray objectAtIndex:1];
         }
         else 
         {
           m_RideLengLabel.text=[subStringArray objectAtIndex:0];
         }
       }
      if(m_Ride.m_Age)
      {
        if(m_Ride.m_Age==100000)
          m_AgeLabel.text=@"All Ages";
        else 
          m_AgeLabel.text=[NSString stringWithFormat:@"%d+",m_Ride.m_Age];
      }
      [self downloadImages];
    }
	}
}	

-(void)downloadImages
{
  NSMutableArray *pArray=[[NSMutableArray alloc]init];
  Ride *pRide=[m_RidesArray objectAtIndex:0];
  RemoteFile *pRemoteFile=[[RemoteFile alloc]init];
  pRemoteFile.m_remoteURL=pRide.m_ImageId;
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *filePath = [paths objectAtIndex:0];
  paths=nil;
  NSString *identifier=[pRide.m_ImageId lastPathComponent];
  NSString *identifier1=[identifier stringByDeletingPathExtension];
  //printf("\n%s\n",[identifier1 UTF8String]);
  filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"Ride%@%@%@",m_LandId,pRide.m_RideId,identifier1]];
  if(![[NSFileManager defaultManager]fileExistsAtPath:filePath])
  {
  pRemoteFile.m_localFilePath=filePath;
  filePath=nil;
  [pArray addObject:pRemoteFile];
  if([pArray count])
  {
    m_ImageResourceDownloader=[[ImageResourceDownloader alloc]init];
    m_ImageResourceDownloader.m_delegate=self;
    [m_ImageResourceDownloader downloadImages:pArray];
  }
  }
  
  else
  {
    m_FavoriteButton.userInteractionEnabled=YES;
    m_SubmitButton.userInteractionEnabled=YES;
    m_BackButton.userInteractionEnabled=YES;
    if (m_activityIndicator)
    {
      [m_activityIndicator stopAnimating];
      [m_activityIndicator removeFromSuperview];
      [m_activityIndicator release];
      m_activityIndicator=nil;
    }  
    UIImage *pImage=[[UIImage alloc]initWithContentsOfFile:filePath];
    m_ImageView.image=pImage;
    [pImage release];
    if(m_ImagePath){[m_ImagePath release];m_ImagePath=nil;}
    m_ImagePath=filePath;
    [m_ImagePath retain];
    filePath=nil;
  }
  [pRemoteFile release];
  pRemoteFile=nil;
  if([pArray count])
  {
    [pArray removeAllObjects];
  }
    [pArray release];
    pArray=nil;
}

#pragma mark ImageResourceDownloader delegate methods
- (void)fileDowloadedCompleted:(ImageResourceDownloader*)downloader fileName:(NSString*)name localfile:(NSString *)localfilename
{
  UIImage *pImage=[[UIImage alloc]initWithContentsOfFile:localfilename];
  m_ImageView.image=pImage;
  [pImage release];
  if(m_ImagePath){[m_ImagePath release];m_ImagePath=nil;}
  m_ImagePath=localfilename;
  [m_ImagePath retain];
}

- (void)batchDowloadCompleted:(ImageResourceDownloader*)downloader
{
  m_FavoriteButton.userInteractionEnabled=YES;
  m_SubmitButton.userInteractionEnabled=YES;
  m_BackButton.userInteractionEnabled=YES;
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }  
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
}

- (void)errorDownloading:(ImageResourceDownloader*)downloader
{
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDel=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDel showAlertView];
  }
  
  m_SubmitButton.userInteractionEnabled=YES;
  m_FavoriteButton.userInteractionEnabled=YES;
  m_BackButton.userInteractionEnabled=YES;
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
}

#pragma mark Database methods
-(void)getFromDataBase
{
  m_RideFromDb=[[AmusementParkDB sharedPtDatabase]getRideWithRideId:m_RideId];
 m_TitleLabel.text=m_RideFromDb.m_RideName;
  
  int waittime=[m_RideFromDb.m_WaitTime intValue];
  if(waittime<=20)
    m_WaitTimeLabel.backgroundColor=[UIColor colorWithRed:127.0/255 green:190.0/255 blue:0.0/255 alpha:1.0];
  else if(waittime<60)
    m_WaitTimeLabel.backgroundColor=[UIColor colorWithRed:255.0/255 green:223.0/255 blue:0.0/255 alpha:1.0];
  else if(waittime>=60)
    m_WaitTimeLabel.backgroundColor=[UIColor redColor];  
  
  int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];
  int AdOffsetlocal=0;
  AdOffsetlocal=51;
  
  if(waittime==-1 || open==0)
  {
    m_WaitTimeLabel.frame=CGRectMake(247, 52.5+AdOffsetlocal, 45, 29);
    m_WaitTimeLabel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]]];
    m_WaitTimeLabel.text=@"";
  }
  else if(waittime==75)
  {
    m_WaitTimeLabel.frame=CGRectMake(251, 52+AdOffsetlocal, 42, 30);
    m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",m_RideFromDb.m_WaitTime];
  }
  else 
  {
    m_WaitTimeLabel.frame=CGRectMake(263, 52+AdOffsetlocal, 30, 30);
    m_WaitTimeLabel.text=m_RideFromDb.m_WaitTime;
  }
  
    [self manageWaitTimeLabel];
  m_DescriptonView.text=m_RideFromDb.m_Description;
  m_DescriptionLabel.text=m_RideFromDb.m_RideName;  
  
  UIImage *image=[[UIImage alloc]initWithContentsOfFile:m_RideFromDb.m_ImageId];
  m_ImageView.image=image;
  [image release];
  
  if([m_RideFromDb.m_FastPass intValue])
  {
    [m_FastPassButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/RestDetailPage/mark" ofType:@"png"]] forState:UIControlStateNormal];
  }
  else 
  {
    [m_FastPassButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/cross1" ofType:@"png"]] forState:UIControlStateNormal];
  }
  
  if(m_RideFromDb.m_Height)
  {
    if(m_RideFromDb.m_Height==100000)
    {
      m_HeightReqLabel.text=@"None";
    }
    else 
    {
      if(m_RideFromDb.m_Height>=12)
      {
        int feet=m_RideFromDb.m_Height/12;
        int inch=m_RideFromDb.m_Height%12;
        if(inch>0)
          m_HeightReqLabel.text=[NSString stringWithFormat:@"%d\" %d'",feet,inch];
        else 
          m_HeightReqLabel.text=[NSString stringWithFormat:@"%d\"",feet];
      }
    }
  }
  else 
    m_HeightReqLabel.text=[NSString stringWithFormat:@"%d'",m_RideFromDb.m_Height];
  
  if(m_RideFromDb.m_RideLength!=NULL)
  {
    NSArray *subStringArray=[m_RideFromDb.m_RideLength componentsSeparatedByString:@","];
    if([subStringArray count]>1)
    {
      m_RideLengLabel.text=[subStringArray objectAtIndex:0];
      m_RideLengSecLabel.text=[subStringArray objectAtIndex:1];
    }
    else 
    {
      m_RideLengLabel.text=[subStringArray objectAtIndex:0];
    }
  }
  
  
  if(m_RideFromDb.m_Age)
  {
    if(m_RideFromDb.m_Age==100000)
      m_AgeLabel.text=@"All Ages";
    else 
      m_AgeLabel.text=[NSString stringWithFormat:@"%d+",m_RideFromDb.m_Age];    
  }
  
    if(m_ImagePath){[m_ImagePath release];m_ImagePath=nil;}
    m_ImagePath=m_RideFromDb.m_ImageId;
    [m_ImagePath retain];

    
  [m_RideFromDb retain];
}

-(void)backAction
{
  if(!isFav)
  {
    if(m_Timer)
    {
      [m_Timer invalidate];
      [m_Timer release];
      m_Timer=nil;
    }    
  }
  
  if(m_ImageResourceDownloader)
  {
    [m_ImageResourceDownloader cancel];
    m_ImageResourceDownloader.m_delegate=nil;
    [m_ImageResourceDownloader release];
    m_ImageResourceDownloader=nil;
  }
	if(m_delegate)
		[m_delegate BackAction];
	[m_BackButton removeFromSuperview];
	[m_SubmitButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)refreshData
{
  [self BackAction];
}

-(void)BackAction
{
  if(!isFav)
  {
    m_WaitTimeLabel.backgroundColor=[UIColor clearColor];
    m_WaitTimeLabel.text=@"";
    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [m_activityIndicator startAnimating];
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    [self.view addSubview:m_activityIndicator];         
   [self connectToServer];
  }
}

-(void)AddtoFavouriteAction
{
    [[FullVersionUpgrader sharedUpgrader] showAlert];
    return;
}

-(void)submitWaitTimeAction
{
  if(!isFav)
  {
    if(m_Timer)
    {
      [m_Timer invalidate];
      [m_Timer release];
      m_Timer=nil;
    }    
  }
  
	[m_SubmitButton removeFromSuperview];
  [m_BackButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	if(m_SubmitWaitTimeViewController)
	{
		[m_SubmitWaitTimeViewController.view removeFromSuperview];
		[m_SubmitWaitTimeViewController release];
		m_SubmitWaitTimeViewController=nil;
	}
	m_SubmitWaitTimeViewController=[[SubmitWaitTimeViewController alloc]initWithId:m_RideId Latitude:m_Latitude Longitude:m_Longitude isRide:YES];
	m_SubmitWaitTimeViewController.m_delegate=self;
	[self.navigationController pushViewController:m_SubmitWaitTimeViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated
{
  if(m_SubmitWaitTimeViewController){[m_SubmitWaitTimeViewController.view removeFromSuperview];m_SubmitWaitTimeViewController.m_delegate=nil;[m_SubmitWaitTimeViewController release];m_SubmitWaitTimeViewController=nil;}
  
    
}

- (void)dealloc 
{
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }  
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }      
  if(m_RideFromDb)
  {
    [m_RideFromDb release];
    m_RideFromDb=nil;
  }
  if(m_RidesArray)
  {
    [m_RidesArray removeAllObjects];
    [m_RidesArray release];
    m_RidesArray=nil;
  }
  if(m_StudioId)
  {
    [m_StudioId release];
    m_StudioId=nil;
  }
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(m_LandId){[m_LandId release];m_LandId=nil;}
  if(m_RideId){[m_RideId release];m_RideId=nil;}
  if(m_Ride){[m_Ride release];m_Ride=nil;}
  if(m_LandName){[m_LandName release];m_LandName=nil;}
  if(m_ImagePath){[m_ImagePath release];m_ImagePath=nil;}
  if(m_Latitude){[m_Latitude release];m_Latitude=nil;}
  if(m_Longitude){[m_Longitude release];m_Longitude=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
	if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
	if(m_SubmitButton){[m_SubmitButton removeFromSuperview];[m_SubmitButton release];m_SubmitButton=nil;}
  if(m_FavoriteButton){[m_FavoriteButton removeFromSuperview];[m_FavoriteButton release];m_FavoriteButton=nil;}
	if(m_BackgroundView){[m_BackgroundView removeFromSuperview];[m_BackgroundView release];m_BackgroundView=nil;}
	if(m_ImageView){[m_ImageView removeFromSuperview];[m_ImageView release];m_ImageView=nil;}
  if(m_FavouriteImageView){[m_FavouriteImageView removeFromSuperview];[m_FavouriteImageView release];m_FavouriteImageView=nil;}
	if(m_TitleLabel){[m_TitleLabel removeFromSuperview];[m_TitleLabel release];m_TitleLabel=nil;}
	if(m_WaitTimeTitleLabel){[m_WaitTimeTitleLabel removeFromSuperview];[m_WaitTimeTitleLabel release];m_WaitTimeTitleLabel=nil;}
	if(m_WaitTimeLabel){[m_WaitTimeLabel removeFromSuperview];[m_WaitTimeLabel release];m_WaitTimeLabel=nil;}
	if(m_FavoriteLabel){[m_FavoriteLabel removeFromSuperview];[m_FavoriteLabel release];m_FavoriteLabel=nil;}
	if(m_FacebookLabel){[m_FacebookLabel removeFromSuperview];[m_FacebookLabel release];m_FacebookLabel=nil;}
  if(m_FacebookButton){[m_FacebookButton removeFromSuperview];[m_FacebookButton release];m_FacebookButton=nil;}
	if(m_DescriptionLabel){[m_DescriptionLabel removeFromSuperview];[m_DescriptionLabel release];m_DescriptionLabel=nil;}
	if(m_DescriptonView){[m_DescriptonView removeFromSuperview];[m_DescriptonView release];m_DescriptonView=nil;}
	if(m_SubmitWaitTimeViewController){[m_SubmitWaitTimeViewController.view removeFromSuperview];[m_SubmitWaitTimeViewController release];m_SubmitWaitTimeViewController=nil;}
	if(m_RideLengLabel){[m_RideLengLabel removeFromSuperview];[m_RideLengLabel release];m_RideLengLabel=nil;}
  if(m_RideLengSecLabel){[m_RideLengSecLabel removeFromSuperview];[m_RideLengSecLabel release];m_RideLengSecLabel=nil;}
  if(m_AgeLabel){[m_AgeLabel removeFromSuperview];[m_AgeLabel release];m_AgeLabel=nil;}
  if(m_FastPassButton){[m_FastPassButton removeFromSuperview];[m_FastPassButton release];m_FastPassButton=nil;}
  if(m_ImageResourceDownloader){[m_ImageResourceDownloader cancel];m_ImageResourceDownloader.m_delegate=nil;[m_ImageResourceDownloader release];m_ImageResourceDownloader=nil;}
  if(m_HeightReqLabel){[m_HeightReqLabel removeFromSuperview];[m_HeightReqLabel release];m_HeightReqLabel=nil;}
	[super dealloc];
}


@end
