//
//  DetailViewController.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DealsViewController.h"
#import "FoodViewController.h"
#import "RidesViewController.h"
#import "FavoriteViewController.h"
#import "AmusementParkAppDelegate.h"
#import "MapImageViewController.h"

@interface DetailViewController : UIViewController<MapImageViewControllerDelegate,RidesViewControllerDelegate,FoodViewControllerDelegate,FavoriteViewControllerDelegate,DealsViewControllerDelegate,UITabBarControllerDelegate,UINavigationControllerDelegate>
{
	UIImageView *m_BGImageView;
    AmusementParkAppDelegate * appDelegate;
	RidesViewController *m_pRidesViewController;
    MapImageViewController *m_MapImageViewController;
	FoodViewController *m_pFoodViewController;
	FavoriteViewController *m_pFavoriteViewController;
	DealsViewController *m_pDealsViewController;
    UINavigationController *mapNavCntr;
    UINavigationController *rideNavCntr;
    UINavigationController *favNavCntr;
    UINavigationController *dealNavCntr;
    UINavigationController *foodNavCntr;
    UITabBarController *m_Tabc;
    NSString *m_StudioId;
    NSString *m_ParkId;
    NSString *m_ParkName;
    UIImageView *m_NavigImage;
    UIButton *m_MapButton;
    UIButton *m_FoodButton;
    UIButton *m_RidesButton;
    UIButton *m_FavButton;
    UIButton *m_DealsButton;
}
-(id)initwithStudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId bundleName:(NSString *)pParkName;
-(void)setTabBarVisiblity:(BOOL)isHidden;
@end
