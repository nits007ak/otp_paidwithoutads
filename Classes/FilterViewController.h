//
//  FilterViewController.h
//  AmusementPark
//
//  Created by Smaranika on 7/26/11.
//  Copyright 2011 iPhone_Lancers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmusementParkAppDelegate.h"

@protocol FilterViewControllerDelegate<NSObject>

-(void)BackFromFilterViewControllerDelegate;

@end

@interface FilterViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
  UILabel *pTitleLabel;
	UITableView *m_TableView;
	NSMutableArray *m_TitleArray;
	UIButton *m_BackButton;
    AmusementParkAppDelegate * appDelegate;
    UIImageView * m_BGImageView;
	id<FilterViewControllerDelegate> m_delegate;
	int m_SelectedIndex;  
}
@property(nonatomic ,assign)id<FilterViewControllerDelegate> m_delegate;
-(void)backAction;
-(void)receivedNotification;
@end
