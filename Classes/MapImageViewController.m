    //
//  MapImageViewController.m
//  AmusementPark
//
//  Created by HARSHA on 01/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MapImageViewController.h"
#import "AnnotationInfo.h"
#import "XMLDataParser.h"
#import "Land.h"
#import "Restuarant.h"
#import "Ride.h"
#import "UIDeviceHardware.h"
#import "AmusementParkAppDelegate.h"



@interface MapImageViewController()
-(CGPoint)getScrollOffsetForUserLocation:(CGPoint)userLocation;
@end
@implementation MapImageViewController

@synthesize m_delegate;

- (id)initWithName:(NSString*)refName StudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId isRide:(BOOL)ride
{
  if((self=[super init]))
  {
    m_ImageView=nil;
    m_ScrollView=nil;
    
    m_activityIndicator=nil;
    mURLConnection=nil;
    mResponseData=nil;
    
    pTitleLabel=nil;
    m_HomeButton=nil;
    m_FilterButton=nil;
    
    m_StudioId=pStudioId;
    [m_StudioId retain];
    m_ParkId=pParkId;
    [m_ParkId retain];
		m_parkName = refName;
		[m_parkName retain];
    
    isRide=ride;
    
    m_RestuarantArray=[[NSMutableArray alloc]init];
    m_RidesArray=[[NSMutableArray alloc]init];
    m_RestAnnotArray=[[NSMutableArray alloc]init];
    m_RideAnnotArray=[[NSMutableArray alloc]init];
    
    m_RidesDetailViewController=nil;
    m_RestuarantDetailViewController=nil;
    m_FilterViewController=nil;
      isFirst=YES;
      
    m_Xcoordinate=0;
    m_Ycoordinate=0;
    
    m_Latitude=0.0;
    m_Longitude=0.0;
    
    m_Width=0.0;
    m_Height=0.0;
    m_ZoomScale=0.0;
      isShowing=YES;
      
      m_UserLocationImageView=nil;
    
    m_TapRecognizer=nil;
    
    isRestAnnot=[[NSUserDefaults standardUserDefaults]integerForKey:@"RestFilterIndex"];
    isRideAnnot=[[NSUserDefaults standardUserDefaults]integerForKey:@"RideFilterIndex"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SelectedTwiceNotificationRecieved) name:@"SelectedTwice" object:nil];
    oldCenter=CGPointZero;
    

  }
  return self;
}


-(void)loadView
{

    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
  UIDeviceHardware *pUIDeviceHardware=[[UIDeviceHardware alloc]init];
  NSString *deviceName=[pUIDeviceHardware platformString];
  [pUIDeviceHardware release];
  
  NSString *imageName;
  
  if([deviceName isEqualToString:@"iPod Touch 2G"])
    imageName=[NSString stringWithFormat:@"%@",m_parkName];
  else 
    imageName=[NSString stringWithFormat:@"%@",m_parkName];
  
  m_Latitude=[[NSUserDefaults standardUserDefaults]doubleForKey:@"LATITUDE"];
  m_Longitude=[[NSUserDefaults standardUserDefaults]doubleForKey:@"LONGITUDE"];
  
  if(m_Width==0.0 || m_Height==0.0)
  {
    m_Width=[[NSUserDefaults standardUserDefaults]floatForKey:@"WIDTH"];
    m_Height=[[NSUserDefaults standardUserDefaults]floatForKey:@"HEIGHT"];
  }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGRect scrollFrame;
    CGRect imageFrame;
    if (screenBounds.size.height == 568) {
        scrollFrame = CGRectMake(0, 0, 320, 568);
        imageFrame =  CGRectMake(0, 0, m_Width, m_Height);
        
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        scrollFrame = CGRectMake(0, 0, 320, 480);
        imageFrame =  CGRectMake(0, 0, m_Width, m_Height);
        
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
    
  
   
  self.view.backgroundColor=[UIColor blackColor];
  
  
//  UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
//	imageView.frame=CGRectMake(0, 0, 320, 48.5);
//	[self.navigationController.navigationBar addSubview:imageView];
//	[imageView release];  
  
  if(m_HomeButton==nil)
  {
    m_HomeButton=[UIButton buttonWithType:UIButtonTypeCustom];
    m_HomeButton.frame=CGRectMake(5, 8, 53.5, 30);
    [m_HomeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/homebtn" ofType:@"png"]] forState:UIControlStateNormal];
    [m_HomeButton addTarget:self action:@selector(backtoHomeAction) forControlEvents:UIControlEventTouchUpInside];
    [m_HomeButton retain];
  }
  
  if(isRide)
  {
    if(m_FilterButton==nil)
    {
      m_FilterButton=[UIButton buttonWithType:UIButtonTypeCustom];
      m_FilterButton.frame=CGRectMake(260, 8, 53.5, 30);
      [m_FilterButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Filter" ofType:@"png"]] forState:UIControlStateNormal];
      [m_FilterButton addTarget:self action:@selector(FilterButtonAction) forControlEvents:UIControlEventTouchUpInside];
      [m_FilterButton retain];
    }  
  }
  
  if(pTitleLabel==nil)
    pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 8, 200, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
    
  if(m_UserLocationImageView){[m_UserLocationImageView removeFromSuperview];[m_UserLocationImageView release];m_UserLocationImageView=nil;}
   m_UserLocationImageView= [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 132, 132)];
  m_UserLocationImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/Map/userloc" ofType:@"png"]];
  m_UserLocationImageView.tag=2;

  if(m_ScrollView)
  {
    if(m_ScrollView){[m_ScrollView removeFromSuperview];[m_ScrollView release];m_ScrollView=nil;}
  }
  m_ScrollView = [[UIScrollView alloc] initWithFrame:scrollFrame];
  m_ScrollView.delegate = self;
  m_ScrollView.contentMode = (UIViewContentModeScaleAspectFit);
  m_ScrollView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
  [m_ScrollView setContentSize:CGSizeMake(m_ImageView.frame.size.width, m_ImageView.frame.size.height)];
  m_ScrollView.maximumZoomScale = [[NSUserDefaults standardUserDefaults]floatForKey:@"MAXIMUMZOOM"];
  m_ScrollView.minimumZoomScale =1.0;
  m_ScrollView.backgroundColor=[UIColor clearColor];
  m_ScrollView.clipsToBounds = YES;
    m_ScrollView.showsHorizontalScrollIndicator = NO;
    m_ScrollView.showsVerticalScrollIndicator =NO;
  [m_ScrollView setScrollEnabled:YES];
  
  if(m_ImageView)
  {
    if(m_ImageView){[m_ImageView removeFromSuperview];[m_ImageView release];m_ImageView=nil;}
  }
  m_ImageView = [[UIImageView alloc] initWithFrame:imageFrame];
    [m_ImageView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
  UIImage *image=[[UIImage alloc]initWithContentsOfFile:[[NSBundle mainBundle]pathForResource:imageName ofType:@"jpeg"]];
  m_ImageView.image=image;
  [image release];
  m_ImageView.tag=1;
  
  [m_ScrollView addSubview:m_ImageView];
  [[self view] addSubview:m_ScrollView];
  
  if([imageName length]>0)
  {
    if(isRide)
      [self connectToServer];
    isShowing=YES; 
    [self calculateDistance];
    if(!isRide)
      [self getRestArray];
  }
  else
  {
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Map under construction..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
  }

  
  if(isRide)
  {
    if(m_activityIndicator)
    {
      [m_activityIndicator stopAnimating];
      [m_activityIndicator removeFromSuperview];
      [m_activityIndicator release];
      m_activityIndicator=nil;
    }
    m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
    [m_activityIndicator startAnimating];
    m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
       m_activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    [self.view addSubview:m_activityIndicator];
      [self checkOrientation];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(didRotate:)
                                                   name:@"UIDeviceOrientationDidChangeNotification" object:nil];

      
  }
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
    
}

-(void)layoutSubView
{
    
    if(!appDelegate.isPortrait){
        m_HomeButton.frame=CGRectMake(5, 1, 53.5, 30);
        m_FilterButton.frame=CGRectMake(420, 1, 53.5, 30);
        pTitleLabel.frame = CGRectMake(60, 1, 336, 30);
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 568) {
            m_FilterButton.frame=CGRectMake(510, 1, 53.5, 30);
            pTitleLabel.frame = CGRectMake(60, 1, 420, 30);
        }
        
    }
    else{
        m_HomeButton.frame=CGRectMake(5, 8, 53.5, 30);
        m_FilterButton.frame=CGRectMake(260, 8, 53.5, 30);
        pTitleLabel.frame = CGRectMake(60, 8, 200, 30);
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{ 
    //Taps

      m_TapRecognizer=[[UITapGestureRecognizer alloc]
                  initWithTarget:self
                  action:@selector(foundTap:)];
    m_TapRecognizer.numberOfTapsRequired=2;
    m_TapRecognizer.numberOfTouchesRequired=1; 
    [m_ScrollView addGestureRecognizer:m_TapRecognizer];
  appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    [super viewDidLoad];
}
-(void)viewWillDisappear:(BOOL)animated
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}
-(void)FilterButtonAction
{
  if(m_FilterViewController)
  {
	  [m_FilterViewController.view removeFromSuperview];
    m_FilterViewController.m_delegate=nil;
	  [m_FilterViewController release];
	  m_FilterViewController=nil;
  }
	[m_HomeButton removeFromSuperview];
	[m_FilterButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	m_FilterViewController=[[FilterViewController alloc]init];
	m_FilterViewController.m_delegate=self;
	[self.navigationController pushViewController:m_FilterViewController animated:YES];  
}

- (void)foundTap:(UITapGestureRecognizer *)recognizer
{
  printf("1");
  float zoomScale=m_ScrollView.zoomScale;
  float maximumZoom=[[NSUserDefaults standardUserDefaults]floatForKey:@"MAXIMUMZOOM"];
  float middleLevel=maximumZoom/2.0;
  if(zoomScale<middleLevel && zoomScale>1.0)
    [m_ScrollView setZoomScale:1.0 animated:YES];
  else if(zoomScale>middleLevel && zoomScale<maximumZoom)
    [m_ScrollView setZoomScale:maximumZoom animated:YES];
  else if(zoomScale==1.0)
  {
    m_Increment=YES;
    [m_ScrollView setZoomScale:middleLevel animated:YES];
  }
  else if(zoomScale==maximumZoom)
  {
    m_Increment=NO;
    [m_ScrollView setZoomScale:middleLevel animated:YES];
  }
  else if(zoomScale==middleLevel)
  {
    if(m_Increment)
      [m_ScrollView setZoomScale:maximumZoom animated:YES];
    else 
      [m_ScrollView setZoomScale:1.0 animated:YES];
  }
}

-(void)viewDidAppear:(BOOL)animated
{
  if(m_RidesDetailViewController)
  {
    [m_RidesDetailViewController.view removeFromSuperview];
    m_RidesDetailViewController.m_delegate=nil;
    [m_RidesDetailViewController release];
    m_RidesDetailViewController=nil;
  }
  if(m_RestuarantDetailViewController)
  {
    [m_RestuarantDetailViewController.view removeFromSuperview];
    m_RestuarantDetailViewController.m_delegate=nil;
    [m_RestuarantDetailViewController release];
    m_RestuarantDetailViewController=nil;
  }
  if(m_FilterViewController)
  {
	  [m_FilterViewController.view removeFromSuperview];
    m_FilterViewController.m_delegate=nil;
	  [m_FilterViewController release];
	  m_FilterViewController=nil;
  }  
}

-(void)viewWillAppear:(BOOL)animated
{
    if(isShowing)
    {
        [m_ScrollView setZoomScale:2.0 animated:YES];
        CGRect rect=CGRectMake(m_ImageView.frame.size.width/2.0, m_ImageView.frame.size.height/2.0, 100, 100);
        [m_ScrollView scrollRectToVisible:rect animated:NO];
        isShowing=NO;
    }
    
    
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount) {
        
    }
    else
    {
        [self.navigationController.navigationBar addSubview:m_HomeButton];
        [self.navigationController.navigationBar addSubview:pTitleLabel];
        [self.navigationController.navigationBar addSubview:m_FilterButton];
    }
    
    if(!isRide)
    {
        [m_HomeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/backbtn" ofType:@"png"]] forState:UIControlStateNormal];
        self.navigationItem.hidesBackButton=YES;
    }
    else
    {
        [m_HomeButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/homebtn" ofType:@"png"]] forState:UIControlStateNormal];
    }
    [self checkOrientation];
    [super viewWillAppear:animated];
}


-(void)SelectedTwiceNotificationRecieved
{
  int Index=[[NSUserDefaults standardUserDefaults]integerForKey:@"INDEX"];
  if(Index==0)
  {
    while ([self.navigationController.navigationBar.subviews count] > 2)
    {
      int n=[self.navigationController.navigationBar.subviews count]-1;
      
      [[[self.navigationController.navigationBar subviews] objectAtIndex:n] removeFromSuperview];
    }
    [self.navigationController.navigationBar addSubview:m_HomeButton];
    [self.navigationController.navigationBar addSubview:pTitleLabel];
    [self.navigationController.navigationBar addSubview:m_FilterButton];
  }
}


#pragma mark FilterViewControllerDelegate Methods

-(void)BackFromFilterViewControllerDelegate
{
  if(isRestAnnot == [[NSUserDefaults standardUserDefaults]integerForKey:@"RestFilterIndex"])
  {
  
  }
  else 
  {
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"RestFilterIndex"] == 1)
      [self addRestAnnotations];
    else 
      [self removeRestAnnotations];
    
    isRestAnnot=[[NSUserDefaults standardUserDefaults]integerForKey:@"RestFilterIndex"];
  }
  
  if(isRideAnnot == [[NSUserDefaults standardUserDefaults]integerForKey:@"RideFilterIndex"])
  {
    
  }
  else 
  {
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"RideFilterIndex"] == 1)
      [self addRideAnnoataions];
    else 
      [self removeRideAnnotations];
    
    isRideAnnot = [[NSUserDefaults standardUserDefaults]integerForKey:@"RideFilterIndex"];
  }  
}


-(void)addRideAnnoataions
{
  float width=m_ImageView.frame.size.width;
  float height=m_ImageView.frame.size.height;

  float widthRatio=width/m_Width;
  float heightRatio=height/m_Height;  
  
  for(int i=0;i<[m_RidesArray count];i++)
  {
    Land *pLand=[m_RidesArray objectAtIndex:i];
    for(int j=0;j<[pLand.m_RideArray count];j++)
    {
      Ride *pRide=[pLand.m_RideArray objectAtIndex:j];
        AnnotationInfo *pAnnotationInfo=[[AnnotationInfo alloc]init];
        pAnnotationInfo.m_AnnotX=[pRide.m_Latitude doubleValue];
        pAnnotationInfo.m_AnnotY=[pRide.m_Longitude doubleValue];
        pAnnotationInfo.m_ParkX=m_Latitude;
        pAnnotationInfo.m_ParkY=m_Longitude;
        pAnnotationInfo.title=pRide.m_RideName;
        pAnnotationInfo.Waittime=pRide.m_WaitTime;
        pAnnotationInfo.m_LandIndex=i;
        pAnnotationInfo.m_Index=j;
        pAnnotationInfo.m_Type=@"RIDE";
        
        float width;
        int strleng=[pAnnotationInfo.title length];
        
        if(strleng==8)
        {
          width=strleng*10;
        }
        else if(strleng==9 || strleng==22)
        {
          width=strleng*9;
        }
        else if(strleng==5)
        {
          width=strleng*10;
        }
        else
        {
          width=strleng*8;
        }
        
        CGRect frame=CGRectMake(0, 0, width+30, 30);
        
        AnnoataionView *pAnnotationView=[[AnnoataionView alloc]initWithFrame:frame annotation:pAnnotationInfo];
        pAnnotationView.m_delegate=self;
        pAnnotationView.frame=CGRectMake((pAnnotationView.m_Xcoordinate*widthRatio)-15.0,(pAnnotationView.m_Ycoordinate*heightRatio)-15.0, width+30, 30);
        if(m_ZoomScale>1.5)
          [pAnnotationView showTitle];
        else 
          [pAnnotationView hideTitle];
        [m_ScrollView addSubview:pAnnotationView];
        [m_RideAnnotArray addObject:pAnnotationView];
        
        [pAnnotationInfo release];
        pAnnotationInfo=nil;
        
        [pAnnotationView release];
        pAnnotationView=nil;
    }
  }
}

-(void)removeRideAnnotations
{
  for(UIView *pView in m_RideAnnotArray)
    [pView removeFromSuperview];
  if([m_RideAnnotArray count]){[m_RideAnnotArray removeAllObjects];}
}

-(void)addRestAnnotations
{
  float width=m_ImageView.frame.size.width;
  float height=m_ImageView.frame.size.height;
  
  float widthRatio=width/m_Width;
  float heightRatio=height/m_Height;  
  
  
  for(int i=0;i<[m_RestuarantArray count];i++)
  {
    Land *pLand=[m_RestuarantArray objectAtIndex:i];
    for(int j=0;j<[pLand.m_RestArray count];j++)
    {
      Restuarant *pRestuarant=[pLand.m_RestArray objectAtIndex:j];
        AnnotationInfo *pAnnotationInfo=[[AnnotationInfo alloc]init];
        pAnnotationInfo.m_AnnotX=[pRestuarant.m_Latitude doubleValue];
        pAnnotationInfo.m_AnnotY=[pRestuarant.m_Longitude doubleValue];
        pAnnotationInfo.m_ParkX=m_Latitude;
        pAnnotationInfo.m_ParkY=m_Longitude;
        pAnnotationInfo.title=pRestuarant.m_RestName;
        pAnnotationInfo.Waittime=pRestuarant.m_WaitTime;
        pAnnotationInfo.m_LandIndex=i;
        pAnnotationInfo.m_Index=j;
        pAnnotationInfo.m_Type=@"REST";
        
        float width;
        int strleng=[pAnnotationInfo.title length];
        
        if(strleng==8)
        {
          width=strleng*10;
        }
        else if(strleng==9 || strleng==22)
        {
          width=strleng*9;
        }
        else if(strleng==5)
        {
          width=strleng*10;
        }
        else
        {
          width=strleng*8;
        }
        
        CGRect frame=CGRectMake(0, 0, width+30, 30);      
              
        AnnoataionView *pAnnotationView=[[AnnoataionView alloc]initWithFrame:frame annotation:pAnnotationInfo];
        pAnnotationView.m_delegate=self;
        pAnnotationView.frame=CGRectMake((pAnnotationView.m_Xcoordinate*widthRatio)-15.0, (pAnnotationView.m_Ycoordinate*heightRatio)-15.0, width+30, 30);
        
        if(m_ZoomScale>1.5)
          [pAnnotationView showTitle];
        else 
          [pAnnotationView hideTitle];
        
        [m_ScrollView addSubview:pAnnotationView];
        [m_RestAnnotArray addObject:pAnnotationView];
        
        [pAnnotationInfo release];
        pAnnotationInfo=nil;
        
        [pAnnotationView release];
        pAnnotationView=nil;
    }
  }
}

-(void)removeRestAnnotations
{
  for(UIView *pView in m_RestAnnotArray)
    [pView removeFromSuperview];
  if([m_RestAnnotArray count]){[m_RestAnnotArray removeAllObjects];}
}

#pragma mark scroll View delegate mathods

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView 
{
  return m_ImageView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
  
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    m_ZoomScale=scrollView.zoomScale;
  
    float width=m_ImageView.frame.size.width;
    float height=m_ImageView.frame.size.height;
    

    float widthRatio=width/m_Width;
    float heightRatio=height/m_Height;
    
    for(int i=0;i<[[m_ScrollView subviews] count];i++)
    {
        if([[[m_ScrollView subviews]objectAtIndex:i] isKindOfClass:[AnnoataionView class]])
        {
            AnnoataionView *pView=[[m_ScrollView subviews] objectAtIndex:i];

          pView.frame=CGRectMake((pView.m_Xcoordinate*widthRatio)-15.0,(pView.m_Ycoordinate*heightRatio)-15.0, pView.frame.size.width, pView.frame.size.height);
            if(scrollView.zoomScale<1.5)
                [pView hideTitle];
            else
                [pView showTitle];
        }
        else if([[[m_ScrollView subviews]objectAtIndex:i] isKindOfClass:[UIImageView class]])
        {
          UIImageView *pImageView=[[m_ScrollView subviews]objectAtIndex:i];
          if(pImageView.tag==2)
          pImageView.center=CGPointMake(m_Xcoordinate*widthRatio,m_Ycoordinate*heightRatio);
        }
    }
}

-(void)getRidesArray
{
    isRest=NO;
    if([m_RidesArray count]){[m_RidesArray removeAllObjects];}
    [m_RidesArray addObjectsFromArray:[XMLDataParser getRidesFromXML]];
    [self addRideAnnoataions];
    [self calculateDistance];
    [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"RideFilterIndex"];
    isRideAnnot=1;
}

-(void)getRestArray
{
  if([m_RestuarantArray count])[m_RestuarantArray removeAllObjects];
  [m_RestuarantArray addObjectsFromArray:[XMLDataParser getIndependentRestuarantsFromXML]];
  [self addIndRestAnnotations];
  [self calculateDistance];
}

-(void)calculateDistance
{
  if(m_LocationManager==nil)
  {
    m_LocationManager = [[CLLocationManager alloc] init];
    m_LocationManager.delegate = self;
    m_LocationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    m_LocationManager.desiredAccuracy = kCLLocationAccuracyBest; // 100 m
  }
  [m_LocationManager startUpdatingLocation];  
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation 
{
  if(m_Location){[m_Location release];m_Location=nil;}
  m_Location=m_LocationManager.location;
  [m_Location retain];
    if(isFirst)
    {
      if(isRide)
      {
        [self calculateUserLocation];
        [self CalculateUserDistance];
      }
      else 
      {
        [self calculateUserLocation];
      }
        isFirst=NO;
    }
    else
    {
        [self calculateUserLocation];
    }
}

-(void)calculateUserLocation
{
  float width=m_ImageView.frame.size.width;
  float height=m_ImageView.frame.size.height;
  
  float widthRatio=width/m_Width;
  
  float heightRatio=height/m_Height;  
  
  double latitude=m_Latitude;
  double longitude=m_Longitude;
  
  CLLocation *userLocation=[[CLLocation alloc]initWithLatitude:m_Location.coordinate.latitude longitude:m_Location.coordinate.longitude]; 
  
  CLLocation *parkLoaction=[[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
  
  CLLocation *XLocation=[[CLLocation alloc]initWithLatitude:latitude longitude:m_Location.coordinate.longitude];
  
  CLLocation *YLocation=[[CLLocation alloc]initWithLatitude:m_Location.coordinate.latitude longitude:longitude];
  
  CLLocationDistance distanceX=[userLocation distanceFromLocation:XLocation];
  CLLocationDistance distanceY=[userLocation distanceFromLocation:YLocation];                   
  
  float divisionValue=[[NSUserDefaults standardUserDefaults]floatForKey:@"DIVIDEVALUE"];
  
  float heightRatio1=[[NSUserDefaults standardUserDefaults]floatForKey:@"HEIGHTRATIO"];
  
  NSNumber *XNumber=[NSNumber numberWithDouble:distanceX];
  m_Ycoordinate=[XNumber intValue]/divisionValue*heightRatio1;
  
  NSNumber *YNumber=[NSNumber numberWithDouble:distanceY];
  m_Xcoordinate=[YNumber intValue]/divisionValue*heightRatio1;
  
  if(isFirst)
  {
    CGPoint center=CGPointMake(m_Xcoordinate*widthRatio, m_Ycoordinate*heightRatio-480.0);
    m_UserLocationImageView.center=center;
    [m_ScrollView addSubview:m_UserLocationImageView];
    [UIView beginAnimations:NULL context:nil];
    [UIView setAnimationDuration:0.5];
    m_UserLocationImageView.center=CGPointMake(m_Xcoordinate*widthRatio, m_Ycoordinate*heightRatio);
    [UIView commitAnimations];
  }
  
  CGPoint center=CGPointMake(m_Xcoordinate*widthRatio, m_Ycoordinate*heightRatio);
  if(center.x!=oldCenter.x||center.y!=oldCenter.y)
  {
    oldCenter=center;
    if(center.x<m_ScrollView.contentSize.width&&center.y<m_ScrollView.contentSize.height)
    {
      //User Location is inside the bounds. Adjust the scroll view to show the user location in the center of the screen
      m_ScrollView.contentOffset=[self getScrollOffsetForUserLocation:center];
    }
  }
  m_UserLocationImageView.center=center;
  [userLocation release];
  [parkLoaction release];
  [XLocation release];
  [YLocation release];
}
-(CGPoint)getScrollOffsetForUserLocation:(CGPoint)userLocation
{
  CGPoint offset=CGPointZero;
  CGFloat x=userLocation.x-150;
  CGFloat y=userLocation.y-200;
  if(x<0)
    x=0;
  if(y<0)
    y=0;
  offset=CGPointMake(x,y);
//  printf("%f:%f\n",m_ScrollView.contentSize.width,m_ScrollView.contentSize.height);
//  printf("%f:%f\n",m_ScrollView.bounds.size.width,m_ScrollView.bounds.size.height);
//  if((y+m_ScrollView.bounds.size.height)>m_ScrollView.contentSize.height)
//  {
////    y+=((y+m_ScrollView.bounds.size.height)-m_ScrollView.contentSize.height);
//    y-=200;
//  }
//  if((x+m_ScrollView.bounds.size.width)>m_ScrollView.contentSize.width)
//  {
////    x+=((x+m_ScrollView.bounds.size.width)-m_ScrollView.contentSize.width);
//  }
  return offset;
}
-(void)CalculateUserDistance
{
  if(isRest)
  {
    for(int i=0;i<[m_RestuarantArray count];i++)
    {
      Land *pLand=[m_RestuarantArray objectAtIndex:i];
      for(int j=0;j<[pLand.m_RestArray count];j++)
      {
        Restuarant *pRestuarant=[pLand.m_RestArray objectAtIndex:j];
        CLLocation *rideLocation=[[CLLocation alloc]initWithLatitude:[pRestuarant.m_Latitude doubleValue] longitude:[pRestuarant.m_Longitude doubleValue]]; 
        CLLocationDistance distance=[m_Location distanceFromLocation:rideLocation];
        NSNumber *d=[NSNumber numberWithDouble:distance];
        pRestuarant.m_Distance=[d intValue];
        [rideLocation release];
        rideLocation=nil;
      }
    }
  }
  else 
  {
    for(int i=0;i<[m_RidesArray count];i++)
    {
      Land *pLand=[m_RidesArray objectAtIndex:i];
      for(int j=0;j<[pLand.m_RideArray count];j++)
      {
        Ride *pRide=[pLand.m_RideArray objectAtIndex:j];
        CLLocation *rideLocation=[[CLLocation alloc]initWithLatitude:[pRide.m_Latitude doubleValue] longitude:[pRide.m_Longitude doubleValue]]; 
        CLLocationDistance distance=[m_Location distanceFromLocation:rideLocation];
        NSNumber *d=[NSNumber numberWithDouble:distance];
        [rideLocation release];
        rideLocation=nil;
        pRide.m_Distance=[d intValue];
      }
    }
  }
}



#pragma mark URLConnectionManager Methods

-(void)connectToServer
{
  if(isRide)
    m_FilterButton.enabled=NO;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
  //printf("%s",[m_ParkId UTF8String]);
  if(isRide)
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getrest&parkid=%@",m_ParkId]]];
  else 
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getindrest&parkid=%@",m_ParkId]]];
  
  [request setHTTPMethod:@"POST"];
  mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }        
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate showAlertView];
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  if(isRide)
  {
    [self getRidesArray];
    m_FilterButton.enabled=YES;
  }
  
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }  
  
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  
	if(mResponseData)
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *filePath = [paths objectAtIndex:0];
		paths=nil;
    
    filePath = [filePath stringByAppendingPathComponent:@"ParkRestuarants.xml"];
    
		[mResponseData writeToFile:filePath atomically:YES];
		filePath=nil;
		[mResponseData release];
		mResponseData=nil;
    if([m_RestuarantArray count]){[m_RestuarantArray removeAllObjects];}
      [m_RestuarantArray addObjectsFromArray:[XMLDataParser getRestuarantsFromXML]];
        
    if([m_RestuarantArray count] && isRide)
    {
      isRest=YES;
      [self calculateDistance];
      isRestAnnot=2;
      [[NSUserDefaults standardUserDefaults]setInteger:2 forKey:@"RestFilterIndex"];
    }    
  }	
}

-(void)addIndRestAnnotations
{
  for(int i=0;i<[m_RestuarantArray count];i++)
  {
      Restuarant *pRestuarant=[m_RestuarantArray objectAtIndex:i];
      int waitTime=[pRestuarant.m_WaitTime intValue];
      if(waitTime != -1)
      {
        AnnotationInfo *pAnnotationInfo=[[AnnotationInfo alloc]init];
        pAnnotationInfo.m_AnnotX=[pRestuarant.m_Latitude doubleValue];
        pAnnotationInfo.m_AnnotY=[pRestuarant.m_Longitude doubleValue];
        pAnnotationInfo.m_ParkX=m_Latitude;
        pAnnotationInfo.m_ParkY=m_Longitude;
        pAnnotationInfo.title=pRestuarant.m_RestName;
        pAnnotationInfo.Waittime=pRestuarant.m_WaitTime;
        pAnnotationInfo.m_Index=i;
        pAnnotationInfo.m_Type=@"REST";
        
        float width;
        int strleng=[pAnnotationInfo.title length];
        
        if(strleng==8)
        {
          width=strleng*10;
        }
        else if(strleng==9 || strleng==22)
        {
          width=strleng*9;
        }
        else if(strleng==5)
        {
          width=strleng*10;
        }
        else
        {
          width=strleng*8;
        }
        
        CGRect frame=CGRectMake(0, 0, width+30, 30);      
        AnnoataionView *pAnnotationView=[[AnnoataionView alloc]initWithFrame:frame annotation:pAnnotationInfo];
        pAnnotationView.m_delegate=self;
        pAnnotationView.frame=CGRectMake(pAnnotationView.m_Xcoordinate-15.0, pAnnotationView.m_Ycoordinate-15.0, 30+width, 30);

        
        [m_ScrollView addSubview:pAnnotationView];
        
        [pAnnotationInfo release];
        pAnnotationInfo=nil;
        
        [pAnnotationView release];
        pAnnotationView=nil;
      }
  }
}


-(void)gotoDetailPagewithlandIndex:(int)landIndex Index:(int)pIndex type:(NSString *)pType
{
    [m_HomeButton removeFromSuperview];
    [pTitleLabel removeFromSuperview];
    [m_FilterButton removeFromSuperview];

  if([pType isEqualToString:@"RIDE"])
  {
    Land *pLand=[m_RidesArray objectAtIndex:landIndex];
    Ride *pRide=[pLand.m_RideArray objectAtIndex:pIndex];
    if(m_RidesDetailViewController)
    {
      [m_RidesDetailViewController.view removeFromSuperview];
      [m_RidesDetailViewController release];
      m_RidesDetailViewController=nil;
    }
    m_RidesDetailViewController=[[RidesDetailViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId LandId:pLand.m_LandId RideId:pRide.m_RideId LandName:pLand.m_LandName Lat:pRide.m_Latitude Long:pRide.m_Longitude Distance:pRide.m_Distance isFavourite:NO];
    m_RidesDetailViewController.m_delegate=self;
    [self.navigationController pushViewController:m_RidesDetailViewController animated:YES];
  }
  else 
  {
    Land *pLand;
    Restuarant *pRestuarant;
    if(isRide)
    {
      pLand=[m_RestuarantArray objectAtIndex:landIndex];
      pRestuarant=[pLand.m_RestArray objectAtIndex:pIndex];
    }
    else 
      pRestuarant =[m_RestuarantArray objectAtIndex:pIndex];
    
    if(m_RestuarantDetailViewController)
    {
      [m_RestuarantDetailViewController.view removeFromSuperview];
      [m_RestuarantDetailViewController release];
      m_RestuarantDetailViewController=nil;
    }
    if(isRide)
    {
      m_RestuarantDetailViewController=[[RestuarantDetailViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId LandId:pLand.m_LandId RestId:pRestuarant.m_RestId LandName:pLand.m_LandName Distance:pRestuarant.m_Distance isFavourite:NO imagePath:NULL pLat:pRestuarant.m_Latitude pLong:pRestuarant.m_Longitude];
      m_RestuarantDetailViewController.isInd=NO;
    }
    else 
    {
      m_RestuarantDetailViewController=[[RestuarantDetailViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId LandId:@"" RestId:pRestuarant.m_RestId LandName:@"" Distance:pRestuarant.m_Distance isFavourite:NO imagePath:NULL pLat:pRestuarant.m_Latitude pLong:pRestuarant.m_Longitude];
      m_RestuarantDetailViewController.isInd=YES;
    }
    m_RestuarantDetailViewController.m_delegate=self;
    [self.navigationController pushViewController:m_RestuarantDetailViewController animated:YES];
  }
}

-(void)backtoHomeAction
{
  if(m_LocationManager)
  {
    [m_LocationManager stopUpdatingHeading];
    m_LocationManager.delegate=nil;
  }
    if (appDelegate.isIndependent) {
        [m_HomeButton removeFromSuperview];
        [pTitleLabel removeFromSuperview];
        // [m_FilterButton removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if(isRide)
        {
            if(m_delegate)
                [m_delegate gotoHome];
        }
        else
        {
            [m_HomeButton removeFromSuperview];
            [pTitleLabel removeFromSuperview];
            [m_FilterButton removeFromSuperview];
            if(m_delegate)
                [m_delegate gotoHome];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
}

-(void)BackAction
{}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];    
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}


- (void)dealloc 
{
   while ([[m_ScrollView subviews] count]>0)
   {
    [[[m_ScrollView subviews]objectAtIndex:0] removeFromSuperview];
   }
    
  if(m_ScrollView){[m_ScrollView removeFromSuperview];[m_ScrollView release];m_ScrollView=nil;}
  if(m_ImageView){[m_ImageView removeFromSuperview];[m_ImageView release];m_ImageView=nil;}
  
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
  if(m_HomeButton){[m_HomeButton removeFromSuperview];[m_HomeButton release];m_HomeButton=nil;}
  if(m_FilterButton){[m_FilterButton removeFromSuperview];[m_FilterButton release];m_FilterButton=nil;}
  
  if(m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}    
  if(m_LocationManager){m_LocationManager.delegate=nil;[m_LocationManager release];m_LocationManager=nil;}
  if(m_Location){[m_Location release];m_Location=nil;}
  
  if(m_RestuarantArray){[m_RestuarantArray removeAllObjects];[m_RestuarantArray release];m_RestuarantArray=nil;}
  if(m_RidesArray){[m_RidesArray removeAllObjects];[m_RidesArray release];m_RidesArray=nil;}
  if(m_RestAnnotArray){[m_RestAnnotArray removeAllObjects];[m_RestAnnotArray release];m_RestAnnotArray=nil;}
  if(m_RideAnnotArray){[m_RideAnnotArray removeAllObjects];[m_RideAnnotArray release];m_RideAnnotArray=nil;}
    
  if(m_TapRecognizer){[m_TapRecognizer release];m_TapRecognizer=nil;}
  
    if(m_UserLocationImageView){[m_UserLocationImageView removeFromSuperview];[m_UserLocationImageView release];m_UserLocationImageView=nil;}
    
    [super dealloc];
}


@end
