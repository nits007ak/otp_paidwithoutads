    //
//  ApptasmicViewController.m
//  AmusementPark
//
//  Created by HARSHA on 06/07/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ApptasmicViewController.h"


@implementation ApptasmicViewController

-(id)init 
{
  if((self=[super init]))
  {
    m_ScrollView=nil;
    m_ImageView=nil;
  }
  return self;
}

-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320.0, 762.0)] autorelease];
    self.view.backgroundColor=[UIColor clearColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
	imageView.frame=CGRectMake(0, 0, 320, 48.5);
	//[self.navigationController.navigationBar addSubview:imageView];
	[imageView release];
    
    if(m_BGImageView==nil)
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    //	m_BGImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"RidesBG" ofType:@"png"]];
    m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
    m_BGImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[self.view addSubview:m_BGImageView];
    
    if(m_BackButton==nil)
    {
        m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
        m_BackButton.frame=CGRectMake(5, 8, 58, 30);
        [m_BackButton setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/AboutUS/abtus" ofType:@"png"]] forState:UIControlStateNormal];
        [m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
        [m_BackButton retain];
    }
    [self.navigationController.navigationBar addSubview:m_BackButton];
    
    if(m_ScrollView==nil)
    {
        m_ScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 10, 320.0, 762.0)];
        m_ScrollView.contentMode = (UIViewContentModeScaleAspectFit);
        m_ScrollView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight );
        [m_ScrollView setContentSize:CGSizeMake(320.0, 936.0)];
        m_ScrollView.backgroundColor=[UIColor clearColor];
        m_ScrollView.clipsToBounds = YES;
        [m_ScrollView setScrollEnabled:YES];
        m_ScrollView.showsVerticalScrollIndicator=NO;
    }
    
    if(m_ImageView==nil)
    {
        m_ImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 815)];
        m_ImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/AboutUS/Apptasmic" ofType:@"png"]];
    }
    
    [m_ScrollView addSubview:m_ImageView];
    [[self view] addSubview:m_ScrollView];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}


-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubViews];
}


- (void) didRotate:(NSNotification *)notification{
    [self checkOrientation];
    
    
}
-(void)layoutSubViews  //cp
{
    if (appDelegate.isPortrait) {
        m_BackButton.frame=CGRectMake(5, 8, 58, 30);
        m_ImageView.frame = CGRectMake(0, 0, 300, 815);
        
        
        m_ScrollView.frame = CGRectMake(0, 10, 320.0, 762.0);
        [m_ScrollView setContentSize:CGSizeMake(320.0, 936.0)];
        
    }
    else
    {
        m_BackButton.frame=CGRectMake(5, 3, 58, 30);
        m_ImageView.frame = CGRectMake(0, 0, 480, 815);
        m_ScrollView.frame = CGRectMake(0, 10, 480, 762.0);
        [m_ScrollView setContentSize:CGSizeMake(480.0, 936.0)];
        if ([[UIScreen mainScreen]bounds].size.height == 568) {
            m_ImageView.frame = CGRectMake(0, 0, 568, 815);
            m_ScrollView.frame = CGRectMake(0, 10, 568, 762.0);
            [m_ScrollView setContentSize:CGSizeMake(568, 936.0)];
        }
    }
}
- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}
-(void)viewDidLoad
{
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}
-(void)viewWillAppear:(BOOL)animated
{
  [self.navigationController setNavigationBarHidden:NO];
  self.navigationItem.hidesBackButton=YES;
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void)backAction
{
  [m_BackButton removeFromSuperview];
  [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
  if(m_ImageView){[m_ImageView removeFromSuperview];[m_ImageView release];m_ImageView=nil;}
  if(m_ScrollView){[m_ScrollView removeFromSuperview];[m_ScrollView release];m_ScrollView=nil;}
  if(m_BGImageView){[m_BGImageView removeFromSuperview];[m_BGImageView release];m_BGImageView=nil;}
  if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];}
    [super dealloc];
}


@end
