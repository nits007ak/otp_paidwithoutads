//
//  PhotoDB.h
//  PhotoLock
//
//  Created by ashwini on 03/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sqlite3.h>
#import "Ride.h"
#import "Restuarant.h"
#import "Land.h"
#import "LandInfo.h"
class PTMutex;

@interface AmusementParkDB : NSObject 
{
	sqlite3 *m_database;
	PTMutex *m_pMutex;
	BOOL m_Saved;
}

+(AmusementParkDB *)sharedPtDatabase;
- (void)close;
-(void)insertRide:(Ride *)pRide;
-(int)getDuplicate:(Ride *)pRide;
-(void)insertRestuarant:(Restuarant *)pRestuarant;
-(void)insertLand:(LandInfo *)pLand;
-(int)getDuplicateRest:(Restuarant *)pRest;
-(int)getDuplicateLand:(LandInfo *)pLand;
-(NSMutableArray *)getLandsFromDBWithParkId:(NSString *)pParkId;
-(NSMutableArray *)getRidesWithLandId:(NSString *)pLandId;
-(NSMutableArray *)getRestsWithLandId:(NSString *)pLandId;
-(Ride *)getRideWithRideId:(NSString *)pRideId;
-(Restuarant *)getRestWithRestId:(NSString *)pRestId;
-(BOOL)deleteRestFromFavorite:(NSString *)pRestId;
-(BOOL)deleteRideFromFavorite:(NSString *)pRideId;
@end
