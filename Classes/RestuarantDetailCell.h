//
//  RestuarantDetailCell.h
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface RestuarantDetailCell : UITableViewCell 
{
	UIImageView *m_ImageView;
  UIImageView *m_WaitTimeImageView;
	UILabel *m_TitleLabel;
  UILabel *m_WaitTimeLabel;
  UILabel *m_YardsLabel;
}
@property(nonatomic,assign) UIImageView *m_WaitTimeImageView;
@property(nonatomic,assign) UIImageView *m_ImageView;
@property(nonatomic,assign) UILabel *m_TitleLabel;
@property(nonatomic,assign) UILabel *m_WaitTimeLabel;
@property(nonatomic,assign) UILabel *m_YardsLabel;

-(void)SetOpacityHigh;
-(void)SetOpacityLow;

@end
