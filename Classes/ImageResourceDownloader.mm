#import "ImageResourceDownloader.h"
@implementation RemoteFile : NSObject

@synthesize m_remoteURL;
@synthesize m_localFilePath;

- (id)init
{
	self = [super init];
	if(self == nil)   return nil;
	
	m_remoteURL = nil;
	m_localFilePath = nil;
	

	return self;
}

- (void)dealloc
{
 
	if(m_remoteURL)  { [m_remoteURL release];m_remoteURL=nil;}
	if(m_localFilePath) {  [m_localFilePath release];m_localFilePath=nil;}
	[super dealloc];
}

@end

@interface ImageResourceDownloader(PRIVATE)
- (void)downloadFile;
@end


@implementation ImageResourceDownloader

@synthesize m_delegate;

- (id)init
{
	self = [super init];
	if(self == nil) return nil;
	
	m_urls = [[NSMutableArray alloc] init];
	m_currentDownloadingURLIndex = 0;
	
	m_delegate = nil;
	m_connection = nil;
	
	m_currentBufferIndex = 0;
	m_bStop = NO;
	
	mResponseData = nil;

	return self;
}

- (void)dealloc
{
  if (m_delegate)
  {
    m_delegate=nil;
  }
	if(m_connection)  
	{
		[m_connection cancel];
		[m_connection release];
		m_connection = nil;
	}
	
	if(m_urls){[m_urls release];m_urls=nil;}
	if (mResponseData){[mResponseData setLength:0];[mResponseData release];mResponseData=nil;}
	[super dealloc];
}


- (void)downloadImages:(NSArray*)remoteFiles
{
	m_bStop = NO;
	[m_urls addObjectsFromArray:remoteFiles];
	m_currentDownloadingURLIndex = 0;
    [self downloadFile];
}

- (void)downloadFile
{
	if(m_bStop)    return;
	
	if(m_connection)  
	{
		[m_connection cancel];
		[m_connection release];
		m_connection = nil;
	}
	
	m_currentBufferIndex = 0;
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
	RemoteFile* remoteFile = [m_urls objectAtIndex:m_currentDownloadingURLIndex];
	[request setURL:[NSURL URLWithString:remoteFile.m_remoteURL]];
	remoteFile.m_remoteURL = [remoteFile.m_remoteURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]; 
	m_connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	if (!m_connection)
	{
		if (m_delegate)
		{
			[m_delegate errorDownloading:self];
		}
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection	
{
	if(m_bStop)
		return;
	
	if(m_currentBufferIndex > 0)
	{
		RemoteFile* remoteFile = [m_urls objectAtIndex:m_currentDownloadingURLIndex];

		[mResponseData writeToFile:remoteFile.m_localFilePath atomically:YES];
		if (m_delegate)
		{
			[m_delegate fileDowloadedCompleted:self fileName:remoteFile.m_remoteURL localfile:remoteFile.m_localFilePath];
		}
	}
	
	++m_currentDownloadingURLIndex;
	
	if(m_currentDownloadingURLIndex < [m_urls count])
	{
		[self downloadFile];
	}
	else
		if (m_delegate)
		{
			[m_delegate batchDowloadCompleted:self];
		}
	
	m_currentBufferIndex = 0;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data	
{
	if(m_bStop)
		return;
	
	[mResponseData appendData:data];

	m_currentBufferIndex += [data length];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error	
{
	if(m_bStop)
		return;
	if (m_delegate)
	{
		[m_delegate errorDownloading:self];
	}
}

- (void)cancel
{
	m_bStop = YES;
	if(m_connection)  
	{
		[m_connection cancel];
		[m_connection release];
		m_connection = nil;
	}  
}

@end
