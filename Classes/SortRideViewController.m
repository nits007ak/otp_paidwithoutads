    //
//  SortRideViewController.m
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SortRideViewController.h"
#import "RidesDetailCell.h"
@implementation SortRideViewController
@synthesize m_delegate;
-(id)initWithBool:(BOOL)pRide
{
	if((self=[super init]))
	{
    isRide=pRide;
		m_TableView=nil;
		m_TitleArray=nil;
    pTitleLabel=nil;
    m_SelectedIndex=1;
    if(isRide)
    {
      if([[NSUserDefaults standardUserDefaults]integerForKey:@"SelectdeRideIndex"]==0)
        [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"SelectdeRideIndex"];
    }
    else 
    {
      if([[NSUserDefaults standardUserDefaults]integerForKey:@"SelectdeRestIndex"]==0)
        [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"SelectdeRestIndex"];
    }

		m_TitleArray=[[NSMutableArray alloc]initWithObjects:@"Filter By (Pick one only)",@"Distance",@"Name",@"Wait Time",nil];
	}
	return self;
}

-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
  	CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	self.view.backgroundColor=[UIColor whiteColor];
	[self.navigationItem setHidesBackButton:YES animated:NO];
	
	
    if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }
  m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];	
	[m_BGImageView release];
	
  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(100, 8, 140, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:19];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  if(isRide)
    pTitleLabel.text=@"Sort Rides";
  else 
    pTitleLabel.text=@"Sort Food";
  
  if(m_BackButton==nil)
  {
	m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
	[m_BackButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/backbtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	[m_BackButton retain];
  }
	
  if(m_TableView==nil)
  m_TableView = [[UITableView alloc]init];
  m_TableView.frame=CGRectMake(0,10,320,325);
     [m_TableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
  [m_TableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];  
	m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.scrollEnabled = YES;
	m_TableView.backgroundColor=[UIColor clearColor];
	m_TableView.rowHeight=53;
	[self.view addSubview:m_TableView];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
    
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
        m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
        
    }
    else{
        appDelegate.isPortrait = YES;
        m_BackButton.frame=CGRectMake(5, 3, 53.5, 30);
    }
    [self layoutSubView];
}

-(void)layoutSubView
{
    
    [m_TableView reloadData];
    CGRect frame = [[UIScreen mainScreen]bounds];
    if (appDelegate.isPortrait) {
        m_BGImageView.frame = CGRectMake(0, 0, 320, 480);
        pTitleLabel.frame = CGRectMake(100, 8, 140, 30);
        m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
        if(frame.size.height == 568)m_BGImageView.frame = CGRectMake(0, 0, 320, 568);
    }
    else{
        pTitleLabel.frame = CGRectMake(170, 3, 140, 30);
        m_BackButton.frame=CGRectMake(5, 3, 53.5, 30);
        m_BGImageView.frame = CGRectMake(0, 0, 480, 320);
        if(frame.size.height == 568)m_BGImageView.frame = CGRectMake(0, 0, 568,320);
    }
   
}
- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}
-(void)backAction
{
	if(m_delegate && [m_delegate respondsToSelector:@selector(BackFromSortRideViewController:)])
		[m_delegate BackFromSortRideViewController:m_SelectedIndex-1];
	[m_BackButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	[self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
		return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	RidesDetailCell *cell = (RidesDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"MainPageCell"];
	if(cell == nil)
	{  
    cell=[[[RidesDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainPageCell"]autorelease];
    cell.m_CustomButton.hidden=YES;
    cell.accessoryType = UITableViewCellAccessoryNone;
		cell.selectionStyle=UITableViewCellSelectionStyleGray;
	}
	
		  cell.m_WaitTimeLabel.hidden=YES;
	    cell.m_TitleLabel.text=[m_TitleArray objectAtIndex:indexPath.row];
      UIImageView *bgImgView = [[UIImageView alloc]init];
     	if(indexPath.row==0)
      {
        cell.m_TitleLabel.frame=CGRectMake(55,15,250,30);
          if (!appDelegate.isPortrait) {
              cell.m_TitleLabel.frame=CGRectMake(170,15,250,30);
              
              if ([[UIScreen mainScreen]bounds].size.height == 568) {
                  cell.m_TitleLabel.frame=CGRectMake(200,15,250,30);
              }
            
          }
        bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/uppercell3" ofType:@"png"]];
        cell.m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:17];
        cell.m_TitleLabel.textColor = [UIColor colorWithRed:82.0/255 green:21.0/255 blue:89.0/255 alpha:1.0];
        cell.m_TitleLabel.shadowColor=[UIColor whiteColor];
        cell.m_TitleLabel.shadowOffset=CGSizeMake(0, 1);
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
      }
		else if(indexPath.row==[m_TitleArray count]-1)
    {
			bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/downcell3" ofType:@"png"]];
      cell.m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
      cell.m_TitleLabel.textAlignment=UITextAlignmentCenter;
      cell.m_TitleLabel.frame=CGRectMake(25,10,250,30);
        if (!appDelegate.isPortrait) {
            cell.m_TitleLabel.frame=CGRectMake(130,10,250,30);
            if ([[UIScreen mainScreen]bounds].size.height == 568) {
                cell.m_TitleLabel.frame=CGRectMake(160,10,250,30);
            }
        }
    }
		else
    {
			bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/normalcell" ofType:@"png"]];
      cell.m_TitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
      cell.m_TitleLabel.textAlignment=UITextAlignmentCenter;
      cell.m_TitleLabel.frame=CGRectMake(25,10,250,30);
        if (!appDelegate.isPortrait) {
            
            cell.m_TitleLabel.frame=CGRectMake(130,10,250,30);
            if ([[UIScreen mainScreen]bounds].size.height == 568) {
                
                cell.m_TitleLabel.frame=CGRectMake(160,10,250,30);
            }
            
        }
    }
  if(isRide)
    m_SelectedIndex=[[NSUserDefaults standardUserDefaults] integerForKey:@"SelectdeRideIndex"];
  else
    m_SelectedIndex=[[NSUserDefaults standardUserDefaults] integerForKey:@"SelectdeRestIndex"];

  if(m_SelectedIndex==indexPath.row)
  {
		cell.m_Selected.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/marked" ofType:@"png"]];
    if(indexPath.row==3)
      bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/downcell2old" ofType:@"png"]];
    else
      bgImgView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/selectedcell3" ofType:@"png"]];
  }
	else
		cell.m_Selected.image=nil;
  
    cell.backgroundView=bgImgView;
		[bgImgView release];
	
  UIImageView *selBgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0,316, 51)];
  if(indexPath.row==3)
    selBgImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/downcell2old" ofType:@"png"]];
  else
    selBgImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/selectedcell3" ofType:@"png"]];  
  cell.selectedBackgroundView=selBgImageView;
  [selBgImageView release];
  selBgImageView=nil;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if(indexPath.row!=0)
  {
    if(isRide)
      [[NSUserDefaults standardUserDefaults]setInteger:indexPath.row forKey:@"SelectdeRideIndex"];
    else 
      [[NSUserDefaults standardUserDefaults]setInteger:indexPath.row forKey:@"SelectdeRestIndex"];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    m_SelectedIndex=indexPath.row;
    [tableView reloadData];
      
      if(m_delegate && [m_delegate respondsToSelector:@selector(BackFromSortRideViewController:)])
          [m_delegate BackFromSortRideViewController:m_SelectedIndex-1];
      [m_BackButton removeFromSuperview];
      [pTitleLabel removeFromSuperview];
      [self.navigationController popViewControllerAnimated:YES];
  }
}


-(void)viewDidLoad
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount)
    {
        
    }
    else
    {
        [self.navigationController.navigationBar addSubview:pTitleLabel];
        [self.navigationController.navigationBar addSubview:m_BackButton];
    }
    
	[super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}


- (void)dealloc 
{
	if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
	if(m_TitleArray){[m_TitleArray removeAllObjects];[m_TitleArray release];m_TitleArray=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
	if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
    [super dealloc];
}

@end
