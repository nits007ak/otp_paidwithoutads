//
//  DealsViewCell.h
//  AmusementPark
//
//  Created by HARSHA on 26/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DealsViewCell : UITableViewCell 
{
  UILabel *m_TitleLabel; 
  UITextView *m_DescriptonView;
}
@property(nonatomic,assign) UILabel *m_TitleLabel;
@property(nonatomic,assign) UITextView *m_DescriptonView;
@end
