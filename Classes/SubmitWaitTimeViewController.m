    //
//  SubmitWaitTimeViewController.m
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SubmitWaitTimeViewController.h"
#import "XMLDataParser.h"
#import "AmusementParkAppDelegate.h"
#import "AdsManager.h"
@implementation SubmitWaitTimeViewController
@synthesize m_delegate;
-(id)initWithId:(NSString *)pId Latitude:(NSString *)pLat Longitude:(NSString *)pLong isRide:(BOOL)pRide
{
	if(self=[super init])
	{
        m_WaitTime=-1;
        m_pId=pId;
        [m_pId retain];
        m_Latitude=pLat;
        [m_Latitude retain];
        m_Longitude=pLong;
        [m_Longitude retain];
        isRide=pRide;
		m_DescriptionLabel=nil;
        pTitleLabel=nil;
		m_WaitTimePickerview=nil;
		m_PickerView=nil;
		m_NavigBar=nil;
		m_BackButton=nil;
        mURLConnection=nil;
        mResponseData=nil;
        m_activityIndicator=nil;
        m_LocationManager=nil;
        m_Location=nil;
        m_SubmitButton=nil;
        m_ParkName=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
        [m_ParkName retain];
        //		m_TimeArray=[[NSArray alloc]initWithObjects:@"Closed",@"0",@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"50",@"55",@"60",@"65",@"70",@"75+",nil];
        m_TimeArray=[[NSArray alloc]initWithObjects:@"Closed",@"0",@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"50",@"55",@"60",@"70",@"80",@"90",@"100",@"110",@"120+",nil];
        
	}
	return self;
}

-(void)loadView
{
     appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
  	CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }

	self.view.backgroundColor=[UIColor clearColor];
	[self.navigationItem setHidesBackButton:YES animated:NO];

    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0,320, 480)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.contentSize = CGSizeMake(320, 10);
    scrollView.scrollEnabled = NO;
    

	
    if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    }
  m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];	
	[m_BGImageView release];	
	
	pDescImageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 300, 100)];
	pDescImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SubmitWaitTimePage/DescBg" ofType:@"png"]];
     pDescImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[scrollView addSubview:pDescImageView];
	[pDescImageView release];
	
  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(75, 10, 200, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:19];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=@"Submit Wait Time";
  
  if(m_BackButton==nil)
  {
	m_BackButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
	[m_BackButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SortPage/backbtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_BackButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	[m_BackButton retain];	
  }
	
  m_DescLAbel1=[[UILabel alloc]initWithFrame:CGRectMake(25, 30, 285, 40)];
	m_DescLAbel1.backgroundColor=[UIColor clearColor];
  m_DescLAbel1.numberOfLines=0;
  m_DescLAbel1.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:17];
  m_DescLAbel1.textColor = [UIColor colorWithRed:209.0/255 green:167.0/255 blue:204.0/255 alpha:1.0];
  m_DescLAbel1.shadowColor=[UIColor blackColor];
  m_DescLAbel1.shadowOffset=CGSizeMake(1.0,1.0);
  [scrollView addSubview:m_DescLAbel1];
  [m_DescLAbel1 release];
  
    if(m_DescriptionLabel==nil)
        m_DescriptionLabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 40, 275, 100)];
	m_DescriptionLabel.layer.cornerRadius=10;
	m_DescriptionLabel.layer.masksToBounds=YES;
	m_DescriptionLabel.backgroundColor=[UIColor clearColor];
	m_DescriptionLabel.numberOfLines=0;
    m_DescriptionLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    // NSString *desctext=[NSString stringWithFormat:@"You can only submit the wait time if you are close to the park %@",m_ParkName];
    NSString * text = [[NSUserDefaults standardUserDefaults]objectForKey:@"submit_screen_message"];
    
    NSArray * array = [text componentsSeparatedByString:@"."];
    NSString * decriptionString = [[array objectAtIndex:1] stringByReplacingOccurrencesOfString:@" \n " withString:@""];
    //    NSRange replaceRange = [decriptionString rangeOfString:@" "];
    //    if (replaceRange.location != NSNotFound){
    //        NSString* result = [decriptionString stringByReplacingCharactersInRange:replaceRange withString:@""];
    //        NSLog(@"%@", result);
    //    }
    
    
    
    NSString *desctext=[NSString stringWithFormat:@"%@ %@",decriptionString,m_ParkName];
    
    m_DescriptionLabel.text=desctext;
	m_DescriptionLabel.textAlignment=UITextAlignmentLeft;
    m_DescriptionLabel.shadowColor=[UIColor blackColor];
    m_DescriptionLabel.shadowOffset=CGSizeMake(1.0,1.0);
    m_DescriptionLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
    m_DescriptionLabel.textColor = [UIColor colorWithRed:229.0/255 green:204.0/255 blue:226.0/255 alpha:1.0];
	[scrollView addSubview:m_DescriptionLabel];
	
  if(isRide)
  {
      // m_DescLAbel1.text=@"Submit Wait time for the ride.";
      m_DescriptionLabel.text = [array objectAtIndex:0];
      m_DescLAbel1.textAlignment=UITextAlignmentLeft;
      m_DescLAbel1.frame=CGRectMake(25, 25, 285, 40);
      m_DescriptionLabel.frame=CGRectMake(25, 30, 275, 100);
  }
  else
  {
    m_DescLAbel1.text=@"Submit Wait time for \n the restaurant.";
    m_DescLAbel1.textAlignment=UITextAlignmentCenter;
    m_DescLAbel1.frame=CGRectMake(25, 30, 265, 40);
    m_DescriptionLabel.frame=CGRectMake(25, 40, 275, 100);
  }
  
  if(m_PickerView==nil)
	m_PickerView=[[UIView alloc]initWithFrame:CGRectMake(0,140,200,260)];
	m_PickerView.backgroundColor=[UIColor clearColor];
    m_PickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	[scrollView addSubview:m_PickerView];
	
  if(m_WaitTimePickerview==nil)
	m_WaitTimePickerview=[[UIPickerView alloc]initWithFrame:CGRectMake(0,40,325,220)];
	m_WaitTimePickerview.delegate=self;
	m_WaitTimePickerview.dataSource=self;
    m_WaitTimePickerview.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	m_WaitTimePickerview.showsSelectionIndicator=YES;
	[m_PickerView addSubview:m_WaitTimePickerview];
	
    m_NavigImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 325, 40)];
	m_NavigImage.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SubmitWaitTimePage/PickerTab" ofType:@"png"]];
     m_NavigImage.autoresizingMask = UIViewAutoresizingFlexibleWidth;
  [m_PickerView addSubview:m_NavigImage];
	[m_NavigImage release];
	
  if(m_SubmitButton==nil)
  {
	m_SubmitButton=[UIButton buttonWithType:UIButtonTypeCustom];
	m_SubmitButton.frame=CGRectMake(115, 3, 85, 39);
	[m_SubmitButton setBackgroundColor:[UIColor clearColor]];
	[m_SubmitButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/SubmitWaitTimePage/submitbtn" ofType:@"png"]] forState:UIControlStateNormal];
	[m_SubmitButton addTarget:self action:@selector(SubmitAction) forControlEvents:UIControlEventTouchUpInside];
	[m_PickerView addSubview:m_SubmitButton];
  [m_SubmitButton retain];
  }
    [self.view addSubview:scrollView];
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    
    
    [self layoutSubView];
}

-(void)layoutSubView
{
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
    if (appDelegate.isPortrait) {
        m_BGImageView.frame = CGRectMake(0, 0, 320, 480);
        scrollView.scrollEnabled = NO;
        m_PickerView.frame = CGRectMake(0,140,320,260);
        scrollView.contentSize = CGSizeMake(480, 10);
        scrollView.frame = CGRectMake(0, 0, 320,480);
        m_NavigImage.frame = CGRectMake(0, 0, 325, 40);
        pTitleLabel.frame = CGRectMake(75, 10, 200, 30);
        m_SubmitButton.frame=CGRectMake(115, 3, 85, 39);
        m_BackButton.frame=CGRectMake(5, 8, 53.5, 30);
        pDescImageView.frame = CGRectMake(10, 20, 300, 100);
        m_WaitTimePickerview.frame =CGRectMake(0,40,325,220);
        if(screenFrame.size.height == 568)m_BGImageView.frame = CGRectMake(0, 0, 320, 568);
    }
    else
    {
        scrollView.scrollEnabled = YES;
        m_PickerView.frame = CGRectMake(0,140,480,260);
        scrollView.contentSize = CGSizeMake(480, 480);
        scrollView.frame = CGRectMake(0, 0, 480, 320);
        pTitleLabel.frame = CGRectMake(160, 4, 200, 30);
        m_WaitTimePickerview.frame =CGRectMake(0,40,480,220);
        m_SubmitButton.frame=CGRectMake(200, 3, 85, 39);
        m_BackButton.frame=CGRectMake(5, 3, 53.5, 30);
        m_NavigImage.frame = CGRectMake(0, 0, 480, 40);
        m_BGImageView.frame = CGRectMake(0, 0, 480, 320);
        if (screenFrame.size.height == 568) {
            scrollView.contentSize = CGSizeMake(568, 480);
            scrollView.frame = CGRectMake(0, 0, 568, 320);
            m_PickerView.frame = CGRectMake(0,140,568,260);
            m_NavigImage.frame = CGRectMake(0, 0, 568, 40);
            m_WaitTimePickerview.frame =CGRectMake(0,40,568,220);
            m_SubmitButton.frame=CGRectMake(250, 3, 85, 39);
            pTitleLabel.frame = CGRectMake(200, 4, 200, 30);
            m_BGImageView.frame = CGRectMake(0, 0, 568, 320);
        }
    }
}
- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}
-(void)viewDidLoad{
    [super viewDidLoad];
    appDelegate.subviewCount = self.navigationController.navigationBar.subviews;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount)
    {
        
    }
    else
    {
        [self.navigationController.navigationBar addSubview:pTitleLabel];
        [self.navigationController.navigationBar addSubview:m_BackButton];
    }
    
	[super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];

}



-(void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  [[AdsManager sharedAdsManager]RemoveAdBannerAndFullVersionBanner];

}


-(void)backAction
{
  if(m_LocationManager)
  {
    [m_LocationManager stopUpdatingLocation];  
    m_LocationManager.delegate=nil;
  }
  [m_BackButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	if(m_delegate)
		[m_delegate BackAction];
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)SubmitAction
{
  m_SubmitButton.userInteractionEnabled=NO;
  m_BackButton.userInteractionEnabled=NO;
  if(m_LocationManager)
  {
    if(m_LocationManager){m_LocationManager.delegate=nil;[m_LocationManager release];m_LocationManager=nil;}
  }
    m_LocationManager = [[CLLocationManager alloc] init];
    m_LocationManager.delegate = self;
    m_LocationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    m_LocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
  [m_LocationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation 
{
  if(m_Location){[m_Location release];m_Location=nil;}
  m_Location=m_LocationManager.location;
  [m_Location retain];
  [self stopUpDatingLocation];
}

-(void)stopUpDatingLocation
{
  [m_LocationManager stopUpdatingLocation];  
   m_LocationManager.delegate=nil;
  
  
  
  BOOL isInside=[self getLocations];
  
  if(isInside)
  {
      m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
      [m_activityIndicator startAnimating];
      m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
      [self.view addSubview:m_activityIndicator];        
      [self connectToServer];
  }
  else 
  {
    m_BackButton.userInteractionEnabled=YES;
    m_SubmitButton.userInteractionEnabled=YES;
    NSString *desctext=[NSString stringWithFormat:@"Sorry. You can only submit the wait time if you are close to the park %@",m_ParkName];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:desctext delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
    alert=nil;
  }
}
  

-(BOOL)getLocations
{
  double latitude=[[NSUserDefaults standardUserDefaults]doubleForKey:@"LATITUDE"];
  double longitude=[[NSUserDefaults standardUserDefaults]doubleForKey:@"LONGITUDE"];
  
  CLLocation *userLocation=[[CLLocation alloc]initWithLatitude:m_Location.coordinate.latitude longitude:m_Location.coordinate.longitude]; 
  
  CLLocation *XLocation=[[CLLocation alloc]initWithLatitude:latitude longitude:m_Location.coordinate.longitude];
  
  CLLocation *YLocation=[[CLLocation alloc]initWithLatitude:m_Location.coordinate.latitude longitude:longitude];
  
  
  CLLocationDistance distanceX=[userLocation distanceFromLocation:XLocation];
  CLLocationDistance distanceY=[userLocation distanceFromLocation:YLocation];                   
  
  float divisionValue=[[NSUserDefaults standardUserDefaults]floatForKey:@"DIVIDEVALUE"];
  
  
  NSNumber *XNumber=[NSNumber numberWithDouble:distanceX];
  float Ycoordinate=[XNumber intValue]/divisionValue;
  
  NSNumber *YNumber=[NSNumber numberWithDouble:distanceY];
  float Xcoordinate=[YNumber intValue]/divisionValue;
  
  BOOL widthInside=NO,heightInside=NO;
  
  if(Ycoordinate > 0.0 && Ycoordinate < [[NSUserDefaults standardUserDefaults]floatForKey:@"HEIGHT"])
    heightInside=YES;
  
  if(Xcoordinate > 0.0 && Xcoordinate < [[NSUserDefaults standardUserDefaults]floatForKey:@"WIDTH"])
    widthInside=YES;
    
  if(widthInside && heightInside)
    return YES;
  else 
    return NO;
  
  [userLocation release];
  [XLocation release];
  [YLocation release];
}
#pragma mark UIAlertView delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if(alertView.tag==1)
  {
    [self.navigationController popToRootViewControllerAnimated:YES];
  }
}

#pragma mark URLConnectionManager Methods

-(void)connectToServer
{
  m_BackButton.userInteractionEnabled=NO;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
  if(isRide)
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=setwait&rideid=%@&wait=%d",m_pId,m_WaitTime]]];
  else 
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=setwaitrest&restid=%@&wait=%d",m_pId,m_WaitTime]]];
    
	[request setHTTPMethod:@"POST"];
	mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
  m_BackButton.userInteractionEnabled=YES;
  m_SubmitButton.userInteractionEnabled=YES;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDelegate=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate showAlertView];
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  m_BackButton.userInteractionEnabled=YES;
  m_SubmitButton.userInteractionEnabled=YES;
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }      if (m_activityIndicator)
    if(mURLConnection) 
    {
      [mURLConnection cancel];
      [mURLConnection release];
      mURLConnection=nil;
    }
	if(mResponseData)
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *filePath = [paths objectAtIndex:0];
		paths=nil;
		filePath = [filePath stringByAppendingPathComponent:@"Response.xml"];
		[mResponseData writeToFile:filePath atomically:YES];
		filePath=nil;
		[mResponseData release];
		mResponseData=nil;
		int response=[XMLDataParser getWaitTimeResponse];
  if(response)
    {
      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Wait Time Successfully Submitted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
      alert.tag=1;
      [alert show];
      [alert release];          
      alert=nil;
    }
    else
    {
      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Problem With Server" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
      [alert show];
      [alert release];                
      alert=nil;
    }
  }
}


#pragma mark UIPickerView delegate methods


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 2;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if(component==0)
  {
    if(row!=0)
    {
      NSString *pWaittime=[m_TimeArray objectAtIndex:row];
      m_WaitTime=[pWaittime intValue];
      //printf("\n\nWaitTime:%d\n\n",m_WaitTime);
    }
    else
      m_WaitTime=-1;
    if(row==0)
      [pickerView selectRow:0 inComponent:1 animated:YES];
    else 
      [pickerView selectRow:1 inComponent:1 animated:YES];
  }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	if(component==0)
	   return [m_TimeArray count];
	else 
		return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	if(component==0)
		return [m_TimeArray objectAtIndex:row];
	else
  {
    if(row!=0)
      return @"Minutes";
    else 
      return @"";
  }
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}


- (void)dealloc 
{
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }   
  if(m_ParkName){[m_ParkName release];m_ParkName=nil;}
  if(m_TimeArray){[m_TimeArray release];m_TimeArray=nil;}
  if(m_pId){[m_pId release];m_pId=nil;}
  if(m_Latitude){[m_Latitude release];m_Latitude=nil;}
  if(m_Longitude){[m_Longitude release];m_Longitude=nil;}
  if(m_Location){[m_Location release];m_Location=nil;}
  if(m_LocationManager){m_LocationManager.delegate=nil;[m_LocationManager release];m_LocationManager=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
	if(m_BackButton){[m_BackButton removeFromSuperview];[m_BackButton release];m_BackButton=nil;}
  if(m_SubmitButton){[m_SubmitButton removeFromSuperview];[m_SubmitButton release];m_SubmitButton=nil;}
	if(m_PickerView){[m_PickerView removeFromSuperview];[m_PickerView release];m_PickerView=nil;}
	if(m_WaitTimePickerview){[m_WaitTimePickerview removeFromSuperview];[m_WaitTimePickerview release];m_WaitTimePickerview=nil;}
	if(m_NavigBar){[m_NavigBar removeFromSuperview];[m_NavigBar release];m_NavigBar=nil;}
	if(m_DescriptionLabel){[m_DescriptionLabel removeFromSuperview];[m_DescriptionLabel release];m_DescriptionLabel=nil;}
    [super dealloc];
}


@end
