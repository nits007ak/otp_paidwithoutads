//
//  Parade.m
//  AmusementPark
//
//  Created by HARSHA on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Parade.h"


@implementation Parade
@synthesize m_ParadeId,m_ParadeName,m_ParadeLocation,m_Paradetype,m_Longitude,m_Latitude,m_StartTime,m_EndTime,m_ParkStartTime,m_ParkEndTime;
-(id)init
{
  if(self=[super init])
  {
    m_ParadeId=nil;
    m_ParadeName=nil;
    m_ParadeLocation=nil;
    m_Paradetype=nil;
    m_Latitude=nil;
    m_Longitude=nil;
    m_StartTime=nil;
    m_EndTime=nil;
    m_ParkStartTime=nil;
    m_ParkEndTime=nil;

  }
  return self;
}

-(void)dealloc
{
  if(m_ParadeId){[m_ParadeId release];m_ParadeId=nil;}
  if(m_ParadeName){[m_ParadeName release];m_ParadeName=nil;}
  if(m_ParadeLocation){[m_ParadeLocation release];m_ParadeLocation=nil;}
  if(m_Paradetype){[m_Paradetype release];m_Paradetype=nil;}
  if(m_StartTime){[m_StartTime release];m_StartTime=nil;}
  if(m_EndTime){[m_EndTime release];m_EndTime=nil;}
  if(m_Latitude){[m_Latitude release];m_Latitude=nil;}
  if(m_Longitude){[m_Longitude release];m_Longitude=nil;}
  if(m_ParkStartTime){[m_ParkStartTime release];m_ParkStartTime=nil;}
  if(m_ParkEndTime){[m_ParkEndTime release];m_ParkEndTime=nil;}
  [super dealloc];
}

@end
