//
//  RidesTableViewCell.h
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RidesTableViewCell : UITableViewCell 
{
   	UILabel *m_TitleLabel;
	UILabel *m_TImeLabel;
}
@property(nonatomic,assign) UILabel *m_TitleLabel;
@property(nonatomic,assign) UILabel *m_TImeLabel;

@end
