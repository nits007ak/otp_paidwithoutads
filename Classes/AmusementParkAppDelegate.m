//
//  AmusementParkAppDelegate.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AmusementParkAppDelegate.h"
#import "MainViewController.h"
#import "SplashScreen.h"

#import "SBJson.h"
#import "InAppPurchaseObserver.h"
#import "BuyViewController.h"

#import "UAPush.h"
#import "UAirship.h"
#import "TicketPopupView.h"
//#import "FlurryAnalytics.h"
#import "Flurry.h"
//AppRedeem
#import <CommonCrypto/CommonDigest.h>
#import <dlfcn.h>
#import <mach-o/dyld.h>
#import <RevMobAds/RevMobAds.h>
#import "OpenUDID.h"


//cp
#define Xml_Error @"xml_error"
#define Upgrade_Button @"upgrade_button"
#define Tab_Rides @"tab_rides"
#define Tab_Food @"tab_food"
#define Time_Promt @"time_prompt"
#define Submit_Screen_Title @"submit_screen_title"
#define Submit_Screen_Message @"submit_screen_message"


@implementation AmusementParkAppDelegate
@synthesize RootViewController;
@dynamic ITunesLinkDictionary;
@dynamic AppNames;
@synthesize isPortrait;
@synthesize subviewCount;
@synthesize isIndependent;



-(NSArray*)getAppNames
{
  return _appNames;
}
-(NSDictionary*)ITunesLinkDictionary
{
  return itunesLinkDict;
}


-(UIViewController*)RootViewController
{
  return m_NavigationController;
}

-(BOOL)isValidNotification:(NSDictionary*)info
{
  if(!info)
    return NO;
  NSString *stringRespresentation=[info descriptionInStringsFileFormat];
  if([stringRespresentation rangeOfString:@"action-loc-key"].length>0)
    return YES;
  return NO;
}

#pragma mark - 
#pragma mark AppRedeem
int main(int argc, char *argv[]);
static inline BOOL is_encrypted() {
  const struct mach_header *header;
  Dl_info dlinfo;
  if (dladdr((void *)main, &dlinfo) == 0 || dlinfo.dli_fbase == NULL) {
    return NO;
  }
  header = (struct mach_header *)dlinfo.dli_fbase;
  struct load_command *cmd = (struct load_command *) (header+1);
  for (uint32_t i = 0; cmd != NULL && i < header->ncmds; i++) {
    if (cmd->cmd == LC_ENCRYPTION_INFO) {
      struct encryption_info_command *crypt_cmd = (struct encryption_info_command *)cmd;
      if (crypt_cmd->cryptid < 1) {
        return NO;
      }
      return YES;
    }
    cmd = (struct load_command *)((uint8_t *) cmd + cmd->cmdsize);
  }
  return NO;
}

-(NSString *)hashedUUIDandAppSecret {
  NSString *result = nil;
  int appSecret;
  if([[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"] isEqual:@"Universal Studios Free Wait Times"]
     )
  {
    appSecret=((5445+2272)*88)+754621; //For security, we use math, not strings for secrets, strings are visible in the binary
  }
  else
  {
    appSecret=((5126+4801)*62)+609050; //For security, we use math, not strings for secrets, strings are visible in the binary
    
  }
  
  if(is_encrypted()==NO) {appSecret+=777;}
    NSString* openUDID = [OpenUDID value];
  NSString *uuidAndAppSecret = [[[NSString alloc] initWithFormat:@"%@%d",openUDID,appSecret] autorelease];
  
  if(uuidAndAppSecret) {
    unsigned char digest[16];
    NSData *data = [uuidAndAppSecret dataUsingEncoding:NSASCIIStringEncoding];
    CC_MD5([data bytes], [data length], digest);
    result = [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
              digest[0], digest[1],
              digest[2], digest[3],
              digest[4], digest[5],
              digest[6], digest[7],
              digest[8], digest[9],
              digest[10], digest[11],
              digest[12], digest[13],
              digest[14], digest[15]];
    result = [result uppercaseString];
  }
  return result;
}

-(void)reportAppInstalledToAppReedem {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  
  NSString *appRedeemInstallServer ;
    NSString* openUDID = [OpenUDID value];
  if([[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"] isEqual:@"Universal Studios Free Wait Times"]
     )
  {
    appRedeemInstallServer=[NSString stringWithFormat:
                            @"http://d1.appredeem.com/redeem.php?uuid=%@&appid=%@&k=%@",
                            openUDID, @"480955437",
                            [self hashedUUIDandAppSecret]];

  }
  else
  {
    appRedeemInstallServer=[NSString stringWithFormat:
                            @"http://d1.appredeem.com/redeem.php?uuid=%@&appid=%@&k=%@",
                            openUDID, @"489484003",
                            [self hashedUUIDandAppSecret]];

    
  }
  appRedeemInstallServer=[NSString stringWithFormat:
                                      @"http://d1.appredeem.com/redeem.php?uuid=%@&appid=%@&k=%@",
                                      openUDID, @"480955437",
                                      [self hashedUUIDandAppSecret]];
  NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:appRedeemInstallServer]];
  NSURLResponse *response;
  NSError *error = nil;
  NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
  if((!error) && ([(NSHTTPURLResponse *)response statusCode] == 200) && ([responseData length] > 0)) {
    NSLog(@"Communication with AppRedeem Server - OK");
  }
  [pool release];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                    fallbackHandler:^(FBAppCall *call) {
                        NSLog(@"In fallback handler");
                    }];
}

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
{
    
#ifdef UNIVERSAL_STUDIOS_FREE_RELEASE
    [RevMobAds startSessionWithAppID:@"51a39fedd31c69e994000012"];
#endif
    
#ifdef UNIVERSAL_STUDIOS_FREE_DEBUG
    [RevMobAds startSessionWithAppID:@"51a39fedd31c69e994000012"];
#endif
    
#ifdef DISNEY_WORLD_FREE_DEBUG
   [RevMobAds startSessionWithAppID:@"51a3a06c44b623c4d5000003"];
#endif
    
#ifdef DISNEY_WORLD_FREE_RELEASE
    [RevMobAds startSessionWithAppID:@"51a3a06c44b623c4d5000003"];
#endif
    
	window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
  _appNames=nil;
//    CGRect screenBounds = [[UIScreen mainScreen] bounds];
//    if (screenBounds.size.height == 568) {
//        m_splashScreen = [[SplashScreen alloc] initWithFrame:CGRectMake(0, 20, 320, 548)];
//        // code for 4-inch screen
//    } else {
//        // code for 3.5-inch screen
//        m_splashScreen = [[SplashScreen alloc] initWithFrame:CGRectMake(0, 20, 320, 460)];
//    }

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        m_splashScreen = [[SplashScreen alloc] initWithFrame:CGRectMake(0, 20, 320, 460)];
        if ([UIScreen mainScreen].bounds.size.height == 568) {
       m_splashScreen = [[SplashScreen alloc] initWithFrame:CGRectMake(0, 20, 320, 548)];
        }
    } else if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad){
    
        m_splashScreen = [[SplashScreen alloc] initWithFrame:CGRectMake(0, 20, 768, 1024)];
    }
    
    
    
    
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        isPortrait = NO;
        
    }
    else{
        isPortrait = YES;
    }
    

    isIndependent= YES;
    
   
    NSLog(@"%@",[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"]);
    
    if([[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"] isEqual:@"Universal Studios Free Wait Times"]
       )
    {
	  [Flurry startSession:@"29YW8J26Y8HFGY8RMX5F"];
    }
    else
    {
        [Flurry startSession:@"28Y8PHQ78K4NDRYJ2749"];
        
    }
   [self performSelectorInBackground:@selector(reportAppInstalledToAppReedem) withObject:nil];
        [Flurry setSessionReportsOnCloseEnabled:TRUE];
    [Flurry setSessionReportsOnPauseEnabled:TRUE];
    
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(onSplashScreenExpired) userInfo:nil repeats:NO];
	
    
	m_Timer=nil;
  m_pMainViewController=[[MainViewController alloc]init];
	m_NavigationController=[[UINavigationController alloc]initWithRootViewController:m_pMainViewController];
  if(IsIOS5)
  {
    [m_NavigationController.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]] forBarMetrics:UIBarMetricsDefault];
  }
  else
  {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/tabbar/tabbar" ofType:@"png"]]];
    imageView.frame=CGRectMake(0, 0, 320, 48.5);
    [m_NavigationController.navigationBar insertSubview:imageView atIndex:0];
    [imageView release];
  }
  window.rootViewController=m_NavigationController;
	[window addSubview:m_splashScreen];
  [window makeKeyAndVisible];
  
  NSURLConnection *conn=[NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://admin.apptasmic.com/WebService.php?method=getApps"]] delegate:self];
  [conn start];
  
  if([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.0)
	{
		InAppPurchaseObserver *observer = [[InAppPurchaseObserver alloc] init];
		[[SKPaymentQueue defaultQueue] addTransactionObserver:observer];
	}
  
  
  //Setup Airship
  //Init Airship launch options
  NSMutableDictionary *takeOffOptions = [[[NSMutableDictionary alloc] init] autorelease];
  [takeOffOptions setValue:launchOptions forKey:UAirshipTakeOffOptionsLaunchOptionsKey];
  
  NSString *DisneyWorldWaitTimeDevelopmentAppKey=@"YpPCVXB7SaeUUxh3Fv9Umw";
  NSString *DisneyWorldWaitTimeDevelopmentAppSecret=@"nSp9lddMR5KPBXWKIwtT0A";
    
  NSString *DisneyWorldWaitTimeProductionAppKey=@"-NZbvKM7S2WLQ4y7gJZBYg";
  NSString *DisneyWorldWaitTimeProductionAppSecret=@"YUZh4wdpQgG0s25PCr3AWA";

  
  NSString *UniversalStudioWaitTimeDevelopmentAppKey=@"yygjT18cTAiUkhUnze-z-A";
  NSString *UniversalStudioWaitTimeDevelopmentAppSecret=@"19xtSycKRu6QufPXqW1eJQ";
    
    
  NSString *UniversalStudioWaitTimeProductionAppKey=@"S3_AutGzT1OPf957zV2t1A";
  NSString *UniversalStudioWaitTimeProductionAppSecret=@"-FcuXBXgS9e3TmZIObwGtg";
  
  NSMutableDictionary *airshipConfigOptions = [[[NSMutableDictionary alloc] init] autorelease];

   
#ifdef UNIVERSAL_STUDIOS_FREE_DEBUG
    {
        
        
        [airshipConfigOptions setValue:UniversalStudioWaitTimeDevelopmentAppKey forKey:@"DEVELOPMENT_APP_KEY"];
        [airshipConfigOptions setValue:UniversalStudioWaitTimeDevelopmentAppSecret forKey:@"DEVELOPMENT_APP_SECRET"];
    }
#endif
#ifdef UNIVERSAL_STUDIOS_FREE_RELEASE
    {
        [airshipConfigOptions setValue:UniversalStudioWaitTimeProductionAppKey forKey:@"PRODUCTION_APP_KEY"];
        [airshipConfigOptions setValue:UniversalStudioWaitTimeProductionAppSecret forKey:@"PRODUCTION_APP_SECRET"];
    }
#endif
    
#ifdef DISNEY_WORLD_FREE_DEBUG
    {
        
        
        [airshipConfigOptions setValue:DisneyWorldWaitTimeDevelopmentAppKey forKey:@"DEVELOPMENT_APP_KEY"];
        [airshipConfigOptions setValue:DisneyWorldWaitTimeDevelopmentAppSecret forKey:@"DEVELOPMENT_APP_SECRET"];
    }
#endif
#ifdef DISNEY_WORLD_FREE_RELEASE
    {
        [airshipConfigOptions setValue:DisneyWorldWaitTimeProductionAppKey forKey:@"PRODUCTION_APP_KEY"];
        [airshipConfigOptions setValue:DisneyWorldWaitTimeProductionAppSecret forKey:@"PRODUCTION_APP_SECRET"];
    }
#endif

    
  [airshipConfigOptions setValue:@"YES" forKey:@"APP_STORE_OR_AD_HOC_BUILD"];
    
//#endif
  
  [takeOffOptions setValue:airshipConfigOptions forKey:UAirshipTakeOffOptionsAirshipConfigKey];
  
  // Create Airship singleton that's used to talk to Urban Airhship servers.
  // Please populate AirshipConfig.plist with your info from http://go.urbanairship.com
  [UAirship takeOff:takeOffOptions];
  
  [[UAPush shared] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                       UIRemoteNotificationTypeSound |
                                                       UIRemoteNotificationTypeAlert)];
  if([self isValidNotification:launchOptions])
  {
    NSLog(@"LaunchOptions: %@",launchOptions);
    ticketPageLink=[[[launchOptions valueForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"] valueForKey:@"ticketPageLink"] copy];
    NSLog(@"TicketPageLink: %@",ticketPageLink);
    if ([ticketPageLink length]>0)
    {
      [self performSelector:@selector(buyTicket) withObject:nil afterDelay:3];

    }
    
  }
  return YES;
}

-(void)timerAction
{
  if (![[NSUserDefaults standardUserDefaults]boolForKey:@"Alert"])
  {
    NSString *Message=@"Like the app? Could u take 1 minute to give us 5-star rating?";
    
    UIAlertView *Alert=[[UIAlertView alloc]initWithTitle:@"Rate the App" message:Message delegate:self cancelButtonTitle:@"No, Thanks" otherButtonTitles:@"Rate It Now", @"Remind Me Later",nil];
    
    [Alert show];
    Alert.tag=1;
    Alert.delegate=self;
    [Alert release];
    Alert=nil;
    
  }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if(alertView.tag==777)
  {
      [m_NavigationController popToRootViewControllerAnimated:TRUE];
  }
  if(alertView.tag!=999)
  {
    ++aTimerTaped;
    if (alertView.tag==1) 
    {
      if (buttonIndex==0)
      {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Alert"];
        
        if(Timer)
        {
          [Timer invalidate];
          Timer=nil;
        }
        
      }
      
      else if (buttonIndex==1)
      {
        NSString *iTunes=[[NSUserDefaults standardUserDefaults]valueForKey:@"iTunesLink"];
      
        if ([iTunes length]>0)
        {
          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunes]];
          [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Alert"];
          
          if(Timer)
          {
            [Timer invalidate];
            Timer=nil;
          }
        }
        
      }
      
      else if (buttonIndex==2)
      {
        if(Timer)
        {
          [Timer invalidate];
          Timer=nil;
        }
        
        if (aTimerTaped==1)
        {
          
          Timer=[[NSTimer scheduledTimerWithTimeInterval:120.0  target:self selector:@selector(timerAction) userInfo:nil repeats:NO] retain];
          
        }
        else
        {
          Timer=[[NSTimer scheduledTimerWithTimeInterval:300.0  target:self selector:@selector(timerAction) userInfo:nil repeats:NO] retain];
        }
        printf("\n2\n");
        
      }
      
    }
  }
  else if(alertView.tag==999) //Push Notification Alert
  {
    if(buttonIndex==1)
    {
      if ([ticketPageLink length]>0)
      {
        //Buy Clicked
        [self buyTicket];

      }
    }
  }
}

-(void)onSplashScreenExpired
{
    
    [window addSubview:m_appyeaLogoScreen];
  if(m_splashScreen)
  {
	  [m_splashScreen removeFromSuperview];
	  [m_splashScreen release];
	  m_splashScreen=nil;
  }
    

    
}

-(void)showAlertView
{
  [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"RESETALERTFLAG"];
  UIAlertView *pAlert=[[UIAlertView alloc]initWithTitle:nil message:@"Problem in Internet Connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
  pAlert.tag = 777;
  pAlert.delegate = self;
  [pAlert show];
  [pAlert release];
  pAlert=nil;
  if(m_Timer){[m_Timer release];m_Timer=nil;}
  m_Timer=[NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(resetAlertFlag) userInfo:nil repeats:NO];
  [m_Timer retain];
}

-(void)resetAlertFlag
{
  [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"RESETALERTFLAG"];
  if(m_Timer){[m_Timer release];m_Timer=nil;}
}

- (void)applicationWillResignActive:(UIApplication *)application 
{
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
  if(m_pMainViewController)
    [m_pMainViewController removeVonnection];
  [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}


- (void)applicationWillEnterForeground:(UIApplication *)application 
{
  
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSettings setDefaultAppID:@"194394577408666"];
    
    [FBAppEvents activateApp];
    
    
    [FBAppCall handleDidBecomeActive];
    
    [self getLanguageStrings];
}


- (void)applicationWillTerminate:(UIApplication *)application 
{
    [FBSession.activeSession close];
    
  if(m_pMainViewController)
    [m_pMainViewController removeVonnection];
}

-(void)LoadItuneDictWhenNoInternet
{
  NSURLConnection *conn=[NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://admin.apptasmic.com/WebService.php?method=getApps"]] delegate:self];
  [conn start];
  
  NoInternetAtFirstLaunch=YES;
}

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
}


- (void)dealloc
{
  if(Timer)
  {
    [Timer invalidate];
    Timer=nil;
  }
  if(m_Timer){[m_Timer release];m_Timer=nil;}
	if(m_NavigationController){[m_NavigationController.view removeFromSuperview];[m_NavigationController release];m_NavigationController=nil;}
	if(m_pMainViewController){[m_pMainViewController.view removeFromSuperview];[m_pMainViewController release];m_pMainViewController=nil;}
  if(m_splashScreen)
  {
	  [m_splashScreen removeFromSuperview];
	  [m_splashScreen release];
	  m_splashScreen=nil;
  }
    if(m_appyeaLogoScreen)
    {
        [m_appyeaLogoScreen removeFromSuperview];
        [m_appyeaLogoScreen release];
        m_appyeaLogoScreen=nil;
    }
  [window release];window=nil;
  [super dealloc];
}

//Link to get App iTunes link
//http://admin.apptasmic.com/WebService.php?method=getApps
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
  if(!buffer)
  {
    buffer=[[NSMutableData data] retain];
  }
  [buffer appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  NSString *response=[[NSString alloc]initWithData:buffer encoding:NSUTF8StringEncoding];
  NSArray *array=[response JSONValue];
    
  itunesLinkDict=[[NSMutableDictionary alloc]init];
  NSMutableArray *names=[NSMutableArray array];
  for (NSDictionary *dict in array)
  {
    [itunesLinkDict setValue:[dict valueForKey:@"iTunesLink"] forKey:[dict valueForKey:@"appName"]];
    [names addObject:[dict valueForKey:@"appName"]];
  }
  _appNames=[[NSArray arrayWithArray:names] retain];
  
  if (NoInternetAtFirstLaunch)
  {
    NoInternetAtFirstLaunch=NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DataLoadedNow" object:nil];
  }
  
  NSString *ProductName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
  NSString *iTunes=[itunesLinkDict valueForKey:ProductName];
  
/*itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=*/
  if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FirstTime"])
  {
    NSArray *arr = [iTunes componentsSeparatedByString:@"?"];
    NSArray *Arr1 = [[arr objectAtIndex:0] componentsSeparatedByString:@"/"];
    NSArray *Arr2 = [[Arr1 objectAtIndex:6] componentsSeparatedByString:@"id"];
    printf("\nstri=%s \n",[[Arr2 objectAtIndex:1] UTF8String]);
    NSString *str=[Arr2 objectAtIndex:1];
    NSString *link=[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",str];
    [[NSUserDefaults standardUserDefaults] setValue:link forKey:@"iTunesLink"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FirstTime"];
  }
  
  if (![[NSUserDefaults standardUserDefaults]boolForKey:@"Alert"])
  {
    aTimerInterval=120.0;
    Timer=[[NSTimer scheduledTimerWithTimeInterval:0.30  target:self selector:@selector(timerAction) userInfo:nil repeats:NO] retain];
  }
  
}

#pragma mark -- Functions Related to Push Notification
/*
 {"aps": {"badge": 1, "alert": { "body" : "Tickets for Disney World available at 50% discount!!!\nBuy them now", "action-loc-key" : "BUY" }}, "device_tokens": ["B6D7786837E0449FDCEB678F0DE6D3519A8C18BA7E8FB4EF70DCAA977646D039"]}
 */
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
 
  // Updates the device token and registers the token with UA
  [[UAPush shared] registerDeviceToken:deviceToken];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  
  // Get application state for iOS4.x+ devices, otherwise assume active
  UIApplicationState appState = UIApplicationStateActive;
  if ([application respondsToSelector:@selector(applicationState)]) {
    appState = application.applicationState;
  }

  if(appState==UIApplicationStateActive)
  {

    if([self isValidNotification:userInfo])
    {
        

        NSDictionary *Parameters= [NSDictionary dictionaryWithObjectsAndKeys:@"TICKET_LINK", @"MAINVIEW", nil]; 
        [Flurry logEvent:@"NOTIFICATION_EVENT" withParameters:Parameters];
        
      ticketPageLink=[[userInfo valueForKey:@"ticketPageLink"] copy];
      NSString *message=[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
      NSString *buttonTitle=[[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"action-loc-key"];
      NSString *appName=[[[NSBundle mainBundle] infoDictionary]   objectForKey:@"CFBundleName"];
      UIAlertView *alert=[[[UIAlertView alloc]initWithTitle:appName message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:buttonTitle, nil] autorelease];
      alert.tag=999;
      [alert show];
    }
  }
  else
  {

    if([self isValidNotification:userInfo])
    {
      ticketPageLink=[[userInfo valueForKey:@"ticketPageLink"] copy];
      if ([ticketPageLink length]>0)
      {
        [self buyTicket];
      }
    }
  }
}
-(void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
// NSLog(@"ERROR:%@",error);
}
-(void)doSomething
{
  if ([[NSUserDefaults standardUserDefaults]boolForKey:@"AdTickets"])
  {
      NSDictionary *Parameters= [NSDictionary dictionaryWithObjectsAndKeys:@"TICKET_LINK", @"MAINVIEW", nil]; 
      [Flurry logEvent:@"ADS_CLICK_EVENT" withParameters:Parameters];
      
    TicketPopupView *mTicketPopupView=[[TicketPopupView alloc]initWithLink:ticketPageLink];
    [mTicketPopupView show];
    [ticketPageLink release];
    ticketPageLink=nil;

  }
  else
  {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UsingPushNotification"];
      NSDictionary *Parameters= [NSDictionary dictionaryWithObjectsAndKeys:@"APPS_PURCHASE", @"MAINVIEW", nil]; 
      [Flurry logEvent:@"ADS_CLICK_EVENT" withParameters:Parameters];
      
      
    NSString *productIdentifier=nil;
    if(IsDisneyWorldFreeWaitTimes)
      productIdentifier=DisneyTicketsInAppIdentifier;
    else if(IsUniversalStudioFreeWaitTimes)
      productIdentifier=UniversalTicketsInAppIdentifier;
    
    [[BuyViewController sharedBuyViewController] MakeInAppPurchaseWithProductId:productIdentifier withTag:4];
  }
}
-(void)buyTicket
{
  [self performSelector:@selector(doSomething) withObject:nil afterDelay:0.5]; 
}

-(void)IsInappPurchaseSucceded:(BOOL)Success
{
  if (Success)
  {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"UsingPushNotification"];
    
    [self performSelector:@selector(doSomething) withObject:nil afterDelay:0.5]; 
  }
  else
  {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"UsingPushNotification"];

  }
  
}



//cp

-(void)getLanguageStrings
{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString* xmlFileName = [NSString stringWithFormat:@"strings_%@",[NSString stringWithFormat:@"%@",language]];
    
    NSString *xmlPath = [[NSBundle mainBundle] pathForResource:xmlFileName ofType:@"xml"];
    if (xmlPath == nil) {
        xmlPath = [[NSBundle mainBundle] pathForResource:@"strings_en" ofType:@"xml"];
    }
    NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
    xmlParser.delegate = self;
    [xmlParser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([attributeDict count]>0) {
        xmlHelper = [attributeDict mutableCopy];
    }
    currentString = @"";
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	currentString = [[currentString stringByAppendingString:string] mutableCopy];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    if ([[xmlHelper valueForKey:@"name"] isEqualToString:Xml_Error]) {
        [[NSUserDefaults standardUserDefaults] setValue:currentString forKey:Xml_Error];
        NSLog(@"%@ ",currentString);
    }
    if ([[xmlHelper valueForKey:@"name"] isEqualToString:Upgrade_Button]) {
        [[NSUserDefaults standardUserDefaults] setValue:currentString forKey:Upgrade_Button];
        NSLog(@"%@ ",currentString);
    }
    
    if ([[xmlHelper valueForKey:@"name"] isEqualToString:Tab_Rides]) {
        [[NSUserDefaults standardUserDefaults] setValue:currentString forKey:Tab_Rides];
        NSLog(@"%@ ",currentString);
    }
    if ([[xmlHelper valueForKey:@"name"] isEqualToString:Tab_Food]) {
        [[NSUserDefaults standardUserDefaults] setValue:currentString forKey:Tab_Food];
        NSLog(@"%@ ",currentString);
    }
    if ([[xmlHelper valueForKey:@"name"] isEqualToString:Time_Promt]) {
        [[NSUserDefaults standardUserDefaults] setValue:currentString forKey:Time_Promt];
        NSLog(@"%@ ",currentString);
    }
    if ([[xmlHelper valueForKey:@"name"] isEqualToString:Submit_Screen_Title]) {
        [[NSUserDefaults standardUserDefaults] setValue:currentString forKey:Submit_Screen_Title];
        NSLog(@"%@ ",currentString);
    }
    if ([[xmlHelper valueForKey:@"name"] isEqualToString:Submit_Screen_Message]) {
        [[NSUserDefaults standardUserDefaults] setValue:currentString forKey:Submit_Screen_Message];
        NSLog(@"%@ ",currentString);
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

@end
