//
//  TicketPopupViewController.m
//  AmusementPark
//
//  Created by Amresh Kumar on 17/01/12.
//  Copyright (c) 2012 Permeative Technologies Pvt. Ltd. All rights reserved.
//

#import "TicketPopupView.h"

@implementation TicketPopupView
-(id)initWithLink:(NSString *)webLink
{
  self=[super initWithFrame:CGRectMake(0,20,320,460)];
  if(self)
  {
    UIImageView *topImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ImagesCommon/UpGrade/CancelBGImage.png"]];
    topImage.frame=CGRectMake(0,0,320,44);
    [self addSubview:topImage];
    [topImage release];
    
    webView=[[UIWebView alloc]initWithFrame:CGRectMake(0,44,320,416)];
    [self addSubview:webView];
    [webView release];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:webLink]]];
    webView.delegate=self;
    webView.scalesPageToFit=YES;
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    button.frame=CGRectMake(5,0,50,44);
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    
    int badgeCount=[UIApplication sharedApplication].applicationIconBadgeNumber;
    if(badgeCount==10)
    {
      badgeCount=0;
    }
    else
    {
      badgeCount--;
      if(badgeCount<0)
        badgeCount=0;
    }
    
  }
  return self;
}

-(void)dealloc
{
  webView.delegate=nil;
  [webView stopLoading];
  [super dealloc];
}

#pragma mark -- Public functions
-(void)show
{
  if(self.superview)
    return;
  [[UIApplication sharedApplication].keyWindow addSubview:self];
  [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self];
  
  self.transform=CGAffineTransformMakeScale(0.001,0.001);
//  self.transform = CGAffineTransformScale(self.transform, 0.001, 0.001);
  [UIView beginAnimations:nil context:nil];
  [UIView setAnimationDuration:0.3];
  [UIView setAnimationDelegate:self];
  [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
  self.transform=CGAffineTransformMakeScale(1.1,1.1);
  [UIView commitAnimations];
}
-(void)bounce1AnimationStopped 
{
  [UIView beginAnimations:nil context:nil];
  [UIView setAnimationDuration:0.3/2];
  [UIView setAnimationDelegate:self];
  [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
  self.transform=CGAffineTransformMakeScale(0.9,0.9);
  [UIView commitAnimations];
}

-(void)bounce2AnimationStopped 
{
  [UIView beginAnimations:nil context:nil];
  [UIView setAnimationDuration:0.3/2];
  self.transform=CGAffineTransformMakeScale(1,1);
  [UIView commitAnimations];
}
-(void)hide
{
  [UIView beginAnimations:nil context:nil];
  [UIView setAnimationDuration:0.3];
  [UIView setAnimationDelegate:self];
  [UIView setAnimationDidStopSelector:@selector(postDismissCleanup)];
  self.alpha = 0;
  [UIView commitAnimations];
}

-(void)postDismissCleanup 
{
  webView.delegate=nil;
  [webView stopLoading];
  
  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [self removeFromSuperview];
}
#pragma mark -- UI Actions
-(void)buttonAction
{
  //Cancel the page
  [self hide];
}
#pragma mark -- UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
  [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;  
}
@end
