//
//  SubmitWaitTimeViewController.h
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import "AmusementParkAppDelegate.h"


@protocol SubmitWaitTimeViewControllerDelegate

-(void)BackAction;

@end

@interface SubmitWaitTimeViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,CLLocationManagerDelegate,UIAlertViewDelegate>
{
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    UIActivityIndicatorView *m_activityIndicator;
	UILabel *m_DescriptionLabel;
    UILabel *pTitleLabel;
	UIView *m_PickerView;
	UIPickerView *m_WaitTimePickerview;
	UINavigationBar *m_NavigBar;
	NSArray *m_TimeArray;
	UIButton *m_BackButton;
    NSString *m_pId;
    NSString *m_Latitude;
    NSString *m_Longitude;
    NSString *m_ParkName;
    UIButton *m_SubmitButton;
    UIImageView *m_NavigImage;
    int m_WaitTime;
    BOOL isRide;
    UILabel *m_DescLAbel1;
    CLLocationManager *m_LocationManager;
    CLLocation *m_Location;
    UIScrollView * scrollView;
    UIImageView * m_BGImageView;
    UIImageView *pDescImageView;
    AmusementParkAppDelegate * appDelegate;
 	id<SubmitWaitTimeViewControllerDelegate> m_delegate;
}
@property(nonatomic,assign) id<SubmitWaitTimeViewControllerDelegate> m_delegate;
-(void)connectToServer;
-(id)initWithId:(NSString *)pId Latitude:(NSString *)pLat Longitude:(NSString *)pLong isRide:(BOOL)pRide;
-(void)stopUpDatingLocation;
-(void)SubmitAction;
-(BOOL)getLocations;
@end
