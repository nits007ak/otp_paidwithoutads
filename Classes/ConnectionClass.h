//
//  ConnectionClass.h
//  BoxAPIDemo
//
//  Created by Tilak on 23/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ConnectionClassDelegate <NSObject>
-(void)responseObtained:(NSString *)docFilePath;
-(void)connectionFailedWithMessage:(NSString *)msgStr;
@end


@interface ConnectionClass : NSObject
{
	NSURLConnection* mURLConnection;
	NSMutableData* mResponseData;
	NSString *mSavingFilePath;
	id<ConnectionClassDelegate>mDelegate;
}
@property(nonatomic,assign)	id<ConnectionClassDelegate>mDelegate;
-(void)connectToServerWithURL:(NSString *)UrlStr saveTo:(NSString *)filePath;
@end
