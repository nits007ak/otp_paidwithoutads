//
//  AdsManager.m
//  AmusementPark
//
//  Created by Amresh Kumar on 21/10/11.
//  Copyright (c) 2011 Permeative Technologies Pvt. Ltd. All rights reserved.
//

#import "AdsManager.h"
#import "UpgradeViewController.h"
#import "AmusementParkAppDelegate.h"
#import "DetailViewController.h"
#import "InAppPurchaseObserver.h"
#import <objc/message.h>

@interface FullVersionBannerBig : UIView <BuyViewControllerDelegate>
{
  UIButton *fullVersionButton;
  UIButton *UpGradeButton;
  BOOL isDecreasing,isDecreasing2;
  NSTimer *timer,*timer2;
  UIImageView *img,*img2;
}
-(BOOL)RemoveUpGradeButton;
@end

@implementation FullVersionBannerBig
-(id)initWithFrame:(CGRect)frame
{

  self=[super initWithFrame:frame];
  if(self)
  {
    isDecreasing=YES;
    isDecreasing2=YES;

    
//    UIImageView *bannerBG=[[UIImageView alloc]initWithFrame:self.bounds];
//    bannerBG.image=[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/CustomAdBG.png"];
//    [self addSubview:bannerBG];
//    [bannerBG release];
    
    fullVersionButton=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
    fullVersionButton.frame=CGRectMake(189, -10, self.bounds.size.width/2, self.bounds.size.height);

    [fullVersionButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/FullVersion.png"] forState:UIControlStateNormal];
    [fullVersionButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/FullVersion.png"] forState:UIControlStateSelected];
    [fullVersionButton addTarget:self action:@selector(FullVersionClicked) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:fullVersionButton];
    
    img=[[UIImageView alloc]initWithFrame:CGRectMake(216,0,105,32)];
    img.image=[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/FullVersionGlow.png"];
    [self addSubview:img];
    [img release];
    
    UpGradeButton=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
    UpGradeButton.frame=CGRectMake(-30, -10, self.bounds.size.width/2, self.bounds.size.height);
    [UpGradeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/RemoveAdsButtonNormal.png"] forState:UIControlStateNormal];
    [UpGradeButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/RemoveAdsButtonNormalPressed.png"] forState:UIControlStateSelected];
    [UpGradeButton addTarget:self action:@selector(UpGradeBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:UpGradeButton];
    
    img2=[[UIImageView alloc]initWithFrame:CGRectMake(-2,0,102,32)];
    img2.image=[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/RemoveAdsButtonGlow.png"];
    [self addSubview:img2];
    [img2 release];

    
    timer=[[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES] retain];
    [timer fire];
    
    timer2=[[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerAction2) userInfo:nil repeats:YES] retain];
    [timer2 fire];
     
    
      
      [self checkOrientation];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(didRotate:)
                                                   name:@"UIDeviceOrientationDidChangeNotification" object:nil];
//    if ([self RemoveUpGradeButton])
//    {
//      if (img2)
//      {
//        [img2 removeFromSuperview];
//        img2=nil;
//      }
//      
//      if (UpGradeButton)
//      {
//        [UpGradeButton removeFromSuperview];
//        UpGradeButton=nil;
//      }
//      
//      if(timer2)
//      {
//        [timer2 invalidate];
//        timer2=nil;
//      }
//
//      fullVersionButton.frame=CGRectMake(self.frame.size.width/2-self.bounds.size.width/4, -10, self.bounds.size.width/2, self.bounds.size.height);
//      img.frame=CGRectMake(self.frame.size.width/2-105/2,0,105,32);
//
//    }

  }
  return self;
}

- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
      AmusementParkAppDelegate * appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    
}


    
-(void)timerAction
{
  if(isDecreasing)
    img.alpha-=0.1;
  else
    img.alpha+=0.1;
  if(img.alpha==1)
  {  
    isDecreasing=YES;
  }
  
  if(fabs(img.alpha)<=0.1)
  {
    isDecreasing=NO;
  }
}

-(void)timerAction2
{
  if(isDecreasing2)
    img2.alpha-=0.1;
  else
    img2.alpha+=0.1;
  if(img2.alpha==1)
  {  
    isDecreasing2=YES;
  }
  
  if(fabs(img2.alpha)<=0.1)
  {
    isDecreasing2=NO;
  }
}

-(void)dealloc
{
  if(timer)
  {
    [timer invalidate];
    timer=nil;
  }
  if(timer2)
  {
    [timer2 invalidate];
    timer2=nil;
  }
  [super dealloc];
}
-(void)FullVersionClicked
{
  AmusementParkAppDelegate *mAmusementParkAppDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
  UpgradeViewController *mUpgradeViewController=[[[UpgradeViewController alloc]init] autorelease];
  [mAmusementParkAppDelegate.RootViewController presentModalViewController:mUpgradeViewController animated:YES];
}

-(void)UpGradeBtnClicked
{
  AmusementParkAppDelegate *mAmusementParkAppDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
  BuyViewController *m_BuyViewController=[[[BuyViewController alloc]init] autorelease];
  m_BuyViewController.m_delegate=self;
  [mAmusementParkAppDelegate.RootViewController presentModalViewController:m_BuyViewController animated:YES];
}

#pragma BuyViewControllerDelegate
-(void)DonePurchasing
{
//  if ([self RemoveUpGradeButton])
//  {
//    if (img2)
//    {
//      [img2 removeFromSuperview];
//      img2=nil;
//    }
//    
//    if (UpGradeButton)
//    {
//      [UpGradeButton removeFromSuperview];
//      UpGradeButton=nil;
//    }
//    fullVersionButton.frame=CGRectMake(self.frame.size.width/2-self.bounds.size.width/4, -10, self.bounds.size.width/2, self.bounds.size.height);
//    img.frame=CGRectMake(self.frame.size.width/2-105/2,0,105,32);
//  }
}
-(void)AddBigBanner
{
  //BuyViewControllerDelegate;
}
-(BOOL)RemoveUpGradeButton
{
  if ([[NSUserDefaults standardUserDefaults]boolForKey:@"AdTickets"]&&[[NSUserDefaults standardUserDefaults]boolForKey:@"AdDescription"]&&[[NSUserDefaults standardUserDefaults]boolForKey:@"ADDMAP"] && [[NSUserDefaults standardUserDefaults]boolForKey:@"IsInAppPurchaseDone"]) 
  {
    return YES;
  }
  else
    return NO;
}

@end

@interface FullVersionBanner:UIView
{
  UIButton *fullVersionButton;
  BOOL isDecreasing;
  NSTimer *timer;
  UIImageView *img;
}

@end

@implementation FullVersionBanner
-(id)initWithFrame:(CGRect)frame
{
  self=[super initWithFrame:frame];
  if(self)
  {
    isDecreasing=YES;
    
    fullVersionButton=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
    fullVersionButton.frame=CGRectMake(0,0,105,32);
    [fullVersionButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/FullVersion.png"] forState:UIControlStateNormal];
    [fullVersionButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/FullVersion.png"] forState:UIControlStateSelected];
    [fullVersionButton addTarget:self action:@selector(FullVersionClicked) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:fullVersionButton];
    
    img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,32)];
    img.image=[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/FullVersionGlow.png"];
    [self addSubview:img];
    [img release];
    
    timer=[[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES] retain];
    [timer fire];
  }
  return self;
}
-(void)timerAction
{
  if(isDecreasing)
    img.alpha-=0.1;
  else
    img.alpha+=0.1;
  if(img.alpha==1)
  {  
    isDecreasing=YES;
  }
  
  if(fabs(img.alpha)<=0.1)
  {
    isDecreasing=NO;
  }
}
-(void)dealloc
{
  if(timer)
  {
    [timer invalidate];
    timer=nil;
  }
  [super dealloc];
}
-(void)FullVersionClicked
{
  AmusementParkAppDelegate *mAmusementParkAppDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
  UpgradeViewController *mUpgradeViewController=[[[UpgradeViewController alloc]init] autorelease];
  [mAmusementParkAppDelegate.RootViewController presentModalViewController:mUpgradeViewController animated:YES];
}

@end


#pragma mark----------RemoveAdsButtonClass

@interface RemoveAdsButton:UIView
{
  UIButton *removeAdsButton;
  BOOL isDecreasing;
  NSTimer *timer;
  UIImageView *img;
  id _target;
  SEL _action;
}
-(void)addTarget:(id)target action:(SEL)action;
@end

@implementation RemoveAdsButton
-(id)initWithFrame:(CGRect)frame
{
  self=[super initWithFrame:frame];
  if(self)
  {
    isDecreasing=YES;
    
    removeAdsButton=[[UIButton buttonWithType:UIButtonTypeCustom] retain];
    removeAdsButton.frame=CGRectMake(0,0,105,32);
    [removeAdsButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/RemoveAdsButtonNormal.png"] forState:UIControlStateNormal];
    [removeAdsButton setImage:[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/RemoveAdsButtonNormalPressed.png"] forState:UIControlStateSelected];
    [removeAdsButton addTarget:self action:@selector(RemoveAdsClicked) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:removeAdsButton];
    
    img=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,105,32)];
    img.image=[UIImage imageNamed:@"ImagesInFreeVersion/AdImages/RemoveAdsButtonGlow.png"];
    [self addSubview:img];
    [img release];
    
    timer=[[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES] retain];
    [timer fire];
  }
  return self;
}
-(void)timerAction
{
  if(isDecreasing)
    img.alpha-=0.1;
  else
    img.alpha+=0.1;
  if(img.alpha==1)
  {  
    isDecreasing=YES;
  }
  
  if(fabs(img.alpha)<=0.1)
  {
    isDecreasing=NO;
  }
}
-(void)dealloc
{
  if(timer)
  {
    [timer invalidate];
    timer=nil;
  }
  [super dealloc];
}
-(void)RemoveAdsClicked
{
  if(_target)
  {
    if([_target respondsToSelector:_action])
    {
      objc_msgSend(_target,_action,_target);
    }
  }
}
-(void)addTarget:(id)target action:(SEL)action
{
  _target=target;
  _action=action;
}
@end


static AdsManager *_instance=nil;

@interface AdsManager()
@property(nonatomic,assign)UIViewController *_VC;
@end

@implementation AdsManager
@synthesize _VC;

+(AdsManager*)sharedAdsManager
{
  if(!_instance)
  {
    _instance=[[AdsManager alloc]init];
    _instance._VC=nil;
  }
  return _instance;
}
-(void)ShowAdBannerFromVC:(UIViewController*)viewController withFrame:(CGRect)frame
{
  if(!_adBanner)
  {
    _adBanner=[[UIView alloc]initWithFrame:CGRectMake(0.0,
                                                      viewController.view.frame.size.height -
                                                      50,
                                                      frame.size.width,
                                                      50)];
    _adBanner.backgroundColor=[UIColor colorWithWhite:0 alpha:0.5];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
    {
      _iAdBanner=[[ADBannerView alloc]initWithFrame:_adBanner.bounds];
      _iAdBanner.delegate=self;
      
      _gAdBanner=[[GADBannerView alloc]initWithFrame:_adBanner.bounds];
      if(IsDisneyWorldFreeWaitTimes)
        _gAdBanner.adUnitID = @"a14f16c874abee9";
      else if(IsUniversalStudioFreeWaitTimes)
        _gAdBanner.adUnitID = @"a14e5409a1bbd53";
      _gAdBanner.delegate=self;
      [_adBanner addSubview:_gAdBanner];
      [_adBanner addSubview:_iAdBanner];
      
      _gAdBanner.hidden=YES;
      _iAdBanner.hidden=NO;
      
    }
    else
    {
      _adBanner.backgroundColor=[UIColor clearColor];

      if(!_fullVersionBannerBig)
      {
        _fullVersionBannerBig=[[FullVersionBannerBig alloc]initWithFrame:_adBanner.bounds];
      }
      [_adBanner addSubview:_fullVersionBannerBig];
      [_fullVersionBannerBig release];
    
    }
    
  }
  if(_adBanner.superview)
    [_adBanner removeFromSuperview];
  _adBanner.frame=CGRectMake(frame.origin.x,
                             frame.origin.y,
                             frame.size.width,
                             50);
    _iAdBanner.frame = _adBanner.frame;
    _gAdBanner.frame  = _adBanner.frame;
  [viewController.view addSubview:_adBanner];
  [viewController.view bringSubviewToFront:_adBanner];
  
  if(![[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
  {
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    _gAdBanner.rootViewController = viewController;
    // Initiate a generic request to load it with an ad.
    [_gAdBanner loadRequest:[GADRequest request]];
  }
  
  [viewController.view addSubview:_adBanner];
  
}

-(void)ShowFullVersionBannerFromVC:(UIViewController*)viewController withOrigin:(CGPoint)origin
{
  if(![[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
  {
    if(!_fullVersionBanner)
      _fullVersionBanner=[[FullVersionBanner alloc]initWithFrame:CGRectMake(origin.x,origin.y,105,32)];
    else if(_fullVersionBanner.superview)
      [_fullVersionBanner removeFromSuperview];
    
    CGRect frame=CGRectMake(origin.x,origin.y,105,32);
    _fullVersionBanner.frame=frame;
    [viewController.view addSubview:_fullVersionBanner];
    [viewController.view bringSubviewToFront:_fullVersionBanner];
    
    if(!_removeAdsButton)
    {
      _removeAdsButton=[[RemoveAdsButton alloc]initWithFrame:CGRectMake(0,origin.y,105,32)];
      [_removeAdsButton addTarget:self action:@selector(removeAdsButtonClicked)];
    }
    CGRect frame1=CGRectMake(0,origin.y,105,32);
    _removeAdsButton.frame=frame1;
    [viewController.view addSubview:_removeAdsButton];
    [viewController.view bringSubviewToFront:_removeAdsButton];
    
  }
    else
    {
        CGRect frame=CGRectMake(origin.x,origin.y,105,32);
        _fullVersionBanner.frame=frame;
        [viewController.view addSubview:_fullVersionBanner];
        [viewController.view bringSubviewToFront:_fullVersionBanner];
        
        CGRect frame1=CGRectMake(0,origin.y,105,32);
        _removeAdsButton.frame=frame1;
        [viewController.view addSubview:_removeAdsButton];
        [viewController.view bringSubviewToFront:_removeAdsButton];
    }
}

-(void)RemoveAdBannerAndFullVersionBanner
{
  if(_adBanner.superview)
  {
    [_adBanner removeFromSuperview];
  }
  
  if (_fullVersionBanner)
  {
    [_fullVersionBanner removeFromSuperview];
  }
  
  if (_removeAdsButton)
  {
    [_removeAdsButton removeFromSuperview];

  }
  
}

#pragma mark -- GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
  if(!_iAdBanner.isBannerLoaded)
  {
    _iAdBanner.hidden=YES;
    _gAdBanner.hidden=NO;
  }
}

// Sent when an ad request failed.  Normally this is because no network
// connection was available or no ads were available (i.e. no fill).  If the
// error was received as a part of the server-side auto refreshing, you can
// examine the hasAutoRefreshed property of the view.
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
  [view loadRequest:[GADRequest request]];
}

-(void)adViewWillDismissScreen:(GADBannerView *)adView
{
  UIWindow *window=[UIApplication sharedApplication].keyWindow;
  UINavigationController *navC=(UINavigationController *)window.rootViewController;
  UIViewController *currentViewController=navC.topViewController;
  if([currentViewController isKindOfClass:[DetailViewController class]])
  {
    [(DetailViewController*)currentViewController setTabBarVisiblity:YES];
  }
}

-(void)adViewWillPresentScreen:(GADBannerView *)adView
{
  UIWindow *window=[UIApplication sharedApplication].keyWindow;
  UINavigationController *navC=(UINavigationController *)window.rootViewController;
  UIViewController *currentViewController=navC.topViewController;
  if([currentViewController isKindOfClass:[DetailViewController class]])
  {
    [(DetailViewController*)currentViewController setTabBarVisiblity:NO];
  }
}

-(void)adViewWillLeaveApplication:(GADBannerView *)adView
{
  UIWindow *window=[UIApplication sharedApplication].keyWindow;
  UINavigationController *navC=(UINavigationController *)window.rootViewController;
  UIViewController *currentViewController=navC.topViewController;
  if([currentViewController isKindOfClass:[DetailViewController class]])
  {
    [(DetailViewController*)currentViewController setTabBarVisiblity:NO];
  }
}

#pragma mark AdBannerViewDelegate
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
  _iAdBanner.hidden=NO;
  _gAdBanner.hidden=YES;  
}

// This method will be invoked when an error has occurred attempting to get advertisement content.
// The ADError enum lists the possible error codes.
- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
  _iAdBanner.hidden=YES;
  _gAdBanner.hidden=NO;  
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
  UIWindow *window=[UIApplication sharedApplication].keyWindow;
  UINavigationController *navC=(UINavigationController *)window.rootViewController;
  UIViewController *currentViewController=navC.topViewController;
  if([currentViewController isKindOfClass:[DetailViewController class]])
  {
    [(DetailViewController*)currentViewController setTabBarVisiblity:NO];
  }
  return YES;
}

// This message is sent when a modal action has completed and control is returned to the application.
// Games, media playback, and other activities that were paused in response to the beginning
// of the action should resume at this point.
- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
  UIWindow *window=[UIApplication sharedApplication].keyWindow;
  UINavigationController *navC=(UINavigationController *)window.rootViewController;
  UIViewController *currentViewController=navC.topViewController;
  if([currentViewController isKindOfClass:[DetailViewController class]])
  {
    [(DetailViewController*)currentViewController setTabBarVisiblity:YES];
  }
}

#pragma mark -- RemoveAds Button Action
-(void)removeAdsButtonClicked
{
  AmusementParkAppDelegate *mAmusementParkAppDelegate=(AmusementParkAppDelegate*)[UIApplication sharedApplication].delegate;
  BuyViewController *m_BuyViewController=[[[BuyViewController alloc]init] autorelease];
  
  m_BuyViewController.m_delegate=self;
  
  [mAmusementParkAppDelegate.RootViewController presentModalViewController:m_BuyViewController animated:YES];
  
}

#pragma mark -- BuyViewController Delegates

-(void)DonePurchasing
{
  // BuyViewController Delegate;
}
-(void)AddBigBanner
{
  if ([[NSUserDefaults standardUserDefaults]boolForKey:@"IsInAppPurchaseDone"]) 
  {
    if(_iAdBanner)
    {
      [_iAdBanner removeFromSuperview];
      [_iAdBanner release];
      _iAdBanner=nil;
    }
    if(_gAdBanner)
    {
      _gAdBanner.delegate=nil;
      [_gAdBanner removeFromSuperview];
      [_gAdBanner release];
      _gAdBanner=nil;
    }
    if(_fullVersionBanner)
    {
      [_fullVersionBanner removeFromSuperview];
      _fullVersionBanner=nil;
    }
    if(_removeAdsButton)
    {
      [_removeAdsButton removeFromSuperview];
      _removeAdsButton=nil;
    }
    if(!_fullVersionBannerBig)
    {
      _fullVersionBannerBig=[[FullVersionBannerBig alloc]initWithFrame:_adBanner.bounds];
    }
    
    [_adBanner addSubview:_fullVersionBannerBig];
    _adBanner.backgroundColor=[UIColor clearColor];
    //[_fullVersionBannerBig release];

  }
}

@end
