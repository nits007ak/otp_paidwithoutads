//
//  RestuarantDetailViewController.h
//  AmusementPark
//
//  Created by HARSHA on 28/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Restuarant.h"
#import "SubmitWaitTimeViewController.h"
#import "ImageResourceDownloader.h"
#import "AmusementParkAppDelegate.h"


@protocol RestuarantDetailViewControllerDelegate

-(void)BackAction;

@end

@interface RestuarantDetailViewController : UIViewController<SubmitWaitTimeViewControllerDelegate,ImageResourceDownloaderDelegate>
{
    NSURLConnection *mURLConnection;
    NSMutableData *mResponseData;
    UIActivityIndicatorView *m_activityIndicator;
    NSMutableArray *m_RestuarantArray;
    UILabel *pTitleLabel;
    UIButton *m_BackButton;
    AmusementParkAppDelegate * appDelegate;
	UIImageView *m_ImageView;
    UIImageView *m_FavouriteImageView;
    UIImageView *m_FacebookImageView;
	UILabel *m_TitleLabel;
	UILabel *m_CuisineTitleLabel;
	UILabel *m_CuisineTypeLabel;
	UILabel *m_ReservationLabel;
	UIButton *m_ReservationButton;
    UILabel *m_DollarLabel;
	UILabel *m_TableServiceLabel;
	UILabel *m_TableServiceCostLabel;
    UILabel *m_CallNumberLabel;
    UILabel *m_WaitTimeLabel;
	UIButton *m_FavoriteButton;
	UIButton *m_CallButton;
    UIButton *m_SubmitButton;
    UIButton *m_FacebookButton;
    
	UITextView *m_DescriptionView;
    Restuarant *m_RestuarantFromDb;
    BOOL isFav,isInd,isFirst;
    int m_Distance;
    NSString *m_StudioId;
    NSString *m_ParkId;
    NSString *m_LandId;
    NSString *m_RestId;
    NSString *m_LandName;
    NSString *m_ImagePath;
    NSString *m_Latitude;
    NSString *m_Longitude;
    Restuarant *m_Restuarant;
    UIScrollView * scrollView;
    NSTimer *m_Timer;
    UIImageView * pBGImageView;
    
    SubmitWaitTimeViewController *m_SubmitWaitTimeViewController;
    ImageResourceDownloader *m_ImageResourceDownloader;
    id<RestuarantDetailViewControllerDelegate>m_delegate;
}


@property(nonatomic) BOOL isInd;
@property(nonatomic,assign) id<RestuarantDetailViewControllerDelegate>m_delegate;
-(id)initwithStudioId:(NSString *)pStuId ParkId:(NSString *)pParkId LandId:(NSString *)pLandId RestId:(NSString *)pRideId LandName:(NSString *)pLandName Distance:(int)dist isFavourite:(BOOL)fav imagePath:(NSString *)imagePath pLat:(NSString *)lat pLong:(NSString *)longitude;
-(void)connectToServer;
-(void)getFromDataBase;
-(void)submitWaitTimeAction;
-(void)NotificationRecieved;
-(void)downloadImages;
-(void)refreshData;
@end
