    //
//  FoodViewController.m
//  AmusementPark
//
//  Created by HARSHA on 26/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FoodViewController.h"
#import "RestuarantDetailCell.h"
#import "RestuarantDetailViewController.h"
#import "XMLDataParser.h"
#import "Land.h"
#import "Restuarant.h"
#import "AmusementParkAppDelegate.h"
#import "AdsManager.h"
#import "SubmitWaitTimeViewController.h"
@implementation FoodViewController
@synthesize m_delegate;
-(id)initwithStudioId:(NSString *)pStudioId ParkId:(NSString *)pParkId
{
	if(self=[super init])
	{
        m_SelectedSortType=0;
        m_ParkId=pParkId;
        [m_ParkId retain];
        m_StudioId=pStudioId;
        [m_StudioId retain];
		m_TableView=nil;
		m_DateLabel=nil;
        pTitleLabel=nil;
        m_Homebtn=nil;
        m_DistanceButton=nil;
        mURLConnection=nil;
        mResponseData=nil;
        m_activityIndicator=nil;
        m_Location=nil;
        m_RestuarantDetailViewController=nil;
        m_SortRideViewController=nil;
        m_LocationManager=nil;
        isFirst=YES;
        
        m_Timer=nil;
        
		m_RestuarantArray=[[NSMutableArray alloc]init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRecieved) name:@"SelectedTwice" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AdsRemoved) name:AdRemovedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AddMap) name:MapAddedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AdDescription) name:AdDescriptionNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector (AdTickets) name:AdTicketsNotification object:nil];
        
        
	}
	return self;
}


-(void)loadView
{
    appDelegate = (AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)] autorelease];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        self.view=[[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)] autorelease];
    }
	self.view.backgroundColor=[UIColor whiteColor];
	//self.title=@"Food";

  if(pTitleLabel==nil)
  pTitleLabel=[[UILabel alloc]initWithFrame:CGRectMake(60, 8, 190, 30)];
  pTitleLabel.backgroundColor=[UIColor clearColor];
  pTitleLabel.frame=CGRectMake(60, 8, 244, 30);
  
  pTitleLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
  pTitleLabel.textColor=[UIColor whiteColor];
  pTitleLabel.textAlignment=UITextAlignmentCenter;
  pTitleLabel.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"PARKNAME"];
  
  if(m_Homebtn==nil)
  {
    m_Homebtn=[UIButton buttonWithType:UIButtonTypeCustom];
    m_Homebtn.frame=CGRectMake(5, 8, 53.5, 30);
    [m_Homebtn setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/homebtn" ofType:@"png"]] forState:UIControlStateNormal];
    [m_Homebtn addTarget:self action:@selector(backtoHomeAction) forControlEvents:UIControlEventTouchUpInside];
    [m_Homebtn retain];
  
    
  }
  
	
	
    
    if (screenBounds.size.height == 568) {
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, -40, 320, 540)];
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
        m_BGImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, -40, 320, 460)];
    }

	m_BGImageView.image=[UIImage imageNamed:@"ImagesCommon/Background.png"];
	[self.view addSubview:m_BGImageView];	
	[m_BGImageView release];
	
  
   AdOffset=0;
  AdOffset=49;
  
  int FullVersionBannerOffset=0;
  FullVersionBannerOffset=30;
  
  if([[NSUserDefaults standardUserDefaults] boolForKey:@"IsInAppPurchaseDone"])
  {
    FullVersionBannerOffset=0;
    AdOffset=30;
    
  }
  
  NSDate *date = [NSDate date];
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"EEEE - MMMM d, YYYY"];
  NSString *dateString = [dateFormat stringFromDate:date];  
  [dateFormat release];  
  
  DateBarImageView= [[UIImageView alloc] initWithFrame:CGRectMake(0, 5+AdOffset, 320, 27)];
  DateBarImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/datebar" ofType:@"png"]];
[DateBarImageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:DateBarImageView];
  
  if(m_DateLabel==nil)
	m_DateLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 3+AdOffset, 320, 27)];
	m_DateLabel.font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
	m_DateLabel.text=dateString;
	m_DateLabel.backgroundColor=[UIColor clearColor];
     [m_DateLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
  m_DateLabel.textColor=[UIColor colorWithRed:77.0/255 green:20.0/255 blue:84.0/255 alpha:100.0];
	m_DateLabel.textAlignment=UITextAlignmentCenter;
  m_DateLabel.shadowColor = [UIColor whiteColor];
  m_DateLabel.shadowOffset = CGSizeMake(0, 1.0);
	[self.view addSubview:m_DateLabel];

	if(m_TableView==nil)
    {
        if (screenBounds.size.height == 568) {
            m_TableView = [[UITableView alloc] initWithFrame: CGRectMake(-5,40+AdOffset+FullVersionBannerOffset,330,425-AdOffset-FullVersionBannerOffset) style:UITableViewStyleGrouped];
            // code for 4-inch screen
        } else {
            // code for 3.5-inch screen
            m_TableView = [[UITableView alloc] initWithFrame: CGRectMake(-5,40+AdOffset+FullVersionBannerOffset,330,325-AdOffset-FullVersionBannerOffset) style:UITableViewStyleGrouped];
        }
        
    }
    
    m_TableView.backgroundView=nil;
    
  if(m_TableView==nil)
	m_TableView = [[UITableView alloc] initWithFrame: CGRectMake(-5,40+AdOffset+FullVersionBannerOffset,330,325-AdOffset-FullVersionBannerOffset) style:UITableViewStyleGrouped];
   [m_TableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	m_TableView.delegate = self;
	m_TableView.dataSource = self;
	m_TableView.scrollEnabled = YES;
	m_TableView.backgroundColor=[UIColor clearColor];
	[self.view addSubview:m_TableView];
	
  if(m_activityIndicator==nil)
  m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
  [m_activityIndicator startAnimating];
  m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
    [m_activityIndicator setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin)];
    [self.view addSubview:m_activityIndicator];
  [self connectToServer];
    
    [self checkOrientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotate:)
                                                 name:@"UIDeviceOrientationDidChangeNotification" object:nil];
    
}


- (void) didRotate:(NSNotification *)notification{
    
    [self checkOrientation];
    
    
}

-(void)checkOrientation
{
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        appDelegate.isPortrait = NO;
    }
    else{
        appDelegate.isPortrait = YES;
    }
    [self layoutSubView];
}

-(void)layoutSubView
{
    if (appDelegate.isPortrait) {
        m_BGImageView.frame = CGRectMake(0, -40, 320, 480);
        pTitleLabel.frame = CGRectMake(60, 8, 200, 30);
        m_Homebtn.frame=CGRectMake(5, 8, 53.5, 30);
        m_DistanceButton.frame=CGRectMake(260, 8, 53.5, 30);
        [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,320,50)];
        [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 80)];
        if ([[UIScreen mainScreen]bounds].size.height == 568) m_BGImageView.frame =  CGRectMake(0, -40, 320, 568);
        
    }
    else
    {
        pTitleLabel.frame = CGRectMake(60, 1, 370, 30);
        m_Homebtn.frame=CGRectMake(5, 3, 53.5, 30);
        m_DistanceButton.frame=CGRectMake(426, 3, 53.5, 30);
        m_BGImageView.frame = CGRectMake(0, 0, 480, 320);
        [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,480,50)];
        [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(373, 80)];
        if ([[UIScreen mainScreen]bounds].size.height == 568)
        {
            m_BGImageView.frame = CGRectMake(0, -40, 320, 460);
            pTitleLabel.frame = CGRectMake(60, 1, 500, 30);
            m_DistanceButton.frame=CGRectMake(515, 3, 53.5, 30);
            m_BGImageView.frame = CGRectMake(0, 0, 568, 320);
            [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,0,568,50)];
            [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(461, 80)];
        }
    }
    
}

- (void)viewWillLayoutSubviews
{
    
    CGRect frameNav = self.navigationController.navigationBar.frame;
    if (appDelegate.isPortrait) {
        if (frameNav.size.height == 32) {
            frameNav.size.height = 44;
            self.navigationController.navigationBar.frame = frameNav;
            
        }
    }
    
    
}

-(void)AdsRemoved
{
  int AdOffsetlocal=30;
  m_TableView.frame=CGRectMake(-5,40+AdOffsetlocal,330,340-AdOffsetlocal);
  DateBarImageView.frame=CGRectMake(0, 5+AdOffsetlocal, 320, 27);
  m_DateLabel.frame=CGRectMake(0, 3+AdOffsetlocal, 320, 27);
  [[AdsManager sharedAdsManager]RemoveAdBannerAndFullVersionBanner];
  [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,4,320,50)];
  [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 80)];


}

-(void)AddMap
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ADDMAP"];
}

-(void)AdTickets
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdTickets"];
  
}

-(void)AdDescription
{
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AdDescription"];
  
}


-(void)refreshData
{
  [self BackAction];
}

-(void)NotificationRecieved
{
  int index=[[NSUserDefaults standardUserDefaults]integerForKey:@"INDEX"];
  
    if(index==0)
    {
      while ([self.navigationController.navigationBar.subviews count] > 2)
      {
        int n=[self.navigationController.navigationBar.subviews count]-1;
        
        [[[self.navigationController.navigationBar subviews] objectAtIndex:n] removeFromSuperview];
      }
      [self.navigationController.navigationBar addSubview:m_Homebtn];
      [self.navigationController.navigationBar addSubview:pTitleLabel]; 
    }
  
}



-(void)viewDidLoad
{
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

-(void)viewWillAppear:(BOOL)animated
{
    if(m_Timer)
    {
        [m_Timer invalidate];
        [m_Timer release];
        m_Timer=nil;
    }
    m_Timer=[NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(refreshData) userInfo:nil repeats:YES];
    [m_Timer retain];
    [self layoutSubView];
    
    if ([self.navigationController.navigationBar.subviews count] > appDelegate.subviewCount)
    {
        
    }
    else
    {
        [self.navigationController.navigationBar addSubview:m_Homebtn];
        [self.navigationController.navigationBar addSubview:m_DistanceButton];
        [self.navigationController.navigationBar addSubview:pTitleLabel];
    }
    
    
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    appDelegate.subviewCount = [self.navigationController.navigationBar.subviews count];
}

-(void)backtoHomeAction
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
    if(m_delegate)
      [m_delegate gotoHome];
 // }
}

-(void)CalculateDistanceAction
{
  if(m_LocationManager){[m_LocationManager stopUpdatingHeading];m_LocationManager.delegate=nil;[m_LocationManager release];m_LocationManager=nil;isFirst=YES;}
    m_LocationManager = [[CLLocationManager alloc] init];
    m_LocationManager.delegate = self;
    m_LocationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    m_LocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [m_LocationManager startUpdatingLocation];
}

-(void)distanceButtonAction
{
  [self sortRides];
}

#pragma mark MapViewController delegate methods
-(void)gotoHome
{
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation 
{
  if(isFirst)
  {
    isFirst=NO;
  if(m_Location){[m_Location release];m_Location=nil;}
  m_Location=m_LocationManager.location;
  [m_Location retain];
  [self stopUpDatingLocation];
  }
  else 
  {
    CLLocationDistance distance=[newLocation distanceFromLocation:m_Location];
    NSNumber *d=[NSNumber numberWithDouble:distance];
    int dist=[d intValue];
    if(dist>=0.0)
    {
      if(m_Location){[m_Location release];m_Location=nil;}
      m_Location=m_LocationManager.location;
      [m_Location retain];
      [self stopUpDatingLocation];      
    }
  }  
}

-(void)stopUpDatingLocation
{
  for(int i=0;i<[m_RestuarantArray count];i++)
  {
    Land *pLand=[m_RestuarantArray objectAtIndex:i];
    for(int j=0;j<[pLand.m_RestArray count];j++)
    {
      Restuarant *pRestuarant=[pLand.m_RestArray objectAtIndex:j];
      CLLocation *rideLocation=[[CLLocation alloc]initWithLatitude:[pRestuarant.m_Latitude doubleValue] longitude:[pRestuarant.m_Longitude doubleValue]]; 
      CLLocationDistance distance=[m_Location distanceFromLocation:rideLocation];
      NSNumber *d=[NSNumber numberWithDouble:distance];
     // printf("%d\n",[d intValue]);
      pRestuarant.m_Distance=[d intValue];
      [rideLocation release];
      rideLocation=nil;
    }
  }
  switch (m_SelectedSortType)
  {
    case 0:
      [self sortOnDistance];
      break;
    case 1:
      [self sortOnName];
      break;
    case 2:
      [self sortOnWaitTime];
      break;
    default:
      break;
  }  
}

-(void)sortRides
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  if(m_SortRideViewController)
  {
	  [m_SortRideViewController.view removeFromSuperview];
    m_SortRideViewController.m_delegate=nil;
	  [m_SortRideViewController release];
	  m_SortRideViewController=nil;
  }
	[m_Homebtn removeFromSuperview];
	[m_DistanceButton removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	m_SortRideViewController=[[SortRideViewController alloc]initWithBool:NO];
	m_SortRideViewController.m_delegate=self;
	[self.navigationController pushViewController:m_SortRideViewController animated:YES];
}

-(void)BackFromSortRideViewController:(int)selectedIndex
{
  [self SelectSortType:selectedIndex];
}


-(void)SelectSortType:(int)index
{
  if(index==0)
  {
    [self sortOnDistance];
    m_SelectedSortType=0;
  }
  else if(index==1)
  {
    [self sortOnName];
    m_SelectedSortType=1;
  }
  else if(index==2)
  {
    [self sortOnWaitTime];
    m_SelectedSortType=2;
  }
}


-(void)sortOnName
{
  for(int i=0;i<[m_RestuarantArray count];i++)
  {
    Land *pLand=[m_RestuarantArray objectAtIndex:i];
    NSMutableArray *pArray=[[NSMutableArray alloc]init];
    if([pLand.m_RestArray count]>1)
    {
      int arrayCount=[pLand.m_RestArray count];
      for(int j=0;j<arrayCount-1;j++)
      {
        Restuarant *pMinRide=[pLand.m_RestArray objectAtIndex:0];
        for(int k=1;k<[pLand.m_RestArray count];k++)
        {
          Restuarant *curRide=[pLand.m_RestArray objectAtIndex:k];
          NSComparisonResult result = [curRide.m_RestName caseInsensitiveCompare:pMinRide.m_RestName];
          
          if(result == NSOrderedAscending || result==NSOrderedSame)
          {
            pMinRide=curRide;
          }
        }
        [pArray addObject:pMinRide];
        [pLand.m_RestArray removeObject:pMinRide];
      }
      [pArray addObjectsFromArray:pLand.m_RestArray];
      [pLand.m_RestArray removeAllObjects];
      [pLand.m_RestArray addObjectsFromArray:pArray];
    }
    [pArray removeAllObjects];[pArray release];pArray=nil;
  }
  [m_TableView reloadData];
}

-(void)sortOnWaitTime
{
  for(int i=0;i<[m_RestuarantArray count];i++)
  {
    Land *pLand=[m_RestuarantArray objectAtIndex:i];
    NSMutableArray *pArray=[[NSMutableArray alloc]init];
    if([pLand.m_RestArray count]>1)
    {
      int arrayCount=[pLand.m_RestArray count];
      for(int j=0;j<arrayCount-1;j++)
      {
        Restuarant *pMinRide=[pLand.m_RestArray objectAtIndex:0];
        for(int k=1;k<[pLand.m_RestArray count];k++)
        {
          Restuarant *curRide=[pLand.m_RestArray objectAtIndex:k];
          int curTime=[curRide.m_WaitTime intValue];
          int minTime=[pMinRide.m_WaitTime intValue];
          if(curTime > minTime)
          {
            pMinRide=curRide;
          }
        }
        [pArray addObject:pMinRide];
        [pLand.m_RestArray removeObject:pMinRide];
      }
      [pArray addObjectsFromArray:pLand.m_RestArray];
      [pLand.m_RestArray removeAllObjects];
      [pLand.m_RestArray addObjectsFromArray:pArray];
    }
    [pArray removeAllObjects];[pArray release];pArray=nil;
  }
  [m_TableView reloadData];
}

-(void)sortOnDistance
{
  for(int i=0;i<[m_RestuarantArray count];i++)
  {
    Land *pLand=[m_RestuarantArray objectAtIndex:i];
    NSMutableArray *pArray=[[NSMutableArray alloc]init];
    if([pLand.m_RestArray count]>1)
    {
      int arrayCount=[pLand.m_RestArray count];
      for(int j=0;j<arrayCount-1;j++)
      {
        Restuarant *pMinRest=[pLand.m_RestArray objectAtIndex:0];
        for(int k=1;k<[pLand.m_RestArray count];k++)
        {
          Restuarant *curRest=[pLand.m_RestArray objectAtIndex:k];
          int curDist=curRest.m_Distance;
          int minDist=pMinRest.m_Distance;
          if(curDist < minDist)
          {
            pMinRest=curRest;
          }
        }
        [pArray addObject:pMinRest];
        [pLand.m_RestArray removeObject:pMinRest];
      }
      [pArray addObjectsFromArray:pLand.m_RestArray];
      [pLand.m_RestArray removeAllObjects];
      [pLand.m_RestArray addObjectsFromArray:pArray];
    }
    [pArray removeAllObjects];[pArray release];pArray=nil;
  }
  [m_TableView reloadData];
}

-(void)BackAction
{
  [self connectToServer];
  m_activityIndicator=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(150, 200, 30, 30)];
  [m_activityIndicator startAnimating];
  m_activityIndicator.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
  [self.view addSubview:m_activityIndicator];      
}


#pragma mark URLConnectionManager Methods

-(void)connectToServer
{
  m_TableView.userInteractionEnabled=NO;
  m_DistanceButton.userInteractionEnabled=NO;
  m_Homebtn.userInteractionEnabled=NO;
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	
	NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
  //printf("%s",[m_ParkId UTF8String]);
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://admin.apptasmic.com/xml.php?method=getrest&parkid=%@",m_ParkId]]];
    [request setHTTPMethod:@"POST"];
    mURLConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];	
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	if(mResponseData == nil)
		mResponseData = [[NSMutableData alloc] init];
	[mResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if(mResponseData != nil)
		[mResponseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  m_TableView.userInteractionEnabled=YES;
  m_DistanceButton.userInteractionEnabled=YES;
  m_Homebtn.userInteractionEnabled=YES;
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }        
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
	if(mResponseData)
	{
		[mResponseData release];
		mResponseData=nil;
	}
	//printf("\n %s",[[error localizedDescription] UTF8String]);
  if(![[NSUserDefaults standardUserDefaults]integerForKey:@"RESETALERTFLAG"])
  {
    AmusementParkAppDelegate *appDelegatelocal=(AmusementParkAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegatelocal showAlertView];
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  if (m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }  
  
	if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}
  
  m_TableView.userInteractionEnabled=YES;
  m_DistanceButton.userInteractionEnabled=YES;
  m_Homebtn.userInteractionEnabled=YES;      
  
	if(mResponseData)
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *filePath = [paths objectAtIndex:0];
		paths=nil;
		filePath = [filePath stringByAppendingPathComponent:@"ParkRestuarants.xml"];
		[mResponseData writeToFile:filePath atomically:YES];
		filePath=nil;
		[mResponseData release];
		mResponseData=nil;
    if([m_RestuarantArray count]){[m_RestuarantArray removeAllObjects];}
      [m_RestuarantArray addObjectsFromArray:[XMLDataParser getRestuarantsFromXML]];
    if([m_RestuarantArray count])
    {
      [self CalculateDistanceAction];
    }
 }
	
    [m_TableView reloadData];
}

  
#pragma mark tableView delegate methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	Land *pLand=[m_RestuarantArray objectAtIndex:section];
  return [pLand.m_RestArray count];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [m_RestuarantArray count];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
    UIView* customView = [[[UIView alloc] initWithFrame:CGRectMake(20, 0.0, 320.0, 20)] autorelease];
    
    // create the button object
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.opaque = NO;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.highlightedTextColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    headerLabel.frame = CGRectMake(20, 0.0, 320.0, 20);
    
    // If you want to align the header text as centered
    Land *pLand=[m_RestuarantArray objectAtIndex:section];
    headerLabel.text =pLand.m_LandName;
    
    [customView addSubview:headerLabel];
  [headerLabel release];
  headerLabel=nil;
    return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Land *pLand=[m_RestuarantArray objectAtIndex:indexPath.section];
    if(indexPath.row==0 || indexPath.row==([pLand.m_RestArray count]-1))
      return 135/2+3;
    else 
      return 135/2;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	RestuarantDetailCell *cell = (RestuarantDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"RestuarantDetailCell"];
	if(cell == nil)
	{  
    cell=[[[RestuarantDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RestuarantDetailCell"]autorelease];

		cell.accessoryType = UITableViewCellAccessoryNone;
		cell.selectionStyle=UITableViewCellSelectionStyleGray;
	}
  Land *pLand=[m_RestuarantArray objectAtIndex:indexPath.section];
  Restuarant *pRestuarant=[pLand.m_RestArray objectAtIndex:indexPath.row];
	cell.m_TitleLabel.text=pRestuarant.m_RestName;
  
  if([pRestuarant.m_RestServiceType isEqualToString:@"Quick"])
    cell.m_ImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/FoodPage/Quick" ofType:@"png"]];
  else 
    cell.m_ImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesInPaidVersion/FoodPage/Table" ofType:@"png"]];

  int waittime=[pRestuarant.m_WaitTime intValue];
  if(waittime<=20)
    cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/green1" ofType:@"png"]];
  else if(waittime<60)
    cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/yellow1" ofType:@"png"]];
  else if(waittime>=60)
    cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/block/red1" ofType:@"png"]];
  
  int open=[[NSUserDefaults standardUserDefaults]integerForKey:@"OPENCLOSE"];
  if(waittime==-1 || open==0)
  {
    cell.m_WaitTimeImageView.frame=CGRectMake(21,22,45,29);
    cell.m_WaitTimeImageView.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"ImagesCommon/Rides/closed2" ofType:@"png"]];
    cell.m_WaitTimeLabel.frame=CGRectMake(21,22,45,29);
    cell.m_WaitTimeLabel.text=@"";
    [cell SetOpacityLow];
    cell.m_WaitTimeLabel.layer.opacity=1.0;
    cell.userInteractionEnabled=YES;
  }
  else if(waittime==75)
  {
    cell.m_WaitTimeImageView.frame=CGRectMake(21,17,42,31);
    cell.m_WaitTimeLabel.frame=CGRectMake(21,17,42,31);
    cell.m_WaitTimeLabel.text=[NSString stringWithFormat:@"%@+",pRestuarant.m_WaitTime];
    [cell SetOpacityHigh];
  }
  else 
  {
    cell.m_WaitTimeImageView.frame=CGRectMake(21,17,31,31);
    cell.m_WaitTimeLabel.frame=CGRectMake(21,17,31,31);
    cell.m_WaitTimeLabel.text=pRestuarant.m_WaitTime;
    [cell SetOpacityHigh];
  }
  
  NSNumber *number = [NSNumber numberWithInt:pRestuarant.m_Distance];
  NSNumberFormatter *frmtr = [[NSNumberFormatter alloc] init];
  [frmtr setNumberStyle: NSNumberFormatterDecimalStyle];
  NSString *commaString = [frmtr stringFromNumber:number];
  NSString *pYardsStr=[commaString stringByAppendingString:@" yards"];
  cell.m_YardsLabel.text=pYardsStr;
  [frmtr release];

  UIImageView *bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 135/2)];
  if([pLand.m_RestArray count]==1)
  bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restsingle1.png"];
  else if(indexPath.row==0)
  bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restup1.png"];
  else if(indexPath.row==[pLand.m_RestArray count]-1)
  bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restbottom1.png"];
  else
  bgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/restmiddle1.png"];
  cell.backgroundView=bgImgView;
  [bgImgView release];  
  
  UIImageView *selbgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 316, 135/2)];
  if([pLand.m_RestArray count]==1)
  selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestsingle1.png"];
  else if(indexPath.row==0)
  selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestup1.png"];
  else if(indexPath.row==[pLand.m_RestArray count]-1)
  selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestbottom1.png"];
  else
  selbgImgView.image=[UIImage imageNamed:@"ImagesInPaidVersion/FoodPage/selrestmiddle1.png"];
  cell.selectedBackgroundView=selbgImgView;
  [selbgImgView release];  
  
  
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  [m_DistanceButton removeFromSuperview];
  [m_Homebtn removeFromSuperview];
  [pTitleLabel removeFromSuperview];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
  Land *pLand=[m_RestuarantArray objectAtIndex:indexPath.section];
  Restuarant *pRestuarant=[pLand.m_RestArray objectAtIndex:indexPath.row]; 
  
  if([[NSUserDefaults standardUserDefaults] boolForKey:@"AdDescription"])
  {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [paths objectAtIndex:0];
    paths=nil;
    filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"Food%@%@",pLand.m_LandId,pRestuarant.m_RestId]];
    if(m_RestuarantDetailViewController)
    {
      m_RestuarantDetailViewController.m_delegate=nil;
      [m_RestuarantDetailViewController.view removeFromSuperview];
      [m_RestuarantDetailViewController release];
      m_RestuarantDetailViewController=nil;
    }  
    m_RestuarantDetailViewController=[[RestuarantDetailViewController alloc]initwithStudioId:m_StudioId ParkId:m_ParkId LandId:pLand.m_LandId RestId:pRestuarant.m_RestId LandName:pLand.m_LandName Distance:pRestuarant.m_Distance isFavourite:NO imagePath:filePath pLat:pRestuarant.m_Latitude pLong:pRestuarant.m_Longitude];
    m_RestuarantDetailViewController.m_delegate=self;
    filePath=nil;
    
    [self.navigationController pushViewController:m_RestuarantDetailViewController animated:YES];

  }
  else
  {
    SubmitWaitTimeViewController *mSubmitWaitTimeViewController=[[[SubmitWaitTimeViewController alloc]initWithId:pRestuarant.m_RestId Latitude:pRestuarant.m_Latitude Longitude:pRestuarant.m_Longitude isRide:NO] autorelease];
    
    [self.navigationController pushViewController:mSubmitWaitTimeViewController animated:YES];

  }
  
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

- (void)viewDidUnload 
{
    [super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated
{
	if(m_RestuarantDetailViewController)
  { 
    [m_RestuarantDetailViewController.view removeFromSuperview];
    m_RestuarantDetailViewController.m_delegate=nil;
    [m_RestuarantDetailViewController release];
    m_RestuarantDetailViewController=nil;
  }
 // [[AdsManager sharedAdsManager] ShowAdBannerFromVC:self withFrame:CGRectMake(0,4,320,50)];
 // [[AdsManager sharedAdsManager] ShowFullVersionBannerFromVC:self withOrigin:CGPointMake(213, 80)];
  [super viewDidAppear:animated];
}

- (void)dealloc 
{
  if(m_activityIndicator)
  {
    [m_activityIndicator stopAnimating];
    [m_activityIndicator removeFromSuperview];
    [m_activityIndicator release];
    m_activityIndicator=nil;
  }
  if(mURLConnection) 
	{
		[mURLConnection cancel];
		[mURLConnection release];
		mURLConnection=nil;
	}  
  
  if(m_Timer)
  {
    [m_Timer invalidate];
    [m_Timer release];
    m_Timer=nil;
  }
  
  if(m_Location){[m_Location release];m_Location=nil;}
  if(m_LocationManager){[m_LocationManager stopUpdatingHeading];m_LocationManager.delegate=nil;[m_LocationManager release];m_LocationManager=nil;}
  if(m_StudioId){[m_StudioId release];m_StudioId=nil;}
  if(m_ParkId){[m_ParkId release];m_ParkId=nil;}
  if(pTitleLabel){[pTitleLabel removeFromSuperview];[pTitleLabel release];pTitleLabel=nil;}
  if(m_Homebtn){[m_Homebtn removeFromSuperview];[m_Homebtn release];m_Homebtn=nil;}
  if(m_DistanceButton){[m_DistanceButton removeFromSuperview];[m_DistanceButton release];m_DistanceButton=nil;}
	if(m_DateLabel){[m_DateLabel removeFromSuperview];[m_DateLabel release];m_DateLabel=nil;}
	if(m_TableView){[m_TableView removeFromSuperview];[m_TableView release];m_TableView=nil;}
	if(m_RestuarantArray){[m_RestuarantArray removeAllObjects];[m_RestuarantArray release];m_RestuarantArray=nil;}
	if(m_RestuarantDetailViewController){[m_RestuarantDetailViewController.view removeFromSuperview];m_RestuarantDetailViewController.m_delegate=self;[m_RestuarantDetailViewController release];m_RestuarantDetailViewController=nil;}
  if(m_SortRideViewController){[m_SortRideViewController.view removeFromSuperview];[m_SortRideViewController release];m_SortRideViewController=nil;}
  if(DateBarImageView){[DateBarImageView removeFromSuperview];[DateBarImageView release];DateBarImageView=nil;}
    [super dealloc];
}


@end
